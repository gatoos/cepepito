﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Cepepito.EndToEndTests.Framework
{
    public class LoggerInitializer
    {
        private bool _loggerInitialized;

        public void Initialize(string targetDirectory)
        {
            if (!_loggerInitialized)
            {
                var config = new LoggingConfiguration();

                var fileTarget = new FileTarget();
                config.AddTarget("file", fileTarget);

                fileTarget.FileName = targetDirectory + "/cepepito.log";
                fileTarget.Layout = @"${date:format=HH\:mm\:ss} ${logger} - ${level} - ${message}";

#if END_TO_END_TESTS
                CepepitoEndToEndTests.InjectLoggerInConfig(config);
#endif

                var generalRule = new LoggingRule("*", LogLevel.Debug, fileTarget);
                config.LoggingRules.Add(generalRule);

                LogManager.Configuration = config;
                _loggerInitialized = true;
            }
        }
    }
}