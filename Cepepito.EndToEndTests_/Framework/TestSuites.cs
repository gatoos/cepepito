/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Xml;
using VisualStudioAPIConnectors;

namespace Cepepito.EndToEndTests.Framework
{
    public class TestSuites
    {
        private readonly Dictionary<string, TestSuite> _testSuites;

        internal TestSuites()
        {
            _testSuites = new Dictionary<string, TestSuite>();
        }

        internal void Clear()
        {
            _testSuites.Clear();
        }

        internal TestCase PushTestCase(string testSuiteName, string testCaseName)
        {
            if (!_testSuites.ContainsKey(testSuiteName))
                _testSuites.Add(testSuiteName, new TestSuite(testSuiteName));

            TestSuite testCaseTestSuite = null;
            _testSuites.TryGetValue(testSuiteName, out testCaseTestSuite);
            return testCaseTestSuite?.PushTestCase(testCaseName);
        }

        internal void SaveXmlRepresentation()
        {
            var cepepitoInstallDirectoryRetriever = new AssemblyDirectoryRetriever();
            var xmlDocument = new XmlDocument();
            var xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDocument.AppendChild(xmlDeclaration);
            var testSuitesElement = xmlDocument.CreateElement("testsuites");
            foreach (var testSuite in _testSuites.Values)
                testSuitesElement.AppendChild(testSuite.BuildXmlElement(xmlDocument));

            xmlDocument.AppendChild(testSuitesElement);

            xmlDocument.Save(
                cepepitoInstallDirectoryRetriever.RetrieveAssemblyDirectory(typeof(TestSuites)) + "/endToEndTests.xml");
        }
    }
}