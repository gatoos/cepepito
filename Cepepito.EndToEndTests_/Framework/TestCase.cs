/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Xml;

namespace Cepepito.EndToEndTests.Framework
{
    public class TestCase
    {
        private readonly Stopwatch _executionTimer;

        private readonly List<string> _failures;

        internal TestCase(string testCaseName)
        {
            Name = testCaseName;
            _failures = new List<string>();
            _executionTimer = new Stopwatch();
        }

        internal string Name { get; set; }
        internal double ExecutionTime { get; set; }

        internal int NumberOfFailures => _failures.Count;

        internal void AddFailure(string failureMessage)
        {
            _failures.Add(failureMessage);
        }

        internal void StartExecutionTimer()
        {
            _executionTimer.Start();
        }

        internal void StopExecutionTimer()
        {
            ExecutionTime = _executionTimer.Elapsed.TotalMinutes;
            _executionTimer.Stop();
        }

        internal XmlNode BuildXmlElement(string testSuiteName, XmlDocument xmlDocument)
        {
            var testSuiteElement = xmlDocument.CreateElement("testcase");
            testSuiteElement.SetAttribute("classname", testSuiteName);
            testSuiteElement.SetAttribute("name", Name);
            testSuiteElement.SetAttribute("time", ExecutionTime.ToString("0.######", CultureInfo.InvariantCulture));
            foreach (var failure in _failures)
            {
                var failureElement = xmlDocument.CreateElement("failure");
                failureElement.SetAttribute("message", failure);
                testSuiteElement.AppendChild(failureElement);
            }

            return testSuiteElement;
        }
    }
}