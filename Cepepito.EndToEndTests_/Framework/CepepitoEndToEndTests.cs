﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;
using NLog;
using NLog.Config;
using NLog.Targets;
using VisualStudioAPIConnectors;

namespace Cepepito.EndToEndTests.Framework
{
    public class CepepitoEndToEndTests
    {
        private readonly DteRetriever _dteRetriever;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IServiceProvider _serviceProvider;
        private readonly TestSuites _testSuites = new TestSuites();
        private TestCase _currentTestCase;

        public CepepitoEndToEndTests(DteRetriever dteRetriever, IServiceProvider serviceProvider)
        {
            _dteRetriever = dteRetriever;
            _serviceProvider = serviceProvider;
        }

        public void ExecuteTests()
        {
            _logger.Info("End to end tests start");
            var dte = _dteRetriever.GetDte();
            var commandService = _serviceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;

            _testSuites.Clear();

            // Extension registration
            //var extensionRegistrationTest = new ExtensionRegistrationTest();
            //extensionRegistrationTest.CommandsRegistered(dte, this);

            // License
            //var licenseTest = new LicenseTest();
            //licenseTest.TestTrialLicense(dte, this);
            //licenseTest.TestCommandsActivation(dte, commandService, this);

            _testSuites.SaveXmlRepresentation();
            _logger.Info("End to end tests end");

            dte.Quit();
        }

        public void StartTest(string testSuiteName, string testCaseName)
        {
            _currentTestCase = _testSuites.PushTestCase(testSuiteName, testCaseName);
            _currentTestCase.StartExecutionTimer();
        }

        public void EndTest()
        {
            _currentTestCase.StopExecutionTimer();
            _currentTestCase = null;
        }

        public void AssertTrue(bool a, string message)
        {
            if (!a)
                _currentTestCase?.AddFailure(message);
        }

        internal static void InjectLoggerInConfig(LoggingConfiguration config)
        {
            var cepepitoInstallDirectoryRetriever = new AssemblyDirectoryRetriever();
            var endToEndTestFileTarget = new FileTarget();
            config.AddTarget("endToEndTestFile", endToEndTestFileTarget);

            endToEndTestFileTarget.FileName = cepepitoInstallDirectoryRetriever.RetrieveAssemblyDirectory(typeof(CepepitoEndToEndTests)) +
                                              "/endToEndTests.log";
            endToEndTestFileTarget.Layout = @"${date:format=HH\:mm\:ss} ${logger} - ${level} - ${message}";

            var endToEndTestsRule =
                new LoggingRule("Cepepito.EndToEndTests.*", LogLevel.Trace, endToEndTestFileTarget) {Final = true};
            config.LoggingRules.Add(endToEndTestsRule);
        }
    }
}