/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

namespace Cepepito.EndToEndTests.Framework
{
    public class TestSuite
    {
        private readonly Dictionary<string, TestCase> _testCases;

        internal TestSuite(string name)
        {
            Name = name;
            _testCases = new Dictionary<string, TestCase>();
        }

        private string Name { get; }

        private int NumberOfTests => _testCases.Count;

        private double ExecutionTime
        {
            get
            {
                double executionTime = 0;
                foreach (var testCase in _testCases.Values)
                    executionTime += testCase.ExecutionTime;

                return executionTime;
            }
        }

        private int NumberOfFailures
        {
            get
            {
                var numberOfFailures = 0;

                foreach (var testCase in _testCases.Values)
                    numberOfFailures += testCase.NumberOfFailures;

                return numberOfFailures;
            }
        }

        internal TestCase PushTestCase(string testCaseName)
        {
            TestCase returnValue = null;

            if (!_testCases.ContainsKey(testCaseName))
                _testCases.Add(testCaseName, new TestCase(testCaseName));

            _testCases.TryGetValue(testCaseName, out returnValue);
            return returnValue;
        }

        internal XmlElement BuildXmlElement(XmlDocument xmlDocument)
        {
            var testSuiteElement = xmlDocument.CreateElement("testsuite");
            testSuiteElement.SetAttribute("name", Name);
            testSuiteElement.SetAttribute("tests", NumberOfTests.ToString());
            testSuiteElement.SetAttribute("time", ExecutionTime.ToString("0.######", CultureInfo.InvariantCulture));
            testSuiteElement.SetAttribute("failures", NumberOfFailures.ToString());
            testSuiteElement.SetAttribute("errors", "0");
            testSuiteElement.SetAttribute("skipped", "0");

            foreach (var testCase in _testCases.Values)
                testSuiteElement.AppendChild(testCase.BuildXmlElement(Name, xmlDocument));

            return testSuiteElement;
        }
    }
}