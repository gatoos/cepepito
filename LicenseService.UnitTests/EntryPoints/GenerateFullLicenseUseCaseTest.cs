﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using FakeItEasy;
using LicenseService.Core.Entities;
using LicenseService.ServiceProviders;
using LicenseService.UnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LicenseService.UnitTests.EntryPoints
{
    [TestClass]
    public class GenerateFullLicenseUseCaseTest
    {
        [TestMethod]
        public void
            When_CallingGenerateFullLicenseAndThenGetLicenseInformation_Then_ItShouldRetrieveFullLicenseFromLicenseGeneratorAndReturnGeneratedLicenseInformation()
        {
            var fakePurchaseCode = "fake Purchase Code";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var generatedFullLicense = new FakeLicenseBuilder(CepepitoLicenseType.Corporate, DateTime.Today).Build();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorGeneratingFullLicense(privateKey, (fakePurchaseCode, generatedFullLicense))
                .Build();

            licenseService.GenerateFullLicense(fakePurchaseCode);

            Assert.AreEqual(generatedFullLicense, licenseService.GetActiveLicense());
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicenseAndThenGetLicenseInformation_Then_ItShouldReturnGeneratedLicenseInformationButContactLicenseGeneratorOnlyOnce()
        {
            var fakePurchaseCode = "fake Purchase Code";
            var authorizationCode = "authorizationCode";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var generatedFullLicense = new FakeLicenseBuilder(CepepitoLicenseType.Corporate, DateTime.Today).Build();

            (var licenseService, var licenseGenerator) = new FakeLicenseServiceBuilder()
                .WithAuthorizationCode(authorizationCode).WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorGeneratingFullLicense(privateKey, (fakePurchaseCode, generatedFullLicense))
                .Build();

            licenseService.GenerateFullLicense(fakePurchaseCode);
            licenseService.GetActiveLicense();

            A.CallTo(() => licenseGenerator.GenerateFullLicenseEncryptedString(authorizationCode, fakePurchaseCode))
                .MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicenseAndThenGetLicenseInformationTwice_Then_ItShouldReturnGeneratedLicenseInformationObtainedForEachGeneratorCall()
        {
            var fakePurchaseCode1 = "fake purchase code 1";
            var fakePurchaseCode2 = "fake purchase code 2";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var firstCepepitoLicense = new FakeLicenseBuilder(CepepitoLicenseType.Invalid, DateTime.Today).Build();
            var secondCepepitoLicense = new FakeLicenseBuilder(CepepitoLicenseType.Corporate, DateTime.Today).Build();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorGeneratingFullLicenses(privateKey,
                    new[] {(fakePurchaseCode1, firstCepepitoLicense), (fakePurchaseCode2, secondCepepitoLicense)})
                .Build();

            licenseService.GenerateFullLicense(fakePurchaseCode1);
            var firstRetrievedLicense = licenseService.GetActiveLicense();
            licenseService.GenerateFullLicense(fakePurchaseCode2);
            var secondRetrievedLicense = licenseService.GetActiveLicense();

            Assert.AreEqual(firstCepepitoLicense, firstRetrievedLicense);
            Assert.AreEqual(secondCepepitoLicense, secondRetrievedLicense);
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicenseAndLicenseGeneratorIsNotAvailable_Then_ItShouldRetrieveFullLicenseFromLicenseGeneratorAndReturnExistingLicenseInformationAndEnqueueLicenseGeneratorErrorMessage()
        {
            var fakePurchaseCode = "fake purchase code";
            var licenseGeneratorErrorMessage = "error message";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var existingLicense = new FakeLicenseBuilder(CepepitoLicenseType.Corporate, DateTime.Today).Build();
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate).WithExistingLicense(privateKey, existingLicense)
                .WithLicenseGeneratorThrowingExceptionOnGenerateFullLicense(licenseGeneratorErrorMessage)
                .WithErrorsMessagesQueue(errorsMessagesQueue).Build();

            licenseService.GenerateFullLicense(fakePurchaseCode);
            var retrievedLicense = licenseService.GetActiveLicense();

            Assert.AreEqual(existingLicense, retrievedLicense);
            AssertThatErrorMessageQueueContainsErrorMessage(errorsMessagesQueue, licenseGeneratorErrorMessage);
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicense_Then_ItShouldNotifyAnySubscribedListenerThatLicenseHasBeenUpdated()
        {
            bool licenseIsUpdated = false;
            var fakePurchaseCode = "fake Purchase Code";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var generatedFullLicense = new FakeLicenseBuilder(CepepitoLicenseType.Corporate, DateTime.Today).Build();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorGeneratingFullLicense(privateKey, (fakePurchaseCode, generatedFullLicense))
                .Build();

            licenseService.LicenseUpdated += (sender, args) => { licenseIsUpdated = true; };
            licenseService.GenerateFullLicense(fakePurchaseCode);

            Assert.IsTrue(licenseIsUpdated);
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicenseAndLicenseGeneratorIsNotAvailable_Then_ItShouldNotNotifyAnySubscribedListenerWithLicenseUpdatedEvent()
        {
            bool licenseIsUpdated = false;
            var fakePurchaseCode = "fake purchase code";
            var licenseGeneratorErrorMessage = "error message";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var existingLicense = new FakeLicenseBuilder(CepepitoLicenseType.Corporate, DateTime.Today).Build();
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate).WithExistingLicense(privateKey, existingLicense)
                .WithLicenseGeneratorThrowingExceptionOnGenerateFullLicense(licenseGeneratorErrorMessage)
                .WithErrorsMessagesQueue(errorsMessagesQueue).Build();

            licenseService.LicenseUpdated += (sender, args) => { licenseIsUpdated = true; };

            Assert.IsFalse(licenseIsUpdated);
        }

        private static void AssertThatErrorMessageQueueContainsErrorMessage(IErrorsMessagesQueue errorsMessagesQueue,
            string trialLicenseGeneratorErrorMessage)
        {
            A.CallTo(() =>
                errorsMessagesQueue.AddErrorMessage(A<string>.That.Matches(s =>
                    s.Contains(trialLicenseGeneratorErrorMessage)))).MustHaveHappenedOnceExactly();
        }
    }
}