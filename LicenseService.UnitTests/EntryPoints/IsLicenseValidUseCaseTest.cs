﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using LicenseService.Core.Entities;
using LicenseService.ServiceProviders;
using LicenseService.UnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LicenseService.UnitTests.EntryPoints
{
    [TestClass]
    public class IsLicenseValidUseCaseTest
    {
        private const int TrialLicenseValidityPeriodInDays = 30;
        private const int FullLicenseValidityPeriodInDays = 366;
        private static readonly DateTime LicenseGenerationDate = new DateTime(1985, 01, 10);

        [TestMethod]
        public void When_LicenseIsInvalid_Then_ItShouldReturnFalse()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate).WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Invalid, DateTime.Today).Build()).Build();

            Assert.IsFalse(licenseService.IsLicenseValid());
        }

        [TestMethod]
        public void When_LicenseIsTrialAndExpiryDateIsReached_Then_ItShouldReturnFalse()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Trial, LicenseGenerationDate).Build())
                .WithCurrentDateBeing(LicenseGenerationDate.AddDays(TrialLicenseValidityPeriodInDays + 1)).Build();

            Assert.IsFalse(licenseService.IsLicenseValid());
        }

        [TestMethod]
        public void When_LicenseIsTrialAndExpiryDateIsNotReachedAndClockHasntBeenManipulated_Then_ItShouldReturnTrue()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Trial, LicenseGenerationDate).Build())
                .WithCurrentDateBeing(LicenseGenerationDate.AddDays(TrialLicenseValidityPeriodInDays - 1)).Build();

            Assert.IsTrue(licenseService.IsLicenseValid());
        }

        [TestMethod]
        public void
            When_LicenseIsTrialAndExpiryIsNotReachedButClockHasBeenManipulated_Then_ItShouldReturnFalseWithAnErrorMessage()
        {
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();

            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Trial, LicenseGenerationDate).Build())
                .WithCurrentDateBeing(LicenseGenerationDate.AddDays(TrialLicenseValidityPeriodInDays - 1))
                .WithManipulatedClock().WithErrorsMessagesQueue(errorsMessagesQueue).Build();

            Assert.IsFalse(licenseService.IsLicenseValid());
            A.CallTo(() => errorsMessagesQueue.AddErrorMessage(A<string>.That.Matches(errorMessage =>
                    new[] {"clock", "manipulated"}.All(s => errorMessage.ToLower().Contains(s)))))
                .MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void When_LicenseIsFullAndExpiryDateIsReached_Then_ItShouldReturnFalse()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Corporate, LicenseGenerationDate).Build())
                .WithCurrentDateBeing(LicenseGenerationDate.AddDays(FullLicenseValidityPeriodInDays + 1)).Build();

            Assert.IsFalse(licenseService.IsLicenseValid());
        }

        [TestMethod]
        public void When_LicenseIsFullAndExpiryDateIsNotReachedAndClockHasntBeenManipulated_Then_ItShouldReturnTrue()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Corporate, LicenseGenerationDate).Build())
                .WithCurrentDateBeing(LicenseGenerationDate.AddDays(FullLicenseValidityPeriodInDays - 1)).Build();

            Assert.IsTrue(licenseService.IsLicenseValid());
        }

        [TestMethod]
        public void When_LicenseIsFullAndExpiryIsNotReachedButClockHasBeenManipulated_Then_ItShouldReturnFalse()
        {
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();

            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Corporate, LicenseGenerationDate).Build())
                .WithCurrentDateBeing(LicenseGenerationDate.AddDays(FullLicenseValidityPeriodInDays - 1))
                .WithManipulatedClock().WithErrorsMessagesQueue(errorsMessagesQueue).Build();

            Assert.IsFalse(licenseService.IsLicenseValid());
            AssertThatErrorMessageQueueContainsStrings(errorsMessagesQueue, new[] {"clock", "manipulated"});
        }

        [TestMethod]
        public void When_LicenseIsFullAndExpiryIsReachedAndClockHasntBeenManipulatedOnFirstCallButManipulatedOnSecondCall_Then_FirstCheckShouldReturnFalseAndSecondCheckAlso()
        {
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();

            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();

            var fakeClockManager = new FakeLicenseServiceBuilder.FakeClockManager
            {
                CurrentDateTime = LicenseGenerationDate.AddDays(FullLicenseValidityPeriodInDays + 1),
                ClockManipulated = false
            };

            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Corporate, LicenseGenerationDate).Build())
                .WithClockManager(fakeClockManager)
                .WithErrorsMessagesQueue(errorsMessagesQueue).Build();
            
            Assert.IsFalse(licenseService.IsLicenseValid());

            // Manipulate the clock
            fakeClockManager.CurrentDateTime = LicenseGenerationDate.AddDays(FullLicenseValidityPeriodInDays - 1);

            Assert.IsFalse(licenseService.IsLicenseValid());
        }

        [TestMethod]
        public void When_LicenseIsFullAndExpiryIsNotReachedAndClockHasntBeenManipulated_Then_CallingIsLicenseValidSeveralTimesShouldNotCallClockManagerMoreThanOnce()
        {
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();

            var (publicCertificate, privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();

            var fakeClockManager = A.Fake<IClockManager>();
            A.CallTo(() => fakeClockManager.GetCurrentDateTime())
                .Returns(LicenseGenerationDate.AddDays(FullLicenseValidityPeriodInDays - 1));
            A.CallTo(() => fakeClockManager.ClockHasBeenManipulated()).Returns(false);
            

            var (licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithExistingLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Corporate, LicenseGenerationDate).Build())
                .WithClockManager(fakeClockManager)
                .WithErrorsMessagesQueue(errorsMessagesQueue).Build();

            licenseService.IsLicenseValid();
            licenseService.IsLicenseValid();

            A.CallTo(() => fakeClockManager.GetCurrentDateTime()).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeClockManager.ClockHasBeenManipulated()).MustHaveHappenedOnceExactly();
        }

        private static void AssertThatErrorMessageQueueContainsStrings(IErrorsMessagesQueue errorsMessagesQueue,
            IEnumerable<string> errorMessageParts)
        {
            A.CallTo(() => errorsMessagesQueue.AddErrorMessage(A<string>.That.Matches(errorMessage =>
                errorMessageParts.All(s => errorMessage.ToLower().Contains(s))))).MustHaveHappenedOnceExactly();
        }
    }
}