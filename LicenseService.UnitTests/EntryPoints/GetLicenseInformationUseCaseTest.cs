﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using FakeItEasy;
using LicenseService.Core.Entities;
using LicenseService.ServiceProviders;
using LicenseService.UnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LicenseService.UnitTests.EntryPoints
{
    [TestClass]
    public class GetLicenseInformationUseCaseTest
    {
        [TestMethod]
        public void When_LicenseIsAvailable_Then_ItShouldRetrieveExistingLicenseInformation()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var existingLicense = new FakeLicenseBuilder(CepepitoLicenseType.Trial, DateTime.Today).Build();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate).WithExistingLicense(privateKey, existingLicense)
                .Build();

            var activeLicense = licenseService.GetActiveLicense();

            Assert.AreEqual(existingLicense, activeLicense);
        }

        [TestMethod]
        public void
            When_NoLicenseIsAvailableAndLicenseGeneratorIsWorking_Then_ItShouldRetrieveTrialLicenseFromLicenseGeneratorAndReturnItsInformation()
        {
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var generatedTrialLicense = new FakeLicenseBuilder(CepepitoLicenseType.Trial, DateTime.Today).Build();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorGeneratingTrialLicense(privateKey, generatedTrialLicense).Build();

            var activeLicense = licenseService.GetActiveLicense();

            Assert.AreEqual(generatedTrialLicense, activeLicense);
        }

        [TestMethod]
        public void
            When_NoLicenseIsAvailableAndLicenseGeneratorIsNotWorking_Then_ItShouldRetrieveTrialLicenseFromLicenseGeneratorAndReturnInvalidLicenseInformationAndEnqueueLicenseGeneratorErrorMessage()
        {
            (var publicCertificate, _) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var trialLicenseGeneratorErrorMessage = "Error when retrieving trial license.";
            var errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();
            (var licenseService, _) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorThrowingExceptionOnGenerateTrialLicense(trialLicenseGeneratorErrorMessage)
                .WithErrorsMessagesQueue(errorsMessagesQueue).Build();

            var activeLicense = licenseService.GetActiveLicense();

            Assert.IsTrue(activeLicense.Type == CepepitoLicenseType.Invalid);
            AssertThatErrorMessageQueueContainsErrorMessage(errorsMessagesQueue, trialLicenseGeneratorErrorMessage);
        }

        [TestMethod]
        public void
            When_NoLicenseIsAvailableAndLicenseGeneratorIsNotWorkingFirstAndThenWorks_Then_WhenCallingLicenseInformationItShouldReturnAnInvalidLicenseAndContactLiceseGeneratorOnlyOnce()
        {
            var authorizationCode = "authorization code";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            var trialLicenseGeneratorErrorMessage = "trial license generator error message";
            var generatedLicenseOnSecondCallToGenerate =
                new FakeLicenseBuilder(CepepitoLicenseType.Trial, DateTime.Today).Build();
            (var licenseService, var licenseGenerator) = new FakeLicenseServiceBuilder()
                .WithAuthorizationCode(authorizationCode)
                .WithPublicKeyCertificateRepository(publicCertificate)
                .WithLicenseGeneratorThrowingExceptionOnGenerateTrialLicenseAndThenGeneratingTrialLicense(
                    trialLicenseGeneratorErrorMessage, privateKey, generatedLicenseOnSecondCallToGenerate).Build();

            var firstCallLicense = licenseService.GetActiveLicense();
            var secondCallLicense = licenseService.GetActiveLicense();

            Assert.AreEqual(firstCallLicense, secondCallLicense);
            A.CallTo(() => licenseGenerator.GenerateTrialLicenseEncryptedString(authorizationCode))
                .MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void
            When_NoLicenseIsAvailableAndLicenseGeneratorIsWorking_Then_WhenCallingTwiceGetLicenseInformationItShouldReturnTheSameInformationAndContactLicenseGeneratorOnlyOnce()
        {
            var authorizationCode = "authorization code";
            (var publicCertificate, var privateKey) =
                FakeLicenseServiceBuilder.GeneratePrivateKeyAndPublicKeyCertificate();
            (var licenseService, var licenseGenerator) = new FakeLicenseServiceBuilder()
                .WithPublicKeyCertificateRepository(publicCertificate).WithAuthorizationCode(authorizationCode)
                .WithLicenseGeneratorGeneratingTrialLicense(privateKey,
                    new FakeLicenseBuilder(CepepitoLicenseType.Trial, DateTime.Today).Build()).Build();

            var firstCallLicenseInformation = licenseService.GetActiveLicense();
            var secondCallLicenseInformation = licenseService.GetActiveLicense();

            Assert.AreEqual(firstCallLicenseInformation, secondCallLicenseInformation);
            A.CallTo(() => licenseGenerator.GenerateTrialLicenseEncryptedString(authorizationCode))
                .MustHaveHappenedOnceExactly();
        }

        private static void AssertThatErrorMessageQueueContainsErrorMessage(IErrorsMessagesQueue errorsMessagesQueue,
            string trialLicenseGeneratorErrorMessage)
        {
            A.CallTo(() =>
                errorsMessagesQueue.AddErrorMessage(A<string>.That.Matches(s =>
                    s.Contains(trialLicenseGeneratorErrorMessage)))).MustHaveHappenedOnceExactly();
        }
    }
}