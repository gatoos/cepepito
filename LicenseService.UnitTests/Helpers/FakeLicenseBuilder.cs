﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using LicenseService.Core.Entities;

namespace LicenseService.UnitTests.Helpers
{
    public class FakeLicenseBuilder
    {
        private readonly DateTime _licenseGenerationDate;
        private readonly CepepitoLicenseType _licenseType;

        public FakeLicenseBuilder(CepepitoLicenseType licenseType, DateTime licenseGenerationDate)
        {
            _licenseType = licenseType;
            _licenseGenerationDate = licenseGenerationDate;
        }

        public CepepitoLicense Build()
        {
            return new CepepitoLicense("", "", _licenseType, _licenseGenerationDate);
        }
    }
}