﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using FakeItEasy;
using LicenseService.Core.Entities;
using LicenseService.ServiceProviders;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace LicenseService.UnitTests.Helpers
{
    public class FakeLicenseServiceBuilder
    {
        private readonly FakeLicenseBuilder _fakeLicenseBuilder;
        private string _authorizationCode;
        private DateTime _currentDate;
        private IErrorsMessagesQueue _errorsMessagesQueue;

        private string _exceptionMessageOnGenenerateFullLicense;
        private string _exceptionMessageOnGenerateTrialLicense;

        private (string trialLicenseGenerationErrorMessage, CepepitoLicense trialLicenseGeneratedLicense)?
            _exceptionMessageOnGenerateTrialLicenseAndThenGeneratedTrialLicense;

        private CepepitoLicense? _existingLicense;

        private (string purchaseCode, CepepitoLicense generatedFullLicense)? _generatedFullLicense;
        private (string purchaseCode, CepepitoLicense encryptedLicense)[] _generatedFullLicenses;
        private CepepitoLicense? _generatedTrialLicense;
        private bool _manipulatedClock;
        private IClockManager _clockManager;
        private RSA _privateKey;
        private byte[] _publicKeyCertificate;

        public FakeLicenseServiceBuilder()
        {
            _fakeLicenseBuilder = null;
            _currentDate = DateTime.Today;
            _manipulatedClock = false;
            _clockManager = null;
            _errorsMessagesQueue = A.Fake<IErrorsMessagesQueue>();
            _existingLicense = null;
            _generatedTrialLicense = null;
            _exceptionMessageOnGenerateTrialLicense = null;
            _exceptionMessageOnGenerateTrialLicenseAndThenGeneratedTrialLicense = null;
            _generatedFullLicense = null;
            _generatedFullLicenses = null;
            _exceptionMessageOnGenenerateFullLicense = null;
            _publicKeyCertificate = null;
            _authorizationCode = "";
            _privateKey = null;
        }

        public FakeLicenseServiceBuilder WithCurrentDateBeing(DateTime currentDate)
        {
            _currentDate = currentDate;
            return this;
        }

        public FakeLicenseServiceBuilder WithManipulatedClock()
        {
            _manipulatedClock = true;
            return this;
        }

        public FakeLicenseServiceBuilder WithClockManager(IClockManager clockManager)
        {
            _clockManager = clockManager;
            return this;
        }

        public FakeLicenseServiceBuilder WithErrorsMessagesQueue(IErrorsMessagesQueue errorsMessagesQueue)
        {
            _errorsMessagesQueue = errorsMessagesQueue;
            return this;
        }

        internal FakeLicenseServiceBuilder WithExistingLicense(RSA privateKey, CepepitoLicense existingLicense)
        {
            _privateKey = privateKey;
            _existingLicense = existingLicense;
            return this;
        }

        public (LicenseService.EntryPoints.LicenseService licenseService, ILicenseGenerator licenseGenerator) Build()
        {
            var fakeLicenseRepository = BuildFakeLicenseRepository();
            var clockManager = BuildFakeClockManager();
            var fakeLicenseGenerator = BuildFakeLicenseGenerator();

            return (licenseService: new LicenseService.EntryPoints.LicenseService(fakeLicenseRepository,
                    fakeLicenseGenerator, _publicKeyCertificate,
                    _errorsMessagesQueue, clockManager, _authorizationCode),
                licenseGenerator: fakeLicenseGenerator);
        }

        private ILicenseRepository BuildFakeLicenseRepository()
        {
            var fakeLicenseRepository = A.Fake<ILicenseRepository>();
            var existingLicense = _existingLicense ?? _fakeLicenseBuilder?.Build();
            if (existingLicense != null)
                A.CallTo(() => fakeLicenseRepository.LoadLicenseAsEncryptedString())
                    .Returns(EncryptLicense(_privateKey, _authorizationCode, existingLicense.Value));
            else
                A.CallTo(() => fakeLicenseRepository.LoadLicenseAsEncryptedString())
                    .Throws(new Exception("File not found"));

            return fakeLicenseRepository;
        }

        private IClockManager BuildFakeClockManager()
        {
            if (_clockManager != null)
                return _clockManager;

            var clockManager = A.Fake<IClockManager>();
            A.CallTo(() => clockManager.GetCurrentDateTime()).Returns(_currentDate);
            A.CallTo(() => clockManager.ClockHasBeenManipulated()).Returns(_manipulatedClock);
            return clockManager;
        }

        private ILicenseGenerator BuildFakeLicenseGenerator()
        {
            var fakeLicenseGenerator = A.Fake<ILicenseGenerator>();

            ConfigureGenerateTrialLicenseCall(_privateKey, _authorizationCode, fakeLicenseGenerator);
            ConfigureGenerateFullLicenseCall(_privateKey, _authorizationCode, fakeLicenseGenerator);

            return fakeLicenseGenerator;
        }

        private void ConfigureGenerateTrialLicenseCall(RSA privateKey, string authorizationCode,
            ILicenseGenerator fakeLicenseGenerator)
        {
            if (_exceptionMessageOnGenerateTrialLicenseAndThenGeneratedTrialLicense != null)
                A.CallTo(() => fakeLicenseGenerator.GenerateTrialLicenseEncryptedString(authorizationCode))
                    .Throws(new Exception(_exceptionMessageOnGenerateTrialLicenseAndThenGeneratedTrialLicense.Value
                        .trialLicenseGenerationErrorMessage)).Once().Then.Returns(
                        EncryptLicense(privateKey, authorizationCode,
                            _exceptionMessageOnGenerateTrialLicenseAndThenGeneratedTrialLicense.Value
                                .trialLicenseGeneratedLicense));
            else if (_exceptionMessageOnGenerateTrialLicense != null)
                A.CallTo(() => fakeLicenseGenerator.GenerateTrialLicenseEncryptedString(A<string>.Ignored))
                    .Throws(new Exception(_exceptionMessageOnGenerateTrialLicense));
            else if (_generatedTrialLicense != null)
                A.CallTo(() => fakeLicenseGenerator.GenerateTrialLicenseEncryptedString(authorizationCode))
                    .Returns(EncryptLicense(privateKey, authorizationCode, _generatedTrialLicense.Value));
        }

        private void ConfigureGenerateFullLicenseCall(RSA privateKey, string authorizationCode,
            ILicenseGenerator fakeLicenseGenerator)
        {
            if (_generatedFullLicenses != null)
                foreach (var fullLicenseInformation in _generatedFullLicenses)
                    A.CallTo(() =>
                            fakeLicenseGenerator.GenerateFullLicenseEncryptedString(_authorizationCode,
                                fullLicenseInformation
                                    .purchaseCode))
                        .Returns(EncryptLicense(privateKey, authorizationCode,
                            fullLicenseInformation.encryptedLicense));
            else if (_generatedFullLicense != null)
                A.CallTo(() =>
                        fakeLicenseGenerator.GenerateFullLicenseEncryptedString(_authorizationCode,
                            _generatedFullLicense.Value.purchaseCode))
                    .Returns(
                        EncryptLicense(privateKey, authorizationCode,
                            _generatedFullLicense.Value.generatedFullLicense));
            else if (_exceptionMessageOnGenenerateFullLicense != null)
                A.CallTo(() =>
                        fakeLicenseGenerator.GenerateFullLicenseEncryptedString(A<string>.Ignored, A<string>.Ignored))
                    .Throws(new Exception(_exceptionMessageOnGenenerateFullLicense));
        }

        public static string EncryptLicense(RSA privateKey, string authorizationCode, CepepitoLicense license)
        {
            var licenseAsXml = new XmlDocument();
            using (var stringWriter = new StringWriter())
            {
                var serializer = new XmlSerializer(typeof(CepepitoLicense));
                serializer.Serialize(stringWriter, license);
                licenseAsXml.LoadXml(stringWriter.ToString());
            }

            var signedXml = new SignedXml(licenseAsXml) {SigningKey = privateKey};
            var reference = new Reference {Uri = ""};

            var envelop = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(envelop);

            signedXml.AddReference(reference);
            signedXml.ComputeSignature();

            var xmlDigitalSignature = signedXml.GetXml();
            licenseAsXml.DocumentElement?.AppendChild(licenseAsXml.ImportNode(xmlDigitalSignature, true));

            return '"' + Convert.ToBase64String(Encoding.UTF8.GetBytes(licenseAsXml.OuterXml)) + '"';
        }

        public FakeLicenseServiceBuilder WithLicenseGeneratorGeneratingTrialLicense(
            RSA privateKey, CepepitoLicense generatedTrialLicense)
        {
            _privateKey = privateKey;
            _generatedTrialLicense = generatedTrialLicense;
            return this;
        }

        public FakeLicenseServiceBuilder WithLicenseGeneratorThrowingExceptionOnGenerateTrialLicense(
            string trialLicenseGeneratorErrorMessage)
        {
            _exceptionMessageOnGenerateTrialLicense = trialLicenseGeneratorErrorMessage;
            return this;
        }

        public FakeLicenseServiceBuilder
            WithLicenseGeneratorThrowingExceptionOnGenerateTrialLicenseAndThenGeneratingTrialLicense(
                string trialLicenseGeneratorErrorMessage, RSA privateKey, CepepitoLicense generatedTrialLicense)
        {
            _privateKey = privateKey;
            _exceptionMessageOnGenerateTrialLicenseAndThenGeneratedTrialLicense =
                (trialLicenseGenerationErrorMessage: trialLicenseGeneratorErrorMessage, trialLicenseGeneratedLicense:
                generatedTrialLicense);
            return this;
        }

        public FakeLicenseServiceBuilder WithLicenseGeneratorGeneratingFullLicense(RSA privateKey,
            (string purchaseCode, CepepitoLicense generatedFullLicense) generatedFullLicense)
        {
            _privateKey = privateKey;
            _generatedFullLicense = generatedFullLicense;
            return this;
        }

        public FakeLicenseServiceBuilder WithLicenseGeneratorGeneratingFullLicenses(RSA privateKey,
            (string purchaseCode, CepepitoLicense firstCepepitoLicense)[] generatedFullLicenses)
        {
            _privateKey = privateKey;
            _generatedFullLicenses = generatedFullLicenses;
            return this;
        }

        public FakeLicenseServiceBuilder WithLicenseGeneratorThrowingExceptionOnGenerateFullLicense(
            string licenseGeneratorErrorMessage)
        {
            _exceptionMessageOnGenenerateFullLicense = licenseGeneratorErrorMessage;
            return this;
        }

        public FakeLicenseServiceBuilder WithPublicKeyCertificateRepository(byte[] publicKeyCertificate)
        {
            _publicKeyCertificate = publicKeyCertificate;
            return this;
        }

        public static (byte[] PublicKeyCertificate, RSA PrivateKey) GeneratePrivateKeyAndPublicKeyCertificate()
        {
            var randomGenerator = new CryptoApiRandomGenerator();
            var random = new SecureRandom(randomGenerator);

            var keypairgen = new RsaKeyPairGenerator();
            keypairgen.Init(new KeyGenerationParameters(random, 1024));

            var keypair = keypairgen.GenerateKeyPair();

            var gen = new X509V3CertificateGenerator();

            var CN = new X509Name("CN=" + "FakeCertificate");
            var SN = BigInteger.ProbablePrime(120, new Random());

            ISignatureFactory signatureFactory = new Asn1SignatureFactory("SHA512WITHRSA", keypair.Private, random);

            gen.SetSerialNumber(SN);
            gen.SetSubjectDN(CN);
            gen.SetIssuerDN(CN);
            gen.SetNotAfter(DateTime.MaxValue);
            gen.SetNotBefore(DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            gen.SetPublicKey(keypair.Public);

            var newCert = gen.Generate(signatureFactory);

            var x509Certificate = new X509Certificate2(DotNetUtilities.ToX509Certificate(newCert));
            var privateKey = DotNetUtilities.ToRSA(keypair.Private as RsaPrivateCrtKeyParameters);


            return (PublicKeyCertificate: x509Certificate.GetRawCertData(), PrivateKey: privateKey);
        }

        public FakeLicenseServiceBuilder WithAuthorizationCode(string authorizationCode)
        {
            _authorizationCode = authorizationCode;
            return this;
        }

        public class FakeClockManager : IClockManager
        {
            public DateTime CurrentDateTime { get; set; }
            public bool ClockManipulated { get; set; }

            public DateTime GetCurrentDateTime()
            {
                return CurrentDateTime;
            }
            
            public bool ClockHasBeenManipulated()
            {
                return ClockManipulated;
            }
        }
    }
}