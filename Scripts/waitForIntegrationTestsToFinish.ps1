﻿param(
[string]$filename
)

$success = 0

$stopWatch = New-Object -TypeName System.Diagnostics.Stopwatch
$stopwatch.Reset();
$stopWatch.Start();

Do
{
    $fileExists = Test-Path $filename
}
While(($fileExists -eq 0) -and ($stopWatch.Elapsed.Minutes -lt 10))

$stopWatch.Restart();

if($fileExists -ne 0)
{
    Do
    {
        $result = Select-String -Path $filename -Pattern 'End to end tests end'
            
        if(($result.Matches.count -gt 0))
        {
             $success = 1
        }    
    }
    while(($success -eq 0) -and ($stopWatch.Elapsed.Minutes -lt 10))
}



if($success -eq 1)
{
    Write-Host "Success"
}
else
{
    Write-Host "Failure"
}