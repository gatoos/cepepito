@echo off
setlocal EnableDelayedExpansion

IF "%~1"=="" GOTO :Usage

SET PATH=%PATH%;"c:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE";"c:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\Extensions\TestPlatform";"c:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin"

set /A launchCleanBuild=0
set /A launchBuild=0
set /A launchUnitTests=0
set /A launchIntegrationTests=0
set /A launchEndToEndTests=0

:parse
IF "%~1"=="" GOTO endparse
IF "%~1"=="-clean" set /A launchCleanBuild=1
IF "%~1"=="-build" set /A launchBuild=1
IF "%~1"=="-unittests" set /A launchUnitTests=1
IF "%~1"=="-inttests" set /A launchIntegrationTests=1
IF "%~1"=="-e2etests" set /A launchEndToEndTests=1
IF "%~1"=="-all" (
set /A launchCleanBuild=1
set /A launchBuild=1
set /A launchUnitTests=1
set /A launchIntegrationTests=1
set /A launchEndToEndTests=1
)
SHIFT
GOTO parse
:endparse

set cepepitoSourceDirectory=%cd%\..

cd %cepepitoSourceDirectory%

cd %cepepitoSourceDirectory%\Scripts

IF NOT EXIST buildLogs (
mkdir buildLogs
) 

cd %cepepitoSourceDirectory%

rem //////////////////////////// BUILD CLEAN ///////////////////////////

IF /I "%launchCleanBuild%" NEQ "0" (

echo ------------------------------------------
echo Clearing previous build files
echo ------------------------------------------

cd %cepepitoSourceDirectory%\scripts

IF EXIST TestResults (
rmdir /s /q TestResults
)

IF EXIST buildLogs (
rmdir /s /q buildLogs
mkdir buildLogs
) 

)

rem //////////////////////////// BUILD /////////////////////////////////

IF /I "%launchBuild%" NEQ "0" (
cd %cepepitoSourceDirectory%

echo --------------------------------------
echo Building Debug x86 2017 version
echo --------------------------------------
nuget restore
Msbuild /p:Configuration=Debug /p:Platform=x86

echo ------------------------------------------
echo Building End to end test x86 2017 version
echo ------------------------------------------
rem nuget restore
rem Msbuild /p:Configuration=EndToEndTests /p:Platform=x86
)

rem //////////////////////// UNIT TESTS ///////////////////////////////

IF /I "%launchUnitTests%" NEQ "0" (

echo --------------------------------------
echo Launching unit tests
echo --------------------------------------


vstest.console.exe %cepepitoSourceDirectory%\CepepitoCore.UnitTests\bin\Debug\CepepitoCore.UnitTests.dll %cepepitoSourceDirectory%\CepepitoCoreAdapters.UnitTests\bin\Debug\CepepitoCoreAdapters.UnitTests.dll %cepepitoSourceDirectory%\ExtensionRegistrationService.UnitTests\bin\Debug\ExtensionRegistrationService.UnitTests.dll %cepepitoSourceDirectory%\LicenseService.UnitTests\bin\Debug\LicenseService.UnitTests.dll %cepepitoSourceDirectory%\LicenseServiceAdapters.UnitTests\bin\Debug\LicenseServiceAdapters.UnitTests.dll /Logger:trx /InIsolation
rem cd %cepepitoSourceDirectory%\scripts
rem msxsl.exe TestResults\*.trx mstest-to-junit.xsl -o %cepepitoSourceDirectory%\scripts\buildLogs\UnitTestsResults.xml
rem msxsl.exe %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoCoreAdapters.UnitTestsResults.trx mstest-to-junit.xsl -o %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoCoreAdapters.UnitTestsResults.xml
rem msxsl.exe %cepepitoSourceDirectory%\scripts\buildLogs\ExtensionRegistrationService.UnitTestsResults.trx mstest-to-junit.xsl -o %cepepitoSourceDirectory%\scripts\buildLogs\ExtensionRegistrationService.UnitTestsResults.xml
rem msxsl.exe %cepepitoSourceDirectory%\scripts\buildLogs\LicenseService.UnitTestsResults.trx mstest-to-junit.xsl -o %cepepitoSourceDirectory%\scripts\buildLogs\LicenseService.UnitTestsResults.xml
rem msxsl.exe %cepepitoSourceDirectory%\scripts\buildLogs\LicenseServiceAdapters.UnitTestsResults.trx mstest-to-junit.xsl -o %cepepitoSourceDirectory%\scripts\buildLogs\LicenseServiceAdapters.UnitTestsResults.xml

)

rem //////////////////////// INTEGRATION TESTS ///////////////////////////////

IF /I "%launchIntegrationTests%" NEQ "0" (

echo --------------------------------------
echo Launching integration tests
echo --------------------------------------


mstest.exe /resultsfile:%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoIntegrationTestsResults.trx /testcontainer:%cepepitoSourceDirectory%\CepepitoIntegrationTests\bin\Debug\CepepitoIntegrationTests.dll /nologo
cd %cepepitoSourceDirectory%\scripts
msxsl.exe %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoIntegrationTestsResults.trx mstest-to-junit.xsl -o %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoIntegrationTestsResults.xml
)

rem //////////////////// END TO END TESTS ////////////////////////////

IF /I "%launchEndToEndTests%" NEQ "0" (
echo --------------------------------------
echo Killing opened visual studio instances
echo --------------------------------------
Taskkill /IM devenv.exe /F >nul 2>&1

echo -------------------------------------------
echo Uninstalling previous Cepepito installation
echo -------------------------------------------
echo Processing...
erase %TEMP%\CepepitoUninstall.log
Taskkill /IM VCTIP.EXE /F >nul 2>&1
Taskkill /IM mspdbsrv.exe /F >nul 2>&1
Taskkill /IM ServiceHub.Host.Node.x86.exe /F >nul 2>&1
Taskkill /IM ServiceHub.IdentityHost.exe /F >nul 2>&1
start /wait vsixinstaller /q /l:CepepitoUninstall.log /u:Cepepito.cocorico.eef1cbb2-7ecf-4ac9-bedf-bff8ddb9e8d6

PowerShell.exe -ExecutionPolicy Bypass -File %cepepitoSourceDirectory%\scripts\waitForUninstallToFinish.ps1 -filename %TEMP%\CepepitoUninstall.log>%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoUninstallStatus.log
set /p cepepitoUninstallStatus=<%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoUninstallStatus.log
del %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoUninstallStatus.log
copy %TEMP%\CepepitoUninstall.log %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoUninstall.log
if "!cepepitoUninstallStatus!" NEQ "Success" GOTO :UninstallError

echo -----------------------------------------
echo Installing Cepepito for end to end tests
echo -----------------------------------------
echo Processing...
cd %cepepitoSourceDirectory%\bin\EndToEndTests
erase %TEMP%\CepepitoInstall.log
Taskkill /IM VCTIP.EXE /F >nul 2>&1
Taskkill /IM mspdbsrv.exe /F >nul 2>&1
Taskkill /IM ServiceHub.Host.Node.x86.exe /F >nul 2>&1
Taskkill /IM ServiceHub.IdentityHost.exe /F >nul 2>&1
start /wait vsixinstaller /q /s:Community /v:15.0 /l:CepepitoInstall.log "Cepepito.vsix"

PowerShell.exe -ExecutionPolicy Bypass -File %cepepitoSourceDirectory%\scripts\waitForInstallToFinish.ps1 -filename %TEMP%\CepepitoInstall.log>%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstallStatus.log
set /p cepepitoInstallStatus=<%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstallStatus.log
del %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstallStatus.log
copy %TEMP%\CepepitoInstall.log %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstall.log
if "!cepepitoInstallStatus!" NEQ "Success" GOTO :InstallError

PowerShell.exe -ExecutionPolicy Bypass -File %cepepitoSourceDirectory%\scripts\retrieveInstallPath.ps1 -filename %TEMP%\CepepitoInstall.log> %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstallDirectory.log
set /p cepepitoInstallDirectory=<%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstallDirectory.log


echo ---------------------------
echo Launching end to end tests
echo ---------------------------
start /wait devenv

PowerShell.exe -ExecutionPolicy Bypass -File %cepepitoSourceDirectory%\scripts\waitForIntegrationTestsToFinish.ps1 -filename !cepepitoInstallDirectory!endToEndTests.log>%cepepitoSourceDirectory%\scripts\buildLogs\EndToEndTestRunStatus.log
set /p EndToEndTestRunStatus=<%cepepitoSourceDirectory%\scripts\buildLogs\EndToEndTestRunStatus.log
del %cepepitoSourceDirectory%\scripts\buildLogs\EndToEndTestRunStatus.log
copy !cepepitoInstallDirectory!endToEndTests.xml %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoEndToEndTestsResults.xml
if "!endToEndTestRunStatus!" NEQ "Success" GOTO :EndToEndTestsError

echo End to end tests results :
type %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoEndToEndTestsResults.xml

Taskkill /IM devenv.exe /F >nul 2>&1
echo -------------------------------------------
echo Restoring test environment
echo -------------------------------------------
echo Processing...
erase %TEMP%\CepepitoUninstall.log
Taskkill /IM VCTIP.EXE /F >nul 2>&1
Taskkill /IM mspdbsrv.exe /F >nul 2>&1
Taskkill /IM ServiceHub.Host.Node.x86.exe /F >nul 2>&1
Taskkill /IM ServiceHub.IdentityHost.exe /F >nul 2>&1
start /wait vsixinstaller /q /l:CepepitoUninstall.log /u:Cepepito.cocorico.eef1cbb2-7ecf-4ac9-bedf-bff8ddb9e8d6

PowerShell.exe -ExecutionPolicy Bypass -File %cepepitoSourceDirectory%\scripts\waitForUninstallToFinish.ps1 -filename %TEMP%\CepepitoUninstall.log>%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoRestoreEnvStatus.log
set /p cepepitoRestoreEnvStatus=<%cepepitoSourceDirectory%\scripts\buildLogs\CepepitoRestoreEnvStatus.log
del %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoRestoreEnvStatus.log
copy %TEMP%\CepepitoUninstall.log %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoEnvRestore.log
if "!cepepitoRestoreEnvStatus!" NEQ "Success" GOTO :EnvCleanError
)

goto :End

:Usage

echo usage : build.bat [-clean] [-build] [-unittests] [-inttests] [-all]
echo -clean : cleans the previous run logs
echo -build : rebuilds the solution in debug mode and for integration tests
echo -unittests : launches unit tests
echo -inttests : launches integration tests
echo -e2etests : launches end to end tests
echo -all : combines all the previous
goto :End

:UninstallError

echo An error occured when trying to uninstall cepepito extension, integration tests stopped, check %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoUninstall.log
goto :End

:InstallError

echo An error occured when trying to Install cepepito extension, integration tests stopped, check %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoInstall.log
goto :End

:EndToEndTestsError
echo An error occured during end to end tests execution, end to end tests stopped, check %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoEndToEndTestsResults.log
goto :End

:EnvCleanError

echo An error occured when trying to clean cepepito test environment check %cepepitoSourceDirectory%\scripts\buildLogs\CepepitoEnvRestore.log
goto :End


:End

endlocal