def resetVirtualMachineState(String vmId)
{
    def vmInitiallyRegistered = true;
    try
    {
        sh("/usr/local/bin/prlctl list \"${vmId}\"")
    }
    catch(e)
    {
        vmInitiallyRegistered = false;
    }
    
    if(vmInitiallyRegistered)
    {
        try
        {
            sh("/usr/local/bin/prlctl stop \"${vmId}\"")
        }
        catch(e)
        {}
        
        sh("/usr/local/bin/prlctl unregister \"${vmId}\"")
    }
    
}

def suspendVirtualMachine(String vmId)
{
    try
        {
            sh("/usr/local/bin/prlctl suspend \"${vmId}\"")
        }
        catch(e)
        {}
}

def resumeVirtualMachine(String vmId)
{
    try
        {
            sh("/usr/local/bin/prlctl resume \"${vmId}\"")
        }
        catch(e)
        {}
}

stage('Resetting virtual machines state')
{
    node('master')
    {
        resetVirtualMachineState("Windows 10 VS 2017 build")
        resetVirtualMachineState("Windows 10 VS 2017 prod")
    }
}

stage('Start VS 2017 build vm') {
node('master')
{
    sh('/usr/local/bin/prlctl register "/Volumes/Photos/CepepitoVms/Vms/Windows10_vs2017_build.pvm/"')
    try
    {
        sh('/usr/local/bin/prlctl start "Windows 10 VS 2017 build"')
    }
    catch(e)
    {
        suspendVirtualMachine("My Boot Camp");
        sh('/usr/local/bin/prlctl start "Windows 10 VS 2017 build"')
    }
    
    echo 'Windows VS 2017 build virtual machine succesfully started'
}
}

stage('Sync sources for build')
{
    node('Win10Vs2017Build')
    {
        git credentialsId: 'BitBucketSSH', url: 'git@bitbucket.org:rhedia/cppunittests.git'
    }
}

stage('Build debug')
{
    node('Win10Vs2017Build')
    {
        dir('Scripts') 
        {
            bat('build.bat -clean -build')
        }
    }
}

stage ('Publish unit tests results')
{
    node('Win10Vs2017Build')
    {
		dir('Scripts') 
        {
            bat('build.bat -unittests')
        }
        junit 'Scripts/buildLogs/CepepitoUnitTestResults.xml'
    }
}

stage ('Publish integration tests results')
{
    node('Win10Vs2017Build')
    {
		dir('Scripts') 
        {
            bat('build.bat -inttests')
        }
        junit 'Scripts/buildLogs/CepepitoIntegrationTestsResults.xml'
    }
}

stage ('Archive artifacts')
{
    node('Win10Vs2017Build')
    {
        dir('bin\\EndToEndTests') 
        {
            stash name: "CepepitoEndToEndTestsVSIX", includes: "*.vsix"
        }
    }
}

stage('Shutdown VS 2017 build vm')
{
node('master')
{
    sh('/usr/local/bin/prlctl stop "Windows 10 VS 2017 build"')
    sh('/usr/local/bin/prlctl unregister "Windows 10 VS 2017 build"')
    echo 'Windows VS 2017 build virtual machine successfully stopped'
}
}

stage ('Start VS 2017 prod vm')
{
    node('master')
    {
        sh('/usr/local/bin/prlctl register "/Volumes/Photos/CepepitoVms/Vms/Windows10_vs2017_prod.pvm/"')
        sh('/usr/local/bin/prlctl start "Windows 10 VS 2017 prod"')
        echo 'Windows VS 2017 prod virtual machine succesfully started'
    }
}

stage('Sync sources for prod')
{
    node('Win10Vs2017Prod')
    {
        git credentialsId: 'BitBucketSSH', url: 'git@bitbucket.org:rhedia/cppunittests.git'
    }
}

stage ('End to end Tests on prod')
{
    node('Win10Vs2017Prod')
    {
        try
        {
            bat('mkdir bin')
        }
        catch(e)
        {}
        
        dir('bin')
        {
            try
            {
            bat('mkdir EndToEndTests')
            }
            catch(e)
            {}
        }
        dir('bin\\EndToEndTests')
        {
            unstash "CepepitoEndToEndTestsVSIX"
        }
        dir('Scripts')
        {
            bat('build.bat -e2etests')
        }
    }
}

stage ('Publish end to end tests results')
{
    node('Win10Vs2017Prod')
    {
        junit 'Scripts/buildLogs/CepepitoEndToEndTestsResults.xml'
    }
}

stage('Shutdown VS 2017 prod vm')
{
node('master')
{
    sh('/usr/local/bin/prlctl stop "Windows 10 VS 2017 prod"')
    sh('/usr/local/bin/prlctl unregister "Windows 10 VS 2017 prod"')
    echo 'Windows VS 2017 prod virtual machine successfully stopped'
}
}

stage('Resume vm environment')
{
    resumeVirtualMachine("My Boot Camp")
}
