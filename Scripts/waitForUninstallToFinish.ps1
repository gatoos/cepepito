﻿param(
[string]$filename
)

$success = 0

$stopWatch = New-Object -TypeName System.Diagnostics.Stopwatch
$stopwatch.Reset();
$stopWatch.Start();

Do
{
    $fileExists = Test-Path $filename
}
While(($fileExists -eq 0) -and ($stopWatch.Elapsed.Minutes -lt 5))

$stopWatch.Restart();

if($fileExists -ne 0)
{
    Do
    {
        $result = Select-String -Path $filename -Pattern "Désinstallation de Cepepito terminée"
        $resultEnglish = Select-String -Path $filename -Pattern "Successfully uninstalled Cepepito"
        $resultDontExist = Select-String -Path $filename -Pattern "Microsoft.VisualStudio.ExtensionManager.NotInstalledException" 
    
        if(($result.Matches.count -gt 0) -or ($resultEnglish.Matches.count -gt 0) -or ($resultDontExist.Matches.count -gt 0))
        {
             $success = 1
        }    
    }
    while(($success -eq 0) -and ($stopWatch.Elapsed.Minutes -lt 5))
}



if($success -eq 1)
{
    Write-Host "Success"
}
else
{
    Write-Host "Failure"
}