﻿param(
[string]$filename
)

$installDirectory = "";

$stopWatch = New-Object -TypeName System.Diagnostics.Stopwatch
$stopwatch.Reset();
$stopWatch.Start();

Do
{
    $fileExists = Test-Path $filename
}
While(($fileExists -eq 0) -and ($stopWatch.Elapsed.Minutes -lt 5))

$stopWatch.Restart();

if($fileExists -ne 0)
{
    Do
    {
        $result = Select-String -Path $filename -Pattern 'Visual Studio Community.*(C:\\.*)'
        if($result.Matches.count -eq 1)
        {
            $installDirectory = $result.Matches.Groups[1].Value;            
        }    
    }
    while(($result.Matches.Count -eq 0) -and ($stopWatch.Elapsed.Minutes -lt 5))
}

Write-Host $installDirectory