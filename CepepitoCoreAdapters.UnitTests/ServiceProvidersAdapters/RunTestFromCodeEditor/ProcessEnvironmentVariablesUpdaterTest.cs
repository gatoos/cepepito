﻿using System;
using System.Collections.Specialized;
using CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor;
using Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.UnitTests.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    class FakeLoggerFactory : ILoggerFactory
    {
        public ILogger BuildPerAssemblyLogger(Type assemblyType)
        {
            return new FakeLogger();
        }
    }

    internal class FakeLogger : ILogger
    {
        public void Debug(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Info(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Warn(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Error(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Fatal(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Debug(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Info(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Warn(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Error(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Fatal(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public bool IsDebugEnabled { get; }
        public bool IsInfoEnabled { get; }
        public bool IsWarnEnabled { get; }
        public bool IsErrorEnabled { get; }
        public bool IsFatalEnabled { get; }
    }

    [TestClass]
    public class ProcessEnvironmentVariablesUpdaterTest
    {
        [DataRow(false)]
        [DataRow(true)]
        [TestMethod]
        public void Given_EmptyEnvironmentVariables_When_UpdatingEnvironmentVariablesWithNewVariables_Then_ItShouldUpdateTheProcessEnvironmentVariablesWithNewValues(bool mergeVariables)
        {
            var processEnvironmentVariables = new StringDictionary();
            var projectEnvironmentVariables = new[] { new EnvironmentVariable("EnvVar2", "EnvVar2Value") };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, mergeVariables);

            Assert.AreEqual(1, processEnvironmentVariables.Count);
            Assert.AreEqual("EnvVar2Value", processEnvironmentVariables["EnvVar2"]);
        }

        [DataRow(false)]
        [DataRow(true)]
        [TestMethod]
        public void Given_ANonEmptyEnvironmentVariables_When_UpdatingEnvironmentVariablesWithEmptyVariables_Then_ItShouldNotModifyTheOriginalEnvironmentVariables(bool mergeVariables)
        {
            var processEnvironmentVariables = new StringDictionary { { "EnvVar1", "EnvVar1Value" } };
            var projectEnvironmentVariables = new EnvironmentVariable[] { };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, mergeVariables);

            Assert.AreEqual(1, processEnvironmentVariables.Count);
            Assert.AreEqual("EnvVar1Value", processEnvironmentVariables["EnvVar1"]);
        }

        [DataRow(false)]
        [DataRow(true)]
        [DataTestMethod]
        public void When_UpdatingEnvironmentVariablesWithNewVariables_Then_ItShouldUpdateTheProcessEnvironmentVariablesWithNewValues(bool mergeVariables)
        {
            var processEnvironmentVariables = new StringDictionary { { "EnvVar1", "EnvVar1Value" } };
            var projectEnvironmentVariables = new[] { new EnvironmentVariable("EnvVar2", "EnvVar2Value") };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, mergeVariables);

            Assert.AreEqual(2, processEnvironmentVariables.Count);
            Assert.AreEqual("EnvVar2Value", processEnvironmentVariables["EnvVar2"]);
        }

        [DataRow(false)]
        [DataRow(true)]
        [DataTestMethod]
        public void When_UpdatingEnvironmentVariablesWithNewVariablesTwice_Then_ItShouldUpdateTheProcessEnvironmentVariablesWithNewValues(bool mergeVariables)
        {
            var processEnvironmentVariables = new StringDictionary { { "EnvVar1", "EnvVar1Value" } };
            var projectEnvironmentVariables = new[] { new EnvironmentVariable("EnvVar2", "EnvVar2Value") };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, mergeVariables);

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, mergeVariables);

            Assert.AreEqual(2, processEnvironmentVariables.Count);
            Assert.AreEqual("EnvVar2Value", processEnvironmentVariables["EnvVar2"]);
        }

        [DataRow(false)]
        [DataRow(true)]
        [DataTestMethod]
        public void When_UpdatingEnvironmentVariablesWithExistingVariablesWhereKeyIsNotPath_Then_ItShouldUpdateTheOverrideProcessEnvironmentVariablesWithNewValues(bool mergeVariables)
        {
            var processEnvironmentVariables = new StringDictionary { { "EnvVar1", "EnvVar1Value" } };
            var projectEnvironmentVariables = new[] { new EnvironmentVariable("EnvVar1", "EnvVar2Value") };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, mergeVariables);

            Assert.AreEqual(1, processEnvironmentVariables.Count);
            Assert.AreEqual("EnvVar2Value", processEnvironmentVariables["EnvVar1"]);
        }

        [TestMethod]
        public void When_UpdatingEnvironmentVariablesWithExistingVariablesWhereKeyIsPathWithMergeVariablesFalse_Then_ItShouldUpdateTheOverrideProcessEnvironmentVariablesWithNewValues()
        {
            var processEnvironmentVariables = new StringDictionary { { "PATH", "EnvVar1Value" } };
            var projectEnvironmentVariables = new[] { new EnvironmentVariable("PATH", "EnvVar2Value") };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, false);

            Assert.AreEqual(1, processEnvironmentVariables.Count);
            Assert.AreEqual("EnvVar2Value", processEnvironmentVariables["PATH"]);
        }

        [DataRow("EnvVar1Value", "EnvVar2Value", "EnvVar1Value;EnvVar2Value")]
        [DataRow("EnvVar1Value;", "EnvVar2Value", "EnvVar1Value;EnvVar2Value")]
        [DataRow("EnvVar1Value", ";EnvVar2Value", "EnvVar1Value;EnvVar2Value")]
        [DataRow("EnvVar1Value;", ";EnvVar2Value", "EnvVar1Value;EnvVar2Value")]
        [DataRow("EnvVar1Value;;", ";EnvVar2Value", "EnvVar1Value;EnvVar2Value")]
        [DataRow("EnvVar1Value;;", ";;EnvVar2Value", "EnvVar1Value;EnvVar2Value")]
        [DataRow(null, ";;EnvVar2Value", ";;EnvVar2Value")]
        [DataRow("", ";;EnvVar2Value", ";;EnvVar2Value")]
        [DataRow("EnvVar1Value", null, "EnvVar1Value")]
        [DataRow("EnvVar1Value;;", null, "EnvVar1Value;;")]
        [DataRow("EnvVar1Value", "", "EnvVar1Value")]
        [DataRow("EnvVar1Value;;", "", "EnvVar1Value;;")]
        [DataTestMethod]
        public void When_UpdatingEnvironmentVariablesWithExistingVariablesWhereKeyIsPathWithMergeVariablesTrue_Then_ItShouldUpdateTheOverrideProcessEnvironmentVariablesWithNewValues(string processEnvVarValue, string projectEnvVarValue, string expectedEnvValue)
        {
            var processEnvironmentVariables = new StringDictionary { { "PATH", processEnvVarValue } };
            var projectEnvironmentVariables = new[] { new EnvironmentVariable("PATH", projectEnvVarValue) };

            new ProcessEnvironmentVariablesUpdater(new FakeLoggerFactory()).UpdateExecutionEnvironmentVariables(processEnvironmentVariables,
                projectEnvironmentVariables, true);

            Assert.AreEqual(1, processEnvironmentVariables.Count);
            Assert.AreEqual(expectedEnvValue, processEnvironmentVariables["PATH"]);
        }
    }
}

