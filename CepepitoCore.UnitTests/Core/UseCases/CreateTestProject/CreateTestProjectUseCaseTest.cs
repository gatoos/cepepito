﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;
using CepepitoCore.UnitTests.Core.UseCases.Common;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CepepitoCore.UnitTests.Core.UseCases.CreateTestProject
{
    [TestClass]
    public class CreateTestProjectUseCaseTest
    {
        [TestMethod]
        public void When_ProjectIsAnExecutableCppProject_Then_AProjectContainingAllSourceProjectFilesShouldBeCreated()
        {
        }

        [TestMethod]
        public void When_ProjectIsDynamicLinkedLibraryCppProject_Then_AProjectReferencingTheOriginalProjectShouldBeCreated()
        {
        }

        [TestMethod]
        public void When_ProjectIsAStaticallyLinkedLibraryCppProject_Then_AProjectReferencingTheOriginalProjectStaticLibraryShouldBeCreated()
        {
        }

        [TestMethod]
        public void When_ProjectIsAVs2012Project_Then_ThePreprocessorsDefinitionShouldBeUpdatedAccordingly()
        {
        }

        [TestMethod]
        public void When_TestProjectIsCreated_Then_CreatedProjectShouldContainATestRunner()
        {
        }

        // Add tests to ensure include directories & libraries have been correctly set (or a least correctly passed)

        [TestMethod]
        public void When_ProjectIsNotACppProject_Then_AnErrorShouldBeSentToErrorsMessageQueue()
        {
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var selectedProjectPropertiesRetriever = A.Fake<ISelectedProjectPropertiesRetriever>();
            A.CallTo(() => selectedProjectPropertiesRetriever.ProjectIsCppProject).Returns(false);

            var createTestProjectUseCase = new CreateTestProjectUseCase(selectedProjectPropertiesRetriever,
                A.Fake<ISolutionProjectsRepository>(),
                A.Fake<ITestingLibrariesConfiguration>(), "", errorMessagesQueue);

            createTestProjectUseCase.Execute();

            Assert.IsTrue(FakeErrorsMessagesQueue.ErrorMessagesQueueContainOneErrorMessageContaining(errorMessagesQueue,
                new List<string> { "project", "not", "cpp" }));
        }

        [TestMethod]
        public void When_ProjectIsACppProjectWithATestRunner_Then_AnErrorShouldBeSentToErrorsMessageQueue()
        {
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var selectedProjectPropertiesRetriever = A.Fake<ISelectedProjectPropertiesRetriever>();

            A.CallTo(() => selectedProjectPropertiesRetriever.ProjectIsCppProject).Returns(true);
            A.CallTo(() => selectedProjectPropertiesRetriever.ProjectContainsTestRunner).Returns(true);

            var createTestProjectUseCase = new CreateTestProjectUseCase(selectedProjectPropertiesRetriever,
                A.Fake<ISolutionProjectsRepository>(),
                A.Fake<ITestingLibrariesConfiguration>(), "", errorMessagesQueue);

            createTestProjectUseCase.Execute();

            Assert.IsTrue(FakeErrorsMessagesQueue.ErrorMessagesQueueContainOneErrorMessageContaining(errorMessagesQueue,
                new List<string> { "project", "contains", "test", "runner" }));
        }
    }
}
