﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Linq;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.Core.UseCases.RunTests;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.UI;
using CepepitoCore.UnitTests.Core.UseCases.Common;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CepepitoCore.UnitTests.Core.UseCases.RunTestFromCodeEditor
{
    [TestClass]
    public class RunTestFromCodeEditorUseCaseControllerTest
    {
        [TestMethod]
        public void When_DocumentContentCantBeRetrieved_Then_NothingHappens()
        {
            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            var testRunnerViewerViewModel = A.Fake<ITestRunnerViewerViewModel>();
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var loggerFactory = new FakeLoggerFactory();
            var runTestFromCodeEditorUseCaseController =
                new RunTestFromCodeEditorUseCaseController(testRunnerViewerViewModel,
                    currentDocumentInformationRetriever, projectOutputLauncherStub,
                    errorMessagesQueue, loggerFactory);
            runTestFromCodeEditorUseCaseController.LaunchTestAtCursorPosition();

            A.CallTo(() => testRunnerViewerViewModel.UpdateTestCaseExecutionResults(A<TestCaseExecutionResults>._,
                A<TestCaseIdentifierWithAssociatedProjects>._)).MustNotHaveHappened();
        }

        [TestMethod]
        // The error message queue doesn't contain the exception message anymore, because the exception application exception handling is treated higher in the stack in a single place
        //public void When_DocumentContentContainsTestCaseInformationButProjectOutputLauncherThrowsAnError_Then_AnErrorIsThrownFromTheErrorMessageQueueContainsTheError() 
        public async Task When_DocumentContentContainsTestCaseInformationButProjectOutputLauncherThrowsAnError_Then_AnErrorIsThrownFromTheUseCaseController()
        {
            var testprojectLabel = "TestProject";
            var testClassLabel = "TestClass";
            var testCaseLabel = "testCase1";
            var projectOutputLauncherExceptionDescription = "Error occured during build";
            var testLauncherArgument = "*" + testClassLabel + "." + testCaseLabel + "*";
            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(
                new TestCaseIdentifier(testClassLabel + "." + testCaseLabel),
                new ProjectIdentifier(testprojectLabel));
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var loggerFactory = new FakeLoggerFactory();


            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentCursorLocationLineContent())
                .Returns("TEST(" + testClassLabel + ", " + testCaseLabel + ")");

            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(testprojectLabel);

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() => projectOutputLauncherStub.LaunchProjectWithArguments(testprojectLabel, testLauncherArgument))
                .Throws(new Exception(projectOutputLauncherExceptionDescription));

            var runTestFromCodeEditorUseCaseController =
                new RunTestFromCodeEditorUseCaseController(A.Fake<ITestRunnerViewerViewModel>(),
                    currentDocumentInformationRetriever, projectOutputLauncherStub,
                    errorMessagesQueue, loggerFactory);
            await Assert.ThrowsExceptionAsync<Exception>(() => runTestFromCodeEditorUseCaseController.LaunchTestAtCursorPosition());
        }

        [TestMethod]
        public void
            When_CurrentDocumentLineContentContainsTestCaseInformation_Then_RunnerLaunchTestsOnCurrentLineContentTestCaseInformation()
        {
            const string testprojectLabel = "TestProject";
            const string testClassLabel = "TestClass";
            const string testCaseLabel = "testCase1";
            const string testLauncherArgument = "*" + testClassLabel + "." + testCaseLabel + "*";
            var testCaseExecutionResultsAsXml =
                new FakeTestCaseXmlExecutionResultBuilder().BuildFakeTestCaseExecutionResultAsXml(testClassLabel,
                    testCaseLabel);
            var expectedTestSuitesExecutionResults =
                BuildExpectedTestSuitesExecutionResults(testClassLabel, testCaseLabel);
            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(
                new TestCaseIdentifier(testClassLabel + "." + testCaseLabel),
                new ProjectIdentifier(testprojectLabel));
            var expectedUseCaseOutput = new RunTestsUseCaseOutput(expectedTestSuitesExecutionResults,
                expectedTestCasesIdenfierWithAssociatedProjects);
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var loggerFactory = new FakeLoggerFactory();

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentCursorLocationLineContent())
                .Returns("TEST(" + testClassLabel + ", " + testCaseLabel + ")");

            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(testprojectLabel);

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() => projectOutputLauncherStub.LaunchProjectWithArguments(testprojectLabel, testLauncherArgument))
                .Returns(testCaseExecutionResultsAsXml);

            RunTestsUseCaseOutput testCaseResults = null;
            var testRunnerViewerViewModel = A.Fake<ITestRunnerViewerViewModel>();
            A.CallTo(() =>
                testRunnerViewerViewModel.UpdateTestCaseExecutionResults(A<TestCaseExecutionResults>._,
                    A<TestCaseIdentifierWithAssociatedProjects>._)).Invokes(
                (TestCaseExecutionResults testCaseExecutionResults,
                        TestCaseIdentifierWithAssociatedProjects testCasesIdenfierWithAssociatedProjects) =>
                        testCaseResults = new RunTestsUseCaseOutput(testCaseExecutionResults,
                            testCasesIdenfierWithAssociatedProjects));
            var runTestFromCodeEditorUseCaseController =
                new RunTestFromCodeEditorUseCaseController(testRunnerViewerViewModel,
                    currentDocumentInformationRetriever, projectOutputLauncherStub,
                    errorMessagesQueue, loggerFactory);
            runTestFromCodeEditorUseCaseController.LaunchTestAtCursorPosition();

            Assert.IsTrue(expectedUseCaseOutput.Equals(testCaseResults));
        }

        [TestMethod]
        public void
            When_CurrentDocumentLineContentDoesntContainTestCaseInformation_Then_RunnerLaunchTestsOnWholeDocumentContent()
        {
            const string testprojectLabel = "TestProject";
            const string testClassLabel = "TestClass";
            const string testCase1Label = "testCase1";
            const string testCase2Label = "testCase2";
            var documentContent = "TEST(" + testClassLabel + ", " + testCase1Label + ")" +
                                  Environment.NewLine +
                                  "{}" +
                                  Environment.NewLine +
                                  "TEST(" + testClassLabel + ", " + testCase2Label + ")" +
                                  Environment.NewLine +
                                  "{}" +
                                  Environment.NewLine;

            const string testLauncherArgument = "*" + testClassLabel + "." + testCase1Label + "*" +
                                                ":" + "*" + testClassLabel + "." + testCase2Label + "*";
            var testCaseExecutionResultsAsXml =
                new FakeTestCaseXmlExecutionResultBuilder().BuildFakeTestCaseExecutionResultAsXml(testClassLabel,
                    testCase1Label, testCase2Label);
            var expectedTestSuitesExecutionResults =
                BuildExpectedTestSuitesExecutionResults(testClassLabel, testCase1Label, testCase2Label);

            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(
                new TestCaseIdentifier(testClassLabel + "." + testCase1Label),
                new ProjectIdentifier(testprojectLabel));
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(
                new TestCaseIdentifier(testClassLabel + "." + testCase2Label),
                new ProjectIdentifier(testprojectLabel));
            var expectedUseCaseOutput = new RunTestsUseCaseOutput(expectedTestSuitesExecutionResults,
                expectedTestCasesIdenfierWithAssociatedProjects);
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var loggerFactory = new FakeLoggerFactory();

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentCursorLocationLineContent())
                .Returns("{}");
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentContent())
                .Returns(documentContent);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(testprojectLabel);

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() => projectOutputLauncherStub.LaunchProjectWithArguments(testprojectLabel, testLauncherArgument))
                .Returns(testCaseExecutionResultsAsXml);

            RunTestsUseCaseOutput testCaseResults = null;
            var testRunnerViewerViewModel = A.Fake<ITestRunnerViewerViewModel>();
            A.CallTo(() =>
                testRunnerViewerViewModel.UpdateTestCaseExecutionResults(A<TestCaseExecutionResults>._,
                    A<TestCaseIdentifierWithAssociatedProjects>._)).Invokes(
                (TestCaseExecutionResults testCaseExecutionResults,
                        TestCaseIdentifierWithAssociatedProjects testCasesIdenfierWithAssociatedProjects) =>
                        testCaseResults = new RunTestsUseCaseOutput(testCaseExecutionResults,
                            testCasesIdenfierWithAssociatedProjects));
            var runTestFromCodeEditorUseCaseController =
                new RunTestFromCodeEditorUseCaseController(testRunnerViewerViewModel,
                    currentDocumentInformationRetriever, projectOutputLauncherStub,
                    errorMessagesQueue, loggerFactory);
            runTestFromCodeEditorUseCaseController.LaunchTestAtCursorPosition();

            Assert.IsTrue(expectedUseCaseOutput.Equals(testCaseResults));
        }

        [TestMethod]
        public void
            When_CurrentDocumentLineContentDoesntContainTestCaseInformationAnDocumentContainTwiceTheSameTestCase_Then_RunnerLaunchTestsOnWholeDocumentContent()
        {
            const string testprojectLabel = "TestProject";
            const string testClassLabel = "TestClass";
            const string testCase1Label = "testCase1";
            var documentContent = "TEST(" + testClassLabel + ", " + testCase1Label + ")" +
                                  Environment.NewLine +
                                  "{}" +
                                  Environment.NewLine +
                                  "TEST(" + testClassLabel + ", " + testCase1Label + ")" +
                                  Environment.NewLine +
                                  "{}" +
                                  Environment.NewLine;

            const string testLauncherArgument = "*" + testClassLabel + "." + testCase1Label + "*" + ":" +
                                                "*" + testClassLabel + "." + testCase1Label + "*";

            var testCaseExecutionResultsAsXml =
                new FakeTestCaseXmlExecutionResultBuilder().BuildFakeTestCaseExecutionResultAsXml(testClassLabel,
                    testCase1Label);
            var expectedTestSuitesExecutionResults =
                BuildExpectedTestSuitesExecutionResults(testClassLabel, testCase1Label);

            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(
                new TestCaseIdentifier(testClassLabel + "." + testCase1Label),
                new ProjectIdentifier(testprojectLabel));
            var expectedUseCaseOutput = new RunTestsUseCaseOutput(expectedTestSuitesExecutionResults,
                expectedTestCasesIdenfierWithAssociatedProjects);
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var loggerFactory = new FakeLoggerFactory();

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentCursorLocationLineContent())
                .Returns("{}");
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentContent())
                .Returns(documentContent);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(testprojectLabel);

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() => projectOutputLauncherStub.LaunchProjectWithArguments(testprojectLabel, testLauncherArgument))
                .Returns(testCaseExecutionResultsAsXml);

            RunTestsUseCaseOutput testCaseResults = null;
            var testRunnerViewerViewModel = A.Fake<ITestRunnerViewerViewModel>();
            A.CallTo(() =>
                testRunnerViewerViewModel.UpdateTestCaseExecutionResults(A<TestCaseExecutionResults>._,
                    A<TestCaseIdentifierWithAssociatedProjects>._)).Invokes(
                (TestCaseExecutionResults testCaseExecutionResults,
                        TestCaseIdentifierWithAssociatedProjects testCasesIdenfierWithAssociatedProjects) =>
                        testCaseResults = new RunTestsUseCaseOutput(testCaseExecutionResults,
                            testCasesIdenfierWithAssociatedProjects));
            var runTestFromCodeEditorUseCaseController =
                new RunTestFromCodeEditorUseCaseController(testRunnerViewerViewModel,
                    currentDocumentInformationRetriever, projectOutputLauncherStub,
                    errorMessagesQueue, loggerFactory);
            runTestFromCodeEditorUseCaseController.LaunchTestAtCursorPosition();

            Assert.IsTrue(expectedUseCaseOutput.Equals(testCaseResults));
        }

        private static TestCaseExecutionResults BuildExpectedTestSuitesExecutionResults(string testClassLabel,
            string testCase1Label)
        {
            var expectedTestSuitesExecutionResults = new TestCaseExecutionResults("Tests Results");
            var testSuiteExecutionResults = new TestCaseExecutionResults(testClassLabel);
            var testCaseExecutionResults = new TestCaseExecutionResults(testCase1Label)
            {
                TestCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull
            };
            testSuiteExecutionResults.AddChildren(testCaseExecutionResults);
            expectedTestSuitesExecutionResults.AddChildren(testSuiteExecutionResults);
            return expectedTestSuitesExecutionResults;
        }

        private static TestCaseExecutionResults BuildExpectedTestSuitesExecutionResults(string testClassLabel,
            string testCase1Label, string testCase2Label)
        {
            var expectedTestSuitesExecutionResults = new TestCaseExecutionResults("Tests Results");
            var testSuiteExecutionResults = new TestCaseExecutionResults(testClassLabel);
            var testCase1ExecutionResults = new TestCaseExecutionResults(testCase1Label)
            {
                TestCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull
            };
            var testCase2ExecutionResults = new TestCaseExecutionResults(testCase2Label)
            {
                TestCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull
            };
            testSuiteExecutionResults.AddChildren(testCase1ExecutionResults);
            testSuiteExecutionResults.AddChildren(testCase2ExecutionResults);
            expectedTestSuitesExecutionResults.AddChildren(testSuiteExecutionResults);
            return expectedTestSuitesExecutionResults;
        }
    }
}