﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.Core.Entities;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.UnitTests.Core.UseCases.Common;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CepepitoCore.UnitTests.Core.UseCases.RunTestsFromTestRunner
{
    [TestClass]
    public class RunTestsFromTestRunnerUseCaseControllerTest
    {
        [TestMethod]
        public void
            When_TestExecutionResultSelectedNodeCorrespondsToTestCaseAndAssociatedProjectFileIsInRepository_Then_TestsShouldBeLaunchedOnSelectedCase()
        {
            // Arrange
            var projectIdentifier = new ProjectIdentifier("project1");
            var testCaseIdentifier = new TestCaseIdentifier("testSuite.testCase");
            var testLauncherArgument =
                "*" + testCaseIdentifier.TestSuite + "." + testCaseIdentifier.TestCase + "*";
            var testCaseExecutionResultsAsXml =
                new FakeTestCaseXmlExecutionResultBuilder().BuildFakeTestCaseExecutionResultAsXml(
                    testCaseIdentifier.TestSuite, testCaseIdentifier.TestCase);

            var initialTestCaseResults = new TestSuitesResultsBuilder()
                .WithTestSuite(
                    new TestSuiteResultBuilder(testCaseIdentifier.TestSuite).WithTestCase(
                        new TestCaseResultsBuilder(testCaseIdentifier.TestCase,
                            TestCaseExecutionStatus.Failed, true))).Build();

            var initialTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            initialTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(testCaseIdentifier,
                projectIdentifier);

            var expectedTestCaseResults = new TestSuitesResultsBuilder()
                .WithTestSuite(
                    new TestSuiteResultBuilder(testCaseIdentifier.TestSuite).WithTestCase(
                        new TestCaseResultsBuilder(testCaseIdentifier.TestCase,
                            TestCaseExecutionStatus.Successfull, true))).Build();

            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(testCaseIdentifier,
                projectIdentifier);

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() =>
                    projectOutputLauncherStub.LaunchProjectWithArguments(projectIdentifier.Identifier,
                        testLauncherArgument))
                .Returns(testCaseExecutionResultsAsXml);

            var errorMessagesQueue = new FakeErrorsMessagesQueue();

            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel(initialTestCaseResults,
                initialTestCasesIdenfierWithAssociatedProjects);

            // Act
            new RunTestsFromTestRunnerUseCaseController(testRunnerViewerViewModel, errorMessagesQueue,
                projectOutputLauncherStub, new FakeLoggerFactory()).Execute();

            // Assert
            Assert.AreEqual(expectedTestCaseResults, testRunnerViewerViewModel.TestCaseExecutionResults);
            Assert.AreEqual(expectedTestCasesIdenfierWithAssociatedProjects,
                testRunnerViewerViewModel.TestCaseIdentifierWithAssociatedProjects);
            Assert.IsTrue(errorMessagesQueue.IsEmpty);
        }

        [TestMethod]
        public void
            When_TestExecutionResultSelectedNodeCorrespondsToTestCaseButAssociatedProjectFileIsNotInRepository_Then_NothingHappens()
        {
            // Arrange
            var testCaseIdentifier = new TestCaseIdentifier("testSuite.testCase");

            var initialTestCaseResults = new TestSuitesResultsBuilder()
                .WithTestSuite(
                    new TestSuiteResultBuilder(testCaseIdentifier.TestSuite).WithTestCase(
                        new TestCaseResultsBuilder(testCaseIdentifier.TestCase, isSelected: true))).Build();

            var initialTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();

            var expectedTestCaseResults = new TestSuitesResultsBuilder()
                .WithTestSuite(
                    new TestSuiteResultBuilder(testCaseIdentifier.TestSuite).WithTestCase(
                        new TestCaseResultsBuilder(testCaseIdentifier.TestCase, isSelected: true))).Build();

            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            var errorMessagesQueue = new FakeErrorsMessagesQueue();

            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel(initialTestCaseResults,
                initialTestCasesIdenfierWithAssociatedProjects);

            // Act
            new RunTestsFromTestRunnerUseCaseController(testRunnerViewerViewModel, errorMessagesQueue,
                projectOutputLauncherStub, new FakeLoggerFactory()).Execute();

            // Assert
            Assert.AreEqual(expectedTestCaseResults, testRunnerViewerViewModel.TestCaseExecutionResults);
            Assert.AreEqual(expectedTestCasesIdenfierWithAssociatedProjects,
                testRunnerViewerViewModel.TestCaseIdentifierWithAssociatedProjects);
            Assert.IsTrue(ErrorMessagesQueueContainOneErrorMessageContaining(errorMessagesQueue,
                new List<string> {"cannot", "find", "project"}));
        }

        [TestMethod]
        public void
            When_TestExecutionResultSelectedNodeCorrespondsToTestCasesNodeBelongingToSameProjectAndAssociatedProjectFileIsInRepository_Then_TestsShouldBeLaunchedOnSelectedCases()
        {
            // Arrange
            var projectIdentifier = new ProjectIdentifier("project1");
            var testCase1Identifier = new TestCaseIdentifier("testSuite.testCase1");
            var testCase2Identifier = new TestCaseIdentifier("testSuite.testCase2");
            var testLauncherArgument = "*" + testCase1Identifier.TestSuite + "." +
                                       testCase1Identifier.TestCase + "*" + ":" +
                                       "*" + testCase2Identifier.TestSuite + "." + testCase2Identifier.TestCase + "*";
            var testCaseExecutionResultsAsXml =
                new FakeTestCaseXmlExecutionResultBuilder().BuildFakeTestCaseExecutionResultAsXml(
                    testCase1Identifier.TestSuite, testCase1Identifier.TestCase, testCase2Identifier.TestCase);

            var initialTestCaseResults = new TestSuitesResultsBuilder()
                .WithTestSuite(
                    new TestSuiteResultBuilder(testCase1Identifier.TestSuite, true)
                        .WithTestCase(new TestCaseResultsBuilder(testCase1Identifier.TestCase,
                            TestCaseExecutionStatus.Failed))
                        .WithTestCase(new TestCaseResultsBuilder(testCase2Identifier.TestCase,
                            TestCaseExecutionStatus.Successfull))).Build();

            var initialTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            initialTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase1Identifier,
                projectIdentifier);
            initialTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase2Identifier,
                projectIdentifier);

            var expectedTestCaseResults = new TestSuitesResultsBuilder()
                .WithTestSuite(
                    new TestSuiteResultBuilder(testCase1Identifier.TestSuite, true)
                        .WithTestCase(new TestCaseResultsBuilder(testCase1Identifier.TestCase,
                            TestCaseExecutionStatus.Successfull))
                        .WithTestCase(new TestCaseResultsBuilder(testCase2Identifier.TestCase,
                            TestCaseExecutionStatus.Successfull))).Build();

            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase1Identifier,
                projectIdentifier);
            expectedTestCasesIdenfierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase2Identifier,
                projectIdentifier);

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() =>
                    projectOutputLauncherStub.LaunchProjectWithArguments(projectIdentifier.Identifier,
                        testLauncherArgument))
                .Returns(testCaseExecutionResultsAsXml);

            var errorMessagesQueue = new FakeErrorsMessagesQueue();

            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel(initialTestCaseResults,
                initialTestCasesIdenfierWithAssociatedProjects);

            // Act
            new RunTestsFromTestRunnerUseCaseController(testRunnerViewerViewModel, errorMessagesQueue,
                projectOutputLauncherStub, new FakeLoggerFactory()).Execute();

            // Assert
            Assert.AreEqual(expectedTestCaseResults, testRunnerViewerViewModel.TestCaseExecutionResults);
            Assert.AreEqual(expectedTestCasesIdenfierWithAssociatedProjects,
                testRunnerViewerViewModel.TestCaseIdentifierWithAssociatedProjects);
            Assert.IsTrue(errorMessagesQueue.IsEmpty);
        }

        [TestMethod]
        public void
            When_TestExecutionResultSelectedNodeCorrespondsToTestCasesNodeBelongingToDifferentProjects_Then_NoTestsAreRunAndErrorMessageIsReturned()
        {
            // Arrange
            var testCase1Identifier = new TestCaseIdentifier("testSuite1.testCase1");
            var testCase2Identifier = new TestCaseIdentifier("testSuite1.testCase2");

            var initialTestCaseResults = new TestSuitesResultsBuilder().WithTestSuite(
                new TestSuiteResultBuilder(testCase1Identifier.TestSuite, true)
                    .WithTestCase(new TestCaseResultsBuilder(testCase1Identifier.TestCase))
                    .WithTestCase(new TestCaseResultsBuilder(testCase2Identifier.TestCase))).Build();

            var initialTestCasesIdentifierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            initialTestCasesIdentifierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase1Identifier,
                new ProjectIdentifier("project1"));
            initialTestCasesIdentifierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase2Identifier,
                new ProjectIdentifier("project2"));

            var expectedTestCaseResults = new TestSuitesResultsBuilder().WithTestSuite(
                new TestSuiteResultBuilder(testCase1Identifier.TestSuite, true)
                    .WithTestCase(new TestCaseResultsBuilder(testCase1Identifier.TestCase))
                    .WithTestCase(new TestCaseResultsBuilder(testCase2Identifier.TestCase))).Build();

            var expectedTestCasesIdentifierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();
            expectedTestCasesIdentifierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase1Identifier,
                new ProjectIdentifier("project1"));
            expectedTestCasesIdentifierWithAssociatedProjects.AddTestCaseAssociatedProject(testCase2Identifier,
                new ProjectIdentifier("project2"));

            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();

            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel(initialTestCaseResults,
                initialTestCasesIdentifierWithAssociatedProjects);

            // Act
            new RunTestsFromTestRunnerUseCaseController(testRunnerViewerViewModel, errorMessagesQueue,
                projectOutputLauncherStub, new FakeLoggerFactory()).Execute();

            // Assert
            Assert.AreEqual(expectedTestCaseResults, testRunnerViewerViewModel.TestCaseExecutionResults);
            Assert.AreEqual(expectedTestCasesIdentifierWithAssociatedProjects,
                testRunnerViewerViewModel.TestCaseIdentifierWithAssociatedProjects);
            Assert.IsTrue(ErrorMessagesQueueContainOneErrorMessageContaining(errorMessagesQueue,
                new List<string> {"different", "projects"}));
        }

        [TestMethod]
        public void When_TestExecutionResultSelectedNodeDoesntCorrespondsToTestCase_Then_NothingHappens()
        {
            // Arrange
            var initialTestCaseResults = new FakeTestCaseExecutionResultsBuilder().BuildEmptyTestCaseExecutionResults();

            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel(initialTestCaseResults,
                new TestCaseIdentifierWithAssociatedProjects());
            var errorMessagesQueue = new FakeErrorsMessagesQueue();

            // Act
            new RunTestsFromTestRunnerUseCaseController(testRunnerViewerViewModel, errorMessagesQueue,
                projectOutputLauncherStub, new FakeLoggerFactory()).Execute();

            // Assert
            Assert.AreEqual(initialTestCaseResults, testRunnerViewerViewModel.TestCaseExecutionResults);
            Assert.AreEqual(new TestCaseIdentifierWithAssociatedProjects(),
                testRunnerViewerViewModel.TestCaseIdentifierWithAssociatedProjects);
            Assert.IsTrue(errorMessagesQueue.IsEmpty);
        }

        private bool ErrorMessagesQueueContainOneErrorMessageContaining(FakeErrorsMessagesQueue fakeErrorsMessagesQueue,
            List<string> wordsToMatch)
        {
            if (fakeErrorsMessagesQueue.ErrorMessages.Count() != 1)
                return false;

            foreach (var errorMessage in fakeErrorsMessagesQueue.ErrorMessages)
            foreach (var wordToMatch in wordsToMatch)
                if (!errorMessage.ToLower().Contains(wordToMatch.ToLower()))
                    return false;


            return true;
        }
    }
}