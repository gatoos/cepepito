﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.Core.Entities;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromSolutionExplorer;
using CepepitoCore.UnitTests.Core.UseCases.Common;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CepepitoCore.UnitTests.Core.UseCases.RunTestsFromSolutionExplorer
{
    [TestClass]
    public class RunTestsFromSolutionExplorerUseCaseControllerTest
    {
        [TestMethod]
        public void When_SelectedProjectContainsTests_Then_TestRunnerViewModelShouldBeUpdatedWithTestResults()
        {
            var testProjectIdentifier = new ProjectIdentifier("TestProject");
            var testCase1Identifier = new TestCaseIdentifier("TestClass1", "TestCase1");
            var testCase2Identifier = new TestCaseIdentifier("TestClass1", "TestCase2");
            var testCase3Identifier = new TestCaseIdentifier("TestClass2", "TestCase1");

            var testSuiteDescriptor =
                new TestSuiteDescriptor(new[] {testCase1Identifier, testCase2Identifier, testCase3Identifier});
            var testCaseExecutionResultsAsXml = testSuiteDescriptor.AsXmlTestExecutionResults();
            var expectedTestResults = testSuiteDescriptor.AsTestExecutionResults();

            var document1Content = testCase1Identifier.EmptyTestDeclaration +
                                   testCase2Identifier.EmptyTestDeclaration;

            var document2Content = testCase3Identifier.EmptyTestDeclaration;

            var testLauncherArgument = "*" + testCase1Identifier.Descriptor + "*" + ":" +
                                       "*" + testCase2Identifier.Descriptor + "*" + ":" +
                                       "*" + testCase3Identifier.Descriptor + "*" ;

            var expectedTestCasesIdenfierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects(
                new Dictionary<TestCaseIdentifier, ProjectIdentifier>
                {
                    {testCase1Identifier, testProjectIdentifier},
                    {testCase2Identifier, testProjectIdentifier},
                    {testCase3Identifier, testProjectIdentifier}
                });

            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel();
            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();
            A.CallTo(() =>
                    projectOutputLauncherStub.LaunchProjectWithArguments(testProjectIdentifier.Identifier,
                        testLauncherArgument))
                .Returns(testCaseExecutionResultsAsXml);

            var selectedProjectRetrieverStub = A.Fake<ISelectedProjectsRetriever>();
            A.CallTo(() => selectedProjectRetrieverStub.GetSelectedProjects()).Returns(new List<ProjectWithContent>
            {
                new ProjectWithContent(testProjectIdentifier, new List<IEnumerable<string>>
                {
                    new List<string> {document1Content},
                    new List<string> {document2Content}
                })
            });

            new RunTestsFromSolutionExplorerUseCaseController(testRunnerViewerViewModel, projectOutputLauncherStub,
                selectedProjectRetrieverStub, errorMessagesQueue, new FakeLoggerFactory()).Execute();

            Assert.AreEqual(expectedTestResults, testRunnerViewerViewModel.TestCaseExecutionResults);
            Assert.AreEqual(expectedTestCasesIdenfierWithAssociatedProjects,
                testRunnerViewerViewModel.TestCaseIdentifierWithAssociatedProjects);
            Assert.IsTrue(errorMessagesQueue.IsEmpty);
        }

        [TestMethod]
        public void When_NoProjectsAreSelected_Then_AnErrorMessageQueueShouldContainError()
        {
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel();
            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();

            var selectedProjectRetrieverStub = A.Fake<ISelectedProjectsRetriever>();
            A.CallTo(() => selectedProjectRetrieverStub.GetSelectedProjects()).Returns(new List<ProjectWithContent>());

            new RunTestsFromSolutionExplorerUseCaseController(testRunnerViewerViewModel, projectOutputLauncherStub,
                selectedProjectRetrieverStub, errorMessagesQueue, new FakeLoggerFactory()).Execute();

            Assert.IsTrue(ErrorMessagesQueueContainOneErrorMessageContaining(errorMessagesQueue,
                new List<string> {"no", "project", "selected"}));
        }

        [TestMethod]
        public void When_SeveralProjectsAreSelected_Then_AnErrorMessageQueueShouldContainError()
        {
            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel();
            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();

            var selectedProjectRetrieverStub = A.Fake<ISelectedProjectsRetriever>();
            A.CallTo(() => selectedProjectRetrieverStub.GetSelectedProjects()).Returns(new List<ProjectWithContent>
            {
                new ProjectWithContent(new ProjectIdentifier("testProject1"), new List<IEnumerable<string>>
                {
                    new List<string>()
                }),
                new ProjectWithContent(new ProjectIdentifier("testProject2"), new List<IEnumerable<string>>
                {
                    new List<string>()
                })
            });

            new RunTestsFromSolutionExplorerUseCaseController(testRunnerViewerViewModel, projectOutputLauncherStub,
                selectedProjectRetrieverStub, errorMessagesQueue, new FakeLoggerFactory()).Execute();

            Assert.IsTrue(ErrorMessagesQueueContainOneErrorMessageContaining(errorMessagesQueue,
                new List<string> {"Cannot", "launch", "tests"}));
        }

        [TestMethod]
        public void When_SelectedProjectDoesntContainTests_Then_NothingHappens()
        {
            var testProjectIdentifier = new ProjectIdentifier("TestProject");

            var errorMessagesQueue = new FakeErrorsMessagesQueue();
            var testRunnerViewerViewModel = new FakeTestRunnerViewerViewModel();
            var projectOutputLauncherStub = A.Fake<IProjectOutputLauncher>();

            var selectedProjectRetrieverStub = A.Fake<ISelectedProjectsRetriever>();
            A.CallTo(() => selectedProjectRetrieverStub.GetSelectedProjects()).Returns(new List<ProjectWithContent>
            {
                new ProjectWithContent(testProjectIdentifier, new List<IEnumerable<string>>
                {
                    new List<string>()
                })
            });

            new RunTestsFromSolutionExplorerUseCaseController(testRunnerViewerViewModel, projectOutputLauncherStub,
                selectedProjectRetrieverStub, errorMessagesQueue, new FakeLoggerFactory()).Execute();

            Assert.IsTrue(errorMessagesQueue.IsEmpty);
        }

        private bool ErrorMessagesQueueContainOneErrorMessageContaining(FakeErrorsMessagesQueue fakeErrorsMessagesQueue,
            List<string> wordsToMatch)
        {
            if (fakeErrorsMessagesQueue.ErrorMessages.Count() != 1)
                return false;

            foreach (var errorMessage in fakeErrorsMessagesQueue.ErrorMessages)
            foreach (var wordToMatch in wordsToMatch)
                if (!errorMessage.ToLower().Contains(wordToMatch.ToLower()))
                    return false;


            return true;
        }
    }
}