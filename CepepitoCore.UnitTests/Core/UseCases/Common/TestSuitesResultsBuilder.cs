﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    public class TestSuitesResultsBuilder
    {
        private readonly List<TestSuiteResultBuilder> _testSuiteResultBuilders;

        public TestSuitesResultsBuilder(bool isSelected = false)
        {
            Label = "Tests Results";
            IsSelected = isSelected;
            _testSuiteResultBuilders = new List<TestSuiteResultBuilder>();
        }

        public string Label { get; }
        public bool IsSelected { get; }

        public TestSuitesResultsBuilder WithTestSuite(TestSuiteResultBuilder testSuiteResultBuilder)
        {
            _testSuiteResultBuilders.Add(testSuiteResultBuilder);
            return this;
        }

        public TestCaseExecutionResults Build()
        {
            var testSuitesExecutionResults = new TestCaseExecutionResults(Label)
            {
                IsSelected = IsSelected
            };
            foreach (var testSuiteBuilder in _testSuiteResultBuilders)
                testSuitesExecutionResults.AddChildren(testSuiteBuilder.Build());

            return testSuitesExecutionResults;
        }
    }

    public class TestSuiteResultBuilder
    {
        private readonly List<TestCaseResultsBuilder> _testCaseResultBuilders;

        public TestSuiteResultBuilder(string label, bool isSelected = false)
        {
            Label = label;
            IsSelected = isSelected;
            _testCaseResultBuilders = new List<TestCaseResultsBuilder>();
        }

        public string Label { get; }
        public bool IsSelected { get; }

        public TestSuiteResultBuilder WithTestCase(TestCaseResultsBuilder testCaseResultsBuilder)
        {
            _testCaseResultBuilders.Add(testCaseResultsBuilder);
            return this;
        }

        public TestCaseExecutionResults Build()
        {
            var testSuitesExecutionResults = new TestCaseExecutionResults(Label)
            {
                IsSelected = IsSelected
            };
            foreach (var testCaseResultBuilder in _testCaseResultBuilders)
                testSuitesExecutionResults.AddChildren(testCaseResultBuilder.Build());

            return testSuitesExecutionResults;
        }
    }

    public class TestCaseResultsBuilder
    {
        public TestCaseResultsBuilder(string label,
            TestCaseExecutionStatus executionStatus = TestCaseExecutionStatus.Successfull, bool isSelected = false)
        {
            Label = label;
            ExecutionStatus = executionStatus;
            IsSelected = isSelected;
        }

        public string Label { get; }
        public TestCaseExecutionStatus ExecutionStatus { get; set; }
        public bool IsSelected { get; }

        public TestCaseExecutionResults Build()
        {
            var testCaseExecutionResult = new TestCaseExecutionResults(Label)
            {
                TestCaseExecutionStatusMember = ExecutionStatus,
                IsSelected = IsSelected
            };
            return testCaseExecutionResult;
        }
    }
}