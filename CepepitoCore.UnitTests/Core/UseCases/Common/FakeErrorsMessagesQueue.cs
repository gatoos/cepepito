﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.ServiceProviders.Interfaces.Common;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    public class FakeErrorsMessagesQueue : IErrorsMessagesQueue
    {
        private readonly List<string> _errorMessages;

        public FakeErrorsMessagesQueue()
        {
                    _errorMessages = new List<string>();
        }

        public IEnumerable<string> ErrorMessages => _errorMessages;

        public bool IsEmpty =>!_errorMessages.Any();

        public void AddErrorMessage(string message)
        {
            _errorMessages.Add(message);
        }

        protected bool Equals(FakeErrorsMessagesQueue other)
        {
            return Equals(_errorMessages, other._errorMessages);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FakeErrorsMessagesQueue) obj);
        }

        public override int GetHashCode()
        {
            return (_errorMessages != null ? _errorMessages.GetHashCode() : 0);
        }

        public static bool ErrorMessagesQueueContainOneErrorMessageContaining(FakeErrorsMessagesQueue fakeErrorsMessagesQueue,
            List<string> wordsToMatch)
        {
            if (fakeErrorsMessagesQueue.ErrorMessages.Count() != 1)
                return false;

            foreach (var errorMessage in fakeErrorsMessagesQueue.ErrorMessages)
            foreach (var wordToMatch in wordsToMatch)
                if (!errorMessage.ToLower().Contains(wordToMatch.ToLower()))
                    return false;


            return true;
        }
    }
}