﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;
using CepepitoCore.UI;
using Logging;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    public class FakeTestRunnerViewerViewModel : ITestRunnerViewerViewModel
    {
        public FakeTestRunnerViewerViewModel()
        {
            TestCaseExecutionResults = null;
            TestCaseIdentifierWithAssociatedProjects = null;
        }

        public FakeTestRunnerViewerViewModel(TestCaseExecutionResults testCaseResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects)
        {
            TestCaseExecutionResults = testCaseResults;
            TestCaseIdentifierWithAssociatedProjects = testCaseIdentifierWithAssociatedProjects;
        }

        public TestCaseExecutionResults TestCaseExecutionResults { get; private set; }
        public TestCaseIdentifierWithAssociatedProjects TestCaseIdentifierWithAssociatedProjects { get; private set; }

        public bool TestsAreRunning { get; set; }

        public void UpdateTestCaseExecutionResults(TestCaseExecutionResults testCaseExecutionResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects)
        {
            TestCaseExecutionResults = testCaseExecutionResults;
            TestCaseIdentifierWithAssociatedProjects = testCaseIdentifierWithAssociatedProjects;
        }
    }
}