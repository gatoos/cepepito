﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    public class TestSuiteDescriptor
    {
        private readonly ILookup<string, string> _byTestSuiteTestCases;

        public TestSuiteDescriptor(IEnumerable<TestCaseIdentifier> testCases)
        {
            _byTestSuiteTestCases = testCases.ToLookup(t => t.TestSuite, t => t.TestCase);
        }

        public XDocument AsXmlTestExecutionResults()
        {
            return new XDocument(
                new XDeclaration("1.0", "UTF-16", null),
                new XElement("testsuites",
                    from testSuiteEntry in _byTestSuiteTestCases
                    select new XElement("testsuite", new XAttribute("name", testSuiteEntry.Key),
                        from testCaseEntry in testSuiteEntry
                        select new XElement("testcase", new XAttribute("name", testCaseEntry)))
                )
            );
        }

        public TestCaseExecutionResults AsTestExecutionResults()
        {
            return new TestCaseExecutionResults("Tests Results", true,
                from testSuiteEntry in _byTestSuiteTestCases
                select new TestCaseExecutionResults(testSuiteEntry.Key, true,
                    from testCaseEntry in testSuiteEntry
                    select new TestCaseExecutionResults(testCaseEntry, true)
                )
            );
        }
    }
}