﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Xml.Linq;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    public class FakeTestCaseXmlExecutionResultBuilder
    {
        internal XDocument BuildFakeTestCaseExecutionResultAsXml(string testClassLabel, string testCaseLabel)
        {
            return new XDocument(
                new XDeclaration("1.0", "UTF-16", null),
                new XElement("testsuites",
                    new XElement("testsuite", new XAttribute("name", testClassLabel),
                        new XElement("testcase", new XAttribute("name", testCaseLabel)))));
        }

        internal XDocument BuildFakeTestCaseExecutionResultAsXml(string testClassLabel, string testCase1Label,
            string testCase2Label)
        {
            return new XDocument(
                new XDeclaration("1.0", "UTF-16", null),
                new XElement("testsuites",
                    new XElement("testsuite", new XAttribute("name", testClassLabel),
                        new XElement("testcase", new XAttribute("name", testCase1Label)),
                        new XElement("testcase", new XAttribute("name", testCase2Label))
                    )));
        }
    }
}