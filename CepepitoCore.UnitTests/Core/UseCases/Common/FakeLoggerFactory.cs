﻿using System;
using Logging;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    class FakeLoggerFactory : ILoggerFactory
    {
        public ILogger BuildPerAssemblyLogger(Type assemblyType)
        {
            return new FakeLogger();
        }
    }

    internal class FakeLogger : ILogger
    {
        public void Debug(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Info(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Warn(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Error(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Fatal(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Debug(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Info(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Warn(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Error(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public void Fatal(object message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
        {
        }

        public bool IsDebugEnabled { get; }
        public bool IsInfoEnabled { get; }
        public bool IsWarnEnabled { get; }
        public bool IsErrorEnabled { get; }
        public bool IsFatalEnabled { get; }
    }
}