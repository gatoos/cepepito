﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;

namespace CepepitoCore.UnitTests.Core.UseCases.Common
{
    public class FakeTestCaseExecutionResultsBuilder
    {
        internal TestCaseExecutionResults BuildEmptyTestCaseExecutionResults()
        {
            return new TestCaseExecutionResults("Tests Results");
        }

        internal TestCaseExecutionResults BuildTestCaseExecutionResultsWithNoNodeSelected(
            TestCaseIdentifier testCaseIdentifier)
        {
            var testSuitesExecutionResults = new TestCaseExecutionResults("Tests Results");
            var testSuiteExecutionResults = new TestCaseExecutionResults(testCaseIdentifier.TestSuite);
            var testCaseExecutionResults = new TestCaseExecutionResults(testCaseIdentifier.TestCase);
            testSuiteExecutionResults.AddChildren(testCaseExecutionResults);
            testSuitesExecutionResults.AddChildren(testSuiteExecutionResults);
            return testSuitesExecutionResults;
        }

        internal TestCaseExecutionResults BuildTestCaseExecutionResultsWithNodeSelected(
            TestCaseIdentifier testCaseIdentifier)
        {
            var testSuitesExecutionResults = new TestCaseExecutionResults("Tests Results");

            var testSuiteExecutionResults = new TestCaseExecutionResults(testCaseIdentifier.TestSuite)
            {
                IsSelected = true
            };

            var testCaseExecutionResults = new TestCaseExecutionResults(testCaseIdentifier.TestCase);
            testSuiteExecutionResults.AddChildren(testCaseExecutionResults);
            testSuitesExecutionResults.AddChildren(testSuiteExecutionResults);
            return testSuitesExecutionResults;
        }

        internal TestCaseExecutionResults BuildTestCaseExecutionResultsWithLeafSelected(
            TestCaseIdentifier testCaseIdentifier)
        {
            var testSuitesExecutionResults = new TestCaseExecutionResults("Tests Results");
            var testSuiteExecutionResults = new TestCaseExecutionResults(testCaseIdentifier.TestSuite);

            var testCaseExecutionResults = new TestCaseExecutionResults(testCaseIdentifier.TestCase)
            {
                IsSelected = true
            };

            testSuiteExecutionResults.AddChildren(testCaseExecutionResults);
            testSuitesExecutionResults.AddChildren(testSuiteExecutionResults);
            return testSuitesExecutionResults;
        }
    }
}