﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CepepitoCore.UnitTests.Core.UseCases.GenerateCodeInFile
{
    [TestClass]
    public class GenerateTestForClassUseCaseControllerTest
    {
        [TestMethod]
        public void
            When_ProjectCorrectlySelectedAndParserCorrectlyGeneratesResults_Then_ANewFileShouldBeCreatedInTargetProjectWithRightNameAndContent()
        {
            var expectedTargetProjectIdentifier = new ProjectIdentifier("targetProject");
            var sourceProjectIdentifier = "sourceProject";
            var documentName = "TestClass.cpp";
            var solutionUnsavedDocuments = Enumerable.Empty<(string, string)>();
            var includeDirectories = Enumerable.Empty<string>();
            var cursorPosition = new CursorPosition();
            var parserResults = (typename : "TestClassTest", parsedContent : "TEST(TestClassTest, Add){}", errorMessage: "");
            var generatedFileExtension = "cpp";
            var targetFileDirectory = "c:\\";
            var expectedDocumentName = Path.Combine(targetFileDirectory, parserResults.typename + "." + generatedFileExtension);

            var projectSelector = A.Fake<IProjectSelector>();
            A.CallTo(() => projectSelector.SelectProject()).Returns((expectedTargetProjectIdentifier, Path.GetDirectoryName(expectedDocumentName)));

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.DocumentName).Returns(documentName);
            A.CallTo(() => currentDocumentInformationRetriever.CursorPosition).Returns(cursorPosition);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(sourceProjectIdentifier);

            var solution = A.Fake<ISolution>();
            A.CallTo(() => solution.UnsavedDocuments).Returns(solutionUnsavedDocuments);
            A.CallTo(() => solution.GetProjectIncludeDirectories(new ProjectIdentifier(sourceProjectIdentifier)))
                .Returns(includeDirectories);
            A.CallTo(() => solution.FileExistsInProject(documentName, expectedTargetProjectIdentifier)).Returns(false);

            (string, string, string) Parser(ParserInput parserInput)
            {
                return parserInput.Equals(new ParserInput(parserInput.DocumentName, parserInput.UnsavedDocuments,
                    parserInput.SourceProjectIncludeDirectories, parserInput.TargetProjectIncludeDirectories, parserInput.CursorPosition))
                    ? parserResults
                    : (typename: null, parsedContent: null, errorMessage: null);
            }

            var errorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            var executionTask = new GenerateCodeInFileUseCaseController(projectSelector, currentDocumentInformationRetriever, solution, includeDirectoriesParam => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                Parser, errorMessageQueue, generatedFileExtension, A.Fake<IFileOverwriteConfirmationQuery>(), new FakeProgressDialogBox()).Execute();

            executionTask.Wait(2000);

            A.CallTo(() => solution.AddAndOpenNewDocumentInProject(expectedTargetProjectIdentifier,
                expectedDocumentName,
                A<string>.That.Contains(parserResults.parsedContent))).MustHaveHappened();
        }

        [TestMethod]
        public void When_NoProjectIsSelected_Then_NothingShouldWrongHappen()
        {
            var projectSelector = A.Fake<IProjectSelector>();
            A.CallTo(() => projectSelector.SelectProject()).Returns((null, null));

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            var solution = A.Fake<ISolution>();

            (string, string, string) Parser(ParserInput parserInput)
            {
                return (null, null, null);
            }

            var errorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            ;
            var executionTask = new GenerateCodeInFileUseCaseController(projectSelector, currentDocumentInformationRetriever, solution, 
                includeDirectoriesParam => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                Parser, errorMessageQueue, "", A.Fake<IFileOverwriteConfirmationQuery>(), A.Fake<IProgressDialogBox>()).Execute();

            executionTask.Wait(2000);
        }

        [TestMethod]
        public void When_ParserDidntGenerateTest_Then_AnErrorShouldSentToErrorQueue()
        {
            var expectedTargetProjectIdentifier = new ProjectIdentifier("targetProject");
            var sourceProjectIdentifier = "sourceProject";
            var documentName = "TestClass.cpp";
            var solutionUnsavedDocuments = Enumerable.Empty<(string, string)>();
            var includeDirectories = Enumerable.Empty<string>();
            var cursorPosition = new CursorPosition();
            var parseErrorMessage = "Cannot find type definition at cursor location";
            var parserResults = (typename: (string) null, parsedContent: (string) null, errorMessage: parseErrorMessage);

            var projectSelector = A.Fake<IProjectSelector>();
            A.CallTo(() => projectSelector.SelectProject()).Returns((expectedTargetProjectIdentifier, "c:"));

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.DocumentName).Returns(documentName);
            A.CallTo(() => currentDocumentInformationRetriever.CursorPosition).Returns(cursorPosition);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(sourceProjectIdentifier);

            var solution = A.Fake<ISolution>();
            A.CallTo(() => solution.UnsavedDocuments).Returns(solutionUnsavedDocuments);
            A.CallTo(() => solution.GetProjectIncludeDirectories(new ProjectIdentifier(sourceProjectIdentifier)))
                .Returns(includeDirectories);

            (string, string, string) Parser(ParserInput parserInput)
            {
                return parserInput.Equals(new ParserInput(parserInput.DocumentName, parserInput.UnsavedDocuments,
                    parserInput.SourceProjectIncludeDirectories, parserInput.TargetProjectIncludeDirectories, parserInput.CursorPosition))
                    ? parserResults
                    : (typename: null, parsedContent: null, errorMessage: null);
            }

            var errorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            var fakeProgressDialogBox = new FakeProgressDialogBox();
            
            var executionTask = new GenerateCodeInFileUseCaseController(projectSelector, currentDocumentInformationRetriever, solution,
                includeDirectoriesParam => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                Parser, errorMessageQueue, "", A.Fake<IFileOverwriteConfirmationQuery>(), fakeProgressDialogBox).Execute();

            executionTask.Wait(2000);

            A.CallTo(() => errorMessageQueue.AddErrorMessage(parseErrorMessage))
                .MustHaveHappened();
        }

        [TestMethod]
        public void
            When_ProjectCorrectlySelectedAndParserCorrectlyGeneratesResultsButFileAlreadyExist_Then_TheGeneratedFileShouldBeAddedWhenFileOverwriteConfirmationQueryReturnsTrue()
        {
            var expectedTargetProjectIdentifier = new ProjectIdentifier("targetProject");
            var sourceProjectIdentifier = "sourceProject";
            var documentName = "TestClass.cpp";
            var solutionUnsavedDocuments = Enumerable.Empty<(string, string)>();
            var includeDirectories = Enumerable.Empty<string>();
            var cursorPosition = new CursorPosition();
            var parserResults = (typename: "TestClassTest", parsedContent: "TEST(TestClassTest, Add){}", errorMessage: "");
            var generatedFileExtension = "cpp";
            var expectedTargetDocumentDirectory = "c:\\";
            var expectedDocumentName = Path.Combine(expectedTargetDocumentDirectory, parserResults.typename + "." + generatedFileExtension);

            var projectSelector = A.Fake<IProjectSelector>();
            A.CallTo(() => projectSelector.SelectProject()).Returns((expectedTargetProjectIdentifier, expectedTargetDocumentDirectory));

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.DocumentName).Returns(documentName);
            A.CallTo(() => currentDocumentInformationRetriever.CursorPosition).Returns(cursorPosition);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(sourceProjectIdentifier);

            var solution = A.Fake<ISolution>();
            A.CallTo(() => solution.UnsavedDocuments).Returns(solutionUnsavedDocuments);
            A.CallTo(() => solution.GetProjectIncludeDirectories(new ProjectIdentifier(sourceProjectIdentifier)))
                .Returns(includeDirectories);
            A.CallTo(() => solution.FileExistsInProject(expectedDocumentName, expectedTargetProjectIdentifier)).Returns(true);
            
            (string, string, string) Parser(ParserInput parserInput)
            {
                return parserInput.Equals(new ParserInput(parserInput.DocumentName, parserInput.UnsavedDocuments,
                    parserInput.SourceProjectIncludeDirectories, parserInput.TargetProjectIncludeDirectories, parserInput.CursorPosition))
                    ? parserResults
                    : (typename: null, parsedContent: null, errorMessage: null);
            }

            var errorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            var fileOverwriteConfirmationQuery = A.Fake<IFileOverwriteConfirmationQuery>();
            A.CallTo(() => fileOverwriteConfirmationQuery.ShouldOverwriteFile(expectedDocumentName)).Returns(true);

            var executionTask = new GenerateCodeInFileUseCaseController(projectSelector, currentDocumentInformationRetriever, solution,
                includeDirectoriesParam => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                Parser, errorMessageQueue, generatedFileExtension, fileOverwriteConfirmationQuery, new FakeProgressDialogBox()).Execute();

            executionTask.Wait(2000);

            A.CallTo(() => solution.AddAndOpenNewDocumentInProject(expectedTargetProjectIdentifier,
                expectedDocumentName,
                A<string>.That.Contains(parserResults.parsedContent))).MustHaveHappened();
        }

        [TestMethod]
        public void
            When_ProjectCorrectlySelectedAndParserCorrectlyGeneratesResultsButFileAlreadyExist_Then_TheGeneratedFileShouldNotBeAddedWhenFileOverwriteConfirmationQueryReturnsTrue()
        {
            var expectedTargetProjectIdentifier = new ProjectIdentifier("targetProject");
            var sourceProjectIdentifier = "sourceProject";
            var documentName = "TestClass.cpp";
            var solutionUnsavedDocuments = Enumerable.Empty<(string, string)>();
            var includeDirectories = Enumerable.Empty<string>();
            var cursorPosition = new CursorPosition();
            var parserResults = (typename: "TestClassTest", parsedContent: "TEST(TestClassTest, Add){}", errorMessage: "");
            var generatedFileExtension = "cpp";
            var expectedDocumentName = parserResults.typename + "." + generatedFileExtension;
            
            var projectSelector = A.Fake<IProjectSelector>();
            A.CallTo(() => projectSelector.SelectProject()).Returns((expectedTargetProjectIdentifier, Path.GetDirectoryName(expectedDocumentName)));

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.DocumentName).Returns(documentName);
            A.CallTo(() => currentDocumentInformationRetriever.CursorPosition).Returns(cursorPosition);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(sourceProjectIdentifier);

            var solution = A.Fake<ISolution>();
            A.CallTo(() => solution.UnsavedDocuments).Returns(solutionUnsavedDocuments);
            A.CallTo(() => solution.GetProjectIncludeDirectories(new ProjectIdentifier(sourceProjectIdentifier)))
                .Returns(includeDirectories);
            A.CallTo(() => solution.FileExistsInProject(expectedDocumentName, expectedTargetProjectIdentifier)).Returns(true);

            (string, string, string) Parser(ParserInput parserInput)
            {
                return parserInput.Equals(new ParserInput(parserInput.DocumentName, parserInput.UnsavedDocuments,
                    parserInput.SourceProjectIncludeDirectories, parserInput.TargetProjectIncludeDirectories, parserInput.CursorPosition))
                    ? parserResults
                    : (typename: null, parsedContent: null, errorMessage: null);
            }

            var errorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            var fileOverwriteConfirmationQuery = A.Fake<IFileOverwriteConfirmationQuery>();
            A.CallTo(() => fileOverwriteConfirmationQuery.ShouldOverwriteFile(expectedDocumentName)).Returns(false);

            var executionTask = new GenerateCodeInFileUseCaseController(projectSelector, currentDocumentInformationRetriever, solution,
                includeDirectoriesParam => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                Parser, errorMessageQueue, generatedFileExtension, fileOverwriteConfirmationQuery, new FakeProgressDialogBox()).Execute();

            executionTask.Wait(2000);

            A.CallTo(() => solution.AddAndOpenNewDocumentInProject(expectedTargetProjectIdentifier,
                expectedDocumentName,
                A<string>.That.Contains(parserResults.parsedContent))).MustNotHaveHappened();
        }

        [TestMethod]
        public void
            When_ProjectCorrectlySelectedAndParserCorrectlyGeneratesResultsButTaskCancelled_Then_NothingShouldHappen()
        {
            var expectedTargetProjectIdentifier = new ProjectIdentifier("targetProject");
            var sourceProjectIdentifier = "sourceProject";
            var documentName = "TestClass.cpp";
            var solutionUnsavedDocuments = Enumerable.Empty<(string, string)>();
            var includeDirectories = Enumerable.Empty<string>();
            var cursorPosition = new CursorPosition();
            var parserResults = (typename: "TestClassTest", parsedContent: "TEST(TestClassTest, Add){}", errorMessage: "");
            var generatedFileExtension = "cpp";

            var projectSelector = A.Fake<IProjectSelector>();
            A.CallTo(() => projectSelector.SelectProject()).Returns((expectedTargetProjectIdentifier, null));

            var currentDocumentInformationRetriever = A.Fake<ICurrentDocumentInfoRetriever>();
            A.CallTo(() => currentDocumentInformationRetriever.DocumentName).Returns(documentName);
            A.CallTo(() => currentDocumentInformationRetriever.CursorPosition).Returns(cursorPosition);
            A.CallTo(() => currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName())
                .Returns(sourceProjectIdentifier);

            var solution = A.Fake<ISolution>();
            A.CallTo(() => solution.UnsavedDocuments).Returns(solutionUnsavedDocuments);
            A.CallTo(() => solution.GetProjectIncludeDirectories(new ProjectIdentifier(sourceProjectIdentifier)))
                .Returns(includeDirectories);
            A.CallTo(() => solution.FileExistsInProject(documentName, expectedTargetProjectIdentifier)).Returns(false);

            (string, string, string) Parser(ParserInput parserInput)
            {
                return parserResults;
            }

            var errorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            var executionTask = new GenerateCodeInFileUseCaseController(projectSelector, currentDocumentInformationRetriever, solution,
                includeDirectoriesParam => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                Parser, errorMessageQueue, generatedFileExtension, A.Fake<IFileOverwriteConfirmationQuery>(), new FakeCancelledProgressDialogBox()).Execute();

            executionTask.Wait(2000);

            A.CallTo(() => solution.AddAndOpenNewDocumentInProject(A<ProjectIdentifier>._, A<string>._, A<string>._)).MustNotHaveHappened();
            A.CallTo(() => errorMessageQueue.AddErrorMessage(A<string>._)).MustNotHaveHappened();
        }
    }

    class FakeProgressDialogBox : IProgressDialogBox
    {
        public void Show<T>(Task<T> workerTask, CancellationTokenSource cancellationTokenSource)
        {
        }
    }

    class FakeCancelledProgressDialogBox : IProgressDialogBox
    {
        public void Show<T>(Task<T> workerTask, CancellationTokenSource cancellationTokenSource)
        {
            cancellationTokenSource.Cancel();
        }
    }
}