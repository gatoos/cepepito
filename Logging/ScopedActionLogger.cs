﻿using System;

namespace Logging
{
    public class ScopedActionLogger : IDisposable
    {
        private readonly ILogger _logger;
        private readonly string _actionName;

        public ScopedActionLogger(ILogger logger, string actionName)
        {
            _logger = logger;
            _actionName = actionName;
            _logger.Info(new {ActionStart=_actionName});
        }


        public void Dispose()
        {
            _logger.Info(new {ActionEnd=_actionName});
        }
    }
}