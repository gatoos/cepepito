﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.IO;

namespace Logging
{
    internal class AssemblyDirectoryRetriever
    {
        public string RetrieveAssemblyDirectory(Type assemblyType)
        {
            var codebase = assemblyType.Assembly.CodeBase;
            var uri = new Uri(codebase, UriKind.Absolute);
            return Path.GetDirectoryName(uri.LocalPath)?.ToLower();
        }
    }
}
