﻿using System;

namespace Logging
{
    public class LoggerFactory : ILoggerFactory
    {
        /// <summary>
        /// Creates a logger that logs content in cepepito.log located in the assembly folder of the type passed as a parameter
        /// Reads the configuration from log4net.config file located in the assembly folder of the type passed as a parameter
        /// The logger name is the type passed as a parameter
        /// </summary>
        public ILogger BuildPerAssemblyLogger(Type assemblyType)
        {
            return new PerAssemblyLogger(assemblyType);
        }
    }
}