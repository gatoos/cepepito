﻿using System;

namespace Logging
{
    public class TestCaseExecutionException : Exception
    {
        public TestCaseExecutionException(string message) : base(message)
        {
        }
    }
}