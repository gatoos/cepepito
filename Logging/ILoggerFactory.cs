﻿using System;

namespace Logging
{
    public interface ILoggerFactory
    {
        ILogger BuildPerAssemblyLogger(Type assemblyType);
    }
}