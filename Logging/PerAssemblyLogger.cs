﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.ComponentModel;
using System.IO;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Logging
{
    internal class SerializableExpandableContractResolver : DefaultContractResolver
    {
        protected override JsonContract CreateContract(Type objectType)
        {
            if (TypeDescriptor.GetAttributes(objectType).Contains(new TypeConverterAttribute(typeof(ExpandableObjectConverter))))
            {
                return CreateObjectContract(objectType);
            }
            return base.CreateContract(objectType);
        }
    }

    internal class PerAssemblyLogger : ILogger
    {
        private readonly Lazy<ILog> _logger = new Lazy<ILog>(() =>
        {
            if (!LogManager.GetRepository().Configured)
            {
                var assemblyDirectory = new AssemblyDirectoryRetriever().RetrieveAssemblyDirectory(_assemblyType);
                GlobalContext.Properties["LogFileName"] = Path.Combine(assemblyDirectory, "cepepito.log.json");
                var configFile = new FileInfo(Path.Combine(assemblyDirectory, "log4net.config"));

                if (!configFile.Exists)
                {
                    throw new FileLoadException($"The configuration file {configFile} does not exist");
                }

                log4net.Config.XmlConfigurator.ConfigureAndWatch(configFile);
            }

            return LogManager.GetLogger(_assemblyType);
        });

        private static Type _assemblyType;
        private ILog LogImplementation => _logger.Value;

        public PerAssemblyLogger(Type assemblyType)
        {
            _assemblyType = assemblyType;
        }

        private string AdaptMemberName(string memberName)
        {
            if (memberName.Equals(".ctor") || memberName.Equals(".cctor"))
                return "Constructor";

            return memberName;
        }

        public void Debug(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Debug);
        }
        
        public void Info(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Info);
        }
        
        public void Warn(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Warn);
        }
        
        public void Error(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Error);
        }

        public void Fatal(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Fatal);
        }

        public void Debug(object message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Debug);
        }

        public void Info(object message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Info);
        }

        public void Warn(object message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Warn);
        }

        public void Error(object message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Error);
        }

        public void Fatal(object message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            LogInternal(message, memberName, sourceFilePath, sourceLineNumber, LogImplementation.Fatal);
        }

        private void LogInternal(string message, string memberName, string sourceFilePath, int sourceLineNumber,
            Action<string> logMethod)
        {
            logMethod(JsonConvert.SerializeObject(new { Caller = AdaptMemberName(memberName), LineNumber = sourceLineNumber, Description = message }));
        }

        private void LogInternal(object message, string memberName, string sourceFilePath, int sourceLineNumber, Action<string> logMethod)
        {
            try
            {
                logMethod(JsonConvert.SerializeObject(
                    new
                    {
                        Caller = AdaptMemberName(memberName),
                        LineNumber = sourceLineNumber,
                        Description = message
                    }, new JsonSerializerSettings {ContractResolver = new SerializableExpandableContractResolver()}));
            }
            catch (Exception e)
            {
                logMethod(JsonConvert.SerializeObject(new { Caller = AdaptMemberName(memberName), LineNumber = sourceLineNumber, Description = "Exception when trying to log " + message + ".", Exception = e }));
            }
        }


        public bool IsDebugEnabled => LogImplementation.IsDebugEnabled;

        public bool IsInfoEnabled => LogImplementation.IsInfoEnabled;

        public bool IsWarnEnabled => LogImplementation.IsWarnEnabled;

        public bool IsErrorEnabled => LogImplementation.IsErrorEnabled;

        public bool IsFatalEnabled => LogImplementation.IsFatalEnabled;
    }
}