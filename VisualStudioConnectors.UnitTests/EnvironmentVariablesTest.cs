﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VisualStudioAPIConnectors;

namespace VisualStudioConnectors.UnitTests
{
    [TestClass]
    public class EnvironmentVariablesTest
    {
        private static readonly Func<string, string> ParserFunction = x => x.Replace("$", "");

        [TestMethod]
        public void Given_AnEnvironmentVariablesInitializedWithTwoEmptyStrings_When_IteratingOverEntries_ItShouldBeEmpty()
        {
            var environmentVariables = new EnvironmentVariables("", ParserFunction);

            Assert.AreEqual(0, environmentVariables.Count());
        }

        [TestMethod]
        public void Given_AnEnvironmentVariablesInitializedWithAStringContainingNoEqual_When_IteratingOverEntries_ItShouldBeEmpty()
        {
            var environmentVariables = new EnvironmentVariables("$TOTO", ParserFunction);

            Assert.AreEqual(0, environmentVariables.Count());
        }

        [TestMethod]
        public void Given_AnEnvironmentVariablesInitializedWithAStringContainingAnEqualWithNoRightHandValue_When_IteratingOverEntries_ItShouldIterateOverTheSingleEntry()
        {
            var environmentVariables = new EnvironmentVariables("$TOTO=", ParserFunction);

            Assert.AreEqual(1, environmentVariables.Count());
            Assert.IsTrue(environmentVariables.Contains(new EnvironmentVariable("TOTO", "")));
        }

        [TestMethod]
        public void Given_AnEnvironmentVariablesInitializedWithAStringReturningAnEmptyStringUponParse_When_IteratingOverEntries_ItShouldBeEmpty()
        {
            var environmentVariables = new EnvironmentVariables("TOTO=", x => x.StartsWith("$") ? x.Substring(1) : null);

            Assert.AreEqual(0, environmentVariables.Count());
        }

        [TestMethod]
        public void Given_AnEnvironmentVariablesInitializedWithAParserThrowingAnExceptionUponParse_When_IteratingOverEntries_ItShouldBeEmpty()
        {
            var environmentVariables = new EnvironmentVariables("TOTO=", x => throw new Exception("Example exception"));

            Assert.AreEqual(0, environmentVariables.Count());
        }

        [TestMethod]
        public void
            Given_EnvironmentVariableInitializedWithASingleValidVariable_When_IteratingOverEntries_ItShouldIterateOverTheSingleEntry()
        {
            var rawEnvironmentVariables = "$PATH=toto";
            var environmentVariables = new EnvironmentVariables(rawEnvironmentVariables, ParserFunction);
            Assert.AreEqual(1, environmentVariables.Count());
            Assert.AreEqual(new EnvironmentVariable("PATH", "toto"), environmentVariables.First());
            Assert.AreEqual(rawEnvironmentVariables, environmentVariables.Raw);
        }

        [TestMethod]
        public void
            Given_EnvironmentVariablesInitializedWithTwoValidVariables_When_IteratingOverEntries_ItShouldIterateOverBothEntries()
        {
            var rawEnvironmentVariables = "$PATH=toto\nPATH1=tutu";
            var environmentVariables = new EnvironmentVariables(rawEnvironmentVariables, ParserFunction);
            Assert.AreEqual(2, environmentVariables.Count());
            Assert.IsTrue(environmentVariables.Contains(new EnvironmentVariable("PATH", "toto")));
            Assert.IsTrue(environmentVariables.Contains(new EnvironmentVariable("PATH1", "tutu")));
            Assert.AreEqual(rawEnvironmentVariables, environmentVariables.Raw);
        }

        [TestMethod]
        public void
            Given_EnvironmentVariablesInitializedWithTwoValidVariables_When_AddingANewVariable_ItShouldCorrectlyUpdateEntriesAndRawValue()
        {
            var rawEnvironmentVariables = "$PATH=toto\nPATH1=tutu";
            var environmentVariables = new EnvironmentVariables(rawEnvironmentVariables, ParserFunction);
            environmentVariables.Add("$PATH2=titi");
            Assert.AreEqual(3, environmentVariables.Count());
            Assert.IsTrue(environmentVariables.Contains(new EnvironmentVariable("PATH", "toto")));
            Assert.IsTrue(environmentVariables.Contains(new EnvironmentVariable("PATH1", "tutu")));
            Assert.IsTrue(environmentVariables.Contains(new EnvironmentVariable("PATH2", "titi")));
            Assert.AreEqual(rawEnvironmentVariables + "\n$PATH2=titi", environmentVariables.Raw);
        }
    }
}
