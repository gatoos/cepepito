﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text.RegularExpressions;
using System.Windows;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text.Editor;
using IServiceProvider = System.IServiceProvider;

namespace Cepepito.EditorContextMenus
{
    static class ExecuteTestsContextualMenuCommandHandler
    {
        private static bool StringIsTestFunctionPrototype(string content)
        {
            var matchPatterns = new List<string>
            {
                @"^\s*TEST\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)",
                @"^\s*TEST_F\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)",
                @"^\s*TEST_P\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)"
            };

            return matchPatterns.Any(x => Regex.Match(content, x, RegexOptions.Multiline).Success);
        }

        public static void ExecStatic(IServiceProvider serviceProvider, IWpfTextView _textView)
        {
            if (!(serviceProvider.GetService(typeof(SVsUIShell)) is IVsUIShell uiShell)) return;

            const string guidVsPackageContextMenuCmdSet = "277eb236-053b-4f91-9bff-8df20b783172";
            var contextMenuGuid = new Guid(guidVsPackageContextMenuCmdSet);
            var contextMenuToDisplay = RetrieveContextualMenuToDisplay(_textView);

            if (!contextMenuToDisplay.HasValue)
                return;
            
            var textViewOrigin = (_textView as UIElement).PointToScreen(new Point(0, 0));
            var caretPos = _textView.Caret.Position.BufferPosition;
            var charBounds = _textView
                .GetTextViewLineContainingBufferPosition(caretPos)
                .GetCharacterBounds(caretPos);
            var textBottom = charBounds.Bottom;
            var textX = charBounds.Right;
            var newLeft = textViewOrigin.X + textX - _textView.ViewportLeft;
            var newTop = textViewOrigin.Y + textBottom - _textView.ViewportTop;

            var point = new POINTS
            {
                x = (short)newLeft,
                y = (short)newTop
            };

            var points = new[] { point };

            uiShell.ShowContextMenu(0, ref contextMenuGuid, contextMenuToDisplay.Value, points, null);
        }

        private static int? RetrieveContextualMenuToDisplay(ITextView textView)
        {
            const int contextualMenuRunDebugSpecificTestId = 0x1002;
            const int contextualMenuOnRunDebugTestsInFileWithCodeGenerationId = 0x1003;
            const int contextualMenuCodeGenerationId = 0x1004;
            const int contextualMenuOnRunDebugTestsInFileWithoutCodeGenerationId = 0x1013;

            var lineContent = textView.Caret.ContainingTextViewLine.Start.GetContainingLine().GetText();
            var caretPosCharContent = textView.Caret.Position.BufferPosition.GetChar();

            if (StringIsTestFunctionPrototype(lineContent))
                return contextualMenuRunDebugSpecificTestId;

            var fileContent = textView.TextSnapshot.GetText();

            if (StringIsTestFunctionPrototype(fileContent))
            {
                if(!char.IsWhiteSpace(caretPosCharContent))
                    return contextualMenuOnRunDebugTestsInFileWithCodeGenerationId;

                return contextualMenuOnRunDebugTestsInFileWithoutCodeGenerationId;
            }

            if (!char.IsWhiteSpace(caretPosCharContent))
                return contextualMenuCodeGenerationId;

            return null;
        }
    }
}
