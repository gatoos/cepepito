﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCoreAdapters.AdaptersServiceProviders.CreateTestProject
{
    public interface ITestingLibrariesConfiguration
    {
        string GoogleTestIncludeDirectory { get; }
        string GoogleMockIncludeDirectory { get; }
        string GoogleTestLibrary { get; }
    }
}
