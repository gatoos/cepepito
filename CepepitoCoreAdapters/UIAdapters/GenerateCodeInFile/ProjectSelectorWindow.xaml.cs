﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CepepitoCoreAdapters.Annotations;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace CepepitoCoreAdapters.UIAdapters.GenerateCodeInFile
{
    public class ProjectSelectorViewModel : INotifyPropertyChanged
    {
        public readonly IDictionary<string, string> AvailableProjects;
        private string _selectedProject;
        private string _targetFilenameDirectory;

        public ProjectSelectorViewModel(IDictionary<string, string> availableProjects, string selectedProject)
        {
            AvailableProjects = availableProjects;
            AvailableProjectsNames = AvailableProjects.Select(x => x.Key).OrderBy(x => x);
            SelectedProject = selectedProject;
        }

        public IEnumerable<string> AvailableProjectsNames { get; }

        public string SelectedProject
        {
            get => _selectedProject;
            set
            {
                if (value == null)
                {
                    _selectedProject = value;
                    TargetFilenameDirectory = "";
                    return;
                }
                    

                if (!AvailableProjects.ContainsKey(value))
                    return;

                if (!AvailableProjects.TryGetValue(value, out _))
                {
                    Debug.Assert(false, "No project path for project " + value + " in available projects list");
                }

                _selectedProject = value;
                OnPropertyChanged(nameof(SelectedProject));
                TargetFilenameDirectory = AvailableProjects[SelectedProject];
            } 
        }

        public string TargetFilenameDirectory
        {
            get => _targetFilenameDirectory ?? "";
            set
            {
                _targetFilenameDirectory = value;
                OnPropertyChanged(nameof(TargetFilenameDirectory));
                OnPropertyChanged(nameof(TargetFilenameDirectoryDisplay));
            }
        }

        public string TargetFilenameDirectoryDisplay => TargetFilenameDirectory.Length > 60
            ? TargetFilenameDirectory.Substring(0, 60) + "..."
            : TargetFilenameDirectory;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    /// <summary>
    ///     Logique d'interaction pour ProjectSelector.xaml
    /// </summary>
    public partial class ProjectSelectorWindow : DialogWindow
    {
        private bool _escPressed;
        private bool _enterPressed;
        private readonly ProjectSelectorViewModel _projectSelectorViewModel;

        public ProjectSelectorWindow(IDictionary<string, string> availableProjects)
        {
            _projectSelectorViewModel = new ProjectSelectorViewModel(availableProjects, null);
            DataContext = _projectSelectorViewModel;
            _escPressed = false;
            _enterPressed = false;
            
            InitializeComponent();

            Loaded += (s, e) =>
            {
                var item = (ListBoxItem)ProjectsList.ItemContainerGenerator.ContainerFromIndex(0);
                if (item == null) return;
                item.IsSelected = true;
                item.Focus();
            };
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void projectsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        public string SelectedProject => _projectSelectorViewModel.SelectedProject;
        public string TargetFilenameDirectory => _projectSelectorViewModel.TargetFilenameDirectory;
        
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            _projectSelectorViewModel.SelectedProject = null;
            Close();
        }

        private void projectsList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                _enterPressed = true;
        }

        private void projectsList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && _enterPressed)
            {
                _enterPressed = false;
                Close();
            }
        }

        private void ProjectSelectorWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                _escPressed = true;
        }

        private void ProjectSelectorWindow_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape && _escPressed)
            {
                _escPressed = false;
                _projectSelectorViewModel.SelectedProject = null;
                Close();
            }
        }

        private void TargetFilenameClick(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = "Destination folder",
                IsFolderPicker = true,
                InitialDirectory = TargetFilenameDirectory,
                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                DefaultDirectory = TargetFilenameDirectory,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                _projectSelectorViewModel.TargetFilenameDirectory = dlg.FileName;
            }

            SetFocusOnSelectedProject();
        }

        private void SetFocusOnSelectedProject()
        {
            var selectedProject =
                ProjectsList.ItemContainerGenerator.ContainerFromIndex(ProjectsList.SelectedIndex) as ListBoxItem;

            selectedProject?.Focus();
            Keyboard.Focus(selectedProject);
        }

        private void projectsList_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!(e.Source is ListBoxItem selectedItem)) return;
            
            var projectPathRetrieved = _projectSelectorViewModel.AvailableProjects.TryGetValue(selectedItem.Content.ToString(), out var projectPath);
            if (!projectPathRetrieved)
                return;
            _projectSelectorViewModel.SelectedProject = selectedItem.Content.ToString();
            _projectSelectorViewModel.TargetFilenameDirectory = projectPath;
        }
    }

    class FakeProjectSelectorViewModel : ProjectSelectorViewModel
    {

        public FakeProjectSelectorViewModel() : 
            base(new Dictionary<string, string>
            {
                {"project1", "c:\\sources\\projects\\project1"},
                {"googleTestProject", "C:\\Users\\Rached\\AppData\\Local\\Microsoft\\VisualStudio\\15.0_94845cfbExp\\Extensions\\Gatoos\\Cepepito\\1.0\\Thirdparties\\googlemock\\include"}
            }, "googleTestProject")
        {
        }
    }
}