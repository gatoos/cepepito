﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using CepepitoCoreAdapters.Annotations;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace CepepitoCoreAdapters.UIAdapters.GenerateCodeInFile
{
    /// <summary>
    /// Interaction logic for GoogleTestIncludeDirectoryRetrieverWindow.xaml
    /// </summary>
    public sealed partial class GoogleTestIncludeDirectoryRetrieverWindow : DialogWindow, INotifyPropertyChanged
    {
        private readonly string _projectName;
        public string DialogBoxMainQuestion =>
            "Add Google Test include path to " + _projectName + " project include directories?";
        public string GoogleTestIncludeDirectory { get; private set; }
        private bool _enterPressed;
        private bool _escapePressed;

        public GoogleTestIncludeDirectoryRetrieverWindow(string projectName, string googleTestIncludeDirectory)
        {
            _projectName = projectName;
            InitializeComponent();
            GoogleTestIncludeDirectory = googleTestIncludeDirectory;
            SelectedGoogleTestIncludeDirectory = null;
            DataContext = this;
            _enterPressed = false;
            _escapePressed = false;
        }

        private string DisplayFolderSelectionDialog(string title, string initialDirectory)
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = title,
                IsFolderPicker = true,
                InitialDirectory = initialDirectory,
                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                DefaultDirectory = initialDirectory,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };


            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Focus();
                return dlg.FileName;
            }

            Focus();
            return null;
        }

        private void GoogleTestIncludeDirectoryClick(object sender, RoutedEventArgs routedEventArgs)
        {
            var selectedFolder = DisplayFolderSelectionDialog("Google test folder", GoogleTestIncludeDirectory);

            if (selectedFolder == null) return;
            GoogleTestIncludeDirectory = selectedFolder;
            OnPropertyChanged(nameof(DisplayableGoogleTestIncludeDirectory));
        }

        private void AddButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            ValidateInputsAndClose();
        }

        private void DontAddButtonClick(object sender, RoutedEventArgs routedEventArgs)
        {
            Cancel();
        }

        private void Cancel()
        {
            SelectedGoogleTestIncludeDirectory = null;
            Close();
        }

        public string DisplayableGoogleTestIncludeDirectory
        {
            get
            {
                if (GoogleTestIncludeDirectory.Length < 60)
                    return GoogleTestIncludeDirectory;

                return GoogleTestIncludeDirectory.Substring(0, 60) + "...";
            }

            set
            {
                GoogleTestIncludeDirectory = value;
                OnPropertyChanged();
            }
        }

        public string SelectedGoogleTestIncludeDirectory { get; private set; }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Window_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                _enterPressed = true;

            if (e.Key == Key.Escape)
                _escapePressed = true;
        }

        private void Window_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && _enterPressed)
            {
                _enterPressed = false;
                ValidateInputsAndClose();
            }

            if (e.Key == Key.Escape && _escapePressed)
            {
                _escapePressed = false;
                Cancel();
            }
        }

        private void ValidateInputsAndClose()
        {
            SelectedGoogleTestIncludeDirectory = GoogleTestIncludeDirectory;

            Close();
        }
    }
}
