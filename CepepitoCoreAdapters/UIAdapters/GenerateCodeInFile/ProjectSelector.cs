﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;

namespace CepepitoCoreAdapters.UIAdapters.GenerateCodeInFile
{
    public static class ProjectSelector
    {
        public static (string projectIdentifier, string targetFilename) Browse(IDictionary<string, string> projectsList)
        {            
            var projectSelector = new ProjectSelectorWindow(projectsList);
            projectSelector.ShowModal();
            return (projectSelector.SelectedProject, projectSelector.TargetFilenameDirectory);
        }
    }
}