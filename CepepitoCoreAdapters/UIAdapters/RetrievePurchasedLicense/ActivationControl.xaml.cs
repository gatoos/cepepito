﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Windows;
using Microsoft.VisualStudio.PlatformUI;

namespace CepepitoCoreAdapters.UIAdapters.RetrievePurchasedLicense
{
    /// <summary>
    ///     Logique d'interaction pour ActivationWpfControl.xaml
    /// </summary>
    public partial class ActivationControl
    {
        internal ActivationControl(DialogWindow father, ActivationControlViewModel viewModel)
        {
            InitializeComponent();
            Father = father;
            DataContext = viewModel;
        }

        private DialogWindow Father { get; }

        internal void OkButton_Click(object sender, RoutedEventArgs e)
        {
            ((ActivationControlViewModel) DataContext).ApprovedPurchaseCode = true;
            Father.Close();
        }

        internal void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            ((ActivationControlViewModel) DataContext).ApprovedPurchaseCode = false;
            Father.Close();
        }
    }
}