﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using Microsoft.VisualStudio.PlatformUI;

namespace CepepitoCoreAdapters.UIAdapters.RetrievePurchasedLicense
{
    public class ActivationDialog : DialogWindow
    {
        public ActivationDialog(ActivationControlViewModel activationControlViewModel)
        {
            Title = "Activation";
            HasMaximizeButton = false;
            HasMinimizeButton = false;
            MinWidth = 400;
            MaxWidth = 400;
            MinHeight = 375;
            MaxHeight = 375;
            var activationControl = new ActivationControl(this, activationControlViewModel);
            Content = activationControl;
        }
    }
}