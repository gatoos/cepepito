﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCoreAdapters.UIAdapters.RetrievePurchasedLicense
{
    public class ActivationControlViewModel
    {
        public ActivationControlViewModel()
        {
            PurchaseCode = "";
            ApprovedPurchaseCode = false;
        }

        public string PurchaseCode { get; set; }
        public bool ApprovedPurchaseCode { get; set; }
    }
}