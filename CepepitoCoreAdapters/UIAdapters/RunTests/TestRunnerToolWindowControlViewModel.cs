﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using CepepitoCore.Core.Entities;
using CepepitoCore.EntryPoints;
using CepepitoCore.UI;
using CepepitoCoreAdapters.Properties;
using GalaSoft.MvvmLight.Command;
using Logging;

namespace CepepitoCoreAdapters.UIAdapters.RunTests
{
    public class TestRunnerToolWindowControlViewModel : ITestRunnerViewerViewModel, INotifyPropertyChanged
    {
        public List<TestCaseExecutionResults> TestCaseExecutionResultsList { get; }
        public TestCaseExecutionResults TestCaseExecutionResults => TestCaseExecutionResultsList[0];

        public TestCaseIdentifierWithAssociatedProjects TestCaseIdentifierWithAssociatedProjects { get; }

        public ICommand RunTestsCommand { get; }
        public ICommand DebugTestsCommand { get; }
        public ICommand ClearTestsCommand { get; }
        private bool _testsAreRunning;
        private bool _subscribedToSolutionCloseEvent = false;

        private OpenTestLocationInEditorUseCaseController _openTestCaseLocationInEditorController;
        private RunTestsFromTestRunnerUseCaseController _runTestFromTestRunnerUseCaseController;
        private RunTestsFromTestRunnerUseCaseController _debugTestFromTestRunnerUseCaseController;

        public bool TestsAreRunning
        {
            get => _testsAreRunning;
            set
            {
                if (_testsAreRunning != value)
                {
                    _testsAreRunning = value;
                    OnPropertyChanged();
                }
            }
        }

        public Action SubscribeToSolutionCloseEvents { get; set; }

        public void UpdateTestCaseExecutionResults(TestCaseExecutionResults testCaseExecutionResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects)
        {
            // TODO : Remove the code from here and move it to appropriate location
            UpdateTestCaseExecutionQueryService(testCaseExecutionResults);
            //
            UpdateModel(testCaseExecutionResults, testCaseIdentifierWithAssociatedProjects);

            if (!_subscribedToSolutionCloseEvent)
            {
                SubscribeToSolutionCloseEvents();
                _subscribedToSolutionCloseEvent = true;
            }
        }

        // TODO : Remove code from here and move it to appropriate location
        private List<TestCaseExecutionResults> RetrieveLeafNodes(TestCaseExecutionResults testCaseExecutionResults)
        {
            var leafNodes = new List<TestCaseExecutionResults>();

            foreach (var childNode in testCaseExecutionResults.Children)
                leafNodes.AddRange(RetrieveLeafNodes(childNode));

            if (testCaseExecutionResults.Children != null && testCaseExecutionResults.Children.Count == 0)
                leafNodes.Add(testCaseExecutionResults);

            return leafNodes;
        }

        private TestCaseIdentifier RetrieveTestCase(TestCaseExecutionResults node, string pathSeparator)
        {
            string[] stringSeparators = { pathSeparator };
            var nodePathElements = node.FullPath.Split(stringSeparators, StringSplitOptions.None);
            if (nodePathElements.Length == 3)
                return new TestCaseIdentifier(nodePathElements[1] + "." + nodePathElements[2]);
            return null;
        }

        private void UpdateTestCaseExecutionQueryService(TestCaseExecutionResults testCaseExecutionResults)
        {
            var leafNodes = RetrieveLeafNodes(testCaseExecutionResults);

            foreach (var node in leafNodes)
            {
                var testCaseKey =
                    RetrieveTestCase(node, ".");
                if (testCaseKey != null)
                    TestCaseExecutionStatusQueryService.Instance.UpdateStatus(testCaseKey, node.TestCaseExecutionStatusMember);
            }
        }
        ////

        public TestRunnerToolWindowControlViewModel()
        {
            RunTestsCommand = new RelayCommand(RunTests);
            DebugTestsCommand = new RelayCommand(DebugTests);
            ClearTestsCommand = new RelayCommand(ClearTests);

            var rootTestCaseExecutionResult = new TestCaseExecutionResults("Test Results")
            {
                IsExpanded = true,
                TestCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull
            };
            TestCaseExecutionResultsList = new List<TestCaseExecutionResults> {rootTestCaseExecutionResult};
            TestCaseIdentifierWithAssociatedProjects = new TestCaseIdentifierWithAssociatedProjects();

            TestsAreRunning = false;
        }

        public void InitializeCommandHandlers(
            OpenTestLocationInEditorUseCaseController openTestLocationInEditorUseCaseController,
            RunTestsFromTestRunnerUseCaseController runTestFromTestRunnnerUseCaseController,
            RunTestsFromTestRunnerUseCaseController debugTestFromTestRunnerUseCaseController)
        {
            _openTestCaseLocationInEditorController = openTestLocationInEditorUseCaseController;
            _runTestFromTestRunnerUseCaseController = runTestFromTestRunnnerUseCaseController;
            _debugTestFromTestRunnerUseCaseController = debugTestFromTestRunnerUseCaseController;
        }

        private void UpdateModel(TestCaseExecutionResults testCaseExecutionResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProject)
        {
            var rootTestCaseExecutionResult = TestCaseExecutionResults;
            MergeTestCasesExecutionResults(rootTestCaseExecutionResult, testCaseExecutionResults);
            UpdateTestCaseExecutionStatus(rootTestCaseExecutionResult);
            MergeTestCasesAssociatedProjects(TestCaseIdentifierWithAssociatedProjects, testCaseIdentifierWithAssociatedProject);
        }

        private void UpdateTestCaseExecutionStatus(TestCaseExecutionResults testCaseExecutionResults)
        {
            var childrenStatuses = new List<TestCaseExecutionStatus>();

            foreach (var node in testCaseExecutionResults.Children)
            {
                UpdateTestCaseExecutionStatus(node);
                childrenStatuses.Add(node.TestCaseExecutionStatusMember);
            }

            if (childrenStatuses.Count > 0)
                testCaseExecutionResults.TestCaseExecutionStatusMember =
                    ComputeFatherStatusFromChildrenStatuses(childrenStatuses);
        }

        private TestCaseExecutionStatus ComputeFatherStatusFromChildrenStatuses(
            ICollection<TestCaseExecutionStatus> childrenStatuses)
        {
            if (childrenStatuses.Contains(TestCaseExecutionStatus.Failed))
                return TestCaseExecutionStatus.Failed;

            if (childrenStatuses.Contains(TestCaseExecutionStatus.PreviouslyFailed))
                return TestCaseExecutionStatus.PreviouslyFailed;

            if (childrenStatuses.Contains(TestCaseExecutionStatus.PreviouslySuccessfull))
                return TestCaseExecutionStatus.PreviouslySuccessfull;

            return TestCaseExecutionStatus.Successfull;
        }

        private void MergeTestCasesAssociatedProjects(TestCaseIdentifierWithAssociatedProjects target,
            TestCaseIdentifierWithAssociatedProjects source)
        {
            target.Append(source);
        }

        private void MergeTestCasesExecutionResults(TestCaseExecutionResults destination, TestCaseExecutionResults source)
        {
            if (destination == null || source == null)
                return;

            foreach (var sourceTestCaseExecutionResult in source.Children)
            {
                var targetTestCaseExecutionResults =
                    destination.Children.Where(e => e.Label.Equals(sourceTestCaseExecutionResult.Label));

                var targetTestCaseExecutionResult = targetTestCaseExecutionResults.Any()
                    ? targetTestCaseExecutionResults.First()
                    : null;

                if (targetTestCaseExecutionResult != null)
                {
                    targetTestCaseExecutionResult.ErrorsDescription = sourceTestCaseExecutionResult.ErrorsDescription;
                    targetTestCaseExecutionResult.TestCaseExecutionStatusMember =
                        sourceTestCaseExecutionResult.TestCaseExecutionStatusMember;
                    MergeTestCasesExecutionResults(targetTestCaseExecutionResult, sourceTestCaseExecutionResult);
                }
                else
                {
                    destination.Children.Add(sourceTestCaseExecutionResult);
                }
            }
        }

        private void ClearTests()
        {
            TestCaseExecutionResults.Clear();
            
            // TODO : Remove from there
            TestCaseExecutionStatusQueryService.Instance.ClearStatuses();
        }

        private void RunTests()
        {
            Debug.Assert(_runTestFromTestRunnerUseCaseController != null, nameof(_runTestFromTestRunnerUseCaseController) + " != null");
            _runTestFromTestRunnerUseCaseController.Execute();
        }

        private void DebugTests()
        {
            Debug.Assert(_debugTestFromTestRunnerUseCaseController != null, nameof(_debugTestFromTestRunnerUseCaseController) + " != null");
            _debugTestFromTestRunnerUseCaseController.Execute();
        }

        internal void TestSuitesExecutionResultsTreeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Debug.Assert(_openTestCaseLocationInEditorController != null, nameof(_openTestCaseLocationInEditorController) + " != null");
            _openTestCaseLocationInEditorController.Execute(TestCaseExecutionResults, TestCaseIdentifierWithAssociatedProjects);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void OnSolutionClosed()
        {
            ClearTests();
        }
    }
}