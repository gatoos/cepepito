﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CepepitoCoreAdapters.UIAdapters.RunTests
{
    /// <summary>
    ///     Interaction logic for TestRunnerToolWindowControl.
    /// </summary>
    public partial class TestRunnerToolWindowControl
    {
        public static readonly TestRunnerToolWindowControlViewModel TestRunnertoolWindowControlViewModel =
            new TestRunnerToolWindowControlViewModel();

        private void ToolBar_Loaded(object sender, RoutedEventArgs e)
        {
            ToolBar toolBar = sender as ToolBar;
            var overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }
            var mainPanelBorder = toolBar.Template.FindName("MainPanelBorder", toolBar) as FrameworkElement;
            if (mainPanelBorder != null)
            {
                mainPanelBorder.Margin = new Thickness();
            }
        }

        public TestRunnerToolWindowControl()
        {
            InitializeComponent();
            DataContext = TestRunnerToolWindowControl.TestRunnertoolWindowControlViewModel;
            testSuitesExecutionResultsTreeView.ItemsSource = TestRunnerToolWindowControl
                .TestRunnertoolWindowControlViewModel.TestCaseExecutionResultsList;
        }

        public void TestSuitesExecutionResultsTreeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TestRunnerToolWindowControlViewModel.TestSuitesExecutionResultsTreeView_MouseDoubleClick(sender, e);
        }

        public void TestSuitesExecutionResultsTreeView_MouseRightClick(object sender, MouseButtonEventArgs e)
        {
            TestRunnerToolWindowControlViewModel.TestSuitesExecutionResultsTreeView_MouseDoubleClick(sender, e);
        }

        internal TestRunnerToolWindowControlViewModel TestRunnerToolWindowControlViewModel => (TestRunnerToolWindowControlViewModel) DataContext;
    }
}