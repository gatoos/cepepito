﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using CepepitoCore.Core.Entities;
using CepepitoCore.UI;
using Logging;

namespace CepepitoCoreAdapters.UIAdapters.RunTests
{
    public class TestRunnerViewerViewModel : ITestRunnerViewerViewModel
    {
        private readonly Func<TestRunnerToolWindow> _retrieveTestRunnerToolWindow;
        private TestRunnerToolWindow TestRunnerToolWindow => _retrieveTestRunnerToolWindow();

        public TestRunnerViewerViewModel(Func<TestRunnerToolWindow> retrieveTestRunnerToolWindow)
        {
            _retrieveTestRunnerToolWindow = retrieveTestRunnerToolWindow;
        }

        private TestRunnerToolWindowControlViewModel TestRunnerToolWindowControlViewModel => TestRunnerToolWindowControl.TestRunnertoolWindowControlViewModel;
        
        public bool TestsAreRunning
        {
            get => TestRunnerToolWindowControlViewModel.TestsAreRunning;
            set
            {
                TestRunnerToolWindowControlViewModel.TestsAreRunning = value;
                TestRunnerToolWindow.Display();
            }
        }

        public TestCaseExecutionResults TestCaseExecutionResults =>
            TestRunnerToolWindowControlViewModel.TestCaseExecutionResults;

        public TestCaseIdentifierWithAssociatedProjects TestCaseIdentifierWithAssociatedProjects =>
            TestRunnerToolWindowControlViewModel.TestCaseIdentifierWithAssociatedProjects;

        public void UpdateTestCaseExecutionResults(TestCaseExecutionResults testCaseExecutionResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects)
        {
            TestRunnerToolWindowControlViewModel.UpdateTestCaseExecutionResults(testCaseExecutionResults, testCaseIdentifierWithAssociatedProjects);
        }
    }
}
