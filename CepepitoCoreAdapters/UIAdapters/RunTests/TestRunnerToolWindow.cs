﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Runtime.InteropServices;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace CepepitoCoreAdapters.UIAdapters.RunTests
{
    /// <summary>
    ///     This class implements the tool window exposed by this package and hosts a user control.
    /// </summary>
    /// <remarks>
    ///     In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    ///     usually implemented by the package implementer.
    ///     <para>
    ///         This class derives from the ToolWindowPane class provided from the MPF in order to use its
    ///         implementation of the IVsUIElementPane interface.
    ///     </para>
    /// </remarks>
    [Guid("b4a82c04-f90f-4e89-a270-d42a16f12bcf")]
    public sealed class TestRunnerToolWindow : ToolWindowPane
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TestRunnerToolWindow" /> class.
        /// </summary>
        public TestRunnerToolWindow() : base(null)
        {
            Caption = "Cepepito Test Runner";

            // This is the user control hosted by the tool window; Note that, even if this class implements IDisposable,
            // we are not calling Dispose on this object. This is because ToolWindowPane calls Dispose on
            // the object returned by the Content property.
            Content = new CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerToolWindowControl();
        }

        public void Display()
        {
            var windowFrame = (IVsWindowFrame) Frame;
            ErrorHandler.ThrowOnFailure(windowFrame.Show());
        }
    }
}