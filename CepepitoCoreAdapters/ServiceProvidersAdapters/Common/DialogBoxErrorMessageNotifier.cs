﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using ExtensionRegistrationService.ServiceProviders;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.Common
{
    public class DialogBoxErrorMessageNotifier : IErrorsMessagesQueue, IErrorsHandler
    {
        private readonly IServiceProvider _serviceProvider;

        public DialogBoxErrorMessageNotifier(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void AddErrorMessage(string message)
        {
            ShowMessageBox(message);
        }

        private void ShowMessageBox(string message)
        {
            VsShellUtilities.ShowMessageBox(
                _serviceProvider,
                message,
                "Error",
                OLEMSGICON.OLEMSGICON_WARNING,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }

        public void Notify(string errorMessage)
        {
            ShowMessageBox(errorMessage);
        }
    }
}