﻿namespace CepepitoCoreAdapters.ServiceProvidersAdapters.Common
{
    public class ProgressDialogOptions
    {
        public ProgressDialogOptions(string windowTitle, string label)
        {
            WindowTitle = windowTitle;
            Label = label;
        }

        public string WindowTitle { get; }
        public string Label { get; }
    }
}