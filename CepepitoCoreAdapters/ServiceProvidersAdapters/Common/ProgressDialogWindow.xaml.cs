﻿using System.Windows;
using Microsoft.VisualStudio.PlatformUI;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.Common
{
    /// <summary>
    /// Interaction logic for ProgressDialogWindow.xaml
    /// </summary>
    public partial class ProgressDialogWindow : DialogWindow
    {
        public ProgressDialogWindow()
        {
            InitializeComponent();
        }
    }
}
