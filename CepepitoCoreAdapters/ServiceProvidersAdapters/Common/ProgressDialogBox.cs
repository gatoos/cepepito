﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CepepitoCore.ServiceProviders.Interfaces.Common;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.Common
{
    public class ProgressDialogBox : IProgressDialogBox
    {
        private ProgressDialogOptions Options { get; }
        private bool IsCancellable { get; }

        public ProgressDialogBox(ProgressDialogOptions options, bool isCancellable = true)
        {
            Options = options;
            IsCancellable = isCancellable;
        }

        public void Show<T>(Task<T> workerTask, CancellationTokenSource cancellationTokenSource)
        {
            var cancellationToken = cancellationTokenSource.Token;

            var cancelCommand = IsCancellable ? new CancelCommand(cancellationTokenSource) : null;

            var viewModel = new ProgressDialogWindowViewModel(
                Options, cancellationToken, cancelCommand);

            var window = new ProgressDialogWindow
            {
                DataContext = viewModel
            };

            workerTask.ContinueWith(_ => viewModel.Close = true);

            window.ShowModal();
        }
    }
}