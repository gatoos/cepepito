﻿using System.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.Common
{
    public class ProgressDialogWindowViewModel : ViewModelBase
    {
        string _windowTitle;
        string _label;
        bool _close;

        public ProgressDialogWindowViewModel(ProgressDialogOptions progressDialogOptions, CancellationToken cancellationToken,
            CancelCommand cancelCommand)
        {
            WindowTitle = progressDialogOptions.WindowTitle;
            Label = progressDialogOptions.Label;
            CancelCommand = cancelCommand;
            cancellationToken.Register(OnCancel);
        }

        private void OnCancel()
        {
            // Cancellation may come from a background thread.
            if (DispatcherHelper.UIDispatcher != null)
                DispatcherHelper.CheckBeginInvokeOnUI(() => Close = true);
            else
                Close = true;
        }

        public bool IsCancellable => CancelCommand != null;

        public CancelCommand CancelCommand { get; }
        
        public string WindowTitle
        {
            get => _windowTitle;
            set
            {
                _windowTitle = value;
                RaisePropertyChanged(() => WindowTitle);
            }
        }

        public string Label
        {
            get => _label;
            set
            {
                _label = value;
                RaisePropertyChanged(() => Label);
            }
        }

        public bool Close
        {
            get => _close;
            set
            {
                _close = value;
                RaisePropertyChanged(() => Close);
            }
        }
    }
}
