﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CepepitoCore.Core.UseCases.CreateTestProject;
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;
using CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject.@internal;
using Microsoft.VisualStudio.VCProjectEngine;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject
{
    public class SolutionProjectsRepository : ISolutionProjectsRepository
    {
        private readonly DteRetriever _dteRetriever;

        public SolutionProjectsRepository(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
        }

        public string AddProject(string projectName, string customizedProjectXmlDefinition,
            ProjectCustomizations projectCustomizerProjectCustomizationsData, string sourceProjectUniqueName)
        {
            var sourceProject = new Project(new VsSolution(_dteRetriever).GetProjectFromUniqueName(sourceProjectUniqueName));
            var addedProject = CreateProjectFromXml(projectName, sourceProject, customizedProjectXmlDefinition);
            addedProject.Name = projectName;
            addedProject.RootNamespace = projectName;
            CustomizeProjectFiles(projectCustomizerProjectCustomizationsData.ProjectFilesCustomization, addedProject,
                sourceProject);

            foreach (var projectConfiguration in addedProject.Configurations)
            {
                CustomizeProjectIncludeDirectories(
                    projectCustomizerProjectCustomizationsData.AdditionalIncludeDirectories,
                    projectConfiguration);
                CustomizePreprocessorDefinitions(
                    projectCustomizerProjectCustomizationsData.AdditionalPreprocessorDefinitions, projectConfiguration);
                projectConfiguration.RuntimeLibrary = runtimeLibraryOption.rtMultiThreadedDebug;
                CustomizeLinkLibraries(
                    projectCustomizerProjectCustomizationsData.AddOriginalProjectStaticLibraryPath,
                    projectCustomizerProjectCustomizationsData.AdditionalLinkLibrary,
                    projectConfiguration);
            }

            addedProject.Save();
            return addedProject.UniqueName;
        }

        public void AddFileToProject(string targetProjectName, string filename, string fileContent)
        {
            var targetProject = new Project(new VsSolution(_dteRetriever).GetProjectFromUniqueName(targetProjectName));
            targetProject.AddFileFromContent(filename, fileContent);
        }

        private void CustomizeLinkLibraries(bool addOriginalProjectStaticLibraryPath, string additionalLinkLibrary,
            ProjectConfiguration projectConfiguration)
        {
            var addSpace = projectConfiguration.AdditionalDependencies != "" &&
                           !projectConfiguration.AdditionalDependencies.EndsWith(" ");

            var sourceProjectLibraryPath = addOriginalProjectStaticLibraryPath
                ? "\"" + projectConfiguration.GeneratedStaticLibraryPath + "\""
                : "";
            var completeAdditionalDependencies =
                (addSpace ? " " : "") + additionalLinkLibrary + " " + sourceProjectLibraryPath;
            projectConfiguration.AdditionalDependencies += completeAdditionalDependencies;
        }

        private void CustomizePreprocessorDefinitions(string additionalPreprocessorDefinitions,
            ProjectConfiguration projectConfiguration)
        {
            var addSemicolonToPreprocessorList = projectConfiguration.PreprocessorDefinitions != "" &&
                                                 !projectConfiguration.PreprocessorDefinitions.EndsWith(";");

            projectConfiguration.PreprocessorDefinitions +=
                (addSemicolonToPreprocessorList ? ";" : "") + additionalPreprocessorDefinitions;
        }

        private void CustomizeProjectIncludeDirectories(IEnumerable<string> additionalIncludeDirectories,
            ProjectConfiguration projectConfiguration)
        {
            projectConfiguration.AdditionalIncludeDirectories = projectConfiguration.AdditionalIncludeDirectories.ToList().Concat(additionalIncludeDirectories);
        }

        private void CustomizeProjectFiles(ProjectFilesCustomizationEnum projectFilesCustomization, Project project,
            Project sourceProject)
        {
            switch (projectFilesCustomization)
            {
                case ProjectFilesCustomizationEnum.RemoveAllFiles:
                    project.RemoveCppFiles();
                    break;
            }
        }

        private Project CreateProjectFromXml(string projectName, Project sourceProject,
            string customizedProjectXmlDefinition)
        {
            var projectDirectory = Path.GetDirectoryName(sourceProject.FileName);

            Debug.Assert(projectDirectory != null, "projectDirectory is null");
            var createdProjectDirectory = Path.Combine(projectDirectory, projectName);
            var createdProjectFilename = projectName + ".vcxproj";
            var createdProjectFullFilename = Path.Combine(createdProjectDirectory, createdProjectFilename);
            var createdProjectFilterFullFilename = createdProjectFullFilename + ".filters";

            if (!Directory.Exists(createdProjectDirectory) &&
                !File.Exists(createdProjectFullFilename) &&
                !File.Exists(createdProjectFullFilename + ".filters"))
            {
                Directory.CreateDirectory(createdProjectDirectory);
                var fileWriter = File.CreateText(createdProjectFullFilename);
                fileWriter.Write(customizedProjectXmlDefinition);
                fileWriter.Flush();
                fileWriter.Close();
                File.Copy(sourceProject.FullName + ".filters", createdProjectFilterFullFilename);
            }
            else
            {
                throw new Exception("Project " + projectName + " already exists");
            }

            return new Project(new VsSolution(_dteRetriever).AddProjectFromFile(createdProjectFullFilename));
        }
    }
}