﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.UseCases.CreateTestProject;
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;
using CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject.@internal;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject
{
    public class SelectedProjectPropertiesRetriever : ISelectedProjectPropertiesRetriever
    {
        private readonly DteRetriever _dteRetriever;

        public SelectedProjectPropertiesRetriever(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
        }

        private Project SelectedProject => new Project(new VsSolution(_dteRetriever).SelectedProject);

        public ProjectType ProjectType => SelectedProject.ProjectType;

        public string ProjectXmlRepresentation => SelectedProject.XmlRepresentation;

        public string ProjectName => SelectedProject.Name;

        public string ProjectUniqueName => SelectedProject.UniqueName;

        public bool ProjectIsCppProject => SelectedProject.IsACppProject;

        public bool ProjectContainsTestRunner => !SelectedProject.IsACppProjectWithoutTestRunner;

        public string ProjectFullName => SelectedProject.FullName;

        public string ProjectGuid => SelectedProject.Guid;
    }
}