﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject
{
    public class TestingLibrariesConfiguration : ITestingLibrariesConfiguration
    {
        private readonly AdaptersServiceProviders.CreateTestProject.ITestingLibrariesConfiguration _underlyingTestLibrariesConfiguration;

        public TestingLibrariesConfiguration(AdaptersServiceProviders.CreateTestProject.ITestingLibrariesConfiguration underlyingTestLibrariesConfiguration)
        {
            _underlyingTestLibrariesConfiguration = underlyingTestLibrariesConfiguration;
        }

        public string GoogleTestIncludeDirectory => _underlyingTestLibrariesConfiguration.GoogleTestIncludeDirectory;
        public string GoogleMockIncludeDirectory => _underlyingTestLibrariesConfiguration.GoogleMockIncludeDirectory;
        public string GoogleTestLibrary => _underlyingTestLibrariesConfiguration.GoogleTestLibrary;
    }
}