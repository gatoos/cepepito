﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.Core.UseCases.CreateTestProject;
using Microsoft.VisualStudio.VCProjectEngine;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject.@internal
{
    internal class Project
    {
        private readonly VsProject _project;

        public Project(VsProject project)
        {
            _project = project;
        }

        public string Name
        {
            get => _project.Name;
            set => _project.Name = value;
        }

        public string FullName => _project.FullName;
        public string FileName => _project.FileName;

        public string RootNamespace
        {
            set => _project.RootNamespace = value;
        }

        public IEnumerable<ProjectConfiguration> Configurations =>
            _project.Configurations.Select(configuration => new ProjectConfiguration(configuration));

        public string XmlRepresentation => _project.XmlRepresentation;
        public bool IsACppProject => _project.IsACppProject;
        public bool IsACppProjectWithoutTestRunner => _project.IsACppProjectWithoutTestRunner;
        public string Guid => _project.Guid;

        public ProjectType ProjectType
        {
            get
            {
                switch (_project.ActiveProjectConfigurationType)
                {
                    case ConfigurationTypes.typeApplication:
                        return ProjectType.Executable;
                    case ConfigurationTypes.typeDynamicLibrary:
                        return ProjectType.DynamicLinkedLibrary;
                    case ConfigurationTypes.typeStaticLibrary:
                        return ProjectType.StaticLinkedLibrary;
                    default:
                        return ProjectType.InvalidType;
                }
            }
        }

        public string UniqueName => _project.UniqueName;

        public void RemoveCppFiles()
        {
            _project.RemoveCppFiles();
        }

        public void AddCppFilesFromSourceProject(Project sourceProject)
        {
            _project.AddCppFilesFromSourceProject(sourceProject._project);
        }

        public void AddFileFromContent(string filename, string fileContent)
        {
            _project.AddFileFromContent(filename, fileContent);
        }

        public void Save()
        {
            _project.Save();
        }
    }
}