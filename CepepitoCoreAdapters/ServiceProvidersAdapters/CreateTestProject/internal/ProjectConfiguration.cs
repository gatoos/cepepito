﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Collections.Generic;
using Microsoft.VisualStudio.VCProjectEngine;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject.@internal
{
    internal class ProjectConfiguration
    {
        private readonly VsProjectConfiguration _vsProjectConfiguration;

        public ProjectConfiguration(VsProjectConfiguration vsProjectConfiguration)
        {
            _vsProjectConfiguration = vsProjectConfiguration;
        }

        public IEnumerable<string> AdditionalIncludeDirectories
        {
            get => _vsProjectConfiguration.AdditionalIncludeDirectories;
            set => _vsProjectConfiguration.AdditionalIncludeDirectories = value;
        }

        public string PreprocessorDefinitions
        {
            get => _vsProjectConfiguration.PreprocessorDefinitions;
            set => _vsProjectConfiguration.PreprocessorDefinitions = value;
        }

        public runtimeLibraryOption RuntimeLibrary
        {
            get => _vsProjectConfiguration.RuntimeLibrary;
            set => _vsProjectConfiguration.RuntimeLibrary = value;
        }

        public string AdditionalDependencies
        {
            get => _vsProjectConfiguration.AdditionalDependencies;
            set => _vsProjectConfiguration.AdditionalDependencies = value;
        }

        public string GeneratedStaticLibraryPath => _vsProjectConfiguration.GeneratedStaticLibraryPath;
    }
}