﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using EnvDTE;
using EnvDTE80;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    public class CurrentDocumentInfoRetriever : ICurrentDocumentInfoRetriever
    {
        private readonly DteRetriever _dteRetriever;
        private readonly IServiceProvider _serviceProvider;

        public CurrentDocumentInfoRetriever(DteRetriever dteRetriever, IServiceProvider serviceProvider)
        {
            _dteRetriever = dteRetriever ?? throw new ArgumentNullException(nameof(dteRetriever));
            _serviceProvider = serviceProvider;
        }

        private DTE2 Dte => _dteRetriever.GetDte();

        private TextDocument CppTextDocument => Dte?.ActiveDocument?.Language == "C/C++"
            ? Dte?.ActiveDocument?.Object(null) as TextDocument
            : null;

        public string DocumentName => Dte?.ActiveDocument?.FullName;

        public CursorPosition CursorPosition
        {
            get
            {
                var returnValue = new CursorPosition();
                var activePoint = CppTextDocument.Selection.ActivePoint;
                returnValue.Line = activePoint.Line > 0 ? (uint) activePoint.Line : 0;
                returnValue.Column = activePoint.VirtualCharOffset > 0 ? (uint) activePoint.VirtualCharOffset : 0;

                return returnValue;
            }
        }

        public string RetrieveCurrentDocumentCursorLocationLineContent()
        {
            CppTextDocument?.Selection?.SelectLine();
            return CppTextDocument?.Selection?.Text;
        }

        public string RetrieveCurrentDocumentContent()
        {
            var activePoint = CppTextDocument?.Selection?.ActivePoint;
            var currentLine = activePoint?.Line > 0 ? (uint) activePoint.Line : 0;
            var currentDisplayColumn = activePoint?.DisplayColumn > 0 ? (uint) activePoint.DisplayColumn : 0;
            CppTextDocument?.Selection?.SelectAll();
            var returnValue = CppTextDocument?.Selection?.Text;
            CppTextDocument?.Selection?.MoveToDisplayColumn((int) currentLine, (int) currentDisplayColumn);

            return returnValue;
        }

        public string RetrieveCurrentDocumentProjectName()
        {
            return RetrieveActiveDocumentOwnerProjectUniqueName() ?? RetrieveActiveDocumentReferencingProjectUniqueName();
        }

        private string RetrieveActiveDocumentOwnerProjectUniqueName()
        {
            return Dte?.ActiveDocument?.ProjectItem?.ContainingProject?.UniqueName;
        }

        private string RetrieveActiveDocumentReferencingProjectUniqueName()
        {
            // Use VsHierarchy if available to retrieve associated project
            var (projectName, searchPerformed) = RetrieveFileAssociatedProjectByUsingSolutionHierarchy(Dte?.ActiveDocument?.FullName);

            // If it's not, check whether the file is being included by a file in a given project (full textual search used to build 1st level include graph, cf below)
            return searchPerformed ? projectName : RetrieveFileAssociatedProjectByParsingAllFileContentsAndBuildingTheirDependencies(Dte?.ActiveDocument?.FullName);
        }

        private (string, bool) RetrieveFileAssociatedProjectByUsingSolutionHierarchy(string fileFullName)
        {
            try
            {
                return (SolutionProjectsAsEnumerable(Dte?.Solution?.Projects).FirstOrDefault(currentProject =>
                    new VsProject(currentProject).GetAllFiles(_serviceProvider).Any(x =>
                        x.Equals(fileFullName, StringComparison.OrdinalIgnoreCase)))?.UniqueName, true);
            }
            catch (VsHierarchyNotAvailableException)
            {
                return (null, false);
            }
        }

        private string RetrieveFileAssociatedProjectByParsingAllFileContentsAndBuildingTheirDependencies(string fileFullName)
        {
            // Algorithm description (doesn't handle transitive includes):
            // ----------------------
            // Iterates over all projects
            // For each project, iterate over each file
            // On each file, retrieve include directives
            // For each include directive, if it includes the seeked file
            // if it does, if it's an absolute include path, check if it matches the seeked one
            // if it's not, check whether using the currently treated project additional include directories + current file path and concatening them to the include directive gives a match
            
            return SolutionProjectsAsEnumerable(Dte?.Solution?.Projects).
                Select(currentProject => new {Project = currentProject, new VsProject(currentProject).ActiveProjectConfiguration.AdditionalIncludeDirectories}).
                SelectMany(currentProjectWithInfos => 
                    VsProject.RetrieveCppFilesFromProjectItems(currentProjectWithInfos.Project.ProjectItems).AsParallel().
                        SelectMany(currentFile => 
                            File.ReadLines(currentFile).
                                TakeWhile(currentParsedLine => !currentParsedLine.Contains("{")).
                                SkipWhile(currentLine => !currentLine.StartsWith("#include")).
                                Select(readLine => new { CurrentProject = currentProjectWithInfos.Project, currentProjectWithInfos.AdditionalIncludeDirectories, LineContent = readLine, AssociatedFile = currentFile })).
                Where(x => IncludeDirectiveIncludesFullFilename(x.LineContent, fileFullName, new []{Path.GetDirectoryName(x.AssociatedFile)}.Concat(x.AdditionalIncludeDirectories)))).
                Select(x => x.CurrentProject.UniqueName).
                FirstOrDefault();
        }

        private bool IncludeDirectiveIncludesFullFilename(string includeDirective, string fileFullName,
            IEnumerable<string> projectIncludePaths)
        {
            Debug.Assert(includeDirective != null, nameof(includeDirective) + "shouldn't be null");
            Debug.Assert(fileFullName != null, nameof(fileFullName) + " shoudln't be null");

            try
            {
                if (!includeDirective.Contains(Path.GetFileName(fileFullName)))
                    return false;

                var includeDirectiveFilepath = RetrieveFilePathFromDirective(includeDirective);

                if (includeDirectiveFilepath == null)
                    return false;

                var includeDirectiveFilepathStandardFolderSeparator =
                    includeDirectiveFilepath.Replace('/', Path.DirectorySeparatorChar);

                if (Path.IsPathRooted(includeDirectiveFilepathStandardFolderSeparator))
                {
                    return includeDirectiveFilepath.Equals(fileFullName, StringComparison.OrdinalIgnoreCase);
                }


                return projectIncludePaths.
                    Select(x => Path.GetFullPath(Path.Combine(x, includeDirectiveFilepath)).Replace('/', Path.DirectorySeparatorChar)).
                    Any(x => x.Equals(fileFullName, StringComparison.OrdinalIgnoreCase));
            }
            catch (ArgumentException)
            {
                // TODO: log include directive contains invalid path characters
                return false;
            }
        }

        private string RetrieveFilePathFromDirective(string includeDirective)
        {
            var retrieveIncludePathRegex =
                new Regex(@"^\s*#include\s*[""<]([0-9a-zA-Z_/\-. ]+)["">]", RegexOptions.IgnoreCase).Match(includeDirective);

            return retrieveIncludePathRegex.Success ? retrieveIncludePathRegex.Groups[1].Captures[0].Value : null;
        }

        private IEnumerable<Project> SolutionProjectsAsEnumerable(Projects solutionProjects)
        {
            foreach(var project in solutionProjects)
                yield return project as Project;
        }
    }
}