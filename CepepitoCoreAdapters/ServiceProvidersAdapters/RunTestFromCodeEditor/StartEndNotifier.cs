﻿using System;
using Logging;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    internal class StartEndNotifier : IDisposable
    {
        private readonly Action _endNotifier;
        private readonly ILogger _logger;

        public StartEndNotifier(ILoggerFactory loggerFactory, Action startNotifier, Action endNotifier)
        {
            _logger = loggerFactory.BuildPerAssemblyLogger(typeof(ProjectOutputRunner));
            _logger.Debug("Notifying test run start");
            _endNotifier = endNotifier;
            startNotifier();
        }

        public void Dispose()
        {
            _logger.Debug("Notifying test run end");
            _endNotifier();
        }
    }
}