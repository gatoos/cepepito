﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using EnvDTE;
using EnvDTE80;
using Logging;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{

    internal enum DebugSessionOutcome
    {
        Finished,
        Stopped
    }

    public class ProjectOutputDebugger : IProjectOutputLauncher
    {
        private readonly DteRetriever _dteRetriever;
        private readonly ILoggerFactory _loggerFactory;
        public event EventHandler TestRunningStart;
        public event EventHandler TestRunningEnd;

        public ProjectOutputDebugger(DteRetriever dteRetriever, ILoggerFactory loggerFactory)
        {
            _dteRetriever = dteRetriever ?? throw new ArgumentNullException(nameof(dteRetriever));
            _loggerFactory = loggerFactory;
        }

        private DTE2 Dte => _dteRetriever.GetDte();

        public async Task<XDocument> LaunchProjectWithArguments(string currentDocumentProjectName,
            string testLaunchArguments)
        {
            var solutionBuild = Dte?.Solution?.SolutionBuild;
            return await DebugProject(currentDocumentProjectName, testLaunchArguments, solutionBuild);
        }

        private async Task<XDocument> DebugProject(string currentDocumentProjectUniqueName, string testLaunchArguments,
            SolutionBuild solutionBuild)
        {
            var testProject = new VsSolution(_dteRetriever).GetProjectFromUniqueName(currentDocumentProjectUniqueName);
            var testReportXmlFilename = Path.GetTempFileName();

            var debugSessionOutcome = await LaunchDebugger(solutionBuild, testProject, testLaunchArguments, testReportXmlFilename);
            
            if (debugSessionOutcome == DebugSessionOutcome.Finished)
                return await ExtractTestResults(testReportXmlFilename);

            return new XDocument();
        }

        private static async Task<XDocument> ExtractTestResults(string testReportXmlFilename)
        {
            return await Task.Run(() =>
            {
                try
                {
                    return XDocument.Load(testReportXmlFilename);
                }
                catch (Exception)
                {
                    // TODO : correctly handle failure 
                    return null;
                }
            });
        }

        private async Task<DebugSessionOutcome> LaunchDebugger(SolutionBuild solutionBuild, VsProject testProject,
            string testFilter, string testReportXmlFilename)
        {
            using (new DebuggerEnvironmentVariableUpdater(testProject, testReportXmlFilename, testFilter))
            {
                using (var buildEvents = new BuildEventsSubscriber(_dteRetriever.GetDte(), _loggerFactory))
                {
                    using (var debuggerEvents = new DebuggerEventsSubscriber(_dteRetriever))
                    {
                        const int timeOutInMilliseconds = 2000;
                        LaunchDebuggerOnProject(solutionBuild, testProject);

                        var finishedTask = await Task.WhenAny(buildEvents.BuildStarted, debuggerEvents.DebugSessionStarted, Task.Delay(timeOutInMilliseconds));

                        if (finishedTask == buildEvents.BuildStarted)
                        {
                            ShowBuildOutputPanel(Dte);
                            await buildEvents.BuildFinished;

                            if (solutionBuild.LastBuildInfo != 0)
                                throw new Exception("Errors occured when building the solution. Fix errors and try again");

                           finishedTask = await Task.WhenAny(debuggerEvents.DebugSessionStarted,
                                Task.Delay(timeOutInMilliseconds));
                        }

                        if (finishedTask == debuggerEvents.DebugSessionStarted)
                        {
                            TestRunningStart?.Invoke(this, null);
                            var debugSessionOutcome = await debuggerEvents.DebugSessionOutcome;
                            TestRunningEnd?.Invoke(this, null);
                            return debugSessionOutcome;
                        }
                            

                        return DebugSessionOutcome.Stopped;
                    }
                }
            }
        }

        private void ShowBuildOutputPanel(DTE2 dte)
        {
            dte?.ToolWindows.OutputWindow.OutputWindowPanes.Item("Build").Activate();
            dte?.Windows.Item(Constants.vsWindowKindOutput).Activate();
        }

        private static void LaunchDebuggerOnProject(SolutionBuild solutionBuild, VsProject testProject)
        {
            using (new StartupProjectSetter(solutionBuild, testProject.FullName))
            {
                solutionBuild.Debug();
            }
        }
    }
}
