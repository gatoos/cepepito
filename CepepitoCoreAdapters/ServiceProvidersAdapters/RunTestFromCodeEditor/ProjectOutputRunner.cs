﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Xml.Linq;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using EnvDTE80;
using Logging;
using VisualStudioAPIConnectors;
using Process = System.Diagnostics.Process;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    public class ProjectOutputRunner : IProjectOutputLauncher
    {
        private readonly DteRetriever _dteRetriever;
        private readonly ILoggerFactory _loggerFactory;
        public event EventHandler TestRunningStart;
        public event EventHandler TestRunningEnd;
        private readonly ILogger _logger;

        public ProjectOutputRunner(DteRetriever dteRetriever, ILoggerFactory loggerFactory)
        {
            _dteRetriever = dteRetriever ?? throw new ArgumentNullException(nameof(dteRetriever));
            _loggerFactory = loggerFactory;
            _logger = loggerFactory.BuildPerAssemblyLogger(typeof(ProjectOutputRunner));
        }

        private DTE2 Dte => _dteRetriever.GetDte();

        public async Task<XDocument> LaunchProjectWithArguments(string currentDocumentProjectName,
            string testsFilter)
        {
            using (new ScopedActionLogger(_logger, "Launch project with arguments"))
            {
                await new ProjectBuilder(_loggerFactory).BuildProject(Dte, currentDocumentProjectName);
                var tempFilename = await RunTests(currentDocumentProjectName, testsFilter);
                return await ExtractTestResults(tempFilename);
            }
        }

        private async Task<XDocument> ExtractTestResults(string tempFilename)
        {
            return await Task.Run(() =>
            {
                try
                {
                    _logger.Info("Extracting tests results");
                    _logger.Debug(new {TestResultsFilename=tempFilename});
                    return XDocument.Load(tempFilename);
                }
                catch (Exception exception)
                {
                    _logger.Error(new {Description="Failed to load tests execution results", Exception=exception});
                    throw new TestCaseExecutionException("An error occured when trying to retrieve tests execution results.");
                }
            });
        }

        private async Task<string> RunTests(string currentDocumentProjectUniqueName, string testsFilter)
        {
            using (new ScopedActionLogger(_logger, "RunTests"))
            {
                _logger.Debug(new { RetrievedCppProjectName = currentDocumentProjectUniqueName });

                var vsProject = BuildCurrentDocumentVsProject(currentDocumentProjectUniqueName);
                var testBinaryQualifiedName = BuildTestBinaryQualifiedName(vsProject);

                var process = new Process
                {
                    StartInfo =
                    {
                        FileName = testBinaryQualifiedName,
                        WorkingDirectory = vsProject.DebugWorkingDirectory,
                        RedirectStandardOutput = false,
                        RedirectStandardError = false,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    },
                    EnableRaisingEvents = true
                };

                _logger.Debug(new { StartInfo = process });

                _logger.Debug("Retrieving temp filename");
                var tempFilename = GetTempFilename();
                _logger.Debug(new { TemporaryFilename = tempFilename });

                _logger.Debug("Updating environment variables");
                UpdateEnvironmentVariables(testsFilter, vsProject, tempFilename, process);

                using (new StartEndNotifier(_loggerFactory, () => TestRunningStart?.Invoke(this, null),
                    () => TestRunningEnd?.Invoke(this, null)))
                {
                    try
                    {
                        process.Start();

                        await Task.Run(() => process.WaitForExit());
                    }
                    catch (Exception exception)
                    {
                        _logger.Error(new
                        {
                            Description = "Process failed to start",
                            Exception = exception
                        });
                        throw new TestCaseExecutionException("An error occured when launching the tests.");
                    }
                }

                return tempFilename;
            }
        }

        private VsProject BuildCurrentDocumentVsProject(string currentDocumentProjectUniqueName)
        {
            VsProject vsProject;
            try
            {
                vsProject =
                    new VsSolution(_dteRetriever).GetProjectFromUniqueName(currentDocumentProjectUniqueName);

                if (vsProject == null)
                {
                    _logger.Error(new
                    {
                        Description = "Current document doesn't belong to any project in solution",
                        CurrentDocumentProjectName = currentDocumentProjectUniqueName
                    });
                    throw new TestCaseExecutionException(
                        "Current document doesn't belong to any project in solution.");
                }
            }
            catch (VsProjectCreationException)
            {
                _logger.Error(new
                {
                    Description = "Current document belongs to a non C++ project",
                    CurrentDocumentProjectName = currentDocumentProjectUniqueName
                });
                throw new TestCaseExecutionException("Current document belongs to a non C++ project");
            }

            return vsProject;
        }

        private void UpdateEnvironmentVariables(string testsFilter, VsProject vsProject, string tempFilename,
            Process process)
        {
            var gtestOutputEnvironmentVariable = "GTEST_OUTPUT=xml:" + tempFilename;
            var gtestFilterEnvironmentVariable = "GTEST_FILTER=" + testsFilter;

            _logger.Debug(new { GoogleTestOutputEnvironmentVariable = gtestOutputEnvironmentVariable });
            _logger.Debug(new { GoogleTestFilterEnvironmentVariable = gtestFilterEnvironmentVariable });

            var updatedEnvironmentVariables = vsProject.EnvironmentVariables;
            _logger.Debug(new { OriginalProjectEnvironmentVariables = updatedEnvironmentVariables });
            updatedEnvironmentVariables.Add(gtestOutputEnvironmentVariable);
            updatedEnvironmentVariables.Add(gtestFilterEnvironmentVariable);
            _logger.Debug(new { TargetProjectEnvironmentVariables = updatedEnvironmentVariables });

            new ProcessEnvironmentVariablesUpdater(_loggerFactory).UpdateExecutionEnvironmentVariables(
                process.StartInfo.EnvironmentVariables, updatedEnvironmentVariables,
                vsProject.EnvironmentVariablesMerge);
        }

        private string GetTempFilename()
        {
            string tempFilename;

            try
            {
                _logger.Debug("Trying to retrieve temporary filename");
                tempFilename = Path.GetTempFileName();
                _logger.Debug(new { TemporaryFilename = tempFilename });
            }
            catch (IOException ioException)
            {
                _logger.Warn(new { Description = "IOException when retrieving temporary filename", Exception = ioException });
                const string testCaseExceutionExceptionMessage = "An error occured when launching the tests.";
                try
                {
                    _logger.Debug("Trying to retrieve temporary path");
                    var tempPath = Path.GetTempPath();
                    _logger.Debug(new { TemporaryPath = tempPath });
                    string[] files;


                    try
                    {
                        _logger.Debug("Retrieving temporary directory content");
                        files = Directory.GetFiles(tempPath, "*.*", SearchOption.AllDirectories);
                    }
                    catch (Exception getFilesException)
                    {
                        _logger.Error(new { Description = "Exception raised when listing temporary folder content", Exception = getFilesException });
                        throw new TestCaseExecutionException(testCaseExceutionExceptionMessage);
                    }

                    foreach (var file in files)
                    {
                        _logger.Debug("Treating file: " + file);
                        if (!File.Exists(file)) continue;

                        _logger.Debug("File exists");
                        _logger.Debug("Deleting file");
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception fileDeleteException)
                        {
                            _logger.Debug(new { Description = "An error occured when trying to delete the file", Exception = fileDeleteException });
                        }
                    }

                    try
                    {
                        _logger.Debug("Trying to retrieve temporary filename after cleanup");
                        tempFilename = Path.GetTempFileName();
                        _logger.Debug(new { TemporaryFilename = tempFilename });
                    }
                    catch (Exception)
                    {
                        _logger.Error(new { Description = "IOException raised when retrieving temporary filename", Exception = ioException });
                        throw new TestCaseExecutionException(testCaseExceutionExceptionMessage);
                    }
                }
                catch (SecurityException temporaryPathRetrievalSecurityException)
                {
                    _logger.Error(new { Description = "Security exception raised when trying to retrieve temporary path", Exception = temporaryPathRetrievalSecurityException });
                    throw new TestCaseExecutionException(testCaseExceutionExceptionMessage);
                }
            }

            return tempFilename;
        }

        private string BuildTestBinaryQualifiedName(VsProject vsProject)
        {
            var testBinaryQualifiedName = vsProject.DebugCommand;
            _logger.Debug(new { vsProject.DebugCommand });

            if (!File.Exists(testBinaryQualifiedName))
            {
                _logger.Debug(new { Description = "Target executable doesn't exist. Updating target executable with debug working directory", vsProject.DebugWorkingDirectory });

                if (Path.GetInvalidPathChars().Any(x => vsProject.DebugWorkingDirectory.Contains(x)))
                {
                    const string message = "Project debug working directory is not a valid directory.";
                    _logger.Error(new { Description = message, vsProject.DebugWorkingDirectory });

                    throw new TestCaseExecutionException(message);
                }

                try
                {
                    testBinaryQualifiedName = Path.Combine(vsProject.DebugWorkingDirectory, vsProject.DebugCommand);
                    _logger.Debug(new { UpdatedTargetExecutable = testBinaryQualifiedName });
                    if (!File.Exists(testBinaryQualifiedName))
                    {
                        _logger.Error(new {Description="Updated target executable doesn't exists", ExecutablePath=testBinaryQualifiedName});
                        var message = testBinaryQualifiedName + " doesn't exists.";
                        throw new TestCaseExecutionException(message);
                    }
                }
                catch (ArgumentException)
                {
                    var message = "Project debug command contains invalid characters.";
                    _logger.Error(new { Description = message, vsProject.DebugCommand });
                    throw new TestCaseExecutionException(message);
                }
            }

            return testBinaryQualifiedName;
        }
    }
}