﻿using System;
using EnvDTE;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    internal class StartupProjectSetter : IDisposable
    {
        private readonly object _startupProjectBackup;
        private readonly SolutionBuild _solutionBuild;

        public StartupProjectSetter(SolutionBuild solutionBuild, string testProjectFullName)
        {
            _solutionBuild = solutionBuild;
            _startupProjectBackup = _solutionBuild.StartupProjects;
            solutionBuild.StartupProjects = testProjectFullName;

        }

        public void Dispose()
        {
            _solutionBuild.StartupProjects = _startupProjectBackup;
        }
    }
}