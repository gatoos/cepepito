﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using Logging;
using Debugger = EnvDTE.Debugger;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    internal class ProjectBuilder
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger _logger;

        public ProjectBuilder(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            _logger = loggerFactory.BuildPerAssemblyLogger(typeof(ProjectBuilder));
        }

        internal async Task BuildProject(DTE2 dte, string currentDocumentProjectName)
        {
            using (new ScopedActionLogger(_logger, "BuildProject"))
            {
                var solutionBuild = dte?.Solution?.SolutionBuild;
                var debugger = dte?.Debugger;

                Debug.Assert(solutionBuild != null, nameof(solutionBuild) + " != null");
                Debug.Assert(debugger != null, nameof(debugger) + " != null");

                if (SolutionIsBuilding(solutionBuild) || DebuggerIsDebugging(debugger))
                {
                    _logger.Warn("Solution is already being built/debugged, operation cancelled");
                    throw new Exception("Solution is already being built/debugged, operation cancelled");
                }

                ShowBuildOutputPanel(dte);

                var activeConfigurationAsString = BuildActiveConfigurationIdentifier(solutionBuild);
                _logger.Debug(new {ActiveSolutionConfiguration=activeConfigurationAsString});

                using (var buildDoneSubscription = new BuildEventsSubscriber(dte, _loggerFactory))
                {
                    _logger.Debug("Launching project build");
                    solutionBuild.BuildProject(activeConfigurationAsString, currentDocumentProjectName);
                    _logger.Debug("Waiting for build to finish");
                    await buildDoneSubscription.BuildFinished;
                }

                if (solutionBuild.LastBuildInfo != 0)
                {
                    _logger.Warn("Errors occured when building the solution");
                    throw new Exception("Errors occured when building the solution. Fix errors and try again");
                }
            }
        }

        private static bool DebuggerIsDebugging(Debugger debugger)
        {
            return debugger.DebuggedProcesses.Count != 0;
        }

        private static bool SolutionIsBuilding(SolutionBuild solutionBuild)
        {
            return solutionBuild.BuildState == vsBuildState.vsBuildStateInProgress;
        }

        private static void ShowBuildOutputPanel(DTE2 dte)
        {
            dte?.ToolWindows.OutputWindow.OutputWindowPanes.Item("Build").Activate();
            dte?.Windows.Item(Constants.vsWindowKindOutput).Activate();
        }

        private static string BuildActiveConfigurationIdentifier(SolutionBuild solutionBuild)
        {
            var activeConfiguration = solutionBuild.ActiveConfiguration;
            var activeConfigurationAsString =
                activeConfiguration is SolutionConfiguration2 activeConfigurationWithPlatform
                    ? activeConfigurationWithPlatform.Name + "|" + activeConfigurationWithPlatform.PlatformName
                    : activeConfiguration.Name;
            return activeConfigurationAsString;
        }
    }
}