﻿using System;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    internal class DebuggerEnvironmentVariableUpdater : IDisposable
    {
        private readonly EnvironmentVariables _environmentVariablesBackup;
        private readonly VsProject _testProject;

        public DebuggerEnvironmentVariableUpdater(VsProject testProject, string testReportXmlFilename, string testFilter)
        {
            _testProject = testProject;
            _environmentVariablesBackup = _testProject.EnvironmentVariables;
            var updatedEnvironmentVariables = _testProject.EnvironmentVariables;
            updatedEnvironmentVariables.Add("GTEST_OUTPUT=xml:" + testReportXmlFilename);
            updatedEnvironmentVariables.Add("GTEST_FILTER=" + testFilter);

            _testProject.EnvironmentVariables = updatedEnvironmentVariables;
        }

        public void Dispose()
        {
            _testProject.EnvironmentVariables = _environmentVariablesBackup;
        }
    }
}