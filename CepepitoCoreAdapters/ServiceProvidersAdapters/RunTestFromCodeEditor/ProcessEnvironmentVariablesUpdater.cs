﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Logging;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    public class ProcessEnvironmentVariablesUpdater
    {
        private readonly ILogger _logger; 

        public ProcessEnvironmentVariablesUpdater(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.BuildPerAssemblyLogger(typeof(ProjectOutputRunner));
        }

        public void UpdateExecutionEnvironmentVariables(StringDictionary processEnvironmentVariables,
            IEnumerable<EnvironmentVariable> projectEnvironmentVariables, bool mergeEnvironmentVariables)
        {
            using (new ScopedActionLogger(_logger, "UpdateExecutionEnvironmentVariables"))
            {
                _logger.Debug(new {MergeEnvironmentVariables = mergeEnvironmentVariables});
                foreach (var environmentVariable in projectEnvironmentVariables)
                {
                    var environmentVariableSeparator = EnvironmentVariableGetMergeSeparator(environmentVariable.Key);

                    if (!mergeEnvironmentVariables || environmentVariableSeparator == null)
                    {
                        processEnvironmentVariables[environmentVariable.Key] = environmentVariable.Value;
                        _logger.Debug(new
                        {
                            UpdatedEnvironmentVariable = new EnvironmentVariable(environmentVariable.Key,
                                processEnvironmentVariables[environmentVariable.Key])
                        });
                        continue;
                    }

                    if (environmentVariable.Value == null || !environmentVariable.Value.Any())
                    {
                        processEnvironmentVariables[environmentVariable.Key] = processEnvironmentVariables[environmentVariable.Key];
                        _logger.Debug(new
                        {
                            UpdatedEnvironmentVariable = new EnvironmentVariable(environmentVariable.Key,
                                processEnvironmentVariables[environmentVariable.Key])
                        });
                        continue;
                    }

                    if (!processEnvironmentVariables.ContainsKey(environmentVariable.Key) ||
                        processEnvironmentVariables[environmentVariable.Key] == null ||
                        !processEnvironmentVariables[environmentVariable.Key].Any())
                    {
                        processEnvironmentVariables[environmentVariable.Key] = environmentVariable.Value;
                        _logger.Debug(new
                        {
                            UpdatedEnvironmentVariable = new EnvironmentVariable(environmentVariable.Key,
                                processEnvironmentVariables[environmentVariable.Key])
                        });
                        continue;
                    }

                    processEnvironmentVariables[environmentVariable.Key] =
                        processEnvironmentVariables[environmentVariable.Key].TrimEnd(environmentVariableSeparator[0]) +
                        environmentVariableSeparator +
                        environmentVariable.Value?.TrimStart(environmentVariableSeparator[0]);

                    _logger.Debug(new
                    {
                        Description = "Updating process environment variable with project environment variable",
                        ProjectEnvironmentVariable = environmentVariable,
                        EnvironmentVariableSeparator = environmentVariableSeparator,
                        ProcessUpdatedEnvironmentVariable = new EnvironmentVariable(environmentVariable.Key,
                            processEnvironmentVariables[environmentVariable.Key])
                    });

                }

                _logger.Debug(new {UpdatedProcessEnvironmentVariable = processEnvironmentVariables});
            }

        }

        private static string EnvironmentVariableGetMergeSeparator(string environmentVariableKey)
        {
            return !environmentVariableKey.Equals("PATH", StringComparison.InvariantCultureIgnoreCase) ? null : ";";
        }
    }
}