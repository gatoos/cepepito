﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using Logging;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    internal sealed class BuildEventsSubscriber : IDisposable
    {
        private readonly ILogger _logger; 

        public BuildEventsSubscriber(DTE2 dte, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.BuildPerAssemblyLogger(typeof(BuildEventsSubscriber));
            _buildStarted = new TaskCompletionSource<object>();
            _buildFinished = new TaskCompletionSource<object>();
            _buildEvents = dte.Events.BuildEvents;
            _buildStartedDelegate = (scope, action) =>
            {
                _logger.Debug("Build started delegate called");
                _buildStarted.SetResult(null);
            };
            _buildFinishedDelegate = (scope, action) =>
            {
                _logger.Debug("Build finished delegate called");
                _buildFinished.SetResult(null);
            };
            _logger.Debug("Subscribing to build start & build done events");
            _buildEvents.OnBuildBegin += _buildStartedDelegate;
            _buildEvents.OnBuildDone += _buildFinishedDelegate;
        }

        private readonly TaskCompletionSource<object> _buildStarted;
        private readonly TaskCompletionSource<object> _buildFinished;
        private readonly _dispBuildEvents_OnBuildBeginEventHandler _buildStartedDelegate;
        private readonly _dispBuildEvents_OnBuildDoneEventHandler _buildFinishedDelegate;
        private readonly BuildEvents _buildEvents;
        public Task<object> BuildStarted => _buildStarted.Task;
        public Task<object> BuildFinished => _buildFinished.Task;

        #region IDisposable Support

        private bool _disposedValue = false;

        private void Dispose(bool disposing)
        {
            if (_disposedValue) return;
            if (disposing)
            {
                _logger.Debug("Unsubscribing from build start & build done events");
                _buildEvents.OnBuildBegin -= _buildStartedDelegate;
                _buildEvents.OnBuildDone -= _buildFinishedDelegate;
            }

            _disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}