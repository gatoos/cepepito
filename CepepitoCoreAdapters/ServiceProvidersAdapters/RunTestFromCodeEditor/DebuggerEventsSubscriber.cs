﻿using System;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor
{
    internal class DebuggerEventsSubscriber : IDisposable
    {
        private DebuggerEvents _debuggerEvents;
        private _dispDebuggerEvents_OnEnterDesignModeEventHandler _onEnterDesignMode;
        private _dispDebuggerEvents_OnEnterRunModeEventHandler _onEnterRunMode;
        private bool _debuggedProcessStarted;
        private readonly DteRetriever _dteRetriever;
        private readonly TaskCompletionSource<DebugSessionOutcome> _debugSessionOutcomeTaskCompletionSource;
        private readonly TaskCompletionSource<object> _debugStartedTaskCompletionSource;
        private DTE2 Dte => _dteRetriever.GetDte();

        private DebuggerEvents DebuggerEvents => _debuggerEvents ?? (_debuggerEvents = Dte.Events?.DebuggerEvents);

        public DebuggerEventsSubscriber(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
            _debugStartedTaskCompletionSource = new TaskCompletionSource<object>();
            _debugSessionOutcomeTaskCompletionSource = new TaskCompletionSource<DebugSessionOutcome>();
            _debuggedProcessStarted = false;

            SubscribeToDebuggerEvents(_debugStartedTaskCompletionSource, _debugSessionOutcomeTaskCompletionSource);
        }

        public Task<object> DebugSessionStarted => _debugStartedTaskCompletionSource.Task;
        public Task<DebugSessionOutcome> DebugSessionOutcome => _debugSessionOutcomeTaskCompletionSource.Task;

        private void SubscribeToDebuggerEvents(TaskCompletionSource<object> debuggerStarted, TaskCompletionSource<DebugSessionOutcome> debuggerFinished)
        {
            _onEnterRunMode = reason =>
            {
                if (_debuggedProcessStarted) return;

                debuggerStarted.SetResult(null);
                _debuggedProcessStarted = true;
            };

            _onEnterDesignMode = reason =>
            {
                if (!_debuggedProcessStarted)
                    return;

                _debuggedProcessStarted = false;

                switch (reason)
                {
                    case dbgEventReason.dbgEventReasonEndProgram:
                        debuggerFinished.SetResult(RunTestFromCodeEditor.DebugSessionOutcome.Finished);
                        break;
                    case dbgEventReason.dbgEventReasonStopDebugging:
                        debuggerFinished.SetResult(RunTestFromCodeEditor.DebugSessionOutcome.Stopped);
                        break;
                }
            };

            DebuggerEvents.OnEnterRunMode += _onEnterRunMode;
            DebuggerEvents.OnEnterDesignMode += _onEnterDesignMode;
        }

        private void UnsubcribeFromDebugFinishedEvent()
        {
            DebuggerEvents.OnEnterDesignMode -= _onEnterDesignMode;
            DebuggerEvents.OnEnterRunMode -= _onEnterRunMode;
        }

        public void Dispose()
        {
            UnsubcribeFromDebugFinishedEvent();
        }
    }
}