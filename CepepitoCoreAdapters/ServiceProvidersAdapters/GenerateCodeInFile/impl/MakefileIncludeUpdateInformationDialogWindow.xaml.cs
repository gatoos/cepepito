﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.PlatformUI;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile.impl
{
    /// <summary>
    /// Interaction logic for MakefileIncludeUpdateInformationDialogWindow.xaml
    /// </summary>
    public partial class MakefileIncludeUpdateInformationDialogWindow : DialogWindow
    {
        private readonly IEnumerable<string> _includeDirectories;
        private bool _enterPressed;
        private bool _escapePressed;
        public string NotificationMessage { get; }
        public string IncludeDirectories => "- " + _includeDirectories.Aggregate((aggregate, next) => aggregate + Environment.NewLine + Environment.NewLine + "- " + next);
        public int WindowHeight => Math.Min(400, 150 + _includeDirectories.Select(x => 40).Sum());

        public MakefileIncludeUpdateInformationDialogWindow(string notificationMessage,
            IEnumerable<string> includeDirectories)
        {
            NotificationMessage = notificationMessage;
            _includeDirectories = includeDirectories;
            _enterPressed = false;
            DataContext = this;
            InitializeComponent();
        }

        private void Window_OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    _enterPressed = true;
                    break;
                case Key.Escape:
                    _escapePressed = true;
                    break;
            }
        }

        private void Window_OnKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter when _enterPressed:
                    _enterPressed = false;
                    Close();
                    break;
                case Key.Escape when _escapePressed:
                    _escapePressed = false;
                    Close();
                    break;
            }
        }

        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
