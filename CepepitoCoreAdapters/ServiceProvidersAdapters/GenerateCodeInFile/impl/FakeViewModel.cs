﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile.impl
{
    public sealed class FakeViewModel
    {
        public FakeViewModel()
        {}

        private readonly IEnumerable<string> _includeDirectories = new List<string>
        {
            "C:\\Users\\Rached\\AppData\\Local\\Microsoft\\VisualStudio\\15.0_94845cfbExp\\Extensions\\Gatoos\\Cepepito\\1.0\\Thirdparties\\googlemock\\include",
            "C:\\Users\\Rached\\AppData\\Local\\Microsoft\\VisualStudio\\15.0_94845cfbExp\\Extensions\\Gatoos\\Cepepito\\1.0\\Thirdparties\\googletest\\include"
        };

        public string NotificationMessage =>
            "Project Intellisense include search path has been updated with following path" +
            (_includeDirectories.Count() > 1 ? "s" : "") + ": ";

        public string IncludeDirectories => "- " + _includeDirectories.Aggregate((aggregate, next) => aggregate + Environment.NewLine + Environment.NewLine + "- " + next);
        public int WindowHeight => Math.Min(400, 150 + _includeDirectories.Select(x => 40).Sum());
    }
}