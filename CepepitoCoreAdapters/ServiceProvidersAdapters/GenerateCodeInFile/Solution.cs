﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile.impl;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.VCProjectEngine;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public class Solution : ISolution
    {
        private readonly DteRetriever _dteRetriever;
        private readonly IServiceProvider _serviceProvider;

        public Solution(DteRetriever dteRetriever, IServiceProvider serviceProvider)
        {
            _dteRetriever = dteRetriever;
            _serviceProvider = serviceProvider;
        }

        public IEnumerable<(string name, string content)> UnsavedDocuments =>
            from Document document in _dteRetriever.GetDte().Documents
            where document.Language == "C/C++" && !document.Saved
            select (document.FullName, GetDocumentContent(document));

        public IEnumerable<string> GetProjectIncludeDirectories(ProjectIdentifier projectIdentifier)
        {
            var project = RetrieveProjectFromIdentifier(projectIdentifier);

            if (project == null)
                return Enumerable.Empty<string>();

            var vsProject = new VsProject(project);
                        
            if (vsProject.ActiveProjectConfiguration == null)
            {
                var folderProjectConfiguration = RetrieveFolderProjectConfig(project);
                if(folderProjectConfiguration == null)
                    return Enumerable.Empty<string>();
                // TODO : treat the folder case
                return Enumerable.Empty<string>();
            }

            return vsProject.ActiveProjectConfiguration.AdditionalIncludeDirectories;
        }

        public void AddIncludeDirectoriesToProject(ProjectIdentifier projectIdentifier, IEnumerable<string> includeDirectories)
        {
            var project = RetrieveProjectFromIdentifier(projectIdentifier);
            if (project == null)
                throw new Exception("Cannot retrieve project " + projectIdentifier);

            var vsProject = new VsProject(project);
            vsProject.ActiveProjectConfiguration.AdditionalIncludeDirectories = vsProject.ActiveProjectConfiguration.AdditionalIncludeDirectories.ToList().Concat(includeDirectories);

            if (vsProject.ActiveProjectConfiguration.Type != ConfigurationTypes.typeGeneric) return;

            var makefileIncludeUpdateInformationDialogWindow = new MakefileIncludeUpdateInformationDialogWindow(
                "Project Intellisense include search path has been updated with following path" + (includeDirectories.Count() > 1 ? "s" : "") + ": ",
                includeDirectories);

            makefileIncludeUpdateInformationDialogWindow.ShowModal();
        }

        private object RetrieveFolderProjectConfig(Project project)
        {
            // TODO : Handle Folder projects
            return null;
        }


        public void AddAndOpenNewDocumentInProject(ProjectIdentifier targetProjectIdentifier,
            string toBeCreatedDocumentFilename,
            string documentContent)
        {
            var targetProject = RetrieveProjectFromIdentifier(targetProjectIdentifier);

            if (toBeCreatedDocumentFilename?.Length != null)
            {
                if (File.Exists(toBeCreatedDocumentFilename))
                {
                    var window = _dteRetriever.GetDte().ItemOperations.OpenFile(toBeCreatedDocumentFilename);
                    window.Activate();
                    var textDocument = (TextDocument)window.Document.Object(null);
                    textDocument.Selection.SelectAll();
                    textDocument.Selection.Delete();
                    textDocument.Selection.Insert(documentContent);
                    textDocument.Selection.GotoLine(1);
                    window.Document.Save();
                    targetProject.ProjectItems.AddFromFile(toBeCreatedDocumentFilename);
                }
                else
                {
                    var newDocument = NewTextDocument(Path.GetFileName(toBeCreatedDocumentFilename));
                    var textDocument = (TextDocument)newDocument.Object(null);

                    textDocument.Selection.SelectAll();
                    textDocument.Selection.Delete();
                    textDocument.Selection.Insert(documentContent);
                    textDocument.Selection.GotoLine(1);

                    newDocument.Save(toBeCreatedDocumentFilename);
                    var newDocumentFullName = newDocument.FullName;
                    targetProject.ProjectItems.AddFromFile(newDocumentFullName);
                    newDocument.Close();

                    _dteRetriever.GetDte().ItemOperations.OpenFile(newDocumentFullName);
                }
            }
        }

        public bool FileExistsInProject(string filename, ProjectIdentifier targetProjectIdentifier)
        {
            var targetProject = RetrieveProjectFromIdentifier(targetProjectIdentifier);

            if (Path.GetDirectoryName(targetProject.FullName)?.Length != null)
            {
                var toBeCreatedDocumentFilename =
                    Path.Combine(Path.GetDirectoryName(targetProject.FullName), filename);
                return File.Exists(toBeCreatedDocumentFilename);
            }

            return false;
        }

        private string GetDocumentContent(Document document)
        {
            var textDocument = (TextDocument) document.Object(null);
            var activePoint = textDocument.Selection.ActivePoint;
            var currentLine = activePoint.Line > 0 ? (uint) activePoint.Line : 0;
            var currentDisplayColumn = activePoint.DisplayColumn > 0
                ? (uint) activePoint.DisplayColumn
                : 0;

            textDocument.Selection.SelectAll();
            var documentContent = textDocument.Selection.Text;
            textDocument.Selection.MoveToDisplayColumn((int) currentLine, (int) currentDisplayColumn);

            return documentContent;
        }

        private Document NewTextDocument(string documentName)
        {
            _dteRetriever.GetDte().ItemOperations.NewFile(@"General\Text File", documentName);
            return _dteRetriever.GetDte().ActiveDocument;
        }

        private Project RetrieveProjectFromIdentifier(ProjectIdentifier projectIdentifier)
        {
            Project retrievedProject = null;

            foreach (Project project in _dteRetriever.GetDte().Solution.Projects)
                if (project.UniqueName == projectIdentifier.Identifier)
                {
                    retrievedProject = project;
                    break;
                }

            return retrievedProject;
        }
    }
}