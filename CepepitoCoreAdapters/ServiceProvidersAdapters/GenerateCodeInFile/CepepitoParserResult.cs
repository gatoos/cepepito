/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Runtime.InteropServices;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public struct CepepitoParserResult
    {
        public CepepitoParserResult(IntPtr pointer)
        {
            Pointer = pointer;
        }

        public string GetGeneratedTypeName()
        {
            var returnedValue = CepepitoParserAdapter.getGeneratedTypeNameAsCString(this);
            return Marshal.PtrToStringAnsi(returnedValue);
        }

        public override string ToString()
        {
            var returnedValue = CepepitoParserAdapter.getParserResultAsCString(this);
            return Marshal.PtrToStringAnsi(returnedValue);
        }

        public string GetErrorMessage()
        {
            var returnedValue = CepepitoParserAdapter.getErrorMessageAsCString(this);
            return Marshal.PtrToStringAnsi(returnedValue);
        }

        public void Release()
        {
            CepepitoParserAdapter.releaseParserResult(this);
        }

        public IntPtr Pointer;
    }
}