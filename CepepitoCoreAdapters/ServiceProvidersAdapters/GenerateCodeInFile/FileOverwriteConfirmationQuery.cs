﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public class FileOverwriteConfirmationQuery : IFileOverwriteConfirmationQuery
    {
        private readonly IServiceProvider _serviceProvider;

        public FileOverwriteConfirmationQuery(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool ShouldOverwriteFile(string generatedFilename)
        {
            var vsUiShell = _serviceProvider.GetService(typeof(SVsUIShell)) as IVsUIShell;

            return VsShellUtilities.PromptYesNo(
                generatedFilename + " already exists. Do you want to overwrite it ?",
                "Overwrite confirmation",
                OLEMSGICON.OLEMSGICON_QUERY,
                vsUiShell);
        }
    }
}