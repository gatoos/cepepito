﻿using System.Collections.Generic;
using System.Linq;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject;
using CepepitoCoreAdapters.UIAdapters.GenerateCodeInFile;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public class GoogleTestAndGoogleMockIncludeDirectoryRetriever : IRequiredIncludeDirectoriesRetriever
    {
        private readonly TestingLibrariesConfiguration _testingLibrariesConfiguration;

        public GoogleTestAndGoogleMockIncludeDirectoryRetriever(TestingLibrariesConfiguration testingLibrariesConfiguration)
        {
            _testingLibrariesConfiguration = testingLibrariesConfiguration;
        }

        public IEnumerable<string> Retrieve(string projectName)
        {
            var googleTestAndGoogleMockIncludeDirectoryRetrieverWindow =
                new GoogleTestAndGoogleMockIncludeDirectoryRetrieverWindow(projectName, _testingLibrariesConfiguration.GoogleTestIncludeDirectory, _testingLibrariesConfiguration.GoogleMockIncludeDirectory);

            googleTestAndGoogleMockIncludeDirectoryRetrieverWindow.ShowModal();

            if (googleTestAndGoogleMockIncludeDirectoryRetrieverWindow.SelectedGoogleTestIncludeDirectory != null &&
                googleTestAndGoogleMockIncludeDirectoryRetrieverWindow.SelectedGoogleMockIncludeDirectory != null)
            {
                return new List<string>(){ googleTestAndGoogleMockIncludeDirectoryRetrieverWindow.SelectedGoogleTestIncludeDirectory, googleTestAndGoogleMockIncludeDirectoryRetrieverWindow.SelectedGoogleMockIncludeDirectory };
            }

            return Enumerable.Empty<string>();
        }
    }
}
