﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Runtime.InteropServices;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public static class CepepitoParserAdapter
    {
        static CepepitoParserAdapter()
        {
            SetDllDirectory(new AssemblyDirectoryRetriever().RetrieveAssemblyDirectory(typeof(CepepitoParserAdapter)));
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool SetDllDirectory(string lpPathName);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_generateTestSkeletonFromFileWithCursorLocation",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern CepepitoParserResult generateTestSkeletonFromFileWithCursorLocation(string fileToParse,
            string[] unsavedDocumentsFileNames, string[] unsavedDocumentsContent, uint numberOfUnsavedDocuments,
            string[] includeDirectories, uint numberOfIncludeDirectories,
            string[] generatedFileIncludeDirectories, uint genratedFileNumberOfIncludeDirectories,
            uint line, uint column);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_generateMockFromFileWithCursorLocation",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern CepepitoParserResult generateMockFromFileWithCursorLocation(string fileToParse,
            string[] unsavedDocumentsFileNames, string[] unsavedDocumentsContent, uint numberOfUnsavedDocuments,
            string[] includeDirectories, uint numberOfIncludeDirectories,
            string[] generatedFileIncludeDirectories, uint genratedFileNumberOfIncludeDirectories,
            uint line, uint column);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_generateMockSnippetFromFileWithCursorLocation",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern CepepitoParserResult generateMockSnippetFromFileWithCursorLocation(string fileToParse,
            string[] unsavedDocumentsFileNames, string[] unsavedDocumentsContent, uint numberOfUnsavedDocuments,
            string[] includeDirectories, uint numberOfIncludeDirectories, 
            uint line, uint column);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_getGeneratedTypeNameAsCString",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getGeneratedTypeNameAsCString(CepepitoParserResult cepepitoParserResult);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_getParserResultAsCString",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getParserResultAsCString(CepepitoParserResult cepepitoParserResult);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_getErrorMessageAsCString",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getErrorMessageAsCString(CepepitoParserResult cepepitoParserResult);

        [DllImport("CepepitoParser.dll", EntryPoint = "cepepitoparser_releaseParserResult",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern void releaseParserResult(CepepitoParserResult cepepitoParserResult);
    }
}