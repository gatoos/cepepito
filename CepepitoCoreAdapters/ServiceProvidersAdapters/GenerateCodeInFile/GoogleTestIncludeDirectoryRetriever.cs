﻿using System.Collections.Generic;
using System.Linq;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject;
using CepepitoCoreAdapters.UIAdapters.GenerateCodeInFile;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public class GoogleTestIncludeDirectoryRetriever : IRequiredIncludeDirectoriesRetriever
    {
        private readonly TestingLibrariesConfiguration _testingLibrariesConfiguration;

        public GoogleTestIncludeDirectoryRetriever(TestingLibrariesConfiguration testingLibrariesConfiguration)
        {
            _testingLibrariesConfiguration = testingLibrariesConfiguration;
        }

        public IEnumerable<string> Retrieve(string projectName)
        {
            var googleTestIncludeDirectoryRetrieverWindow =
                new GoogleTestIncludeDirectoryRetrieverWindow(projectName, _testingLibrariesConfiguration.GoogleTestIncludeDirectory);

            googleTestIncludeDirectoryRetrieverWindow.ShowModal();
            
            return googleTestIncludeDirectoryRetrieverWindow.SelectedGoogleTestIncludeDirectory != null ? new List<string> { googleTestIncludeDirectoryRetrieverWindow.SelectedGoogleTestIncludeDirectory } : Enumerable.Empty<string>();
        }
    }
}
