﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using EnvDTE;
using Microsoft.VisualStudio.VCProjectEngine;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public class ProjectSelector : IProjectSelector
    {
        private readonly DteRetriever _dteRetriever;

        public ProjectSelector(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
        }

        public (ProjectIdentifier, string) SelectProject()
        {
            var projectsList = (from Project project in _dteRetriever.GetDte().Solution.Projects
                where project.Object is VCProject
                select project).
                ToDictionary(x => x.Name, x => x.UniqueName);

            var projectsListWithFilepath = (from Project project in _dteRetriever.GetDte().Solution.Projects
                    where project.Object is VCProject
                    select project).
                ToDictionary(x => x.Name, x => Path.GetDirectoryName(x.FullName));

            var (selectedProjectName, targetFilename) = UIAdapters.GenerateCodeInFile.ProjectSelector.Browse(projectsListWithFilepath);
            
            if (selectedProjectName == null || !projectsList.ContainsKey(selectedProjectName))
                return (null, null);

            return (new ProjectIdentifier(projectsList[selectedProjectName]), targetFilename);
        }
    }
}