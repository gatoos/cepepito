﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile
{
    public static class IncludeDirectoriesChecker
    {
        public static bool ContainsGoogleTestIncludeDirectory(IEnumerable<string> includeDirectories)
        {
            /*var googleTestInclude = Path.Combine("gtest", "gtest.h");
            return includeDirectories.Any(x => File.Exists(Path.Combine(x, googleTestInclude)));*/
            return true;
        }

        public static bool ContainsGoogleTestAndGoogleMockIncludeDirectory(IEnumerable<string> includeDirectories)
        {
            /*var googleTestAndGoogleMockIncludes =
                new List<string>() {
                    Path.Combine("gtest", "gtest.h"),
                    Path.Combine("gmock", "gmock.h")

                };


            return googleTestAndGoogleMockIncludes.All(x => includeDirectories.Any(y => File.Exists(Path.Combine(y, x))));*/
            return true;
        }
    }
}
