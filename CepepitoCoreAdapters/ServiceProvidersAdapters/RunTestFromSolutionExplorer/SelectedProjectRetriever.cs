﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromSolutionExplorer;
using EnvDTE;
using VisualStudioAPIConnectors;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromSolutionExplorer
{
    public class SelectedProjectRetriever : ISelectedProjectsRetriever
    {
        private readonly DteRetriever _dteRetriever;

        public SelectedProjectRetriever(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
        }

        public IEnumerable<ProjectWithContent> GetSelectedProjects()
        {
            var selectedProjects = _dteRetriever.GetDte().ActiveSolutionProjects;
            var selectedProject = (Project) selectedProjects.GetValue(0);

            if (selectedProject != null)
            {
                var projectCppFilenames = RetrieveCppFilenamesFromProject(selectedProject);

                return new List<ProjectWithContent>
                {
                    new ProjectWithContent(new ProjectIdentifier(selectedProject.UniqueName),
                        projectCppFilenames.Select(File.ReadLines))
                };
            }

            return new List<ProjectWithContent>();
        }

        private IEnumerable<string> RetrieveCppFilenamesFromProject(Project project)
        {
            var cppFilenames = RetrieveCppFilesFromProjectItems(project.ProjectItems);
            foreach (var filename in cppFilenames)
                yield return filename;
        }

        private IEnumerable<string> RetrieveCppFilesFromProjectItems(ProjectItems projectProjectItems)
        {
            var cppFilenames = new List<string>();

            foreach (ProjectItem projectItem in projectProjectItems)
                if (projectItem != null)
                    cppFilenames.AddRange(RetrieveCppFilesFromProjectItem(projectItem));

            return cppFilenames;
        }

        private IEnumerable<string> RetrieveCppFilesFromProjectItem(ProjectItem projectItem)
        {
            var cppFilenames = new List<string>();

            if (projectItem != null)
                if (projectItem.FileCount == 1 &&
                    projectItem.Kind == Constants.vsProjectItemKindPhysicalFile &&
                    projectItem.FileCodeModel != null && projectItem.FileCodeModel.Language ==
                    CodeModelLanguageConstants.vsCMLanguageVC)
                {
                    cppFilenames.Add(projectItem.FileNames[0]);
                }
                else
                {
                    var subProjectItems = projectItem.ProjectItems;
                    if (subProjectItems != null)
                        cppFilenames.AddRange(RetrieveCppFilesFromProjectItems(subProjectItems));
                }

            return cppFilenames;
        }
    }
}