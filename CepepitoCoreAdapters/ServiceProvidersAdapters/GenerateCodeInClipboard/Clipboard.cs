﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInClipboard
{
    public class Clipboard : IClipboard
    {
        private readonly IServiceProvider _serviceProvider;

        public Clipboard(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void Show(string message, string documentContent)
        {
            System.Windows.Clipboard.SetText(documentContent);
            VsShellUtilities.ShowMessageBox(
                _serviceProvider,
                message,
                "",
                OLEMSGICON.OLEMSGICON_INFO,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }
    }
}