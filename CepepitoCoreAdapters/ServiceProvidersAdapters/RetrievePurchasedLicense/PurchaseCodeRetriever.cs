﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.ServiceProviders.Interfaces.RetrievePurchasedLicense;
using CepepitoCoreAdapters.UIAdapters.RetrievePurchasedLicense;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RetrievePurchasedLicense
{
    public class PurchaseCodeRetriever : IPurchaseCodeRetriever
    {
        public string RetrievePurchaseCode()
        {
            var activationControlViewModel = new ActivationControlViewModel();
            var activationDialog = new ActivationDialog(activationControlViewModel);
            activationDialog.ShowDialog();

            if (activationControlViewModel.ApprovedPurchaseCode)
                return activationControlViewModel.PurchaseCode;

            return null;
        }
    }
}