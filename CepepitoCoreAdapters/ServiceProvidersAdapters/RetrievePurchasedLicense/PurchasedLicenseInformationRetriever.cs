﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.RetrievePurchasedLicense;
using LicenseService.ServiceProviders;

namespace CepepitoCoreAdapters.ServiceProvidersAdapters.RetrievePurchasedLicense
{
    public class PurchasedLicenseInformationRetriever : IPurchasedLicenseInformationRetriever
    {
        private readonly LicenseService.EntryPoints.LicenseService _licenseService;

        public PurchasedLicenseInformationRetriever(LicenseService.EntryPoints.LicenseService licenseService)
        {
            _licenseService = licenseService;
        }

        public (LicenseInformation? licenseInformation, string errorMessage) RetrievePurchasedLicenseInformation(
            string purchaseCode)
        {
            try
            {
                var internalErrorMessagesQueue = new InternalMessagesErrorsQueue();
                _licenseService.GenerateFullLicense(purchaseCode, internalErrorMessagesQueue);

                if (internalErrorMessagesQueue.ErrorMessages.Any())
                    return (null, "An error occured during full license generation. Details: " +
                                  internalErrorMessagesQueue.ErrorMessages.FirstOrDefault());

                var retrievedLicense = _licenseService.GetActiveLicense();
                var licenseInformation = new LicenseInformation(
                    _licenseService.GetNumberOfDaysLeftForLicense(retrievedLicense), retrievedLicense.Trial,
                    retrievedLicense.UserName, retrievedLicense.Uid, retrievedLicense.Type.ToString());

                return (licenseInformation, null);
            }
            catch (Exception exception)
            {
                return (null, "An error occured during full license generation. Details: " + exception.Message);
            }
        }

        private class InternalMessagesErrorsQueue : IErrorsMessagesQueue
        {
            public InternalMessagesErrorsQueue()
            {
                ErrorMessages = new List<string>();
            }

            public List<string> ErrorMessages { get; }

            public void AddErrorMessage(string message)
            {
                ErrorMessages.Add(message);
            }

            public void AddWarningMessage(string message)
            {
            }
        }
    }
}