﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Windows;
using Cepepito.Preferences;
using CepepitoCoreAdapters.UIAdapters.RunTests;
using Logging;
using Microsoft.VisualStudio.Shell;

namespace Cepepito
{
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(PackageGuidString)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly",
        Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    [ProvideToolWindow(typeof(TestRunnerToolWindow), Style = VsDockStyle.Tabbed,
        Window = "34E76E81-EE4A-11D0-AE2E-00A0C90FFFC3", Transient = true)]
    //[ProvideOptionPage(typeof(PreferencesDialog), "Cepepito", "Include directories", 0, 0, true)]
    [ProvideBindingPath]
    //[ProvideAutoLoad("ADFC4E64-0397-11D1-9F4E-00A0C911004F")]
    public sealed class CepepitoPackage : Package
    {
        public const string PackageGuidString = "a53f7d5f-dd2c-4962-a797-887beff6538f";

        private readonly ILogger _perAssemblyLogger = new LoggerFactory().BuildPerAssemblyLogger(typeof(CepepitoPackage));

        #region Package Members

        protected override void Initialize()
        {
            try
            {
                _perAssemblyLogger.Info("Initializing extension");
                base.Initialize();
                new ExtensionInitializer().Initialize(this, this, /*(PreferencesDialog)GetDialogPage(typeof(PreferencesDialog))*/ new PreferencesDialog());
#if END_TO_END_TESTS
                cepepitoPackageInternal.CepepitoEndToEndTests =
new EndToEndTests.CepepitoEndToEndTests(new DteRetriever(this), this);
#endif
            }
            catch (Exception)
            {
                MessageBox.Show("Unexpected error occured during initialization",
                    "Cepepito plugin failed to initialize",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        #endregion
    }
}