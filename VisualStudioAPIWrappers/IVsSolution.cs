﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections;

namespace Cepepito.VisualStudioAPIWrappers
{
    public interface IVsSolution
    {
        IVsProject SelectedProject { get; }
        IVsTextDocument CurrentTextDocument { get; }
        IVsProject CurrentDocumentProject { get; }
        ArrayList ProjectsList { get; }
        UnsavedDocuments UnsavedDocuments { get; }

        IVsProject AddProjectFromFile(string createdProjectFullFilename);
        void AddNewDocumentInProject(string documentName, string newDocumentContent, string selectedProject);
        IVsTextDocument OpenFile(string item1);
    }
}