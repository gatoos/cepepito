﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EnvDTE;
using Microsoft.VisualStudio.VCProjectEngine;

namespace Cepepito.VisualStudioAPIWrappers
{
    public class VsProject : IVsProject
    {
        private readonly Project _project;

        public VsProject(Project project)
        {
            _project = project;
        }

        private VCConfiguration ActiveConfiguration
        {
            get
            {
                VCConfiguration activeConfiguration = null;
                var vcProject = (VCProject) _project.Object;
                var activeConfigurationName = _project.ConfigurationManager.ActiveConfiguration.ConfigurationName;
                var activeConfigurationPlatformName = _project.ConfigurationManager.ActiveConfiguration.PlatformName;

                foreach (VCConfiguration vcConfiguration in vcProject.Configurations)
                {
                    VCPlatform platform = vcConfiguration.Platform;
                    if (vcConfiguration.ConfigurationName == activeConfigurationName &&
                        platform.Name == activeConfigurationPlatformName)
                        activeConfiguration = vcConfiguration;
                }

                return activeConfiguration;
            }
        }

        private VCDebugSettings DebugSettings => ActiveConfiguration?.DebugSettings;

        private VCProject VcProject => (VCProject) _project.Object;

        public string[] AdditionalIncludeDirectories
        {
            get
            {
                var includeDirectories = new List<string>();

                var additionalIncludeDirectories = new VsProjectConfiguration(ActiveConfiguration)
                    .AdditionalIncludeDirectories;
                if (additionalIncludeDirectories != "")
                {
                    char[] delimiters = {';'};
                    var subIncludeDirectories = additionalIncludeDirectories.Split(delimiters);
                    foreach (var subIncludeDirectory in subIncludeDirectories)
                        includeDirectories.Add(subIncludeDirectory);
                }

                return includeDirectories.ToArray();
            }
        }

        public string FullName => _project.FullName;

        public string Name
        {
            get => _project.Name;
            set => _project.Name = value;
        }

        public string FileName => _project.FileName;

        public string RootNamespace
        {
            set
            {
                var addedVcProject = (VCProject) _project.Object;
                addedVcProject.RootNamespace = value;
            }
        }

        public IEnumerable<IVsProjectConfiguration> Configurations
        {
            get
            {
                var underlyingVcProject = (VCProject) _project.Object;
                var configurationsCollection = (IVCCollection) underlyingVcProject.Configurations;

                foreach (var configuration in configurationsCollection)
                    yield return new VsProjectConfiguration((VCConfiguration) configuration);
            }
        }

        public ConfigurationTypes ActiveProjectConfigurationType => ActiveConfiguration?.ConfigurationType ??
                                                                    ConfigurationTypes.typeUnknown;

        public bool IsACppProjectWithoutTestRunner
        {
            get
            {
                if (_project.CodeModel.Language == CodeModelLanguageConstants.vsCMLanguageVC)
                {
                    var cppFilenames = RetrieveCppFilesFromProjectItems(_project.ProjectItems);
                    var projectContainsATestRunner = FilesContainTestRunner(cppFilenames);

                    if (projectContainsATestRunner)
                        return false;
                    return true;
                }
                return false;
            }
        }

        public IEnumerable<string> CppFilenames
        {
            get
            {
                var cppFilenames = RetrieveCppFilesFromProjectItems(_project.ProjectItems);
                foreach (var filename in cppFilenames)
                    yield return filename;
            }
        }

        public string DebugCommand => ActiveConfiguration.Evaluate(DebugSettings?.Command ?? "");

        public string DebugPathVariable => ActiveConfiguration.Evaluate(DebugSettings?.Environment ?? "");

        public bool DebugPathVariableMerge => DebugSettings?.EnvironmentMerge ?? false;

        public string DebugWorkingDirectory => ActiveConfiguration.Evaluate(DebugSettings?.WorkingDirectory ?? "");

        public string DebugCommandArguments
        {
            get => DebugSettings?.CommandArguments ?? "";
            set => DebugSettings.CommandArguments = value;
        }

        public bool IsACppProject => _project.Object is VCProject;

        string[] IVsProject.AdditionalIncludeDirectories
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public string Directory => Path.GetDirectoryName(_project.FullName);

        public void AddCppFilesFromSourceProject(IVsProject selectedProject)
        {
            var cppfilesNames = RetrieveCppFilesFromProjectItems(_project.ProjectItems);
            cppfilesNames.ForEach(filename => _project.ProjectItems.AddFromFile(filename));
        }

        public void AddFile(string filename)
        {
            _project.ProjectItems.AddFromFile(filename);
        }

        public void RemoveCppFiles()
        {
            var toBeRemovedProjectItems = RetrieveToBeRemovedElementsFromProjectItems(_project.ProjectItems);
            foreach (var projectItem in toBeRemovedProjectItems)
                projectItem.Remove();
        }

        public void Save()
        {
            _project.Save();
        }

        public void AddFileToProject(IVsTextDocument document)
        {
            _project.ProjectItems.AddFromFile(document.FullName);
            document.Close();
        }

        private bool FilesContainTestRunner(List<string> cppFilenames)
        {
            var filesContainTestRunner = false;

            for (var i = 0; i < cppFilenames.Count && !filesContainTestRunner; ++i)
            {
                var fileContent = File.ReadAllText(cppFilenames[i]);
                filesContainTestRunner = fileContent.Contains("RUN_ALL_TESTS");
            }

            return filesContainTestRunner;
        }

        private List<string> RetrieveCppFilesFromProjectItem(ProjectItem projectItem)
        {
            var cppFilenames = new List<string>();

            if (projectItem != null)
                if (projectItem.FileCount == 1 &&
                    projectItem.Kind == Constants.vsProjectItemKindPhysicalFile &&
                    projectItem.FileCodeModel != null && projectItem.FileCodeModel.Language ==
                    CodeModelLanguageConstants.vsCMLanguageVC)
                {
                    cppFilenames.Add(projectItem.FileNames[0]);
                }
                else
                {
                    var subProjectItems = projectItem.ProjectItems;
                    if (subProjectItems != null)
                        cppFilenames.AddRange(RetrieveCppFilesFromProjectItems(subProjectItems));
                }

            return cppFilenames;
        }

        private List<string> RetrieveCppFilesFromProjectItems(ProjectItems projectItems)
        {
            var cppFilenames = new List<string>();

            foreach (ProjectItem projectItem in projectItems)
                if (projectItem != null)
                    cppFilenames.AddRange(RetrieveCppFilesFromProjectItem(projectItem));

            return cppFilenames;
        }

        private static bool FileIsCppSourceFile(string filename)
        {
            var cppFilesExtensions = new List<string> {".cpp", ".hpp", ".c", ".h"};
            return cppFilesExtensions.Any(filename.EndsWith);
        }

        private List<ProjectItem> RetrieveToBeRemovedElementsFromProjectItems(ProjectItems projectItems)
        {
            var toBeRemovedProjectItems = new List<ProjectItem>();

            foreach (ProjectItem projectItem in projectItems)
                if (projectItem != null)
                {
                    var projectItemLabel = projectItem.Name;
                    if (projectItem.Kind == Constants.vsProjectItemKindPhysicalFile &&
                        FileIsCppSourceFile(projectItemLabel))
                    {
                        toBeRemovedProjectItems.Add(projectItem);
                    }
                    else
                    {
                        var subProjectItems = projectItem.ProjectItems;
                        if (subProjectItems != null)
                            toBeRemovedProjectItems.AddRange(
                                RetrieveToBeRemovedElementsFromProjectItems(subProjectItems));
                    }
                }

            return toBeRemovedProjectItems;
        }
    }
}