﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace Cepepito.VisualStudioAPIWrappers
{
    public class UnsavedDocuments
    {
        public string[] UnsavedDocumentsNames { get; set; }
        public string[] UnsavedDocumentsContents { get; set; }
    }
}