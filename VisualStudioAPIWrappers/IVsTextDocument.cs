﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;

namespace Cepepito.VisualStudioAPIWrappers
{
    public interface IVsTextDocument
    {
        string Language { get; }
        string LineContent { get; }
        string Content { get; set; }
        string FullName { get; }
        CursorPosition CursorPosition { get; }

        void Save(string fullFilename);
        void Close();
        void GotoLine(int lineNumber);
    }
}