﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using EnvDTE;

namespace Cepepito.VisualStudioAPIWrappers
{
    internal class VsTextDocumentBuilder : IVsTextDocumentBuilder
    {
        public IVsTextDocument Build(Document document)
        {
            return new VsTextDocument(document);
        }
    }
}