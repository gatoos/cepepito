﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;

namespace Cepepito.VisualStudioAPIWrappers
{
    public interface IVsProjectConfigurations : IEnumerable<IVsProjectConfiguration>
    {
    }
}