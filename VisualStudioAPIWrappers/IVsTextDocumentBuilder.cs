﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using EnvDTE;

namespace Cepepito.VisualStudioAPIWrappers
{
    public interface IVsTextDocumentBuilder
    {
        IVsTextDocument Build(Document textDocument);
    }
}