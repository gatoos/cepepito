﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using EnvDTE;

namespace Cepepito.VisualStudioAPIWrappers
{
    /*public class VsSolution : IVsSolution
    {
        private readonly IDteRetriever _dteRetriever;
        private readonly IVsProjectBuilder _vsProjectBuilder;
        private readonly IVsTextDocumentBuilder _vsTextDocumentBuilder;

        public VsSolution(IDteRetriever dteRetriever, IVsProjectBuilder vsProjectBuilder,
            IVsTextDocumentBuilder vsTextDocumentBuilder)
        {
            _dteRetriever = dteRetriever;
            _vsProjectBuilder = vsProjectBuilder;
            _vsTextDocumentBuilder = vsTextDocumentBuilder;
        }

        public IVsProject SelectedProject
        {
            get
            {
                var selectedProjects = (Array) _dteRetriever.GetDte().ActiveSolutionProjects;
                var selectedProject = (Project) selectedProjects.GetValue(0);
                return _vsProjectBuilder.Build(selectedProject);
            }
        }

        public IVsTextDocument CurrentTextDocument => _vsTextDocumentBuilder.Build(
            _dteRetriever.GetDte().ActiveDocument);

        public IVsProject CurrentDocumentProject => _vsProjectBuilder.Build(_dteRetriever.GetDte().ActiveDocument
            .ProjectItem.ContainingProject);

        public ArrayList ProjectsList
        {
            get
            {
                var solutionProjectsLabel = new ArrayList();

                foreach (Project project in _dteRetriever.GetDte().Solution.Projects)
                    if (_vsProjectBuilder.Build(project).IsACppProject)
                        solutionProjectsLabel.Add(project.Name);

                return solutionProjectsLabel;
            }
        }

        public UnsavedDocuments UnsavedDocuments
        {
            get
            {
                var unsavedDocumentsNames = new List<string>();
                var unsavedDocumentsContent = new List<string>();

                foreach (Document document in _dteRetriever.GetDte().Documents)
                    if (document.Language == "C/C++")
                        if (!document.Saved)
                        {
                            var documentFileName = document.ProjectFullName;

                            var textDocument = (TextDocument) document.Object(null);
                            var activePoint = textDocument.Selection.ActivePoint;
                            var currentLine = activePoint.Line > 0 ? (uint) activePoint.Line : 0;
                            var currentDisplayColumn = activePoint.DisplayColumn > 0
                                ? (uint) activePoint.DisplayColumn
                                : 0;

                            textDocument.Selection.SelectAll();
                            var documentContent = textDocument.Selection.Text;
                            textDocument.Selection.MoveToDisplayColumn((int) currentLine, (int) currentDisplayColumn);

                            unsavedDocumentsNames.Add(documentFileName);
                            unsavedDocumentsContent.Add(documentContent);
                        }

                var returnValue = new UnsavedDocuments
                {
                    UnsavedDocumentsNames = unsavedDocumentsNames.ToArray(),
                    UnsavedDocumentsContents = unsavedDocumentsContent.ToArray()
                };
                return returnValue;
            }
        }

        public void AddNewDocumentInProject(string documentName, string newDocumentContent, string selectedProject)
        {
            var targetProject = RetrieveProjectFromLabel(selectedProject);

            if (targetProject?.Directory?.Length != null)
            {
                var toBeCreatedDocumentFilename = Path.Combine(targetProject.Directory, documentName);
                if (File.Exists(toBeCreatedDocumentFilename))
                    throw new Exception("The file " + documentName + " already exists");

                var newDocument = NewTextDocument(documentName);
                newDocument.Content = newDocumentContent;

                newDocument.Save(toBeCreatedDocumentFilename);
                targetProject.AddFileToProject(newDocument);
                _dteRetriever.GetDte().ItemOperations.OpenFile(newDocument.ProjectFullName);
            }
        }

        public IVsProject AddProjectFromFile(string createdProjectFullFilename)
        {
            var addedProject = _dteRetriever.GetDte().Solution.AddFromFile(createdProjectFullFilename);
            return _vsProjectBuilder.Build(addedProject);
        }

        public IVsTextDocument OpenFile(string filename)
        {
            var window = _dteRetriever.GetDte().ItemOperations.OpenFile(filename);
            window.Activate();
            return _vsTextDocumentBuilder.Build(window.Document);
        }

        private IVsProject RetrieveProjectFromLabel(string projectName)
        {
            Project retrievedProject = null;

            foreach (Project project in _dteRetriever.GetDte().Solution.Projects)
                if (project.Name == projectName)
                {
                    retrievedProject = project;
                    break;
                }

            if (retrievedProject != null)
                return _vsProjectBuilder.Build(retrievedProject);
            return null;
        }

        private IVsTextDocument NewTextDocument(string filename)
        {
            _dteRetriever.GetDte().ItemOperations.NewFile(@"General\Text File", filename);
            return CurrentTextDocument;
        }
    }*/
}