﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using EnvDTE;

namespace Cepepito.VisualStudioAPIWrappers
{
    internal class VsTextDocument : IVsTextDocument
    {
        private readonly Document _document;

        public VsTextDocument(Document document)
        {
            _document = document;
        }

        private TextDocument TextDocument => (TextDocument) _document.Object(null);

        public string Language => _document.Language;

        public string LineContent
        {
            get
            {
                TextDocument.Selection.SelectLine();
                return TextDocument.Selection.Text;
            }
        }

        public string Content
        {
            get
            {
                var activePoint = TextDocument.Selection.ActivePoint;
                var currentLine = activePoint.Line > 0 ? (uint) activePoint.Line : 0;
                var currentDisplayColumn = activePoint.DisplayColumn > 0 ? (uint) activePoint.DisplayColumn : 0;
                TextDocument.Selection.SelectAll();
                var returnValue = TextDocument.Selection.Text;
                TextDocument.Selection.MoveToDisplayColumn((int) currentLine, (int) currentDisplayColumn);

                return returnValue;
            }

            set
            {
                TextDocument.Selection.SelectAll();
                TextDocument.Selection.Delete();
                TextDocument.Selection.Insert(value);
            }
        }

        public string FullName => _document.FullName;

        public CepepitoCore.Core.Entities.CursorPosition CursorPosition
        {
            get
            {
                var returnValue = new CepepitoCore.Core.Entities.CursorPosition();
                var activePoint = TextDocument.Selection.ActivePoint;
                returnValue.Line = activePoint.Line > 0 ? (uint) activePoint.Line : 0;
                returnValue.Column = activePoint.VirtualCharOffset > 0 ? (uint) activePoint.VirtualCharOffset : 0;

                return returnValue;
            }
        }

        public void Save(string fullFilename)
        {
            _document.Save(fullFilename);
        }

        public void Close()
        {
            _document.Close();
        }

        public void GotoLine(int lineNumber)
        {
            TextDocument.Selection.GotoLine(lineNumber);
        }
    }
}