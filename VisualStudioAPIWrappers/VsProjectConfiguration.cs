﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using Microsoft.VisualStudio.VCProjectEngine;

namespace Cepepito.VisualStudioAPIWrappers
{
    public class VsProjectConfiguration : IVsProjectConfiguration
    {
        private readonly VCConfiguration _configuration;

        public VsProjectConfiguration(VCConfiguration configuration)
        {
            _configuration = configuration;
        }

        private VCCLCompilerTool CompilerTools
        {
            get
            {
                var toolsCollection = (IVCCollection) _configuration.Tools;
                return toolsCollection.Item("VCCLCompilerTool");
            }
        }

        private VCLinkerTool LinkerTool
        {
            get
            {
                var toolsCollection = (IVCCollection) _configuration.Tools;
                return toolsCollection.Item("VCLinkerTool");
            }
        }

        public string AdditionalIncludeDirectories
        {
            get => CompilerTools.AdditionalIncludeDirectories;
            set => CompilerTools.AdditionalIncludeDirectories = value;
        }

        public runtimeLibraryOption RuntimeLibrary
        {
            get => CompilerTools.RuntimeLibrary;
            set => CompilerTools.RuntimeLibrary = value;
        }

        public string AdditionalDependencies
        {
            get => LinkerTool.AdditionalDependencies;
            set => LinkerTool.AdditionalDependencies = value;
        }

        public string PreprocessorDefinitions
        {
            get => CompilerTools.PreprocessorDefinitions;
            set => CompilerTools.PreprocessorDefinitions = value;
        }

        public ConfigurationTypes Type
        {
            get => _configuration.ConfigurationType;
            set => _configuration.ConfigurationType = value;
        }

        public string GeneratedStaticLibraryPath => _configuration?.PrimaryOutput ?? "";
    }
}