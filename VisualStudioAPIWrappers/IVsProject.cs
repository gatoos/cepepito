﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using Microsoft.VisualStudio.VCProjectEngine;

namespace Cepepito.VisualStudioAPIWrappers
{
    public interface IVsProject
    {
        string FullName { get; }
        string Name { get; set; }
        string FileName { get; }
        string RootNamespace { set; }
        string Directory { get; }
        IEnumerable<IVsProjectConfiguration> Configurations { get; }
        ConfigurationTypes ActiveProjectConfigurationType { get; }
        bool IsACppProjectWithoutTestRunner { get; }
        bool IsACppProject { get; }
        IEnumerable<string> CppFilenames { get; }
        string DebugCommand { get; }
        string DebugPathVariable { get; }
        bool DebugPathVariableMerge { get; }
        string DebugWorkingDirectory { get; }
        string DebugCommandArguments { get; set; }
        string[] AdditionalIncludeDirectories { get; set; }

        void Save();
        void AddFile(string filename);
        void RemoveCppFiles();
        void AddCppFilesFromSourceProject(IVsProject selectedProject);

        void AddFileToProject(IVsTextDocument document);
    }
}