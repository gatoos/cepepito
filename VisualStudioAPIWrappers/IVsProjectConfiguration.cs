﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using Microsoft.VisualStudio.VCProjectEngine;

namespace Cepepito.VisualStudioAPIWrappers
{
    public interface IVsProjectConfiguration
    {
        string AdditionalIncludeDirectories { get; set; }
        runtimeLibraryOption RuntimeLibrary { get; set; }
        string AdditionalDependencies { get; set; }
        string PreprocessorDefinitions { get; set; }
        ConfigurationTypes Type { get; set; }
        string GeneratedStaticLibraryPath { get; }
    }
}