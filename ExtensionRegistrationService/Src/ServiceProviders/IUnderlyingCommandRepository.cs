﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using ExtensionRegistrationService.Core.Entities;
using ExtensionRegistrationService.EntryPoints;

namespace ExtensionRegistrationService.ServiceProviders
{
    public interface IUnderlyingCommandRepository
    {
        IDeactivableCommand AddCommand(CommandIdentifier commandIdentifier, Action executableCommand);
    }
}