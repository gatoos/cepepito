﻿namespace ExtensionRegistrationService.ServiceProviders
{
    public interface IErrorsHandler
    {
        void Notify(string errorMessage);
    }
}