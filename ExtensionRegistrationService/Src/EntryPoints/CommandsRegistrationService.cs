﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromSolutionExplorer;
using CepepitoCore.UI;
using ExtensionRegistrationService.Core.Entities;
using ExtensionRegistrationService.ServiceProviders;
using Logging;
using IErrorsMessagesQueue = CepepitoCore.ServiceProviders.Interfaces.Common.IErrorsMessagesQueue;

namespace ExtensionRegistrationService.EntryPoints
{
    public class CommandsRegistrationService
    {
        private Func<bool> IsActiveLicenseValid { get; }
        private ILicenseUpdateNotifier LicenseUpdateNotifier { get; }
        private Infrastructure Infrastructure { get; }
        private DeactivableCommandsRepository CepepitoCommandsRepository { get; }
        private DeactivableCommandsRepository TestRunnerCommandsRepository { get; }
        private Action AboutDialogDisplayer { get; }

        public static readonly CommandIdentifier RunTestCommandIdentifier = new CommandIdentifier(0x10FF);
        public static readonly CommandIdentifier DebugTestCommandIdentifier = new CommandIdentifier(0x11FF);
        public static readonly CommandIdentifier RunAllTestsInFileCommandIdentifier = new CommandIdentifier(0x12FF);
        public static readonly CommandIdentifier DebugAllTestsInFileCommandIdentifier = new CommandIdentifier(0x13FF);
        public static readonly CommandIdentifier GenerateMockInClipboardCommandIdentifier = new CommandIdentifier(0X14FF);
        public static readonly CommandIdentifier GenerateMockFileCommandIdentifier = new CommandIdentifier(0x15FF);
        public static readonly CommandIdentifier GenerateTestForClassCommandIdentifier = new CommandIdentifier(0x16FF);
        public static readonly CommandIdentifier RunTestsFromSolutionExplorerCommandIdentifier = new CommandIdentifier(0x17FF);
        public static readonly CommandIdentifier DebugTestsFromSolutionExplorerCommandIdentifier = new CommandIdentifier(0x18FF);
        public static readonly CommandIdentifier CreateTestProjectCommandId = new CommandIdentifier(0X19FF);
        public static readonly CommandIdentifier DisplayAboutControlCommandId = new CommandIdentifier(0x1AFF);
        public static readonly CommandIdentifier ShowTestRunnerWindowCommandIdentifier = new CommandIdentifier(256);

        public CommandsRegistrationService(Func<bool> isActiveLicenseValid,
            ILicenseUpdateNotifier licenseUpdateNotifier,
            Infrastructure infrastructure,
            IUnderlyingCommandRepository cepepitoCommandsRepository,
            IUnderlyingCommandRepository testRunnerCommandsRepository,
            Action aboutDialogDisplayer)
        {
            IsActiveLicenseValid = isActiveLicenseValid;
            LicenseUpdateNotifier = licenseUpdateNotifier;
            Infrastructure = infrastructure;
            CepepitoCommandsRepository = new DeactivableCommandsRepository(cepepitoCommandsRepository, Infrastructure.ErrorsHandler);
            TestRunnerCommandsRepository = new DeactivableCommandsRepository(testRunnerCommandsRepository, Infrastructure.ErrorsHandler);
            AboutDialogDisplayer = aboutDialogDisplayer;

            LicenseUpdateNotifier.ActiveLicenseModified += (sender, args) => { UpdateCommandsActivationStatus(); };
        }

        private void UpdateCommandsActivationStatus()
        {
            if (!IsActiveLicenseValid())
            {
                CepepitoCommandsRepository.DeactivateCommands();
                TestRunnerCommandsRepository.DeactivateCommands();
            }
            else
            {
                CepepitoCommandsRepository.ActivateCommands();
                TestRunnerCommandsRepository.ActivateCommands();
            }
        }

        public enum CommandExecutionStatus
        {
            SuccessfullyExecuted,
            FailedToExecute,
            CommandDisabled,
            NonExistingCommand
        }

        public void RegisterCommands()
        {
            RegisterCepepitoCommands();
            RegisterTestRunnerCommands();

            if (!IsActiveLicenseValid())
            {
                CepepitoCommandsRepository.DeactivateCommands();
                TestRunnerCommandsRepository.DeactivateCommands();
            }
        }

        private void RegisterTestRunnerCommands()
        {
            TestRunnerCommandsRepository.RegisterCommand(
                ShowTestRunnerWindowCommandIdentifier, Infrastructure.TestRunnerToolWindowDisplay);
        }

        private void RegisterCepepitoCommands()
        {
            CepepitoCommandsRepository.RegisterCommand(RunTestCommandIdentifier,
                () => new RunTestFromCodeEditorUseCaseController(
                        Infrastructure.TestRunnerToolWindowControlModel,
                        Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.ProjectOutputRunner,
                        Infrastructure.DialogBoxErrorsMessageNotifier, Infrastructure.LoggerFactory)
                    .LaunchTestAtCursorPosition());

            CepepitoCommandsRepository.RegisterCommand(DebugTestCommandIdentifier,
                    () => new RunTestFromCodeEditorUseCaseController(Infrastructure.TestRunnerToolWindowControlModel,
                            Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.ProjectOutputDebugger,
                            Infrastructure.DialogBoxErrorsMessageNotifier, Infrastructure.LoggerFactory)
                        .LaunchTestAtCursorPosition());

            CepepitoCommandsRepository.RegisterCommand(RunAllTestsInFileCommandIdentifier,
                
                    () =>
                    new RunTestFromCodeEditorUseCaseController(Infrastructure.TestRunnerToolWindowControlModel,
                            Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.ProjectOutputRunner,
                                Infrastructure.DialogBoxErrorsMessageNotifier, Infrastructure.LoggerFactory)
                        .LaunchTestAtCursorPosition());

            CepepitoCommandsRepository.RegisterCommand(DebugAllTestsInFileCommandIdentifier,
                    () =>
                    new RunTestFromCodeEditorUseCaseController(Infrastructure.TestRunnerToolWindowControlModel,
                            Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.ProjectOutputDebugger,
                                Infrastructure.DialogBoxErrorsMessageNotifier, Infrastructure.LoggerFactory)
                        .LaunchTestAtCursorPosition());

            CepepitoCommandsRepository.RegisterCommand(GenerateTestForClassCommandIdentifier,
                () => LicenseCheckedCommand.Execute(IsActiveLicenseValid, Infrastructure.DialogBoxErrorsMessageNotifier,
                    () => new GenerateCodeInFileUseCaseController(
                        Infrastructure.ProjectSelector,
                        Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.Solution,
                        Infrastructure.GoogleTestIncludeDirectoryPresenceChecker,
                        Infrastructure.GoogleTestIncludeDirectoryRetriever,
                        Infrastructure.GenerateTestSkeletonFromFileWithCursorLocation,
                        Infrastructure.DialogBoxErrorsMessageNotifier, "cpp", Infrastructure.FileOverwriteConfirmationQuery, Infrastructure.TestGenerationProgressDialogBox).Execute()), DeactivableStatus.Deactivable);

            CepepitoCommandsRepository.RegisterCommand(GenerateMockFileCommandIdentifier,
                () => LicenseCheckedCommand.Execute(IsActiveLicenseValid, Infrastructure.DialogBoxErrorsMessageNotifier,
                    () => new GenerateCodeInFileUseCaseController(
                        Infrastructure.ProjectSelector,
                        Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.Solution,
                        Infrastructure.GoogleMockIncludeDirectoryPresenceChecker,
                        Infrastructure.GoogleMockIncludeDirectoryRetriever,
                        Infrastructure.GenerateMockFromFileWithCursorLocation,
                        Infrastructure.DialogBoxErrorsMessageNotifier, "hpp", Infrastructure.FileOverwriteConfirmationQuery, Infrastructure.MockGenerationProgressDialogBox).Execute()), DeactivableStatus.Deactivable);

            CepepitoCommandsRepository.RegisterCommand(GenerateMockInClipboardCommandIdentifier,
                () => LicenseCheckedCommand.Execute(IsActiveLicenseValid, Infrastructure.DialogBoxErrorsMessageNotifier,
                    () =>
                        new GenerateCodeInClipboardUseCaseController(
                            Infrastructure.CurrentDocumentInfoRetriever, Infrastructure.Solution,
                            Infrastructure.GenerateMockSnippetFromFileWithCursorLocation,
                            Infrastructure.Clipboard,
                            Infrastructure.DialogBoxErrorsMessageNotifier,
                            Infrastructure.MockGenerationProgressDialogBox).Execute()), DeactivableStatus.Deactivable);

            CepepitoCommandsRepository.RegisterCommand(
                RunTestsFromSolutionExplorerCommandIdentifier,
                () =>
                {
                    new RunTestsFromSolutionExplorerUseCaseController(
                            Infrastructure.TestRunnerToolWindowControlModel,
                            Infrastructure.ProjectOutputRunner, Infrastructure.SelectedProjectRetriever,
                            Infrastructure.DialogBoxErrorsMessageNotifier, Infrastructure.LoggerFactory)
                        .Execute();
                });

            CepepitoCommandsRepository.RegisterCommand(
                DebugTestsFromSolutionExplorerCommandIdentifier,
                    () =>
                    {
                        new RunTestsFromSolutionExplorerUseCaseController(
                            Infrastructure.TestRunnerToolWindowControlModel,
                            Infrastructure.ProjectOutputDebugger, Infrastructure.SelectedProjectRetriever,
                            Infrastructure.DialogBoxErrorsMessageNotifier, Infrastructure.LoggerFactory)
                        .Execute();

                    });

            CepepitoCommandsRepository.RegisterCommand(
                CreateTestProjectCommandId,
                () => LicenseCheckedCommand.Execute(IsActiveLicenseValid, Infrastructure.DialogBoxErrorsMessageNotifier,
                    () => new CreateTestProjectUseCase(Infrastructure.SelectedProjectPropertiesRetriever, 
                            Infrastructure.SolutionProjectsRepository, Infrastructure.TestingLibrariesConfiguration, 
                            Infrastructure.VisualStudioVersion, Infrastructure.DialogBoxErrorsMessageNotifier)
                        .Execute()), DeactivableStatus.Deactivable);

            CepepitoCommandsRepository.RegisterCommand(DisplayAboutControlCommandId, AboutDialogDisplayer);
        }
    }

    

    public interface ILicenseUpdateNotifier
    {
        event EventHandler ActiveLicenseModified;
    }

    public struct Infrastructure
    {
        public Infrastructure(IErrorsMessagesQueue dialogBoxErrorsMessageNotifier,
            IErrorsHandler errorsHandler,
            ITestRunnerViewerViewModel testRunnerToolWindowControlModel,
            ICurrentDocumentInfoRetriever currentDocumentInfoRetriever, IProjectOutputLauncher projectOutputRunner,
            IProjectOutputLauncher projectOutputDebugger, IProjectSelector projectSelector, ISolution solution,
            Func<ParserInput, (string typeName, string content, string errorMessage)>
                generateTestSkeletonFromFileWithCursorLocation,
            Func<ParserInput, (string typeName, string content, string errorMessage)>
                generateMockFromFileWithCursorLocation,
            Func<ParserInput, (string typeName, string content, string errorMessage)>
                generateMockSnippetFromFileWithCursorLocation,
            IClipboard clipboard, ISelectedProjectsRetriever selectedProjectRetriever,
            Action testRunnerToolWindowDisplay,
            string visualStudioVersion, ISelectedProjectPropertiesRetriever selectedProjectPropertiesRetriever,
            ISolutionProjectsRepository solutionProjectsRepository,
            ITestingLibrariesConfiguration testingLibrariesConfiguration,
            IFileOverwriteConfirmationQuery fileOverwriteConfirmationQuery,
            IProgressDialogBox mockGenerationProgressDialogBox, IProgressDialogBox testGenerationProgressDialogBox,
            Func<IEnumerable<string>, bool> googleTestIncludeDirectoryPresenceChecker,
            IRequiredIncludeDirectoriesRetriever googleTestIncludeDirectoryRetriever,
            Func<IEnumerable<string>, bool> googleMockIncludeDirectoryPresenceChecker,
            IRequiredIncludeDirectoriesRetriever googleMockIncludeDirectoryRetriever,
            ILoggerFactory loggerFactory)
        {
            DialogBoxErrorsMessageNotifier = dialogBoxErrorsMessageNotifier;
            ErrorsHandler = errorsHandler;
            TestRunnerToolWindowControlModel = testRunnerToolWindowControlModel;
            CurrentDocumentInfoRetriever = currentDocumentInfoRetriever;
            ProjectOutputRunner = projectOutputRunner;
            ProjectOutputDebugger = projectOutputDebugger;
            ProjectSelector = projectSelector;
            Solution = solution;
            GenerateTestSkeletonFromFileWithCursorLocation = generateTestSkeletonFromFileWithCursorLocation;
            GenerateMockFromFileWithCursorLocation = generateMockFromFileWithCursorLocation;
            GenerateMockSnippetFromFileWithCursorLocation = generateMockSnippetFromFileWithCursorLocation;
            Clipboard = clipboard;
            SelectedProjectRetriever = selectedProjectRetriever;
            TestRunnerToolWindowDisplay = testRunnerToolWindowDisplay;
            VisualStudioVersion = visualStudioVersion;
            SelectedProjectPropertiesRetriever = selectedProjectPropertiesRetriever;
            SolutionProjectsRepository = solutionProjectsRepository;
            TestingLibrariesConfiguration = testingLibrariesConfiguration;
            FileOverwriteConfirmationQuery = fileOverwriteConfirmationQuery;
            MockGenerationProgressDialogBox = mockGenerationProgressDialogBox;
            TestGenerationProgressDialogBox = testGenerationProgressDialogBox;
            GoogleMockIncludeDirectoryPresenceChecker = googleMockIncludeDirectoryPresenceChecker;
            GoogleTestIncludeDirectoryPresenceChecker = googleTestIncludeDirectoryPresenceChecker;
            GoogleTestIncludeDirectoryRetriever = googleTestIncludeDirectoryRetriever;
            GoogleMockIncludeDirectoryRetriever = googleMockIncludeDirectoryRetriever;
            LoggerFactory = loggerFactory;
        }

        public IErrorsMessagesQueue DialogBoxErrorsMessageNotifier { get; }
        public IErrorsHandler ErrorsHandler { get; }
        public ITestRunnerViewerViewModel TestRunnerToolWindowControlModel { get; }
        public ICurrentDocumentInfoRetriever CurrentDocumentInfoRetriever { get; }
        public IProjectOutputLauncher ProjectOutputRunner { get; }
        public IProjectOutputLauncher ProjectOutputDebugger { get; }
        public IProjectSelector ProjectSelector { get; }
        public ISolution Solution { get; }
        public Func<ParserInput, (string typeName, string content, string errorMessage)> GenerateTestSkeletonFromFileWithCursorLocation { get; }
        public Func<ParserInput, (string typeName, string content, string errorMessage)> GenerateMockFromFileWithCursorLocation { get; }
        public Func<ParserInput, (string typeName, string content, string errorMessage)> GenerateMockSnippetFromFileWithCursorLocation { get; } 
        public IClipboard Clipboard { get; }
        public ISelectedProjectsRetriever SelectedProjectRetriever { get; }
        public Action TestRunnerToolWindowDisplay { get; }
        public string VisualStudioVersion { get; }
        public ISelectedProjectPropertiesRetriever SelectedProjectPropertiesRetriever { get; }
        public ISolutionProjectsRepository SolutionProjectsRepository { get; }
        public ITestingLibrariesConfiguration TestingLibrariesConfiguration { get; }
        public IFileOverwriteConfirmationQuery FileOverwriteConfirmationQuery { get; }
        public IProgressDialogBox MockGenerationProgressDialogBox { get; }
        public IProgressDialogBox TestGenerationProgressDialogBox { get; }
        public Func<IEnumerable<string>, bool> GoogleTestIncludeDirectoryPresenceChecker { get; }
        public Func<IEnumerable<string>, bool> GoogleMockIncludeDirectoryPresenceChecker { get; }
        public IRequiredIncludeDirectoriesRetriever GoogleTestIncludeDirectoryRetriever { get; }
        public IRequiredIncludeDirectoriesRetriever GoogleMockIncludeDirectoryRetriever { get; }
        public ILoggerFactory LoggerFactory { get; }
    }

    internal static class LicenseCheckedCommand
    {
        public static async Task Execute(Func<bool> isActiveLicenseValid,
            IErrorsMessagesQueue errorMessageNotifier, Func<Task> underlyingCommandAction)
        {
            if (isActiveLicenseValid())
            {
                await underlyingCommandAction();
                return;
            }

            errorMessageNotifier.AddErrorMessage("Trial period expired. Please purchase a license");
        }
    }
}