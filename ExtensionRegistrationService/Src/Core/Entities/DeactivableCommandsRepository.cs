﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExtensionRegistrationService.ServiceProviders;
using Logging;

namespace ExtensionRegistrationService.Core.Entities
{
    public class DeactivableCommandsRepository
    {
        private readonly IErrorsHandler _errorsHandler;
        private Dictionary<CommandIdentifier, IDeactivableCommand> Commands { get; }
        private readonly ILogger _logger = new LoggerFactory().BuildPerAssemblyLogger(typeof(DeactivableCommandsRepository));

        public DeactivableCommandsRepository(IUnderlyingCommandRepository underlyingCommandRepository, IErrorsHandler errorsHandler)
        {
            _errorsHandler = errorsHandler;
            UnderlyingCommandRepository = underlyingCommandRepository ??
                                          throw new ArgumentNullException(nameof(underlyingCommandRepository));
            Commands = new Dictionary<CommandIdentifier, IDeactivableCommand>();
            DeactivableCommands = new List<CommandIdentifier>();
        }

        private List<CommandIdentifier> DeactivableCommands { get; }

        private IUnderlyingCommandRepository UnderlyingCommandRepository { get; }

        public bool HasCommand(CommandIdentifier commandIdentifier)
        {
            return Commands.ContainsKey(commandIdentifier);
        }

        public void RegisterCommand(CommandIdentifier commandIdentifier, Func<Task> executableCommand)
        {
            RegisterCommand(commandIdentifier, executableCommand, DeactivableStatus.NotDeactivable);
        }

        public bool CommandIsActive(CommandIdentifier commandIdentifier)
        {
            return HasCommand(commandIdentifier) && Commands[commandIdentifier].Active;
        }

        private async Task ExecuteAndCheckForExceptions(CommandIdentifier commandIdentifier, Func<Task> executableCommand)
        {
            try
            {
                await executableCommand();
            }
            catch (TestCaseExecutionException testCaseExecutionException)
            {
                _logger.Error(new
                {
                    Description = "A application level error occured when executing command",
                    CommandIdentifier = commandIdentifier,
                    Exception = testCaseExecutionException
                });

                _errorsHandler.Notify(testCaseExecutionException.Message);
            }
            catch (Exception exception)
            {
                _logger.Error(new
                {
                    Description = "An exception occured when executing command",
                    CommandIdentifier = commandIdentifier,
                    Exception = exception
                });
            }

        }

        public void RegisterCommand(CommandIdentifier commandIdentifier, Func<Task> executableCommand,
            DeactivableStatus deactivableCommand)
        {
            if (HasCommand(commandIdentifier)) return;

            var addedCommand = UnderlyingCommandRepository.AddCommand(commandIdentifier, () =>
                {
#pragma warning disable 4014
                    ExecuteAndCheckForExceptions(commandIdentifier, executableCommand);
#pragma warning restore 4014
                });

            Commands.Add(commandIdentifier, addedCommand);
            if (deactivableCommand == DeactivableStatus.Deactivable) DeactivableCommands.Add(commandIdentifier);
        }

        public void RegisterCommand(CommandIdentifier commandIdentifier, Action executableCommand)
        {
            RegisterCommand(commandIdentifier, executableCommand, DeactivableStatus.NotDeactivable);
        }

        public void RegisterCommand(CommandIdentifier commandIdentifier, Action executableCommand,
            DeactivableStatus deactivableCommand)
        {
            if (HasCommand(commandIdentifier)) return;

            var addedCommand = UnderlyingCommandRepository.AddCommand(commandIdentifier, () =>
            {
                try
                {
                    executableCommand();
                }
                catch (TestCaseExecutionException testCaseExecutionException)
                {
                    _logger.Error(new
                    {
                        Description = "A application level error occured when executing command",
                        CommandIdentifier = commandIdentifier,
                        Exception = testCaseExecutionException
                    });

                    _errorsHandler.Notify(testCaseExecutionException.Message);
                }
                catch (Exception exception)
                {
                    _logger.Error(new
                    {
                        Description = "An exception occured when executing command",
                        CommandIdentifier = commandIdentifier,
                        Exception = exception
                    });
                }
            });

            Commands.Add(commandIdentifier, addedCommand);
            if (deactivableCommand == DeactivableStatus.Deactivable) DeactivableCommands.Add(commandIdentifier);
        }

        public void DeactivateCommands()
        {
            DeactivableCommands.ForEach(DeactivateCommand);
        }

        public void ActivateCommands()
        {
            DeactivableCommands.ForEach(ActivateCommand);
        }

        private void DeactivateCommand(CommandIdentifier commandIdentifier)
        {
            if (Commands.ContainsKey(commandIdentifier))
            {
                Commands[commandIdentifier].Active = false;
            }
        }

        private void ActivateCommand(CommandIdentifier commandIdentifier)
        {
            if (Commands.ContainsKey(commandIdentifier))
            {
                Commands[commandIdentifier].Active = true;
            }
        }
    }
}