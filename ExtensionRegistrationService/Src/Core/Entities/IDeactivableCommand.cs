﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace ExtensionRegistrationService.Core.Entities
{
    public interface IDeactivableCommand
    {
        bool Active { get; set; }
    }
}