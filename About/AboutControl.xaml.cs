﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Windows;
using Microsoft.VisualStudio.PlatformUI;

namespace Cepepito.About
{
    /// <summary>
    ///     Logique d'interaction pour AboutWpfControl.xaml
    /// </summary>
    public partial class AboutControl
    {
        private DialogWindow ParentDialog { get; }
        
        public AboutControl(DialogWindow parentDialog, AboutControlViewModel aboutControlViewModel)
        {
            InitializeComponent();
            ParentDialog = parentDialog;
            DataContext = aboutControlViewModel;
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            ParentDialog.Close();
        }
    }
}