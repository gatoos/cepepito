﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using Cepepito.Annotations;
using CepepitoCore.EntryPoints;
using CepepitoCore.UI;
using GalaSoft.MvvmLight.Command;

namespace Cepepito.About
{
    public class AboutControlViewModel : INotifyPropertyChanged, ILicenseInformationViewModel
    {
        public RelayCommand PurchaseLicenseCommand { get; }
        public RelayCommand RetrievePurchasedLicenseCommand { get; }
        public RetrievePurchasedLicenseInformationUseCase RetrievePurchasedLicenseInformationUseCase { get; set; }
        private readonly double _applicationVersion;
        private LicenseInformation _licenseInformation;
        

        public LicenseInformation LicenseInformation
        {
            get => _licenseInformation;

            set
            {
                _licenseInformation = value;
                OnPropertyChanged(nameof(FormatedLicenseInformation));
            }
        }

        public string FormatedLicenseInformation
        {
            get
            {
                if (_licenseInformation.Trial)
                {
                    if (_licenseInformation.NumberOfDaysLeft > 0)
                        return _licenseInformation.UserName + Environment.NewLine +
                               "Version: " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", _applicationVersion) +
                               Environment.NewLine +
                               "Authorization code: " + _licenseInformation.Uid + Environment.NewLine +
                               "License: trial version - " + _licenseInformation.NumberOfDaysLeft + " day(s) left";
                    return _licenseInformation.UserName + Environment.NewLine +
                           "Version: " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", _applicationVersion) +
                           Environment.NewLine +
                           "Authorization code: " + _licenseInformation.Uid + Environment.NewLine +
                           "License: expired trial version";
                }
                return _licenseInformation.UserName + Environment.NewLine +
                       "Version: " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", _applicationVersion) +
                       Environment.NewLine +
                       "Authorization code: " + _licenseInformation.Uid + Environment.NewLine +
                       "License type: " + _licenseInformation.Type + " - " + _licenseInformation.NumberOfDaysLeft + " day(s) left";
            }
        }
        

        public AboutControlViewModel(LicenseInformation licenseInformation, double applicationVersion)
        {
            LicenseInformation = licenseInformation;
            _applicationVersion = applicationVersion;
            RetrievePurchasedLicenseInformationUseCase = null;
            PurchaseLicenseCommand = new RelayCommand(PurchaseLicense);
            RetrievePurchasedLicenseCommand = new RelayCommand(RetrievePurchasedLicense);
        }

        private void PurchaseLicense()
        {
            Process.Start(@"https://www.cepepito.com/pricing");
        }

        private void RetrievePurchasedLicense()
        {
            RetrievePurchasedLicenseInformationUseCase.Execute();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}