﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Windows;
using Microsoft.VisualStudio.PlatformUI;

namespace Cepepito.About
{
    public class AboutDialog : DialogWindow
    {
        public AboutDialog(AboutControlViewModel aboutControlViewModel)
        {
            ResizeMode = ResizeMode.NoResize;
            SizeToContent = SizeToContent.WidthAndHeight;
            WindowStyle = WindowStyle.None;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            var aboutControl = new AboutControl(this, aboutControlViewModel);
            Content = aboutControl;
        }
    }
}