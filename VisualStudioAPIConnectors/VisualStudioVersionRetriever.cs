﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Text.RegularExpressions;

namespace VisualStudioAPIConnectors
{
    public class VisualStudioVersionRetriever
    {
        private static double Tolerance => 1e-6;
        private readonly DteRetriever _dteRetriever;

        public VisualStudioVersionRetriever(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
        }

        public string Version
        {
            get
            {
                var versionAsString = _dteRetriever.GetDte().Version;
                var regexpResult = Regex.Match(versionAsString, @"^\d+");
                var majorVersionAsString = regexpResult.Groups[0].Value;
                double version = Convert.ToInt32(majorVersionAsString);

                if (Math.Abs(version - 11) < Tolerance)
                    return "vs2012";
                if (Math.Abs(version - 12) < Tolerance)
                    return "vs2013";
                if (Math.Abs(version - 13) < Tolerance)
                    return "vs2015";
                return "vs2017";
            }
        }
    }
}