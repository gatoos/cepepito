﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using EnvDTE;

namespace VisualStudioAPIConnectors
{
    public class VsSolution
    {
        private readonly DteRetriever _dteRetriever;

        public VsSolution(DteRetriever dteRetriever)
        {
            _dteRetriever = dteRetriever;
        }

        public VsProject GetProjectFromUniqueName(string projectUniqueName)
        {
            Project retrievedProject = null;

            foreach (Project project in _dteRetriever.GetDte().Solution.Projects)
                if (project.UniqueName == projectUniqueName)
                {
                    retrievedProject = project;
                    break;
                }

            return retrievedProject == null ? null : new VsProject(retrievedProject);
        }

        public VsProject AddProjectFromFile(string createdProjectFullFilename)
        {
            var addedProject = _dteRetriever.GetDte().Solution.AddFromFile(createdProjectFullFilename);
            return new VsProject(addedProject);
        }

        public VsProject SelectedProject
        {
            get
            {
                var selectedProjects = (Array)_dteRetriever.GetDte().ActiveSolutionProjects;
                var selectedProject = (Project)selectedProjects.GetValue(0);
                return new VsProject(selectedProject);
            }
        }
    }
}
