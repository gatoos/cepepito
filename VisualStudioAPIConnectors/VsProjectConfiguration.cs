﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.VCProjectEngine;

namespace VisualStudioAPIConnectors
{
    public class VsProjectConfiguration
    {
        private readonly VCConfiguration _configuration;

        public VsProjectConfiguration(VCConfiguration configuration)
        {
            _configuration = configuration;
        }

        private VCCLCompilerTool CompilerTools
        {
            get
            {
                var toolsCollection = (IVCCollection) _configuration?.Tools;
                return toolsCollection?.Item("VCCLCompilerTool"); 
            }
        }

        private VCNMakeTool NMakeTool => _configuration?.Tools?.Item("NMake Tool");

        private string MakefileVcIncludeDirectories
        {
            get => NMakeTool?.IncludeSearchPath ?? "";
            
            set 
            {
                if (NMakeTool != null)
                    NMakeTool.IncludeSearchPath = value;
            }
        }

        private VCLinkerTool LinkerTool
        {
            get
            {
                var toolsCollection = (IVCCollection) _configuration?.Tools;
                return toolsCollection?.Item("VCLinkerTool");
            }
        }

        public IEnumerable<string> AdditionalIncludeDirectories
        {
            get
            {
                var includeDirectories = CompilerTools != null ? (CompilerTools.AdditionalIncludeDirectories ?? "") : MakefileVcIncludeDirectories;
                return _configuration.Evaluate(includeDirectories).Split(';').
                        Where(x => !string.IsNullOrEmpty(x)).
                        Select(x => x).
                        Distinct();
            }

            set
            {
                var filteredValue = value.
                    Where(x => !AdditionalIncludeDirectories.Contains(x)).
                    Select(x => x);

                var currentIncludeWithoutEvaluation =
                    (CompilerTools != null
                        ? (CompilerTools.AdditionalIncludeDirectories ?? "")
                        : MakefileVcIncludeDirectories).Split(';').Where(x => !string.IsNullOrEmpty(x)).Select(x => x)
                    .Distinct();

                var allIncludes = currentIncludeWithoutEvaluation.Concat(filteredValue);

                var concatenatedIncludeDirectories = allIncludes.Any()
                    ? allIncludes.Aggregate((content, nextContent) => content + ';' + nextContent)
                    : "";

                if (CompilerTools != null)
                    CompilerTools.AdditionalIncludeDirectories = concatenatedIncludeDirectories;
                else
                    MakefileVcIncludeDirectories = concatenatedIncludeDirectories;
            }
        }

        public runtimeLibraryOption RuntimeLibrary
        {
            get => CompilerTools?.RuntimeLibrary ?? runtimeLibraryOption.rtMultiThreaded;
            
            set
            {
                if (CompilerTools != null)
                    CompilerTools.RuntimeLibrary = value;
            }
        }

        public string AdditionalDependencies
        {
            get => LinkerTool.AdditionalDependencies;
            set => LinkerTool.AdditionalDependencies = value;
        }

        public string PreprocessorDefinitions
        {
            get => CompilerTools?.PreprocessorDefinitions;
            set
            {
                if (CompilerTools != null)
                    CompilerTools.PreprocessorDefinitions = value;
            }
        }

        public ConfigurationTypes Type
        {
            get => CompilerTools != null
                ? _configuration.ConfigurationType
                : (NMakeTool != null ? ConfigurationTypes.typeGeneric : ConfigurationTypes.typeUnknown);
            set => _configuration.ConfigurationType = value;
        }

        public string GeneratedStaticLibraryPath => _configuration?.PrimaryOutput ?? "";
    }
}