﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using EnvDTE;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.VCProjectEngine;
using Constants = EnvDTE.Constants;

namespace VisualStudioAPIConnectors
{
    public class VsProject
    {
        private readonly Project _underlyingProject;

        public VsProject(Project underlyingProject)
        {
            if (!(underlyingProject.Object is VCProject))
                throw new VsProjectCreationException();

            _underlyingProject = underlyingProject;
        }

        private VCConfiguration ActiveConfiguration
        {
            get
            {
                VCConfiguration activeConfiguration = null;
                var vcProject = (VCProject) _underlyingProject.Object;
                var activeConfigurationName =
                    _underlyingProject.ConfigurationManager.ActiveConfiguration.ConfigurationName;
                var activeConfigurationPlatformName =
                    _underlyingProject.ConfigurationManager.ActiveConfiguration.PlatformName;

                foreach (VCConfiguration vcConfiguration in vcProject.Configurations)
                {
                    VCPlatform platform = vcConfiguration.Platform;
                    if (vcConfiguration.ConfigurationName != activeConfigurationName ||
                        platform.Name != activeConfigurationPlatformName) continue;
                    activeConfiguration = vcConfiguration;
                    break;

                }

                return activeConfiguration;
            }
        }

        private VCDebugSettings DebugSettings => ActiveConfiguration?.DebugSettings;

        public string DebugCommand => ActiveConfiguration.Evaluate(DebugSettings?.Command ?? "");
        public bool EnvironmentVariablesMerge => DebugSettings?.EnvironmentMerge ?? false;
        public string DebugWorkingDirectory => ActiveConfiguration.Evaluate(DebugSettings?.WorkingDirectory ?? "");

        public EnvironmentVariables EnvironmentVariables
        {
            get
            {
                var rawEnvironmentVariables = DebugSettings?.Environment;
                return new EnvironmentVariables(rawEnvironmentVariables, x => ActiveConfiguration.Evaluate(x));
            }
            set
            {
                if (DebugSettings == null) return;

                DebugSettings.Environment = value.Raw;
            }
        }

        public string Name
        {
            get => _underlyingProject.Name;
            set => _underlyingProject.Name = value;
        }

        public string FullName => _underlyingProject.FullName;
        public string FileName => _underlyingProject.FileName;

        public string RootNamespace
        {
            set
            {
                var addedVcProject = (VCProject) _underlyingProject.Object;
                addedVcProject.RootNamespace = value;
            }
        }

        public void RemoveCppFiles()
        {
            var toBeRemovedProjectItems = RetrieveToBeRemovedElementsFromProjectItems(_underlyingProject.ProjectItems);
            foreach (var projectItem in toBeRemovedProjectItems)
                projectItem.Remove();
        }

        private IEnumerable<ProjectItem> RetrieveToBeRemovedElementsFromProjectItems(ProjectItems projectItems)
        {
            var toBeRemovedProjectItems = new List<ProjectItem>();

            foreach (ProjectItem projectItem in projectItems)
                if (projectItem != null)
                {
                    var projectItemLabel = projectItem.Name;
                    if (projectItem.Kind == Constants.vsProjectItemKindPhysicalFile &&
                        FileIsCppSourceFile(projectItemLabel))
                    {
                        toBeRemovedProjectItems.Add(projectItem);
                    }
                    else
                    {
                        var subProjectItems = projectItem.ProjectItems;
                        if (subProjectItems != null)
                            toBeRemovedProjectItems.AddRange(
                                RetrieveToBeRemovedElementsFromProjectItems(subProjectItems));
                    }
                }

            return toBeRemovedProjectItems;
        }

        private static bool FileIsCppSourceFile(string filename)
        {
            var cppFilesExtensions = new List<string> {".cpp", ".hpp", ".c", ".h"};
            return cppFilesExtensions.Any(filename.EndsWith);
        }

        public void AddCppFilesFromSourceProject(VsProject selectedProject)
        {
            var cppfilesNames = RetrieveCppFilesFromProjectItems(selectedProject._underlyingProject.ProjectItems);
            cppfilesNames.ForEach(filename => _underlyingProject.ProjectItems.AddFromFile(filename));
        }

        public static List<string> RetrieveCppFilesFromProjectItems(ProjectItems projectItems)
        {
            var cppFilenames = new List<string>();

            foreach (ProjectItem projectItem in projectItems)
                if (projectItem != null)
                    cppFilenames.AddRange(RetrieveCppFilesFromProjectItem(projectItem));

            return cppFilenames;
        }

        private static IEnumerable<string> RetrieveCppFilesFromProjectItem(ProjectItem projectItem)
        {
            var cppFilenames = new List<string>();

            if (projectItem != null)
                if (projectItem.FileCount == 1 &&
                    projectItem.Kind == Constants.vsProjectItemKindPhysicalFile &&
                    projectItem.FileCodeModel != null && projectItem.FileCodeModel.Language ==
                    CodeModelLanguageConstants.vsCMLanguageVC)
                {
                    cppFilenames.Add(projectItem.FileNames[0]);
                }
                else
                {
                    var subProjectItems = projectItem.ProjectItems;
                    if (subProjectItems != null)
                        cppFilenames.AddRange(RetrieveCppFilesFromProjectItems(subProjectItems));
                }

            return cppFilenames;
        }

        public IEnumerable<VsProjectConfiguration> Configurations
        {
            get
            {
                var underlyingVcProject = (VCProject) _underlyingProject.Object;
                var configurationsCollection = (IVCCollection) underlyingVcProject.Configurations;

                foreach (var configuration in configurationsCollection)
                    yield return new VsProjectConfiguration((VCConfiguration) configuration);
            }
        }

        public bool IsACppProject => _underlyingProject.Object is VCProject;

        public bool IsACppProjectWithoutTestRunner
        {
            get
            {
                if (_underlyingProject.CodeModel.Language == CodeModelLanguageConstants.vsCMLanguageVC)
                {
                    var cppFilenames = RetrieveCppFilesFromProjectItems(_underlyingProject.ProjectItems);
                    var projectContainsATestRunner = FilesContainTestRunner(cppFilenames);

                    if (projectContainsATestRunner)
                        return false;
                    return true;
                }

                return false;
            }
        }

        public string Guid
        {
            get
            {
                // TODO : replace by using to ensure resources release
                var projectXmlDocument = new XmlDocument();
                projectXmlDocument.Load(FullName);
                var projectGuidNode = projectXmlDocument.SelectSingleNode("ProjectGuid");
                return projectGuidNode?.InnerText;
            }
        }

        public string XmlRepresentation
        {
            get
            {
                // TODO : replace by using to ensure resources release
                var projectXmlDocument = new XmlDocument();
                projectXmlDocument.Load(FullName);
                return projectXmlDocument.InnerXml;
            }
        }


        public VsProjectConfiguration ActiveProjectConfiguration
        {
            get
            {
                var activeConfiguration = ActiveConfiguration;
                return activeConfiguration != null ? new VsProjectConfiguration(activeConfiguration) : null;
            }
        }

        public ConfigurationTypes ActiveProjectConfigurationType => ActiveConfiguration?.ConfigurationType ??
                                                                    ConfigurationTypes.typeUnknown;

        public string UniqueName => _underlyingProject.UniqueName;

        private bool FilesContainTestRunner(List<string> cppFilenames)
        {
            var filesContainTestRunner = false;

            for (var i = 0; i < cppFilenames.Count && !filesContainTestRunner; ++i)
            {
                var fileContent = File.ReadAllText(cppFilenames[i]);
                filesContainTestRunner = fileContent.Contains("RUN_ALL_TESTS");
            }

            return filesContainTestRunner;
        }

        public void AddFileFromContent(string filename, string fileContent)
        {
            var targetProjectDirectory = Path.GetDirectoryName(FullName);

            if (targetProjectDirectory == null)
                throw new Exception("Cannot retrieve project directory");

            // TODO : use file content instead of this hardcoded entry
            using (var streamWriter = File.CreateText(Path.Combine(targetProjectDirectory, filename)))
            {
                streamWriter.WriteLine("#include \"gtest/gtest.h\"");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("int main(int argc, char * *argv) {");
                streamWriter.WriteLine("::testing::InitGoogleTest(&argc, argv);");
                streamWriter.WriteLine("return RUN_ALL_TESTS();");
                streamWriter.WriteLine("}");
                streamWriter.WriteLine("");
            }

            _underlyingProject.ProjectItems.AddFromFile(Path.Combine(targetProjectDirectory, filename));

            Save();
        }

        public void Save()
        {
            _underlyingProject.Save();
        }

        private const uint VsitemidNil = 0xFFFFFFFF;
        private const uint VsitemidRoot = 0xFFFFFFFE;

        public IEnumerable<string> GetAllFiles(IServiceProvider serviceProvider)
        {
            var solutionService = (IVsSolution)serviceProvider.GetService(typeof(IVsSolution));
            return ProcessProject(solutionService, _underlyingProject);
        }

        private IEnumerable<string> ProcessProject(IVsSolution solutionService, Project underlyingProject)
        {
            if (solutionService == null ||
                solutionService.GetProjectOfUniqueName(underlyingProject.UniqueName, out var projectHierarchy) != 0 ||
                projectHierarchy == null)
            {
                throw new VsHierarchyNotAvailableException();
            }
            
            return ProcessHierarchy(projectHierarchy);
        }

        private IEnumerable<string> ProcessHierarchy(IVsHierarchy hierarchy)
        {
            // Traverse the nodes of the hierarchy from the root node
            return ProcessHierarchyNodeRecursively(hierarchy, VsitemidRoot);
        }

        private IEnumerable<string> ProcessHierarchyNodeRecursively(IVsHierarchy hierarchy, uint itemId)
        {
            // First, guess if the node is actually the root of another hierarchy (a project, for example)
            var nestedHierarchyGuid = typeof(IVsHierarchy).GUID;
            var result = hierarchy.GetNestedHierarchy(itemId, ref nestedHierarchyGuid, out var nestedHiearchyValue, out var nestedItemIdValue);

            if (result == 0 && nestedHiearchyValue != IntPtr.Zero && nestedItemIdValue == VsitemidRoot)
            {
                // Get the new hierarchy
                var nestedHierarchy = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(nestedHiearchyValue) as IVsHierarchy;
                System.Runtime.InteropServices.Marshal.Release(nestedHiearchyValue);

                if (nestedHierarchy == null) yield break;

                foreach (var entry in ProcessHierarchy(nestedHierarchy))
                    yield return entry;
            }
            else // The node is not the root of another hierarchy, it is a regular node
            {
                var nodeFilePath = RetrieveNodeAssociatedFilePath(hierarchy, itemId);
                if (nodeFilePath != null)
                    yield return nodeFilePath;

                // Get the first visible child node
                result = hierarchy.GetProperty(itemId, (int)__VSHPROPID.VSHPROPID_FirstVisibleChild, out var value);

                while (result == 0 && value != null)
                {
                    if (value is int i && (uint)i == VsitemidNil)
                    {
                        // No more nodes
                        break;
                    }

                    var visibleChildNode = Convert.ToUInt32(value);

                    // Enter in recursion
                    foreach (var entry in ProcessHierarchyNodeRecursively(hierarchy, visibleChildNode))
                    {
                        yield return entry;
                    }

                    // Get the next visible sibling node
                    result = hierarchy.GetProperty(visibleChildNode, (int)__VSHPROPID.VSHPROPID_NextVisibleSibling, out value);
                }
            }
        }

        private static string RetrieveNodeAssociatedFilePath(IVsHierarchy hierarchy, uint itemId)
        {
            var result = hierarchy.GetProperty(itemId, (int)__VSHPROPID.VSHPROPID_SaveName, out var value);

            if (result != 0 || value == null) return null;
            hierarchy.GetCanonicalName(itemId, out var canonicalName);

            try
            {
                return Path.GetFullPath(canonicalName);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}