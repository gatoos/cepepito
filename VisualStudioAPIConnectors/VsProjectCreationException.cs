﻿using System;

namespace VisualStudioAPIConnectors
{
    public class VsProjectCreationException : Exception
    {
        public VsProjectCreationException() : base("Project's not a cpp project")
        { 
        }
    }
}