﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace VisualStudioAPIConnectors
{
    public class EnvironmentVariables : IEnumerable<EnvironmentVariable>
    {
        private readonly Func<string, string> _parser;

        public EnvironmentVariables(string raw, Func<string, string> parser)
        {
            _parser = parser;
            _parsed = new Dictionary<string, string>();
            Add(raw);

            /*parser(raw)?.Split('\n').Where(x => x.Contains("=") && x.Split('=').Length == 2)
                      .GroupBy(x => x.Split('=')[0])
                      .ToDictionary(x => x.Key, x => x.Last().Split('=')[1]) ??
                  new Dictionary<string, string>();*/
        }

        public void Add(string variables)
        {
            if (Raw == null || !Raw.Any())
            {
                Raw = variables;
            }
            else
            {
                Raw += '\n' + variables;
            }

            try
            {
                var parsedVariables = _parser(variables);
                if (parsedVariables == null)
                    return;

                foreach (var environment in parsedVariables.Split('\n')
                    .Where(x => x.Contains("=") && x.Split('=').Length == 2).GroupBy(x => x.Split('=')[0]))
                {
                    _parsed[environment.Key] = environment.Last().Split('=')[1];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        public string Raw { get; private set; }
        private readonly Dictionary<string, string> _parsed;

        public IEnumerator<EnvironmentVariable> GetEnumerator()
        {
            foreach(var entry in _parsed)
                yield return new EnvironmentVariable(entry.Key, entry.Value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}