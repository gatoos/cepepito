﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;

namespace VisualStudioAPIConnectors
{
    /// <summary>
    ///     if a package is marked to load on startup it is possible that the IDE is not yet fully initialized (it is in a
    ///     "zombie" state), and in such
    ///     case the service is null yet. As a work-around, we can detect this.
    ///     More info: http://www.mztools.com/articles/2013/MZ2013029.aspx
    /// </summary>
    public class DteInitializer : IVsShellPropertyEvents
    {
        private readonly Action _callback;
        private readonly IVsShell _shellService;
        private uint _cookie;

        internal DteInitializer(IVsShell shellService, Action callback)
        {
            _shellService = shellService;
            _callback = callback;

            // Set an event handler to detect when the IDE is fully initialized
            var hr = _shellService.AdviseShellPropertyChanges(this, out _cookie);

            ErrorHandler.ThrowOnFailure(hr);
        }

        int IVsShellPropertyEvents.OnShellPropertyChange(int propid, object var)
        {
            if (propid == (int) __VSSPROPID.VSSPROPID_Zombie)
            {
                var isZombie = (bool) var;

                if (!isZombie)
                {
                    // Release the event handler to detect when the IDE is fully initialized
                    var hr = _shellService.UnadviseShellPropertyChanges(_cookie);

                    ErrorHandler.ThrowOnFailure(hr);

                    _cookie = 0;

                    _callback();
                }
            }

            return VSConstants.S_OK;
        }
    }
}