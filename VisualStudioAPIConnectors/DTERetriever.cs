﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Threading.Tasks;
using EnvDTE80;
using Microsoft.VisualStudio.Shell.Interop;

namespace VisualStudioAPIConnectors
{
    public class DteRetriever
    {
        private readonly IServiceProvider _serviceProvider;
        private DTE2 _dte;
        private TaskCompletionSource<DTE2> _tcs;

        public DteRetriever(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public DTE2 GetDte()
        {
            if (_dte == null)
            {
                _dte = _serviceProvider.GetService(typeof(SDTE)) as DTE2; // The IDE is not yet fully initialized

                if (_dte == null)
                {
                    _tcs = new TaskCompletionSource<DTE2>();
                    var shellService = _serviceProvider.GetService(typeof(SVsShell)) as IVsShell;
                    var dteInitializer = new DteInitializer(shellService, NotifyDteInitialized);
                    _dte = _tcs.Task.Result;
                }
            }

            return _dte;
        }

        public void NotifyDteInitialized()
        {
            var retrievedDte = _serviceProvider.GetService(typeof(SDTE)) as DTE2;
            _tcs.SetResult(retrievedDte);
        }
    }
}