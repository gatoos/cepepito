﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.IO;
using LicenseService.ServiceProviders;

namespace LicenseServiceAdapters
{
    public class LicenseRepository : ILicenseRepository
    {
        public LicenseRepository(string installDirectory)
        {
            InstallDirectory = installDirectory;
        }

        private string InstallDirectory { get; }

        public string LoadLicenseAsEncryptedString()
        {
            return File.ReadAllText(Path.Combine(InstallDirectory, "license.lic"));
        }

        public void StoreLicenseAsEncryptedString(string encryptedCepepitoLicense)
        {
            File.WriteAllText(Path.Combine(InstallDirectory, "license.lic"), encryptedCepepitoLicense.Trim());
        }
    }
}