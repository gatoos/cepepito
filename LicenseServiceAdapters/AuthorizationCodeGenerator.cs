﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Management;
using System.Security.Cryptography;
using System.Text;

namespace LicenseServiceAdapters
{
    public class AuthorizationCodeGenerator
    {
        private static string GetDiskVolumeSerialNumber()
        {
            try
            {
                var disk = new ManagementObject(@"Win32_LogicalDisk.deviceid=""c:""");
                disk.Get();
                return disk["VolumeSerialNumber"].ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string GetProcessorId()
        {
            try
            {
                var mbs = new ManagementObjectSearcher("Select ProcessorId From Win32_processor");
                var mbsList = mbs.Get();
                var id = string.Empty;
                foreach (var o in mbsList)
                {
                    var mo = (ManagementObject) o;
                    id = mo["ProcessorId"].ToString();
                    break;
                }

                return id;
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string GetMotherboardId()
        {
            try
            {
                var mbs = new ManagementObjectSearcher("Select SerialNumber From Win32_BaseBoard");
                string id;
                using (var mbsList = mbs.Get())
                {
                    id = string.Empty;
                    foreach (var o in mbsList)
                    {
                        var mo = (ManagementObject) o;
                        id = mo["SerialNumber"].ToString();
                        break;
                    }
                }

                return id;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GenerateAuthorizationCode()
        {
            var byteIds = CombineMachineIdentifiers();
            var checksum = BuildMd5Checksum(byteIds);
            var identifiers = ConvertChecksumToIds(checksum);

            return $"{identifiers.part1}-{identifiers.part2}-{identifiers.part3}-{identifiers.part4}";
        }

        private static (string part1, string part2, string part3, string part4) ConvertChecksumToIds(byte[] checksum)
        {
            var part1 = Base36.Encode(BitConverter.ToUInt32(checksum, 0));
            var part2 = Base36.Encode(BitConverter.ToUInt32(checksum, 4));
            var part3 = Base36.Encode(BitConverter.ToUInt32(checksum, 8));
            var part4 = Base36.Encode(BitConverter.ToUInt32(checksum, 12));
            return (part1, part2, part3, part4);
        }

        private static byte[] BuildMd5Checksum(byte[] byteIds)
        {
            var md5 = new MD5CryptoServiceProvider();
            var checksum = md5.ComputeHash(byteIds);
            return checksum;
        }

        private static byte[] CombineMachineIdentifiers()
        {
            var id = string.Concat("Cepepito", GetProcessorId(), GetMotherboardId(), GetDiskVolumeSerialNumber());
            var byteIds = Encoding.UTF8.GetBytes(id);
            return byteIds;
        }

        private class Base36
        {
            private const string CharList = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            private static readonly char[] CharArray = CharList.ToCharArray();

            public static string Encode(ulong input)
            {
                var sb = new StringBuilder();
                do
                {
                    sb.Append(CharArray[input % (ulong) CharList.Length]);
                    input /= (ulong) CharList.Length;
                } while (input != 0);

                return Reverse(sb.ToString());
            }

            private static string Reverse(string s)
            {
                var charArray = s.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            }
        }
    }
}