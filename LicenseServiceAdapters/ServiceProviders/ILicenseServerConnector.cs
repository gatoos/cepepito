﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace LicenseServiceAdapters.ServiceProviders
{
    public interface ILicenseServerConnector
    {
        string GetContentFromUrl(string url);
    }
}