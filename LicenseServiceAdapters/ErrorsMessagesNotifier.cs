﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using LicenseService.ServiceProviders;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace LicenseServiceAdapters
{
    public class ErrorsMessagesNotifier : IErrorsMessagesQueue
    {
        private readonly IServiceProvider _serviceProvider;

        public ErrorsMessagesNotifier(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void AddErrorMessage(string message)
        {
            VsShellUtilities.ShowMessageBox(
                _serviceProvider,
                message,
                "Error",
                OLEMSGICON.OLEMSGICON_WARNING,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }
    }
}