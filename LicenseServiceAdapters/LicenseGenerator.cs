﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Net;
using System.Net.Http;
using LicenseService.ServiceProviders;
using LicenseServiceAdapters.ServiceProviders;

namespace LicenseServiceAdapters
{
    public class LicenseGenerator : ILicenseGenerator
    {
        public LicenseGenerator(string licenseServerUrl, ILicenseServerConnector licenseServerConnector)
        {
            LicenseServerUrl = licenseServerUrl;
            LicenseServerConnector = licenseServerConnector;
        }

        public LicenseGenerator(string licenseServerUrl) : this(licenseServerUrl, new LicenseServerConnectorImpl())
        {
        }

        private string LicenseServerUrl { get; }
        private ILicenseServerConnector LicenseServerConnector { get; }

        public string GenerateTrialLicenseEncryptedString(string authorizationCode)
        {
            return LicenseServerConnector.GetContentFromUrl(LicenseServerUrl + "/trial?authorizationcode=" +
                                                            authorizationCode);
        }

        public string GenerateFullLicenseEncryptedString(string authorizationCode, string purchaseCode)
        {
            return LicenseServerConnector.GetContentFromUrl(LicenseServerUrl + "/full?purchaseUID=" + purchaseCode +
                                                            " & authorizationcode=" +
                                                            authorizationCode);
        }

        private class LicenseServerConnectorImpl : ILicenseServerConnector
        {
            public string GetContentFromUrl(string url)
            {
                using (var client = new HttpClient(new HttpClientHandler
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                }))
                {
                    try
                    {
                        client.Timeout = TimeSpan.FromSeconds(15);
                        var response = client.GetAsync(url).Result;
                        response.EnsureSuccessStatusCode();
                        return response.Content.ReadAsStringAsync().Result;
                    }
                    catch (Exception)
                    {
                        throw new Exception("Failed to connect to license server.");
                    }
                }
            }
        }
    }
}