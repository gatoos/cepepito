﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using LicenseService.ServiceProviders;

namespace LicenseServiceAdapters
{
    public class ClockManager : IClockManager
    {
        public ClockManager(IEnumerable<DateTime> systemEventsDateTimes)
        {
            SystemEventsDateTimes = systemEventsDateTimes;
        }

        private IEnumerable<DateTime> SystemEventsDateTimes { get; }

        public DateTime GetCurrentDateTime()
        {
            return DateTime.Today;
        }

        public bool ClockHasBeenManipulated()
        {
            return SystemEventsDateTimes.Any(eventDate => eventDate >= DateTime.Today.AddDays(1));
        }
    }
}