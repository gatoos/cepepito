#pragma once

#include <string>
#include <vector>

struct CursorLocation
{
	CursorLocation() :
		m_line(-1),
		m_column(-1)
	{}

	CursorLocation(unsigned int line, unsigned int column) :
		m_line(line),
		m_column(column)
	{}

	int m_line;
	int m_column;
};

class CepepitoParserTestHelper
{
public:
	static CursorLocation getSymbolLocation(const std::string & filename, const std::string & symbol, const std::string & substring);
	static std::string cleanedResult(const char * source);
	static std::string cleanedResult(const std::string & source);
	static std::string getFileContent(const std::string & filename);
	static std::string getTypeDefinitionPath(const std::string & typeName);
};

struct TestCaseInformations
{
	TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring, std::vector<const char *> includeDirectories);
	TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring, std::vector<const char *> includeDirectories, std::vector<const char *> generatedFileIncludeDirectories);
	TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring, std::vector<const char *> includeDirectories, std::vector<std::string> errorMessageFragments);
	TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring);
	

	std::string m_sourceTypeName;
	std::string m_targetTypeName;
	std::string m_cursorLocationSearchString;
	std::string m_cursorLocationSearchSubstring;
	std::vector<const char *> m_includeDirectories;
	std::vector<const char *> m_generatedFileIncludeDirectories;	
	std::vector<std::string> m_errorMessageFragments;
	
	const char ** getIncludeDirectories() const;
	int getNumberOfIncludeDirectories() const;	
	const char ** getGeneratedFileIncludeDirectories() const;
	int getGeneratedFileNumberOfIncludeDirectories() const;
};

