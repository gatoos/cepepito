/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "TypeDefinedInOtherIncludeFile.hpp"
#include "TypeDefinedInOtherIncludeFile2.hpp"

using namespace otherNamespace2;

class Type2
{
public:
	virtual void toto(double);
};

namespace ANiveau1
{
	namespace ANiveau2
	{
		template <class Beta, int Gamma>
struct TotoClass // #29
		{
		public:
			TotoClass(int variable);
			void setVariable(int variable);
			int getVariable() const;
			double calculeLesTotaux(double argentRAched, double argentMarine, int facteur);
			virtual int uniqueVirtualMethodToBeMocked(const char * parameter, Type2 t2, Beta &, otherNamespace1::TypeDefinedInOtherIncludeFile, TypeDefinedInOtherIncludeFile2 *);

		private:
			void privateSetVariable(int variable);
			int privateGetVariable();

		private:
			int m_variable;
			Beta verBeta;
			Gamma bar2;
		};
	}
}

namespace BNiveau1
{
	namespace BNiveau2
	{
		namespace BNiveau3
		{
class TutuClass
			{
			public:
				TutuClass(int variable);
				virtual void setVariableVirtual(int variable);
				virtual int getVariableVirtual() const;
				virtual double calculeLesTotauxVirtual(double argentRAched, double argentMarine, int facteur);
				void setVariable(int variable);
				int getVariable() const;
				double calculeLesTotaux(double argentRAched, double argentMarine, int facteur);

			protected:
				virtual void protectedSetVariableVirtual(int variable);
				virtual int protectedGetVariableVirtual() const;
				void protectedSetVariable(int variable);
				int protectedGetVariable() const;

			private:
				virtual void privateSetVariableVirtual(int variable);
				virtual int privateGetVariableVirtual() const;
				void privateSetVariable(int variable);
				int privateGetVariable() const;

			private:
				int m_variable;
			};
		}
	}
}

void functionBidon(int)
{
}

namespace level1
{
	namespace level2
	{
		class DummyClass
		{
		public:
			virtual void toto() {}
		};

		template<class T>
		class TemplateClass1
		{
		public:
			TemplateClass1(const T &) :
				m_object(T)
			{}

			virtual void toto() {}
		};

		template<class T>
		class TemplateClass2
		{
		public:
			TemplateClass2(const T &) :
				m_object(T)
			{}

			virtual void toto() {}
		};

TemplateClass1<TemplateClass2<DummyClass>> * templateVariable = nullptr;

class Template3
{
public:
virtual void toto() {}
class Template4
{
public:
virtual void toto() {}
};
private:
TemplateClass1<TemplateClass2<DummyClass>> * m_templateVariable;
};
	}
}

class DummyClass
{
public:
	virtual void add() {}

	class DummyClassInternal
	{
	public:
		virtual void addInternal() {}
		virtual void removeInternal() {}
	};

	virtual void remove() {}
};






