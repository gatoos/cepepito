#ifndef BNIVEAU1_BNIVEAU2_BNIVEAU3_TUTUCLASSMOCK_HPP
#define BNIVEAU1_BNIVEAU2_BNIVEAU3_TUTUCLASSMOCK_HPP

#include <gmock/gmock.h>
#include "TestResources/ClassesDefinitions.hpp"
#include "TypeDefinedInOtherIncludeFile.hpp"
#include "TypeDefinedInOtherIncludeFile2.hpp"

namespace ANiveau1
{
	namespace ANiveau2
	{
		struct TotoClassMock : public TotoClass
		{
		public:
			MOCK_METHOD3(uniqueVirtualMethodToBeMocked, int(const char *, otherNamespace1::TypeDefinedInOtherIncludeFile, otherNamespace2::TypeDefinedInOtherIncludeFile2));
		};
	}
}

#endif
