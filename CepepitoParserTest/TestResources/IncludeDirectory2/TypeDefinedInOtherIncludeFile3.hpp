/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

namespace otherNamespace3
{
	struct TypeDefinedInOtherIncludeFile3
	{
		virtual void doSmthing2(double);
		int m_value;
	};

}


