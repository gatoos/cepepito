/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#include "TypeDefinedInOtherIncludeFile3.hpp"

namespace otherNamespace2
{
	struct TypeDefinedInOtherIncludeFile2
	{
		virtual void doSmthing2(otherNamespace3::TypeDefinedInOtherIncludeFile3 &);
	int m_value;
	};

}

