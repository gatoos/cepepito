/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <TypeDefinedInOtherIncludeFile.hpp>
#include <TypeDefinedInOtherIncludeFile2.hpp>

namespace ANiveau1
{
	namespace ANiveau2
	{
		struct TotoClassMock : public TotoClass
		{
		public:
			MOCK_METHOD3(uniqueVirtualMethodToBeMocked, int(const char *, otherNamespace1::TypeDefinedInOtherIncludeFile, otherNamespace2::TypeDefinedInOtherIncludeFile2));
		};
	}
}
namespace BNiveau1
{
	namespace BNiveau2
	{
		namespace BNiveau3
		{
			class TutuClassMock : public TutuClass
			{
			public:				
				MOCK_METHOD1(setVariableVirtual, void(int));
				MOCK_CONST_METHOD0(getVariableVirtual, int());
				MOCK_METHOD3(calculeLesTotauxVirtual, double(double, double, int));				
			protected:
				MOCK_METHOD1(protectedSetVariableVirtual, void(int));
				MOCK_CONST_METHOD0(protectedGetVariableVirtual, int());
			private:
				MOCK_METHOD1(privateSetVariableVirtual, void(int));
				MOCK_CONST_METHOD0(privateGetVariableVirtual, int());				
			};
		}
	}
}
