#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHOPERATORSMOCK_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHOPERATORSMOCK_HPP

#include <gmock/gmock.h>
#include <TestResources/ClassesDefinitions/TypeWithOperators.hpp>

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithOperatorsMock : public cepepito::parser::test::resources::TypeWithOperators
				{
				public:
					MOCK_METHOD1(operatorAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorAssignment(param0); }
					MOCK_METHOD1(operatorAddition, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator+(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorAddition(param0); }
					MOCK_METHOD1(operatorSubstraction, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator-(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorSubstraction(param0); }
					MOCK_METHOD0(operatorUnaryPlus, cepepito::parser::test::resources::TypeWithOperators());
					virtual cepepito::parser::test::resources::TypeWithOperators operator+() { return operatorUnaryPlus(); }
					MOCK_METHOD0(operatorUnaryMinus, cepepito::parser::test::resources::TypeWithOperators());
					virtual cepepito::parser::test::resources::TypeWithOperators operator-() { return operatorUnaryMinus(); }
					MOCK_METHOD1(operatorMultiplication, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator*(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorMultiplication(param0); }
					MOCK_METHOD1(operatorDivision, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator/(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorDivision(param0); }
					MOCK_METHOD1(operatorModulo, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator%(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorModulo(param0); }
					MOCK_METHOD0(operatorIncrementPrefix, cepepito::parser::test::resources::TypeWithOperators &());
					virtual cepepito::parser::test::resources::TypeWithOperators & operator++() { return operatorIncrementPrefix(); }
					MOCK_METHOD1(operatorIncrementPostfix, cepepito::parser::test::resources::TypeWithOperators(int));
					virtual cepepito::parser::test::resources::TypeWithOperators operator++(int param0) { return operatorIncrementPostfix(param0); }
					MOCK_METHOD0(operatorDecrementPrefix, cepepito::parser::test::resources::TypeWithOperators &());
					virtual cepepito::parser::test::resources::TypeWithOperators & operator--() { return operatorDecrementPrefix(); }
					MOCK_METHOD1(operatorDecrementPostfix, cepepito::parser::test::resources::TypeWithOperators(int));
					virtual cepepito::parser::test::resources::TypeWithOperators operator--(int param0) { return operatorDecrementPostfix(param0); }
					MOCK_METHOD1(operatorEqual, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator==(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorEqual(param0); }
					MOCK_METHOD1(operatorNotEqual, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator!=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorNotEqual(param0); }
					MOCK_METHOD1(operatorLessThan, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator<(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorLessThan(param0); }
					MOCK_METHOD1(operatorGreater, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator>(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorGreater(param0); }
					MOCK_METHOD1(operatorLessOrEqual, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator<=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorLessOrEqual(param0); }
					MOCK_METHOD1(operatorGreaterOrEqual, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator>=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorGreaterOrEqual(param0); }
					MOCK_METHOD0(operatorLogicalNot, bool());
					virtual bool operator!() { return operatorLogicalNot(); }
					MOCK_METHOD1(operatorLogicalAnd, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator&&(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorLogicalAnd(param0); }
					MOCK_METHOD1(operatorLogicalOr, bool(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual bool operator||(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorLogicalOr(param0); }
					MOCK_METHOD0(operatorBitwiseNot, cepepito::parser::test::resources::TypeWithOperators());
					virtual cepepito::parser::test::resources::TypeWithOperators operator~() { return operatorBitwiseNot(); }
					MOCK_METHOD1(operatorBitwiseAnd, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator&(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseAnd(param0); }
					MOCK_METHOD1(operatorBitwiseOr, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator|(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseOr(param0); }
					MOCK_METHOD1(operatorBitwiseXor, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator^(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseXor(param0); }
					MOCK_METHOD1(operatorBitwiseLeftShift, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator<<(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseLeftShift(param0); }
					MOCK_METHOD1(operatorBitwiseRightShift, cepepito::parser::test::resources::TypeWithOperators(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators operator>>(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseRightShift(param0); }
					MOCK_METHOD1(operatorAdditionAssignement, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator+=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorAdditionAssignement(param0); }
					MOCK_METHOD1(operatorSubstractionAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator-=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorSubstractionAssignment(param0); }
					MOCK_METHOD1(operatorMultiplicationAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator*=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorMultiplicationAssignment(param0); }
					MOCK_METHOD1(operatorDivisionAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator/=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorDivisionAssignment(param0); }
					MOCK_METHOD1(operatorModuloAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator%=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorModuloAssignment(param0); }
					MOCK_METHOD1(operatorBitwiseAndAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator&=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseAndAssignment(param0); }
					MOCK_METHOD1(operatorBitwiseOrAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator|=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseOrAssignment(param0); }
					MOCK_METHOD1(operatorBitwiseXorAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator^=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseXorAssignment(param0); }
					MOCK_METHOD1(operatorBitwiseLeftShiftAssignement, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator<<=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseLeftShiftAssignement(param0); }
					MOCK_METHOD1(operatorBitwiseRightShiftAssignment, cepepito::parser::test::resources::TypeWithOperators &(const cepepito::parser::test::resources::TypeWithOperators &));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator>>=(const cepepito::parser::test::resources::TypeWithOperators & param0) { return operatorBitwiseRightShiftAssignment(param0); }
					MOCK_METHOD1(operatorBrackets, cepepito::parser::test::resources::TypeWithOperators &(int));
					virtual cepepito::parser::test::resources::TypeWithOperators & operator[](int param0) { return operatorBrackets(param0); }
					MOCK_METHOD0(operatorIndirection, cepepito::parser::test::resources::TypeWithOperators &());
					virtual cepepito::parser::test::resources::TypeWithOperators & operator*() { return operatorIndirection(); }
					MOCK_METHOD0(operatorAddressOf, cepepito::parser::test::resources::TypeWithOperators *());
					virtual cepepito::parser::test::resources::TypeWithOperators * operator&() { return operatorAddressOf(); }
					MOCK_METHOD0(operatorStructureDerefence, int *());
					virtual int * operator->() { return operatorStructureDerefence(); }
					MOCK_METHOD1(operatorMemberSelectedByPointerToMember, int(int));
					virtual int operator->*(int param0) { return operatorMemberSelectedByPointerToMember(param0); }
					MOCK_METHOD0(operatorFunctionCall, void());
					virtual void operator()() { return operatorFunctionCall(); }
					MOCK_METHOD1(operatorComma, cepepito::parser::test::resources::TypeWithOperators(int));
					virtual cepepito::parser::test::resources::TypeWithOperators operator,(int param0) { return operatorComma(param0); }
				};
			}
		}
	}
}

#endif
