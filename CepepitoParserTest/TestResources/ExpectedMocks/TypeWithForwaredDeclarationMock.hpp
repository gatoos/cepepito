#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWAREDDECLARATIONMOCK_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWAREDDECLARATIONMOCK_HPP

#include <gmock/gmock.h>
#include <TestResources/ClassesDefinitions/TypeWithForwaredDeclaration.hpp>

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithForwaredDeclarationMock : public cepepito::parser::test::resources::	TypeWithForwaredDeclaration
				{
				public:
					MOCK_METHOD1(uniqueVirtualMethodToBeMocked, int(int));
				};
			}
		}
	}
}

#endif
