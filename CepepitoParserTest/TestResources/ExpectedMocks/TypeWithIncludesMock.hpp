#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHINCLUDESMOCK_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHINCLUDESMOCK_HPP

#include <gmock/gmock.h>
#include <TestResources/ClassesDefinitions/TypeWithIncludes.hpp>
#include <ClassesDefinitions/TemplateClass.hpp>

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithIncludesMock : public cepepito::parser::test::resources::TypeWithIncludes
				{
				public:
					MOCK_METHOD0(compute, void());
				};
			}
		}
	}
}

#endif
