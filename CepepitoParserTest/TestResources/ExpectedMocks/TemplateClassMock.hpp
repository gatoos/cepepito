#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TEMPLATECLASSMOCK_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TEMPLATECLASSMOCK_HPP

#include <gmock/gmock.h>
#include <TestResources/ClassesDefinitions/TemplateClass.hpp>

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				template<class Alpha, int Beta>
				class TemplateClassMock : public cepepito::parser::test::resources::TemplateClass<Alpha, Beta>
				{
				public:
					MOCK_METHOD2_T(uniqueVirtualMethodToBeMocked, int(const char *, Alpha &));
				};
			}
		}
	}
}

#endif
