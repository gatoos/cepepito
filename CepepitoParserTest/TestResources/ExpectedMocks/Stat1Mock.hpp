#ifndef BENCHMARK_STAT1MOCK_HPP
#define BENCHMARK_STAT1MOCK_HPP

#include <gmock/gmock.h>
#include <TestResources/ClassesDefinitions/Stat1.hpp>

namespace benchmark
{
	template<class VType, class NumType>
	class Stat1Mock : public benchmark::Stat1<VType, NumType>
	{
	public:
		MOCK_METHOD0_T(Clear, void());
		MOCK_CONST_METHOD0_T(Sum, VType());
		MOCK_CONST_METHOD0_T(Mean, VType());
		MOCK_CONST_METHOD1_T(Mean, VType(VType *));
		MOCK_CONST_METHOD0_T(StdDev, VType());
	};
}

#endif
