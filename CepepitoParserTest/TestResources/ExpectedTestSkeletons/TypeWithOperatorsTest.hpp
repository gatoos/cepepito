#include <gtest/gtest.h>
#include <TestResources/ClassesDefinitions/TypeWithOperators.hpp>


namespace {

	class TypeWithOperatorsTest : public ::testing::Test
	{
	public:

	protected:

	};

	TEST_F(TypeWithOperatorsTest, operatorAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorAddition)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorSubstraction)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorUnaryPlus)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorUnaryMinus)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorMultiplication)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorDivision)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorModulo)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorIncrementPrefix)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorIncrementPostfix)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorDecrementPrefix)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorDecrementPostfix)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorEqual)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorNotEqual)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorLessThan)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorGreater)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorLessOrEqual)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorGreaterOrEqual)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorLogicalNot)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorLogicalAnd)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorLogicalOr)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseNot)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseAnd)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseOr)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseXor)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseLeftShift)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseRightShift)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorAdditionAssignement)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorSubstractionAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorMultiplicationAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorDivisionAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorModuloAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseAndAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseOrAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseXorAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseLeftShiftAssignement)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBitwiseRightShiftAssignment)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorBrackets)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorIndirection)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorAddressOf)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorStructureDerefence)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorMemberSelectedByPointerToMember)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorFunctionCall)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorComma)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorNew)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorNewArray)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorDelete)
	{
		// TODO : fill in test body
	}

	TEST_F(TypeWithOperatorsTest, operatorDeleteArray)
	{
		// TODO : fill in test body
	}

}

