#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWARDDECLARATIONINOTHERFILE_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWARDDECLARATIONINOTHERFILE_HPP

#include "TypeWithForwardDeclarationInOtherFile.fwd.hpp"

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithForwardDeclarationInOtherFile {
				public:
					double compute(double lhs, double rhs, int factor);
					virtual int uniqueVirtualMethodToBeMocked(int parameter);

				private:
					void privateSetVariable(int variable);
					int privateGetVariable();
				};
			}
		}
	}
}

#endif
