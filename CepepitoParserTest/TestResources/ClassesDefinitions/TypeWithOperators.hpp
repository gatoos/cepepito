#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHOPERATORS_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHOPERATORS_HPP

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithOperators {
				public:
					
					virtual TypeWithOperators & operator=(const TypeWithOperators &);
					virtual TypeWithOperators operator+(const TypeWithOperators &);
					virtual TypeWithOperators operator-(const TypeWithOperators &);
					virtual TypeWithOperators operator+();
					virtual TypeWithOperators operator-();
					virtual TypeWithOperators operator*(const TypeWithOperators &);
					virtual TypeWithOperators operator/(const TypeWithOperators &);
					virtual TypeWithOperators operator%(const TypeWithOperators &);
					virtual TypeWithOperators & operator++();
					virtual TypeWithOperators operator++(int);
					virtual TypeWithOperators & operator--();
					virtual TypeWithOperators operator--(int);

					virtual bool operator==(const TypeWithOperators &);
					virtual bool operator!=(const TypeWithOperators &);
					virtual bool operator<(const TypeWithOperators &);
					virtual bool operator>(const TypeWithOperators &);
					virtual bool operator<=(const TypeWithOperators &);
					virtual bool operator>=(const TypeWithOperators &);

					virtual bool operator!();
					virtual bool operator&&(const TypeWithOperators &);
					virtual bool operator||(const TypeWithOperators &);
					
					virtual TypeWithOperators operator~();
					virtual TypeWithOperators operator&(const TypeWithOperators &);
					virtual TypeWithOperators operator|(const TypeWithOperators &);
					virtual TypeWithOperators operator^(const TypeWithOperators &);
					virtual TypeWithOperators operator<<(const TypeWithOperators &);
					virtual TypeWithOperators operator>>(const TypeWithOperators &);
					
					virtual TypeWithOperators & operator+=(const TypeWithOperators &);
					virtual TypeWithOperators & operator-=(const TypeWithOperators &);
					virtual TypeWithOperators & operator*=(const TypeWithOperators &);
					virtual TypeWithOperators & operator/=(const TypeWithOperators &);
					virtual TypeWithOperators & operator%=(const TypeWithOperators &);
					virtual TypeWithOperators & operator&=(const TypeWithOperators &);
					virtual TypeWithOperators & operator|=(const TypeWithOperators &);
					virtual TypeWithOperators & operator^=(const TypeWithOperators &);
					virtual TypeWithOperators & operator<<=(const TypeWithOperators &);
					virtual TypeWithOperators & operator>>=(const TypeWithOperators &);

					virtual TypeWithOperators & operator[](int);
					virtual TypeWithOperators & operator*();
					virtual TypeWithOperators * operator&();
					virtual int * operator->();
					virtual int operator->*(int);

					virtual void operator()();
					virtual TypeWithOperators operator,(int);
										
					void * operator new(size_t);
					void * operator new[](size_t);
					void operator delete(void *);
					void operator delete[](void *);
				
				private:
					int value;
				};
			}
		}
	}
}

#endif

