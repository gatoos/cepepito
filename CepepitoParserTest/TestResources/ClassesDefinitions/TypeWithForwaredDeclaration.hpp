#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWARDDECLARATION_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWARDDECLARATION_HPP

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithForwaredDeclaration;

				class TypeWithForwaredDeclaration {
				public:
					double compute(double lhs, double rhs, int factor);
					virtual int uniqueVirtualMethodToBeMocked(int parameter);

				private:
					void privateSetVariable(int variable);
					int privateGetVariable();
				};
			}
		}
	}
}

#endif


