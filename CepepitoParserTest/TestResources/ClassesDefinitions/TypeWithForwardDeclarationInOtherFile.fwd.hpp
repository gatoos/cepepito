#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWARDDECLARATIONINOTHERFILE_FWD_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHFORWARDDECLARATIONINOTHERFILE_FWD_HPP

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithForwardDeclarationInOtherFile;
			}
		}
	}
}

#endif
