#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHOUTVIRTUALMETHOD_HPP
#define CEPEPITO_PARSER_TEST_RESOURCES_TYPEWITHOUTVIRTUALMETHOD_HPP

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				class TypeWithoutVirtualMethods {
				public:
					double compute(double lhs, double rhs, int factor);					

				private:
					void privateSetVariable(int variable);
					int privateGetVariable();
				};
			}
		}
	}
}

#endif


