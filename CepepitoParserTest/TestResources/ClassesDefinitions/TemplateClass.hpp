#ifndef CEPEPITO_PARSER_TEST_RESOURCES_TEMPLATE_CLASS
#define CEPEPITO_PARSER_TEST_RESOURCES_TEMPLATE_CLASS

namespace cepepito
{
	namespace parser
	{
		namespace test
		{
			namespace resources
			{
				template<typename Alpha, int Beta>
				class TemplateClass {
				public:
					typedef TemplateClass<Alpha, Beta> Self;

					TotoClass(int variable);
					void setVariable(int variable);
					int getVariable() const;
					double compute(double lhs, double rhs, int factor);
					virtual int uniqueVirtualMethodToBeMocked(const char * parameter, Alpha &);

				private:
					void privateSetVariable(int variable);
					int privateGetVariable();

				private:
					int m_variable;
					Alpha m_variableAlpha;
					char[Beta] m_variableBeta;
				};
			}
		}		
	}
}

#endif


