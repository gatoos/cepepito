/* Copyright (C) HEDIA Rached - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
*/
#include "gtest/gtest.h"
#include "CepepitoParser.hpp"
#include "CepepitoParserResult.hpp"
#include "CepepitoParserTestHelper.hpp"
#include <fstream>
#include <sstream>

static std::string getExpectedTestSkeleton(const std::string testSkeletonName)
{
	if (testSkeletonName.empty())
		return "";

	std::stringstream filename;
	filename << "TestResources\\ExpectedTestSkeletons\\" << testSkeletonName << ".hpp";
	return CepepitoParserTestHelper::getFileContent(filename.str());
}

class CepepitoParserTestSkeletonGeneratorTest : public ::testing::Test, public ::testing::WithParamInterface<TestCaseInformations>
{
};

INSTANTIATE_TEST_CASE_P(MockTestsExecution,
	CepepitoParserTestSkeletonGeneratorTest,
	::testing::Values(TestCaseInformations("TemplateClass.hpp", "TemplateClassTest", "class TemplateClass {", "struct T"),
		TestCaseInformations("TypeWithForwaredDeclaration.hpp", "TypeWithForwaredDeclarationTest", "class TypeWithForwaredDeclaration {", "class T"),
		TestCaseInformations("Stat1.hpp", "Stat1Test", "class Stat1 {", "struct S"),
		TestCaseInformations("TypeWithOperators.hpp", "TypeWithOperatorsTest", "class TypeWithOperators {", "class T"),
		TestCaseInformations("TypeWithForwardDeclarationInOtherFile.fwd.hpp", "", "class TypeWithForwardDeclarationInOtherFile;", "class T", std::vector<const char *>(), std::vector<std::string>{"Cannot", "retrieve", "TypeWithForwardDeclarationInOtherFile", "definition"})
	));

TEST_P(CepepitoParserTestSkeletonGeneratorTest, GIVEN_a_class_template_definition_WHEN_generating_a_test_skeleton_on_it_THEN_test_skeleton_is_correctly_generated)
{
	std::string fileToParse = CepepitoParserTestHelper::getTypeDefinitionPath(GetParam().m_sourceTypeName);
	std::string expectedResult = getExpectedTestSkeleton(GetParam().m_targetTypeName);
	const char ** includeDirectories = GetParam().getIncludeDirectories();
	unsigned int numberOfIncludeDirectories = GetParam().getNumberOfIncludeDirectories();
	const char ** generatedFileIncludeDirectories = GetParam().getGeneratedFileIncludeDirectories();
	unsigned int generatedFileNumberOfIncludeDirectories = GetParam().getGeneratedFileNumberOfIncludeDirectories();
	auto cursorLocation = CepepitoParserTestHelper::getSymbolLocation(fileToParse, GetParam().m_cursorLocationSearchString, GetParam().m_cursorLocationSearchSubstring);

	CepepitoParserResult * cepepitoParserResult = cepepitoparser_generateTestSkeletonFromFileWithCursorLocation(fileToParse.c_str(), nullptr, nullptr, 0, includeDirectories, numberOfIncludeDirectories, generatedFileIncludeDirectories, generatedFileNumberOfIncludeDirectories, cursorLocation.m_line, cursorLocation.m_column);

	std::string actualTestName = std::string(cepepitoparser_getGeneratedTypeNameAsCString(cepepitoParserResult));
	std::string expectedTestName = GetParam().m_targetTypeName;

	std::string actualTestDefinition = CepepitoParserTestHelper::cleanedResult(cepepitoparser_getParserResultAsCString(cepepitoParserResult));
	std::string expectedTestDefinition = CepepitoParserTestHelper::cleanedResult(expectedResult);

	std::string errorMessage(cepepitoparser_getErrorMessageAsCString(cepepitoParserResult));

	ASSERT_EQ(expectedTestName, actualTestName);
	ASSERT_EQ(expectedTestDefinition, actualTestDefinition);
	for (auto errorMessageFragment : GetParam().m_errorMessageFragments)
	{
		ASSERT_TRUE(errorMessage.find(errorMessageFragment) != std::string::npos);
	}
	cepepitoparser_releaseParserResult(cepepitoParserResult);
}