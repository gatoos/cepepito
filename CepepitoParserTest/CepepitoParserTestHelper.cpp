#include "CepepitoParserTestHelper.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

static bool isSpaceOrTab(char c)
{
	return c == ' ' || c == '\t';
}

/** \brief	Retrieves the line number containing the string symbol in the file filename 
 *			Once found, it shift right after substring.
 *			Ex : I have a file named my-awesome-file with two lines
 *			Hello my name is toto
 *			And I'm on holidays
 *			Calling getSymbolLocation("my-awesome-file", "holidays", "holi") will return line : CursorLocation(1, 15)
 *	\return CursorLocation structure. It contains the line and column matching the research.
 *			If no match is found, it returns a cursor with -1, -1 as location
 *  \throws If the file doesn't exists it throws an exception
 */
CursorLocation CepepitoParserTestHelper::getSymbolLocation(const std::string & filename, const std::string & symbol, const std::string & substring)
{
	string line;
	ifstream myfile(filename);
	int lineNumber = 1;
		
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			if (line.find(symbol) != string::npos)
			{
				myfile.close();
				return CursorLocation(lineNumber, line.find(symbol) + substring.size());
			}
			
			++lineNumber;
		}
		myfile.close();
		
		return CursorLocation();	
	}
	else
	{
		stringstream errorMessage;
		errorMessage << "Failed to open " << filename;
		throw new exception(errorMessage.str().c_str());
	}
}

std::string CepepitoParserTestHelper::cleanedResult(const char * source)
{
	std::string result(source);
	result.erase(std::remove_if(result.begin(), result.end(), isSpaceOrTab), result.end());
	return result;
}

std::string CepepitoParserTestHelper::cleanedResult(const std::string & source)
{
	return cleanedResult(source.c_str());
}

std::string CepepitoParserTestHelper::getFileContent(const std::string & filename)
{
	std::stringstream fileContent;
	std::string line;

	std::ifstream fileStream(filename.c_str(), std::ifstream::in);
	if (fileStream.is_open())
	{
		while (getline(fileStream, line))
		{
			fileContent << line << std::endl;
		}
	}

	return fileContent.str();
}

std::string CepepitoParserTestHelper::getTypeDefinitionPath(const std::string & typeName)
{
	std::stringstream filePath;
	filePath << "TestResources\\ClassesDefinitions\\" << typeName;
	return filePath.str();
}


TestCaseInformations::TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring, std::vector<const char *> includeDirectories) :
	m_sourceTypeName(sourceTypeName),
	m_targetTypeName(targetTypeName),
	m_cursorLocationSearchString(cursorLocationSearchString),
	m_cursorLocationSearchSubstring(cursorLocationSearchSubstring),
	m_includeDirectories(includeDirectories),
	m_generatedFileIncludeDirectories(),
	m_errorMessageFragments()
{

}

TestCaseInformations::TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring, std::vector<const char *> includeDirectories,
	std::vector<const char *> generatedFileIncludeDirectories) :
	m_sourceTypeName(sourceTypeName),
	m_targetTypeName(targetTypeName),
	m_cursorLocationSearchString(cursorLocationSearchString),
	m_cursorLocationSearchSubstring(cursorLocationSearchSubstring),
	m_includeDirectories(includeDirectories),
	m_generatedFileIncludeDirectories(generatedFileIncludeDirectories),
	m_errorMessageFragments()
{

}

TestCaseInformations::TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring, std::vector<const char *> includeDirectories, std::vector<std::string> errorMessageFragments) :
	m_sourceTypeName(sourceTypeName),
	m_targetTypeName(targetTypeName),
	m_cursorLocationSearchString(cursorLocationSearchString),
	m_cursorLocationSearchSubstring(cursorLocationSearchSubstring),
	m_includeDirectories(),
	m_generatedFileIncludeDirectories(),
	m_errorMessageFragments(errorMessageFragments)	
{

}

TestCaseInformations::TestCaseInformations(std::string sourceTypeName, std::string targetTypeName, std::string cursorLocationSearchString, std::string cursorLocationSearchSubstring) :
	m_sourceTypeName(sourceTypeName),
	m_targetTypeName(targetTypeName),
	m_cursorLocationSearchString(cursorLocationSearchString),
	m_cursorLocationSearchSubstring(cursorLocationSearchSubstring),
	m_includeDirectories(),
	m_generatedFileIncludeDirectories(),
	m_errorMessageFragments()
{

}



const char ** TestCaseInformations::getIncludeDirectories() const
{
	if (!m_includeDirectories.empty())
	{
		return const_cast<const char **>(&m_includeDirectories[0]);
	}
	else
	{
		return nullptr;
	}
}

int TestCaseInformations::getNumberOfIncludeDirectories() const
{
	return static_cast<int>(m_includeDirectories.size());
}

const char ** TestCaseInformations::getGeneratedFileIncludeDirectories() const
{
	if (!m_generatedFileIncludeDirectories.empty())
	{
		return const_cast<const char **>(&m_generatedFileIncludeDirectories[0]);
	}
	else
	{
		return nullptr;
	}
}

int TestCaseInformations::getGeneratedFileNumberOfIncludeDirectories() const
{
	return static_cast<int>(m_generatedFileIncludeDirectories.size());
}