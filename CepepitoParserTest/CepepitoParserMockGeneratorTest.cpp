/* Copyright (C) HEDIA Rached - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
*/
#include "gtest/gtest.h"
#include "CepepitoParser.hpp"
#include "CepepitoParserResult.hpp"
#include "CepepitoParserTestHelper.hpp"
#include <fstream>
#include <sstream>
#include <string>

static std::string getExpectedMockDefinition(const std::string mockName)
{
	if (mockName.empty())
		return "";

	std::stringstream filename;
	filename << "TestResources\\ExpectedMocks\\" << mockName << ".hpp";
	return CepepitoParserTestHelper::getFileContent(filename.str());
}

class CepepitoParserMockGeneratorTest : public ::testing::Test, public ::testing::WithParamInterface<TestCaseInformations>
{
};

INSTANTIATE_TEST_CASE_P(MockTestsExecution,
	CepepitoParserMockGeneratorTest,
	::testing::Values(TestCaseInformations("TemplateClass.hpp", "TemplateClassMock", "class TemplateClass {", "struct T"),
		TestCaseInformations("TypeWithForwaredDeclaration.hpp", "TypeWithForwaredDeclarationMock", "class TypeWithForwaredDeclaration {", "class T"),
		TestCaseInformations("Stat1.hpp", "Stat1Mock", "class Stat1 {", "struct S"),
		TestCaseInformations("TypeWithOperators.hpp", "TypeWithOperatorsMock", "class TypeWithOperators {", "class T"),
		TestCaseInformations("TypeWithForwardDeclarationInOtherFile.fwd.hpp", "", "class TypeWithForwardDeclarationInOtherFile;", "class T", std::vector<const char *>(), std::vector<std::string>{"Cannot", "retrieve", "TypeWithForwardDeclarationInOtherFile", "definition"}),
		TestCaseInformations("TypeWithoutVirtualMethods.hpp", "", "class TypeWithoutVirtualMethods {", "class T", std::vector<const char *>(), std::vector<std::string>{"Type", "virtual", "method"}),
		TestCaseInformations("TypeWithIncludes.hpp", "TypeWithIncludesMock", "class TypeWithIncludes {", "class T", std::vector<const char *>(), std::vector<const char *>{"c:/Users/Rached/code/projects/cepepito/cppunittests", "c:/Users/Rached/code/projects/cepepito/cppunittests/CepepitoParserTest/TestResources"}),
			TestCaseInformations("TypeWithIncludes.hpp", "TypeWithIncludesMock", "class TypeWithIncludes {", "class T", std::vector<const char *>(), std::vector<const char *>{"c:/Users/Rached/code/projects/cepepito/cppunittests/CepepitoParserTest/TestResources", "c:/Users/Rached/code/projects/cepepito/cppunittests"})
	));

TEST_P(CepepitoParserMockGeneratorTest, GIVEN_a_class_template_definition_WHEN_generating_a_mock_on_it_THEN_mock_is_correctly_generated)
{
	std::string fileToParse = CepepitoParserTestHelper::getTypeDefinitionPath(GetParam().m_sourceTypeName);
	std::string expectedResult = getExpectedMockDefinition(GetParam().m_targetTypeName);
	const char ** includeDirectories = GetParam().getIncludeDirectories();
	unsigned int numberOfIncludeDirectories = GetParam().getNumberOfIncludeDirectories();
	const char ** generatedFileIncludeDirectories = GetParam().getGeneratedFileIncludeDirectories();
	unsigned int generatedFileNumberOfIncludeDirectories = GetParam().getGeneratedFileNumberOfIncludeDirectories();
	auto cursorLocation = CepepitoParserTestHelper::getSymbolLocation(fileToParse, GetParam().m_cursorLocationSearchString, GetParam().m_cursorLocationSearchSubstring);
	
	CepepitoParserResult * cepepitoParserResult = cepepitoparser_generateMockFromFileWithCursorLocation(fileToParse.c_str(), nullptr, nullptr, 0, includeDirectories, numberOfIncludeDirectories, generatedFileIncludeDirectories, generatedFileNumberOfIncludeDirectories, cursorLocation.m_line, cursorLocation.m_column);
	
	std::string actualMockName = std::string(cepepitoparser_getGeneratedTypeNameAsCString(cepepitoParserResult));
	std::string expectedMockName = GetParam().m_targetTypeName;

	std::string actualMockDefinition = CepepitoParserTestHelper::cleanedResult(cepepitoparser_getParserResultAsCString(cepepitoParserResult));
	std::string expectedMockDefinition = CepepitoParserTestHelper::cleanedResult(expectedResult);

	std::string errorMessage(cepepitoparser_getErrorMessageAsCString(cepepitoParserResult));
		
	ASSERT_EQ(expectedMockName, actualMockName);
	ASSERT_EQ(expectedMockDefinition, actualMockDefinition);
	for (auto errorMessageFragment : GetParam().m_errorMessageFragments)
	{
		ASSERT_TRUE(errorMessage.find(errorMessageFragment) != std::string::npos);
	}	
	cepepitoparser_releaseParserResult(cepepitoParserResult);
}