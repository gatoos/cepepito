/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#include <string>
#include <vector>
#include "clang-c/Index.h"

namespace cepepito
{
	namespace parser
	{
		class IParserResultFormater
		{
		public : 
			virtual ~IParserResultFormater()
			{}
			
			virtual std::string format(const std::string & rawParserResult, CXTranslationUnit & translationUnit, 
				const std::string & toBeMockedTypeDescription, std::vector<std::string> & typeNamespaces, 
				CXFile & typeDefinitionFile,
				const char * includeDirectories[], unsigned int numberOfIncludeDirectories) const = 0;
		};
	}
}

