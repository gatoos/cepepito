/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#include "exportsMacro.hpp"

class CepepitoParserResult
{
public:
	CepepitoParserResult();
	~CepepitoParserResult();
	
	void setGeneratedTypeName(const char * mockedObjectName);
	const char * getGeneratedTypeName() const;
	void setResultCRepresentation(const char * resultCRepresentation);
	const char * getResultCRepresentation() const;
	void setErrorMessage(const char * errorMessage);
	const char * getErrorMessage() const;
	
private:
	char * m_generatedTypeName;
	char * m_resultCRepresentation;
	char * m_errorMessage;	
};

extern "C"
{	
	CEPEPITOPARSER_API const char * cepepitoparser_getGeneratedTypeNameAsCString(CepepitoParserResult *);
	CEPEPITOPARSER_API const char * cepepitoparser_getParserResultAsCString(CepepitoParserResult *);
	CEPEPITOPARSER_API const char * cepepitoparser_getErrorMessageAsCString(CepepitoParserResult *);
	CEPEPITOPARSER_API void cepepitoparser_releaseParserResult(CepepitoParserResult *);
}
