/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#ifdef CEPEPITOPARSER_EXPORTS
#define CEPEPITOPARSER_API __declspec(dllexport) 
#else
#define CEPEPITOPARSER_API __declspec(dllimport) 
#endif
