/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#include "AbstractParser.hpp"

namespace cepepito
{
	namespace parser
	{
		namespace testskeletonparser
		{
			class TestSkeletonParser : public AbstractParser
			{
			public:
				ParserResult parse(CXTranslationUnit & translationUnit, const std::string & typeIdentifier, const CXFile & typeDefinitionFile) const;
			};
		}
	}
}

