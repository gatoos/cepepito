/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

// LibClangCppTest.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "clang-c/Index.h"
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include "CepepitoParser.hpp"
#include "CepepitoParserResult.hpp"
#include "CepepitoParserUtils.hpp"
#include "IParserResultFormater.hpp"
#include "AbstractParser.hpp"
#include "MockParser.hpp"
#include "MockFileParserResultFormater.hpp"
#include "TestSkeletonParser.hpp"
#include "TestSkeletonParserResultFormater.hpp"
#include <fstream>
#include <map>
#include <set>
#include <algorithm>

using namespace cepepito::parser::utils;
using namespace cepepito::parser;

struct ToBeMockedTypeDescription
{
	bool m_typeFound;
	std::string m_typeIdentifier;
	CXFile m_typeDefinitionFile;
	std::string m_errorMessage;
	
	ToBeMockedTypeDescription() :
		m_typeFound(false),
		m_typeIdentifier(),
		m_typeDefinitionFile(),
		m_errorMessage()
	{}
};

ToBeMockedTypeDescription retrieveClassLabelFromCursor(CXCursor cursorAtLine)
{
	ToBeMockedTypeDescription returnValue;
	returnValue.m_typeFound = false;
	CXCursor cursorSemanticParent = cursorAtLine;

	while (!returnValue.m_typeFound && clang_getCursorKind(cursorSemanticParent) != CXCursorKind::CXCursor_FirstInvalid)
	{
		if (clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_ClassDecl ||
			clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_StructDecl ||
			clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_ClassTemplate)
		{
			auto originalCursor = cursorSemanticParent;
			cursorSemanticParent = clang_getCursorDefinition(cursorSemanticParent);
			if (clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_ClassDecl ||
				clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_StructDecl ||
				clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_ClassTemplate)
			{
				returnValue.m_typeIdentifier = getCursorSpelling(cursorSemanticParent);
				returnValue.m_typeFound = true;
				clang_getFileLocation(clang_getCursorLocation(cursorSemanticParent), &returnValue.m_typeDefinitionFile, NULL, NULL, NULL);
			}
			else
			{
				returnValue.m_errorMessage = std::string("Cannot retrieve ") + std::string(getCursorSpelling(originalCursor)) + std::string(" definition. Retry while focusing on type definition instead of forward declaration");
				return returnValue;				
			}
		}			
		else if (clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_TypeRef)
		{
			CXType type = clang_getCursorType(cursorSemanticParent);
			if (type.kind != CXTypeKind::CXType_Unexposed)
			{
				cursorSemanticParent = clang_getTypeDeclaration(type);
			}			
			else
			{
				unsigned int line = 0;
				unsigned int column = 0;
				CXFile file;

				CXSourceRange sourceRange = clang_getCursorExtent(cursorSemanticParent);
				CXSourceLocation endOfNamespaceRefCursorLocation = clang_getRangeEnd(sourceRange);
				clang_getFileLocation(endOfNamespaceRefCursorLocation, &file, &line, &column, NULL);
				CXSourceLocation newSourceLocation = clang_getLocation(clang_Cursor_getTranslationUnit(cursorSemanticParent), file, line, column + 1);
				cursorSemanticParent = clang_getCursor(clang_Cursor_getTranslationUnit(cursorSemanticParent), newSourceLocation);
			}
		}
		else if (clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_TemplateRef)
		{
			do
			{
				unsigned int line = 0;
				unsigned int column = 0;
				CXFile file;

				CXSourceRange sourceRange = clang_getCursorExtent(cursorSemanticParent);
				CXSourceLocation endOfNamespaceRefCursorLocation = clang_getRangeEnd(sourceRange);
				clang_getFileLocation(endOfNamespaceRefCursorLocation, &file, &line, &column, NULL);
				CXSourceLocation newSourceLocation = clang_getLocation(clang_Cursor_getTranslationUnit(cursorSemanticParent), file, line, column + 1);
				cursorSemanticParent = clang_getCursor(clang_Cursor_getTranslationUnit(cursorSemanticParent), newSourceLocation);
			} while (clang_getCursorKind(cursorSemanticParent) != CXCursorKind::CXCursor_VarDecl &&
					 clang_getCursorKind(cursorSemanticParent) != CXCursorKind::CXCursor_FieldDecl &&
					 clang_getCursorKind(cursorSemanticParent) != CXCursor_FirstInvalid);
		}
		else if (clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_VarDecl ||
			clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_FieldDecl)
		{
			CXType type = clang_getCursorType(cursorSemanticParent);

			while (type.kind == CXTypeKind::CXType_Pointer ||
				type.kind == CXTypeKind::CXType_LValueReference ||
				type.kind == CXTypeKind::CXType_RValueReference)
			{
				type = clang_getPointeeType(type);
			}

			cursorSemanticParent = clang_getTypeDeclaration(type);
		}
		else if (clang_getCursorKind(cursorSemanticParent) == CXCursorKind::CXCursor_NamespaceRef)
		{
			unsigned int line = 0;
			unsigned int column = 0;
			CXFile file;

			CXSourceRange sourceRange = clang_getCursorExtent(cursorSemanticParent);
			CXSourceLocation endOfNamespaceRefCursorLocation = clang_getRangeEnd(sourceRange);
			clang_getFileLocation(endOfNamespaceRefCursorLocation, &file, &line, &column, NULL);
			CXSourceLocation newSourceLocation = clang_getLocation(clang_Cursor_getTranslationUnit(cursorSemanticParent), file, line, column + 1);
			cursorSemanticParent = clang_getCursor(clang_Cursor_getTranslationUnit(cursorSemanticParent), newSourceLocation);
		}
		else
		{
			cursorSemanticParent = clang_getCursorSemanticParent(cursorSemanticParent);
		}
	}

	return returnValue;
}

static std::vector<const char *> createDefaultCommandLineArguments()
{
	std::vector<const char *> defaultCommandLineArguments;
	defaultCommandLineArguments.push_back("-x");
	defaultCommandLineArguments.push_back("c++");

	return defaultCommandLineArguments;
}

static CXUnsavedFile * buildUnsavedFiles(unsigned int numberOfUnsavedFiles, const char *unsavedFileNames[],
	const char * unsavedFileContents[])
{
	CXUnsavedFile * cxUnsavedFile = NULL;
	if (numberOfUnsavedFiles > 0)
	{
		cxUnsavedFile = new CXUnsavedFile[numberOfUnsavedFiles];
		for (unsigned int i = 0; i < numberOfUnsavedFiles; ++i)
		{
			cxUnsavedFile[i].Filename = unsavedFileNames[i];
			cxUnsavedFile[i].Contents = unsavedFileContents[i];
			cxUnsavedFile[i].Length = strlen(unsavedFileContents[i]);
		}
	}

	return cxUnsavedFile;
}

static CepepitoParserResult * cepepitoparser_parseAndGenerateFromFileWithCursorLocation(const char * fileToParse,
	const char * unsavedFileNames[], const char * unsavedFileContents[], unsigned int numberOfUnsavedFiles,
	const char * includeDirectories[], unsigned int numberOfIncludeDirectories,
	const char * generatedFileIncludeDirectories[], unsigned int generatedFileNumberOfIncludeDirectories,
	unsigned int line, unsigned int column,	
	const AbstractParser & parser, IParserResultFormater * parserResultFormater)
{
	CepepitoParserResult * returnValuePointer = new CepepitoParserResult();
	CepepitoParserResult & returnValue = *returnValuePointer;

	// parseFile
	std::vector<const char *> commandLineArguments = createDefaultCommandLineArguments();	
	CXUnsavedFile * cxUnsavedFile = buildUnsavedFiles(numberOfUnsavedFiles, unsavedFileNames, unsavedFileContents);	
	CXIndex cxIndex = clang_createIndex(0, 0);
	std::vector<const char *> includePathsArguments = generateCommandLineArgumentsFromIncludeDirectories(includeDirectories, numberOfIncludeDirectories);
	commandLineArguments.insert(commandLineArguments.end(), includePathsArguments.begin(), includePathsArguments.end());
	CXTranslationUnit translationUnit;	
	CXErrorCode translationUnitError = clang_parseTranslationUnit2(cxIndex, fileToParse, &commandLineArguments[0], commandLineArguments.size(), cxUnsavedFile, numberOfUnsavedFiles, 0, &translationUnit);

	// noise to be removed
	for (const char * includePathArgument : includePathsArguments)
	{
		free(const_cast<char *>(includePathArgument));
	}

	if (translationUnitError != CXError_Success)
	{
		returnValue.setErrorMessage("An error occured during file parsing");
	}
	else
	{
		// generate mock from parser tree
		CXSourceLocation sourceLocationAtLineColumn = clang_getLocation(translationUnit, clang_getFile(translationUnit, fileToParse), line, column);
		CXCursor cursorAtLineColumn = clang_getCursor(translationUnit, sourceLocationAtLineColumn);
		ToBeMockedTypeDescription toBeMockedTypeDescription = retrieveClassLabelFromCursor(cursorAtLineColumn);
		
		if (toBeMockedTypeDescription.m_typeFound)
		{
			try
			{
				AbstractParser::ParserResult parserResult = parser.parse(translationUnit, toBeMockedTypeDescription.m_typeIdentifier, toBeMockedTypeDescription.m_typeDefinitionFile);

				if (parserResultFormater)
				{
					std::string  formatedParserResult = parserResultFormater->format(parserResult.m_parserResults,
						translationUnit, toBeMockedTypeDescription.m_typeIdentifier,
						parserResult.m_typeNamespaces,
						toBeMockedTypeDescription.m_typeDefinitionFile, generatedFileIncludeDirectories,
						generatedFileNumberOfIncludeDirectories);

					parserResult.m_parserResults.swap(formatedParserResult);
				}

				returnValue.setGeneratedTypeName(parserResult.m_generatedTypeName.c_str());
				returnValue.setResultCRepresentation(parserResult.m_parserResults.c_str());
				returnValue.setErrorMessage(parserResult.m_errorMessage.c_str());
			}
			catch (const std::exception & parseException)
			{
				returnValue.setResultCRepresentation("");
				returnValue.setErrorMessage(parseException.what());
			}			
		}
		else
		{
			returnValue.setResultCRepresentation("");
			returnValue.setErrorMessage(toBeMockedTypeDescription.m_errorMessage != "" ? toBeMockedTypeDescription.m_errorMessage.c_str() : "Cannot find type definition at cursor location");
		}
	}

	clang_disposeTranslationUnit(translationUnit);
	clang_disposeIndex(cxIndex);
	return returnValuePointer;
}

CepepitoParserResult * cepepitoparser_generateTestSkeletonFromFileWithCursorLocation(const char * fileToParse,
	const char * unsavedFileNames[],
	const char * unsavedFileContents[],
	unsigned int numberOfUnsavedFiles,
	const char * includeDirectories[],
	unsigned int numberOfIncludeDirectories,
	const char * generatedFileIncludeDirectories[],
	unsigned int generatedFileNumberOfIncludeDirectories,	
	unsigned int line, unsigned int column)
{
	TestSkeletonParserResultFormater testSkeletonParserResultFormater;
	return cepepitoparser_parseAndGenerateFromFileWithCursorLocation(fileToParse, unsavedFileNames, unsavedFileContents, numberOfUnsavedFiles, includeDirectories, numberOfIncludeDirectories, 
		generatedFileIncludeDirectories, generatedFileNumberOfIncludeDirectories, line, column, testskeletonparser::TestSkeletonParser(), &testSkeletonParserResultFormater);
}

CepepitoParserResult * cepepitoparser_generateMockFromFileWithCursorLocation(const char * fileToParse,
	const char * unsavedFileNames[],
	const char * unsavedFileContents[],
	unsigned int numberOfUnsavedFiles,
	const char * includeDirectories[],
	unsigned int numberOfIncludeDirectories,
	const char * generatedFileIncludeDirectories[], 
	unsigned int generatedFileNumberOfIncludeDirectories,
	unsigned int line, unsigned int column)
{
	MockFileParserResultFormater mockFileParserResultFormater;
	return cepepitoparser_parseAndGenerateFromFileWithCursorLocation(fileToParse, unsavedFileNames, unsavedFileContents, numberOfUnsavedFiles, includeDirectories, numberOfIncludeDirectories, 
		generatedFileIncludeDirectories, generatedFileNumberOfIncludeDirectories, line, column, mockparser::MockParser(), &mockFileParserResultFormater);
}

CepepitoParserResult * cepepitoparser_generateMockSnippetFromFileWithCursorLocation(const char * fileToParse,
	const char * unsavedFileNames[],
	const char * unsavedFileContents[],
	unsigned int numberOfUnsavedFiles,
	const char * includeDirectories[],
	unsigned int numberOfIncludeDirectories,
	unsigned int line, unsigned int column)
{
	return cepepitoparser_parseAndGenerateFromFileWithCursorLocation(fileToParse, unsavedFileNames, unsavedFileContents, numberOfUnsavedFiles, includeDirectories, numberOfIncludeDirectories, 
		nullptr, 0, line, column, mockparser::MockParser(), nullptr);	
}


