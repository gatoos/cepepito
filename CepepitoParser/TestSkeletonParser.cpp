/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "TestSkeletonParser.hpp"
#include "CepepitoParserUtils.hpp"
#include <map>
#include <sstream>

using namespace cepepito::parser::utils;

namespace cepepito
{
	namespace parser
	{
		namespace testskeletonparser
		{
			static void printMethodsByAccessSpecifier(std::stringstream & parsingResult, std::map<int, std::vector<std::string> > byAccessSpecifierMethods, CX_CXXAccessSpecifier accessLevelSpecifierToBePrinted)
			{
				auto methodsForGivenAccessSpecifier = byAccessSpecifierMethods.find(accessLevelSpecifierToBePrinted);
				if (methodsForGivenAccessSpecifier != byAccessSpecifierMethods.end() && !methodsForGivenAccessSpecifier->second.empty())
				{
					for (auto & methodForGivenAccessSpecifier : methodsForGivenAccessSpecifier->second)
					{
						parsingResult << methodForGivenAccessSpecifier << std::endl;
						parsingResult << "{" << std::endl;
						parsingResult << "// TODO : fill in test body" << std::endl;
						parsingResult << "}" << std::endl;
						parsingResult << std::endl;
					}
				}
			}

			static CXChildVisitResult visitAndGenerateTestSkeletonInternal(CXCursor cursor, CXCursor parent, CXClientData data)
			{
				CXSourceLocation location = clang_getCursorLocation(cursor);
				CXFile cursorFile;
				clang_getFileLocation(location, &cursorFile, NULL, NULL, NULL);
				AbstractParsingUserData * abstractParsingUserData = static_cast<AbstractParsingUserData *>(data);

				if (!clang_File_isEqual(cursorFile, abstractParsingUserData->m_typeDefinitionFile))
				{
					return CXChildVisit_Continue;
				}

				CXCursorKind curKind = clang_getCursorKind(cursor);

				if (curKind == CXCursor_Namespace)
				{
					MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
					std::stringstream & parsingResult = mockParserUserData->m_parserResults;

					MockParserUserData childParsingToBeMockedDescription(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);

					clang_visitChildren(cursor, visitAndGenerateTestSkeletonInternal, &childParsingToBeMockedDescription);

					mockParserUserData->m_typeDefinitionFound = childParsingToBeMockedDescription.m_typeDefinitionFound;
					mockParserUserData->m_typeParsingDone = childParsingToBeMockedDescription.m_typeParsingDone;
					mockParserUserData->m_errorMessage = childParsingToBeMockedDescription.m_errorMessage;

					mockParserUserData->m_typeNamespaces.insert(mockParserUserData->m_typeNamespaces.end(),
						childParsingToBeMockedDescription.m_typeNamespaces.begin(),
						childParsingToBeMockedDescription.m_typeNamespaces.end());

					if (childParsingToBeMockedDescription.m_parserResults.str() != "")
					{
						mockParserUserData->m_typeNamespaces.push_back(getCursorSpelling(cursor));

						parsingResult << childParsingToBeMockedDescription.m_parserResults.str();
					}
					return CXChildVisit_Continue;
				}

				if (!abstractParsingUserData->m_typeDefinitionFound)
				{
					if (curKind == CXCursor_StructDecl)
					{
						MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
						auto cursorDefinition = clang_getCursorDefinition(cursor);
						if (!getCursorSpelling(cursorDefinition).compare(mockParserUserData->m_typeIdentifier))
						{
							mockParserUserData->m_typeDefinitionFound = true;
							std::stringstream & parsingResult = mockParserUserData->m_parserResults;
							ByAccessLevelMethods byAccessLevelMethods(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);
							clang_visitChildren(cursorDefinition, visitAndGenerateTestSkeletonInternal, &byAccessLevelMethods);
							removeDuplicateMethods(byAccessLevelMethods.m_byAccessLevelMethods);
							
							if (!byAccessLevelMethods.m_byAccessLevelMethods.empty())
							{
								for (auto & accessorIterator : byAccessLevelMethods.m_byAccessLevelMethods)
								{
									printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPublic);
									printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXProtected);
									printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPrivate);
								}
							}

							mockParserUserData->m_typeParsingDone = true;

							return CXChildVisit_Continue;
						}						
					}
					else if (curKind == CXCursor_ClassDecl)
					{
						MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
						auto cursorDefinition = clang_getCursorDefinition(cursor);
						if (!getCursorSpelling(cursorDefinition).compare(mockParserUserData->m_typeIdentifier))
						{
							mockParserUserData->m_typeDefinitionFound = true;
							std::stringstream & parsingResult = mockParserUserData->m_parserResults;
							ByAccessLevelMethods byAccessLevelMethods(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);
							clang_visitChildren(cursorDefinition, visitAndGenerateTestSkeletonInternal, &byAccessLevelMethods);
							removeDuplicateMethods(byAccessLevelMethods.m_byAccessLevelMethods);
							
							if (!byAccessLevelMethods.m_byAccessLevelMethods.empty())
							{
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPublic);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXProtected);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPrivate);
							}

							mockParserUserData->m_typeParsingDone = true;

							return CXChildVisit_Continue;
						}						
					}
					else if (curKind == CXCursor_ClassTemplate)
					{
						MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
						auto cursorDefinition = clang_getCursorDefinition(cursor);
						if (!getCursorSpelling(cursorDefinition).compare(mockParserUserData->m_typeIdentifier))
						{
							mockParserUserData->m_typeDefinitionFound = true;
							int numArguments = clang_Cursor_getNumTemplateArguments(cursorDefinition);
							std::stringstream & parsingResult = mockParserUserData->m_parserResults;
							ByAccessLevelMethods byAccessLevelMethods(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);
							clang_visitChildren(cursorDefinition, visitAndGenerateTestSkeletonInternal, &byAccessLevelMethods);
							removeDuplicateMethods(byAccessLevelMethods.m_byAccessLevelMethods);

							if (!byAccessLevelMethods.m_byAccessLevelMethods.empty())
							{
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPublic);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXProtected);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPrivate);
							}

							mockParserUserData->m_typeParsingDone = true;

							return CXChildVisit_Continue;
						}						
					}

					return CXChildVisit_Recurse;
				}
				else if (abstractParsingUserData->m_typeDefinitionFound && !abstractParsingUserData->m_typeParsingDone)
				{
					if (curKind == CXCursor_CXXMethod && abstractParsingUserData->m_typeDefinitionFound)
					{
						ByAccessLevelMethods & byAccessLevelMethods =
							*(static_cast<ByAccessLevelMethods *>(abstractParsingUserData));

						CX_CXXAccessSpecifier accessSpecifier = clang_getCXXAccessSpecifier(cursor);

						if (accessSpecifier == CX_CXXPublic)
						{
							std::stringstream testCaseHeader;
							std::string methodName = getCursorSpelling(cursor);

							testCaseHeader << "TEST_F(" << byAccessLevelMethods.m_typeIdentifier << "Test, " << byAccessLevelMethods.m_abstractParser.adaptMethodNameIfOperator(methodName, clang_Cursor_getNumArguments(cursor) > 0) << ")";

							byAccessLevelMethods.m_byAccessLevelMethods[static_cast<int>(accessSpecifier)].push_back(testCaseHeader.str());
						}
					}

					return CXChildVisit_Continue;
				}
				else
				{
					return CXChildVisit_Continue;
				}
			}


			AbstractParser::ParserResult TestSkeletonParser::parse(CXTranslationUnit & translationUnit, const std::string & typeIdentifier, const CXFile & typeDefinitionFile) const
			{
				MockParserUserData mockParserUserData(typeIdentifier, typeDefinitionFile, *this);
				clang_visitChildren(clang_getTranslationUnitCursor(translationUnit), visitAndGenerateTestSkeletonInternal, &mockParserUserData);
				std::stringstream generatedTypeName;
				generatedTypeName << typeIdentifier << "Test";
				return ParserResult(mockParserUserData.m_parserResults.str(), mockParserUserData.m_typeNamespaces, generatedTypeName.str(), mockParserUserData.m_errorMessage);
			}
		}		
	}
}