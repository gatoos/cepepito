/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "CepepitoParserResult.hpp"
#include <iostream>

CepepitoParserResult::CepepitoParserResult() :
	m_generatedTypeName(_strdup("")),
	m_resultCRepresentation(nullptr),
	m_errorMessage(_strdup(""))
{}

CepepitoParserResult::~CepepitoParserResult()
{
	free(m_generatedTypeName);
	free(m_resultCRepresentation);
	free(m_errorMessage);
}

void CepepitoParserResult::setGeneratedTypeName(const char * mockedObjectName)
{
	if (m_generatedTypeName)
	{
		free(m_generatedTypeName);
		m_generatedTypeName = nullptr;
	}

	if (mockedObjectName)
	{
		m_generatedTypeName = _strdup(mockedObjectName);
	}
}

const char * CepepitoParserResult::getGeneratedTypeName() const
{
	return m_generatedTypeName;
}

void CepepitoParserResult::setResultCRepresentation(const char * resultCRepresentation)
{
	if (m_resultCRepresentation)
	{
		free(m_resultCRepresentation);
		m_resultCRepresentation = nullptr;
	}

	if (resultCRepresentation)
	{
		m_resultCRepresentation = _strdup(resultCRepresentation);
	}
}

const char * CepepitoParserResult::getResultCRepresentation() const
{
	return m_resultCRepresentation;
}

void CepepitoParserResult::setErrorMessage(const char * errorMessage)
{
	if (m_errorMessage)
	{
		free(m_errorMessage);
		m_errorMessage = nullptr;
	}

	if (errorMessage)
	{
		m_errorMessage = _strdup(errorMessage);
	}
}

const char * CepepitoParserResult::getErrorMessage() const
{
	return m_errorMessage;
}

void cepepitoparser_releaseParserResult(CepepitoParserResult * cepepitoParserResult)
{
	delete cepepitoParserResult;
}

const char * cepepitoparser_getGeneratedTypeNameAsCString(CepepitoParserResult * cepepitoParserResult)
{
	return cepepitoParserResult->getGeneratedTypeName();
}

const char * cepepitoparser_getParserResultAsCString(CepepitoParserResult * cepepitoParserResult)
{
	return cepepitoParserResult->getResultCRepresentation();
}

const char * cepepitoparser_getErrorMessageAsCString(CepepitoParserResult * cepepitoParserResult)
{
	return cepepitoParserResult->getErrorMessage();
}

