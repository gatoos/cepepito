/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include "clang-c/Index.h"

namespace cepepito
{
	namespace parser
	{
		class AbstractParser;

		struct AbstractParsingUserData
		{
			AbstractParsingUserData(bool typeDefinitionFound, const CXFile & typeDefinitionFile, const AbstractParser & abstractParser) :
				m_typeDefinitionFound(typeDefinitionFound),
				m_typeParsingDone(false),
				m_typeDefinitionFile(typeDefinitionFile),
				m_typeIsTemplate(false),
				m_abstractParser(abstractParser)
			{}

			bool m_typeDefinitionFound;
			bool m_typeParsingDone;
			const CXFile & m_typeDefinitionFile;
			bool m_typeIsTemplate;
			const AbstractParser & m_abstractParser;
		};

		struct MockParserUserData : public AbstractParsingUserData
		{
			MockParserUserData(const std::string & typeIdentifier, const CXFile & typeDefinitionFile, const AbstractParser & abstractParser) :
				AbstractParsingUserData(false, typeDefinitionFile, abstractParser),
				m_typeIdentifier(typeIdentifier)				
			{}

			MockParserUserData(const std::string & typeIdentifier, const CXFile & typeDefinitionFile, const AbstractParser & abstractParser, std::vector<std::string> & typeNamespaces) :
				AbstractParsingUserData(false, typeDefinitionFile, abstractParser),
				m_typeIdentifier(typeIdentifier),
				m_typeNamespaces(typeNamespaces)
			{}

			std::string m_typeIdentifier;
			std::stringstream m_parserResults;
			std::string m_errorMessage;
			std::vector<std::string> m_typeNamespaces;
		};

		struct ByAccessLevelMethods : public AbstractParsingUserData
		{
			ByAccessLevelMethods(const std::string & typeIdentifier, const CXFile & typeDefinitionFile, const AbstractParser & abstractParser) :
				AbstractParsingUserData(true, typeDefinitionFile, abstractParser),
				m_byAccessLevelMethods(),
				m_typeIdentifier(typeIdentifier)

			{}

			std::map<int, std::vector<std::string> > m_byAccessLevelMethods;
			std::string m_typeIdentifier;
		};

		class AbstractParser
		{
		public :
			
			struct ParserResult
			{
				ParserResult(std::string parserResults, std::vector<std::string> typeNamespaces, const std::string & generatedTypeName, const std::string & errorMessage) :
					m_parserResults(parserResults),
					m_typeNamespaces(typeNamespaces),
					m_generatedTypeName(generatedTypeName),
					m_errorMessage(errorMessage)
				{
				}

				std::string m_parserResults;
				std::vector<std::string> m_typeNamespaces;
				std::string m_generatedTypeName;
				std::string m_errorMessage;
			};

			AbstractParser();
			virtual ~AbstractParser()
			{}

			std::string adaptMethodNameIfOperator(const std::string & methodName, bool methodHasArguments) const;
			bool methodIsOperator(const std::string & methodName) const;
			virtual ParserResult parse(CXTranslationUnit & translationUnit, const std::string & typeIdentifier, const CXFile & typeDefinitionFile) const = 0;

		private:
			std::map<std::pair<std::string, bool>, std::string> operatorToMethodNameEquivalent;
		};
	}
}
