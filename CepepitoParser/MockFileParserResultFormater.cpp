/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "MockFileParserResultFormater.hpp"
#include "CepepitoParserUtils.hpp"
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

using namespace cepepito::parser::utils;

namespace cepepito
{
	namespace parser
	{
		std::string MockFileParserResultFormater::format(const std::string & rawParserResult, 
			CXTranslationUnit & translationUnit,
			const std::string & toBeMockedTypeDescription, std::vector<std::string> & typeNamespaces, 
			CXFile & typeDefinitionFile, const char * includeDirectories[], 
			unsigned int numberOfIncludeDirectories) const
		{
			std::stringstream mockedObjectName;
			mockedObjectName << toBeMockedTypeDescription << "Mock";
			std::stringstream parsingResultWithIncludeGards;
			std::vector<std::string> includeDirectoriesAsVector = generateIncludeDirectoriesAsVector(includeDirectories, numberOfIncludeDirectories);;
			std::string includeDirectives = retrieveIncludeDirectives(translationUnit, typeDefinitionFile, includeDirectoriesAsVector);			

			std::string includeGards = cepepito::parser::utils::generateIncludeGards(typeNamespaces, mockedObjectName.str());
			std::string openingNamespaces = cepepito::parser::utils::generateOpeningNamespaces(typeNamespaces);
			std::string closingNamespaces = cepepito::parser::utils::generateClosingNamespaces(typeNamespaces.size());
			
			parsingResultWithIncludeGards << "#ifndef " << includeGards << std::endl;
			parsingResultWithIncludeGards << "#define " << includeGards << std::endl;
			parsingResultWithIncludeGards << std::endl;
			parsingResultWithIncludeGards << "#include <gmock/gmock.h>" << std::endl;
			parsingResultWithIncludeGards << includeDirectives << std::endl;
			parsingResultWithIncludeGards << openingNamespaces;
			parsingResultWithIncludeGards << rawParserResult;
			parsingResultWithIncludeGards << closingNamespaces;
			parsingResultWithIncludeGards << std::endl;
			parsingResultWithIncludeGards << "#endif" << std::endl;

			return parsingResultWithIncludeGards.str();
		}
	}
}