/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "MockParser.hpp"
#include "CepepitoParserUtils.hpp"
#include <map>
#include <sstream>

using namespace cepepito::parser::utils;

namespace cepepito
{
	namespace parser
	{
		namespace mockparser
		{
			struct TemplateParameter
			{
				TemplateParameter(std::string templatename, std::string templateWithTypeName) :
					m_templatename(templatename),
					m_templateWithTypeName(templateWithTypeName)
				{}

				std::string m_templatename;
				std::string m_templateWithTypeName;				
			};

			static void printMethodsByAccessSpecifier(std::stringstream & parsingResult, std::map<int, std::vector<std::string> > byAccessSpecifierMethods, CX_CXXAccessSpecifier accessLevelSpecifierToBePrinted)
			{
				auto methodsForGivenAccessSpecifier = byAccessSpecifierMethods.find(accessLevelSpecifierToBePrinted);
				if (methodsForGivenAccessSpecifier != byAccessSpecifierMethods.end() && !methodsForGivenAccessSpecifier->second.empty())
				{
					parsingResult << getAccessSpecifierStringRepresentation(accessLevelSpecifierToBePrinted) << " :" << std::endl;
					for (auto & methodForGivenAccessSpecifier : methodsForGivenAccessSpecifier->second)
					{
						parsingResult << methodForGivenAccessSpecifier << std::endl;
					}
				}
			}

			std::string buildTypeNamespacesPath(std::vector<std::string> & typeNamespaces)
			{
				std::stringstream openingNamespaces;
				for (auto namespaceLabelIt = typeNamespaces.begin(); namespaceLabelIt != typeNamespaces.end(); ++namespaceLabelIt)
				{
					openingNamespaces << *namespaceLabelIt << "::";					
				}

				return openingNamespaces.str();
			}

			static CXChildVisitResult retrieveTemplateParameters(CXCursor cursor, CXCursor parent, CXClientData data)
			{
				if (clang_getCursorKind(cursor) == CXCursorKind::CXCursor_TemplateTypeParameter ||
					clang_getCursorKind(cursor) == CXCursorKind::CXCursor_NonTypeTemplateParameter)
				{
					std::vector<TemplateParameter> * templateParameters = static_cast<std::vector<TemplateParameter> *>(data);
					std::stringstream templateWithTypeName;
					if (clang_getCursorKind(cursor) == CXCursorKind::CXCursor_TemplateTypeParameter)
					{
						templateWithTypeName << "class ";
					}
					else
					{
						CXType type = clang_getCursorType(cursor);
						std::string typeSpelling = getTypeSpelling(type);
						templateWithTypeName << typeSpelling << " ";
					}
					templateWithTypeName << getCursorSpelling(cursor);
					templateParameters->push_back(TemplateParameter(getCursorSpelling(cursor), templateWithTypeName.str()));
					return CXChildVisit_Recurse;
				}
				else
				{
					return CXChildVisit_Break;
				}
			}

			static CXChildVisitResult visitAndGenerateMockInternal(CXCursor cursor, CXCursor parent, CXClientData data)
			{
				CXSourceLocation location = clang_getCursorLocation(cursor);
				CXFile cursorFile;
				clang_getFileLocation(location, &cursorFile, NULL, NULL, NULL);
				AbstractParsingUserData * abstractParsingUserData = static_cast<AbstractParsingUserData *>(data);

				if (!clang_File_isEqual(cursorFile, abstractParsingUserData->m_typeDefinitionFile))
				{
					return CXChildVisit_Continue;
				}

				CXCursorKind curKind = clang_getCursorKind(cursor);


				if (curKind == CXCursor_Namespace)
				{
					MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
					std::stringstream & parsingResult = mockParserUserData->m_parserResults;

					MockParserUserData childParsingToBeMockedDescription(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser, mockParserUserData->m_typeNamespaces);
					childParsingToBeMockedDescription.m_typeNamespaces.push_back(getCursorSpelling(cursor));

					clang_visitChildren(cursor, visitAndGenerateMockInternal, &childParsingToBeMockedDescription);

					mockParserUserData->m_typeDefinitionFound = childParsingToBeMockedDescription.m_typeDefinitionFound;
					mockParserUserData->m_typeParsingDone = childParsingToBeMockedDescription.m_typeParsingDone;
					mockParserUserData->m_errorMessage = childParsingToBeMockedDescription.m_errorMessage;
					mockParserUserData->m_typeNamespaces = childParsingToBeMockedDescription.m_typeNamespaces;

					if (childParsingToBeMockedDescription.m_parserResults.str() != "")
					{
						parsingResult << childParsingToBeMockedDescription.m_parserResults.str();
					}					

					return CXChildVisit_Continue;
				}

				if (!abstractParsingUserData->m_typeDefinitionFound)
				{
					if (curKind == CXCursor_StructDecl)
					{
						auto cursorDefinition = clang_getCursorDefinition(cursor);
						MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
						if (!getCursorSpelling(cursorDefinition).compare(mockParserUserData->m_typeIdentifier))
						{
							mockParserUserData->m_typeDefinitionFound = true;
							std::stringstream & parsingResult = mockParserUserData->m_parserResults;
							ByAccessLevelMethods byAccessLevelMethods(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);
							byAccessLevelMethods.m_typeIsTemplate = false;
							clang_visitChildren(cursorDefinition, visitAndGenerateMockInternal, &byAccessLevelMethods);
							removeDuplicateMethods(byAccessLevelMethods.m_byAccessLevelMethods);

							if (!byAccessLevelMethods.m_byAccessLevelMethods.empty())
							{
								parsingResult << "struct " << getCursorSpelling(cursorDefinition) << "Mock : public " << buildTypeNamespacesPath(mockParserUserData->m_typeNamespaces) << getCursorSpelling(cursorDefinition) << std::endl;
								parsingResult << "{" << std::endl;
								for (auto & accessorIterator : byAccessLevelMethods.m_byAccessLevelMethods)
								{
									printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPublic);
									printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXProtected);
									printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPrivate);
								}
								parsingResult << "};" << std::endl;
							}
							else
							{
								throw new std::exception("Type doesn't contain any virtual method");								
							}

							mockParserUserData->m_typeParsingDone = true;

							return CXChildVisit_Continue;
						}						
					}
					else if (curKind == CXCursor_ClassDecl)
					{
						MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
						auto cursorDefinition = clang_getCursorDefinition(cursor);
						if (!getCursorSpelling(cursorDefinition).compare(mockParserUserData->m_typeIdentifier))
						{
							mockParserUserData->m_typeDefinitionFound = true;
							std::stringstream & parsingResult = mockParserUserData->m_parserResults;
							ByAccessLevelMethods byAccessLevelMethods(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);
							byAccessLevelMethods.m_typeIsTemplate = false;
							clang_visitChildren(cursorDefinition, visitAndGenerateMockInternal, &byAccessLevelMethods);
							removeDuplicateMethods(byAccessLevelMethods.m_byAccessLevelMethods);

							if (!byAccessLevelMethods.m_byAccessLevelMethods.empty())
							{
								parsingResult << "class " << getCursorSpelling(cursorDefinition) << "Mock : public " << buildTypeNamespacesPath(mockParserUserData->m_typeNamespaces) << getCursorSpelling(cursorDefinition) << std::endl;
								parsingResult << "{" << std::endl;
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPublic);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXProtected);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPrivate);
								parsingResult << "};" << std::endl;
							}
							else
							{
								throw std::exception("Type doesn't contain any virtual method");
							}

							mockParserUserData->m_typeParsingDone = true;

							return CXChildVisit_Continue;
						}						
					}
					else if (curKind == CXCursor_ClassTemplate)
					{
						MockParserUserData * mockParserUserData = static_cast<MockParserUserData *>(abstractParsingUserData);
						auto cursorDefinition = clang_getCursorDefinition(cursor);
						if (!getCursorSpelling(cursorDefinition).compare(mockParserUserData->m_typeIdentifier))
						{
							mockParserUserData->m_typeDefinitionFound = true;
							int numArguments = clang_Cursor_getNumTemplateArguments(cursorDefinition);
							std::stringstream & parsingResult = mockParserUserData->m_parserResults;
							ByAccessLevelMethods byAccessLevelMethods(mockParserUserData->m_typeIdentifier, mockParserUserData->m_typeDefinitionFile, mockParserUserData->m_abstractParser);
							byAccessLevelMethods.m_typeIsTemplate = true;
							clang_visitChildren(cursorDefinition, visitAndGenerateMockInternal, &byAccessLevelMethods);
							removeDuplicateMethods(byAccessLevelMethods.m_byAccessLevelMethods);

							if (!byAccessLevelMethods.m_byAccessLevelMethods.empty())
							{
								std::vector<TemplateParameter> templateParameters;
								clang_visitChildren(cursorDefinition, retrieveTemplateParameters, &templateParameters);

								if (!templateParameters.empty())
								{
									parsingResult << "template<";
									for (size_t i = 0; i < templateParameters.size(); ++i)
									{
										parsingResult << templateParameters[i].m_templateWithTypeName;
										if (i < templateParameters.size() - 1)
										{
											parsingResult << ", ";
										}
									}
									parsingResult << ">" << std::endl;
								}
								parsingResult << "class " << getCursorSpelling(cursorDefinition) << "Mock : public " << buildTypeNamespacesPath(mockParserUserData->m_typeNamespaces) << getCursorSpelling(cursorDefinition);
								if (!templateParameters.empty())
								{
									parsingResult << "<";
									for (size_t i = 0; i < templateParameters.size(); ++i)
									{
										parsingResult << templateParameters[i].m_templatename;
										if (i < templateParameters.size() - 1)
										{
											parsingResult << ", ";
										}
									}
									parsingResult << ">";
								}
								parsingResult << std::endl;
								parsingResult << "{" << std::endl;
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPublic);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXProtected);
								printMethodsByAccessSpecifier(parsingResult, byAccessLevelMethods.m_byAccessLevelMethods, CX_CXXAccessSpecifier::CX_CXXPrivate);
								parsingResult << "};" << std::endl;
							}
							else
							{
								throw std::exception("Type doesn't contain any virtual method");
							}

							mockParserUserData->m_typeParsingDone = true;

							return CXChildVisit_Continue;
						}						
					}	

					return CXChildVisit_Recurse;
				}
				else if (abstractParsingUserData->m_typeDefinitionFound && !abstractParsingUserData->m_typeParsingDone)
				{
					// Broken for template types, need an additional check if type is a template add a template type case
					if (curKind == CXCursor_CXXMethod && abstractParsingUserData->m_typeDefinitionFound)
					{
						ByAccessLevelMethods & byAccessLevelMethods =
							*(static_cast<ByAccessLevelMethods *>(data));

						if (clang_CXXMethod_isVirtual(cursor))
						{
							std::stringstream mockedMethodDefinition;
							CX_CXXAccessSpecifier accessSpecifier = clang_getCXXAccessSpecifier(cursor);
							CXType returnType = clang_getCursorResultType(cursor);
							std::string methodName = getCursorSpelling(cursor);
							bool isConstMethod = clang_CXXMethod_isConst(cursor);
							int numberOfArgs = clang_Cursor_getNumArguments(cursor);

							if (!byAccessLevelMethods.m_abstractParser.methodIsOperator(methodName))
							{
								mockedMethodDefinition << "MOCK_" << (isConstMethod ? "CONST_" : "") << "METHOD" << numberOfArgs << (byAccessLevelMethods.m_typeIsTemplate ? "_T" : "") << "(" << methodName \
									<< ", " << getTypeSpelling(returnType) << "(";

								for (int i = 0; i < numberOfArgs; ++i)
								{
									CXCursor parameterCursor = clang_Cursor_getArgument(cursor, i);
									mockedMethodDefinition << getTypeSpelling(clang_getCursorType(parameterCursor)) << ((i + 1 < numberOfArgs) ? "," : "");
								}

								mockedMethodDefinition << "));";
							}
							else
							{
								mockedMethodDefinition << "MOCK_" << (isConstMethod ? "CONST_" : "") << "METHOD" << numberOfArgs << (byAccessLevelMethods.m_typeIsTemplate ? "_T" : "") << "(" << \
									byAccessLevelMethods.m_abstractParser.adaptMethodNameIfOperator(methodName, numberOfArgs) << ", " << getTypeSpelling(returnType) << "(";

								for (int i = 0; i < numberOfArgs; ++i)
								{
									CXCursor parameterCursor = clang_Cursor_getArgument(cursor, i);
									mockedMethodDefinition << getTypeSpelling(clang_getCursorType(parameterCursor)) << ((i + 1 < numberOfArgs) ? "," : "");
								}

								mockedMethodDefinition << "));" << std::endl;
								mockedMethodDefinition << "virtual " << getTypeSpelling(returnType) << " " << methodName << "(";
								
								for (int i = 0; i < numberOfArgs; ++i)
								{
									CXCursor parameterCursor = clang_Cursor_getArgument(cursor, i);
									mockedMethodDefinition << getTypeSpelling(clang_getCursorType(parameterCursor)) << " param" << i << ((i + 1 < numberOfArgs) ? ", " : "");									
								}
								mockedMethodDefinition << ") { return " << byAccessLevelMethods.m_abstractParser.adaptMethodNameIfOperator(methodName, numberOfArgs) << "(";
								for (int i = 0; i < numberOfArgs; ++i)
								{
									CXCursor parameterCursor = clang_Cursor_getArgument(cursor, i);
									mockedMethodDefinition << "param" << i << ((i + 1 < numberOfArgs) ? ", " : "");
								}
								mockedMethodDefinition << ");}";
							}

							byAccessLevelMethods.m_byAccessLevelMethods[static_cast<int>(accessSpecifier)].push_back(mockedMethodDefinition.str());
						}

						return CXChildVisit_Continue;
					}

					return CXChildVisit_Continue; // shuold become recursive, method is not necessaryly a direct child of class, in case of ClassTemplate, the direct childs are the template types
				}			
				else
				{
					return CXChildVisit_Continue;
				}
			}


			AbstractParser::ParserResult MockParser::parse(CXTranslationUnit & translationUnit, const std::string & typeIdentifier, const CXFile & typeDefinitionFile) const
			{
				MockParserUserData mockParserUserData(typeIdentifier, typeDefinitionFile, *this);
				clang_visitChildren(clang_getTranslationUnitCursor(translationUnit), visitAndGenerateMockInternal, &mockParserUserData);				
				std::stringstream generatedTypeName;
				generatedTypeName << typeIdentifier << "Mock";
				return ParserResult(mockParserUserData.m_parserResults.str(), mockParserUserData.m_typeNamespaces, generatedTypeName.str(), mockParserUserData.m_errorMessage);
			}
		}
	}
}