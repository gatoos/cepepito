#include "AbstractParser.hpp"

namespace cepepito
{
	namespace parser
	{
		AbstractParser::AbstractParser() :
			operatorToMethodNameEquivalent({
				{ { "operator=", true }, "operatorAssignment" },
		{ { "operator+", true }, "operatorAddition" },
		{ { "operator-", true }, "operatorSubstraction" },
		{ { "operator+", false }, "operatorUnaryPlus" },
		{ { "operator-", false }, "operatorUnaryMinus" },
		{ { "operator*", true }, "operatorMultiplication" },
		{ { "operator/", true }, "operatorDivision" },
		{ { "operator%", true }, "operatorModulo" },
		{ { "operator++", false }, "operatorIncrementPrefix" },
		{ { "operator--", false }, "operatorDecrementPrefix" },
		{ { "operator++", true }, "operatorIncrementPostfix" },
		{ { "operator--", true }, "operatorDecrementPostfix" },
		{ { "operator==", true }, "operatorEqual" },
		{ { "operator!=", true }, "operatorNotEqual" },
		{ { "operator<", true }, "operatorLessThan" },
		{ { "operator>", true }, "operatorGreater" },
		{ { "operator<=", true }, "operatorLessOrEqual" },
		{ { "operator>=", true }, "operatorGreaterOrEqual" },
		{ { "operator!", false }, "operatorLogicalNot" },
		{ { "operator&&", true }, "operatorLogicalAnd" },
		{ { "operator||", true }, "operatorLogicalOr" },
		{ { "operator~", false }, "operatorBitwiseNot" },
		{ { "operator&", true }, "operatorBitwiseAnd" },
		{ { "operator|", true }, "operatorBitwiseOr" },
		{ { "operator^", true }, "operatorBitwiseXor" },
		{ { "operator<<", true }, "operatorBitwiseLeftShift" },
		{ { "operator>>", true }, "operatorBitwiseRightShift" },
		{ { "operator+=", true }, "operatorAdditionAssignement" },
		{ { "operator-=", true }, "operatorSubstractionAssignment" },
		{ { "operator*=", true }, "operatorMultiplicationAssignment" },
		{ { "operator/=", true }, "operatorDivisionAssignment" },
		{ { "operator%=", true }, "operatorModuloAssignment" },
		{ { "operator&=", true }, "operatorBitwiseAndAssignment" },
		{ { "operator|=", true }, "operatorBitwiseOrAssignment" },
		{ { "operator^=", true }, "operatorBitwiseXorAssignment" },
		{ { "operator<<=", true }, "operatorBitwiseLeftShiftAssignement" },
		{ { "operator>>=", true }, "operatorBitwiseRightShiftAssignment" },
		{ { "operator[]", true }, "operatorBrackets" },
		{ { "operator*", false }, "operatorIndirection" },
		{ { "operator&", false }, "operatorAddressOf" },
		{ { "operator->", false }, "operatorStructureDerefence" },
		{ { "operator->*", true }, "operatorMemberSelectedByPointerToMember" },
		{ { "operator()", true }, "operatorFunctionCall" },
		{ { "operator()", false }, "operatorFunctionCall" },
		{ { "operator,", true }, "operatorComma" },
		{ { "operator new", true }, "operatorNew" },
		{ { "operator new[]", true }, "operatorNewArray" },
		{ { "operator delete", true }, "operatorDelete" },
		{ { "operator delete[]", true }, "operatorDeleteArray" }
				})
		{}

		std::string AbstractParser::adaptMethodNameIfOperator(const std::string & methodName, bool methodHasArguments) const
		{
			auto entry = operatorToMethodNameEquivalent.find(std::make_pair(methodName, methodHasArguments));
			if (entry != operatorToMethodNameEquivalent.end())
				return entry->second;

			return methodName;
		}

		bool AbstractParser::methodIsOperator(const std::string & methodName) const
		{
			auto entry = operatorToMethodNameEquivalent.find(std::make_pair(methodName, false));
			if (entry != operatorToMethodNameEquivalent.end())
				return true;
			
			entry = operatorToMethodNameEquivalent.find(std::make_pair(methodName, true));
			if (entry != operatorToMethodNameEquivalent.end())
				return true;

			return false;
		}
	}
}
