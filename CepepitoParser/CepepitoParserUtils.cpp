/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "CepepitoParserUtils.hpp"
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <string>

namespace cepepito
{
	namespace parser
	{
		namespace utils
		{
			std::string getCursorSpelling(CXCursor & cursor)
			{
				std::string returnValue;
				CXString cursorSpelling = clang_getCursorSpelling(cursor);
				returnValue = clang_getCString(cursorSpelling);
				clang_disposeString(cursorSpelling);
				return returnValue;
			}

			std::string getTypeSpelling(CXType & type)
			{
				std::string returnValue;
				CXString typeSpelling = clang_getTypeSpelling(type);
				returnValue = clang_getCString(typeSpelling);
				clang_disposeString(typeSpelling);
				return returnValue;
			}

			std::string getFileName(CXFile & file)
			{
				std::string returnValue;
				CXString fileName = clang_getFileName(file);
				returnValue = clang_getCString(fileName);
				clang_disposeString(fileName);
				return returnValue;
			}

			std::string getAccessSpecifierStringRepresentation(CX_CXXAccessSpecifier accessSpecifier)
			{
				switch (accessSpecifier)
				{
				case CX_CXXAccessSpecifier::CX_CXXPublic:
					return "public";
					break;
				case CX_CXXAccessSpecifier::CX_CXXProtected:
					return "protected";
					break;
				case CX_CXXAccessSpecifier::CX_CXXPrivate:
					return "private";
					break;
				default:
					return "";
					break;
				}
			}

			bool isbackslashr(char c)
			{
				return c == '\r';
			}

			void removeBackslashesRFromString(std::string & stringToBeCleaned)
			{
				stringToBeCleaned.erase(std::remove_if(stringToBeCleaned.begin(), stringToBeCleaned.end(), isbackslashr), stringToBeCleaned.end());
			}

			std::string generateFileFromContent(const std::string & content, bool & errorCode)
			{
				static unsigned int temporaryFileNumber = 0;
				std::stringstream temporaryFilename;
				temporaryFilename << "cepepitoParserTemp" << temporaryFileNumber << ".hpp";
				++temporaryFileNumber;
				bool fileSuccessfullyOpened = true;

				std::ofstream fileStream(temporaryFilename.str());
				if (!fileStream)
				{
					fileSuccessfullyOpened = false;
					std::cerr << "Error writing to " << temporaryFilename.str() << std::endl;
				}
				else {
					fileStream << content;
				}

				return temporaryFilename.str();
			}

			std::string generateIncludeGards(const std::vector<std::string> & typeNamespaces, const std::string & typeIdentifier)
			{
				std::stringstream includeMacro;
				
				for(auto namespaceLabelIt = typeNamespaces.begin(); namespaceLabelIt != typeNamespaces.end(); ++namespaceLabelIt)
				{
					std::string namespaceLabelUpper(*namespaceLabelIt);
					std::transform(namespaceLabelUpper.begin(), namespaceLabelUpper.end(),
						namespaceLabelUpper.begin(), ::toupper);
					includeMacro << namespaceLabelUpper << "_";
				}
				
				std::string typeIdentifierUpper(typeIdentifier);
				std::transform(typeIdentifierUpper.begin(), typeIdentifierUpper.end(),
					typeIdentifierUpper.begin(), ::toupper);
				
				includeMacro << typeIdentifierUpper << "_HPP";

				return includeMacro.str();
			}

			std::vector<const char *> generateCommandLineArgumentsFromIncludeDirectories(const char * includeDirectories[],
				unsigned int numberOfIncludeDirectories)
			{
				std::vector<const char *> returnValue;

				for(unsigned int i = 0; i < numberOfIncludeDirectories; ++i)
				{
					std::stringstream includeDirectoryArgument;
					const char * includeDirectory = includeDirectories[i];
					includeDirectoryArgument << "-I" << includeDirectory;
					returnValue.push_back(_strdup(includeDirectoryArgument.str().c_str()));
				}

				return returnValue;
			}

			std::vector<std::string> generateIncludeDirectoriesAsVector(const char * includeDirectories[],
				unsigned int numberOfIncludeDirectories)
			{
				std::vector<std::string> returnValue;

				for (unsigned int i = 0; i < numberOfIncludeDirectories; ++i)
				{
					std::string unprocessedIncludeDirectory = std::string(includeDirectories[i]);
					std::replace(unprocessedIncludeDirectory.begin(), unprocessedIncludeDirectory.end(), '\\', '/');
					if (unprocessedIncludeDirectory.back() != '/')
					{
						std::stringstream processedIncludeDirectory;
						processedIncludeDirectory << unprocessedIncludeDirectory << "/";
						returnValue.push_back(processedIncludeDirectory.str());
					}
					else
					{
						returnValue.push_back(unprocessedIncludeDirectory);
					}
				}

				return returnValue;
			}

			struct RetrieveIncludeDirectivesUserData
			{
				RetrieveIncludeDirectivesUserData(CXFile & typeDefinitionFile, std::vector<std::string> & includeDirectoriesAsVector) :
					m_typeDefinitionFile(typeDefinitionFile),
					m_includeDirectoriesAsVector(includeDirectoriesAsVector),
					m_inclusionDirectives()
				{}

				CXFile & m_typeDefinitionFile;
				std::vector<std::string> & m_includeDirectoriesAsVector;
				std::stringstream m_inclusionDirectives;
			};
			
			static void retrieveIncludeDirectiveVisitor(CXFile includedFile, CXSourceLocation *includeStack, unsigned includeDepth, CXClientData clientData)
			{
				RetrieveIncludeDirectivesUserData * retrieveIncludeDirectivesUserData = static_cast<RetrieveIncludeDirectivesUserData *>(clientData);
				bool fileIncludingIncludedFileSameAsTypeDefinitionFile = false;
				if (includeDepth > 0)
				{
					CXFile fileIncludingIncludedFile;
					clang_getFileLocation(includeStack[0], &fileIncludingIncludedFile, NULL, NULL, NULL);
					if (fileIncludingIncludedFile)
					{
						std::string fileLocationString = getFileName(fileIncludingIncludedFile);
						fileIncludingIncludedFileSameAsTypeDefinitionFile = clang_File_isEqual(retrieveIncludeDirectivesUserData->m_typeDefinitionFile, fileIncludingIncludedFile) != 0;
					}
				}

				if (fileIncludingIncludedFileSameAsTypeDefinitionFile ||
					clang_File_isEqual(retrieveIncludeDirectivesUserData->m_typeDefinitionFile, includedFile))
				{
					std::string includeFileName = getFileName(includedFile);
					std::string transformedFilename = includeFileName;
					std::replace(includeFileName.begin(), includeFileName.end(), '\\', '/');
					std::string includeFilenameLowercase(includeFileName);
					std::transform(includeFileName.begin(), includeFileName.end(), includeFilenameLowercase.begin(), ::tolower);

					for (auto includeDirectoryPathIterator = retrieveIncludeDirectivesUserData->m_includeDirectoriesAsVector.begin();
						includeDirectoryPathIterator != retrieveIncludeDirectivesUserData->m_includeDirectoriesAsVector.end();
						includeDirectoryPathIterator++)
					{
						unsigned int includeDirectoryPathIteratorSize = includeDirectoryPathIterator->size();
						std::string includeDirectoryPathLowercase(*includeDirectoryPathIterator);
						std::transform(includeDirectoryPathIterator->begin(), includeDirectoryPathIterator->end(), includeDirectoryPathLowercase.begin(), ::tolower);
						if (!includeFilenameLowercase.compare(0, includeDirectoryPathIteratorSize, includeDirectoryPathLowercase))
						{
							std::string newIncludeFileName = includeFileName.substr(includeDirectoryPathIteratorSize,
								includeFileName.size() - includeDirectoryPathIteratorSize);

							if (newIncludeFileName.size() < transformedFilename.size())
								transformedFilename = newIncludeFileName;
						}
					}

					retrieveIncludeDirectivesUserData->m_inclusionDirectives << "#include <" << transformedFilename << ">" << std::endl;
				}
			}


			std::string retrieveIncludeDirectives(CXTranslationUnit & translationUnit, CXFile & typeDefinitionFile, std::vector<std::string> & includeDirectoriesAsVector)
			{
				RetrieveIncludeDirectivesUserData retrieveIncludeDirectivesUserdata(typeDefinitionFile, includeDirectoriesAsVector);
				clang_getInclusions(translationUnit, retrieveIncludeDirectiveVisitor, &retrieveIncludeDirectivesUserdata);
				std::string includeDirectives = retrieveIncludeDirectivesUserdata.m_inclusionDirectives.str();
				std::replace(includeDirectives.begin(), includeDirectives.end(), '\\', '/');

				return includeDirectives;				
			}

			void removeDuplicateMethods(std::map<int, std::vector<std::string> > & byAccessSpecifierMethods)
			{
				for (auto & entry : byAccessSpecifierMethods)
				{
					auto & methods = entry.second;
					std::set<std::string> seen;

					auto newEnd = std::remove_if(methods.begin(), methods.end(), [&seen](const std::string & value)
					{
						if (seen.find(value) != std::end(seen))
							return true;

						seen.insert(value);
						return false;
					});

					methods.erase(newEnd, methods.end());
				}
			}

			std::string generateOpeningNamespaces(std::vector<std::string> & typeNamespaces)
			{
				std::stringstream openingNamespaces;
				for (auto namespaceLabelIt = typeNamespaces.begin(); namespaceLabelIt != typeNamespaces.end(); ++namespaceLabelIt)
				{
					openingNamespaces << "namespace " << *namespaceLabelIt << std::endl;
					openingNamespaces << "{" << std::endl;
				}

				return openingNamespaces.str();				
			}

			std::string generateClosingNamespaces(int namespacesCount)
			{
				std::stringstream closingNamespaces;

				for (auto i = 0; i < namespacesCount; ++i)
				{
					closingNamespaces << "}" << std::endl;
				}

				return closingNamespaces.str();
			}
		}
	}	
}

