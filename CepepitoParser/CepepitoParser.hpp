/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once

#include "exportsMacro.hpp"

class CepepitoParserResult;
class TestCaseInformation;

extern "C"
{
	CEPEPITOPARSER_API CepepitoParserResult * cepepitoparser_generateTestSkeletonFromFileWithCursorLocation(const char * fileToParse, const char * unsavedFileNames[], const char * unsavedFileContents[], unsigned int numberOfUnsavedFiles, 
		const char * includeDirectories[], unsigned int numberOfIncludeDirectories, 
		const char * generatedFileIncludeDirectories[], unsigned int generatedFileNumberOfIncludeDirectories,
		unsigned int line, unsigned int column);
	CEPEPITOPARSER_API CepepitoParserResult * cepepitoparser_generateMockFromFileWithCursorLocation(const char * fileToParse, const char * unsavedFileNames[], const char * unsavedFileContents[], unsigned int numberOfUnsavedFiles, 
		const char * includeDirectories[], unsigned int numberOfIncludeDirectories, 
		const char * generatedFileIncludeDirectories[], unsigned int generatedFileNumberOfIncludeDirectories,
		unsigned int line, unsigned int column);
	CEPEPITOPARSER_API CepepitoParserResult * cepepitoparser_generateMockSnippetFromFileWithCursorLocation(const char * fileToParse, const char * unsavedFileNames[], const char * unsavedFileContents[], unsigned int numberOfUnsavedFiles, 
		const char * includeDirectories[], unsigned int numberOfIncludeDirectories, unsigned int line, unsigned int column);	
}

