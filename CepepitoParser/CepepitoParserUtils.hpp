/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#pragma once
#include <string>
#include <vector>
#include "clang-c/Index.h"
#include <map>

namespace cepepito
{
	namespace parser
	{
		namespace utils
		{
			std::string getCursorSpelling(CXCursor & cursor);
			std::string getTypeSpelling(CXType & type);
			std::string getFileName(CXFile & file);
			std::string getAccessSpecifierStringRepresentation(CX_CXXAccessSpecifier accessSpecifier);
			void removeBackslashesRFromString(std::string & stringToBeCleaned);
			std::string generateFileFromContent(const std::string & content, bool & errorCode);
			std::string generateIncludeGards(const std::vector<std::string> & typeNamespaces, const std::string & typeIdentifier);
			std::vector<const char *> generateCommandLineArgumentsFromIncludeDirectories(const char * includeDirectories[], 
				unsigned int numberOfIncludeDirectories);
			std::vector<std::string> generateIncludeDirectoriesAsVector(const char * includeDirectories[], 
				unsigned int numberOfIncludeDirectories);
			std::string retrieveIncludeDirectives(CXTranslationUnit & translationUnit, CXFile & typeDefinitionFile, std::vector<std::string> & includeDirectoriesAsVector);
			
			void removeDuplicateMethods(std::map<int, std::vector<std::string> > & byAccessSpecifierMethods);

			std::string generateOpeningNamespaces(std::vector<std::string> & typeNamespaces);
			std::string generateClosingNamespaces(int namespacesCount);
		}
	}
}

