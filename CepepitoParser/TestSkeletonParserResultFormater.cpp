/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
#include "TestSkeletonParserResultFormater.hpp"
#include "CepepitoParserUtils.hpp"
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

using namespace cepepito::parser::utils;

namespace cepepito
{
	namespace parser
	{
		std::string TestSkeletonParserResultFormater::format(const std::string & rawParserResult,
			CXTranslationUnit & translationUnit,
			const std::string & toBeMockedTypeDescription, std::vector<std::string> & typeNamespaces,
			CXFile & typeDefinitionFile, const char * includeDirectories[],
			unsigned int numberOfIncludeDirectories) const
		{
			std::stringstream parsingResultWithIncludeGards;
			std::vector<std::string> includeDirectoriesAsVector = generateIncludeDirectoriesAsVector(includeDirectories, numberOfIncludeDirectories);;
			std::string includeDirectives = retrieveIncludeDirectives(translationUnit, typeDefinitionFile, includeDirectoriesAsVector);
			
			parsingResultWithIncludeGards << "#include <gtest/gtest.h>" << std::endl;			
			parsingResultWithIncludeGards << includeDirectives << "\n" << std::endl;
			parsingResultWithIncludeGards << "namespace {" << std::endl;
			parsingResultWithIncludeGards << std::endl;
			parsingResultWithIncludeGards << "class " << toBeMockedTypeDescription << "Test : public ::testing::Test" << std::endl;
			parsingResultWithIncludeGards << "{" << std::endl;
			parsingResultWithIncludeGards << "public :" << std::endl;
			parsingResultWithIncludeGards << std::endl;
			parsingResultWithIncludeGards << "protected :" << std::endl;
			parsingResultWithIncludeGards << std::endl;
			parsingResultWithIncludeGards << "};" << std::endl;
			parsingResultWithIncludeGards << std::endl;
			parsingResultWithIncludeGards << rawParserResult;
			parsingResultWithIncludeGards << "}" << std::endl;
			parsingResultWithIncludeGards << std::endl;

			return parsingResultWithIncludeGards.str();
		}
	}
}