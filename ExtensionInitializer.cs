﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Cepepito.About;
using Cepepito.EditorContextMenus;
using Cepepito.Preferences;
using CepepitoCore.EntryPoints;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCore.UI;
using CepepitoCoreAdapters.ServiceProvidersAdapters.Common;
using CepepitoCoreAdapters.ServiceProvidersAdapters.CreateTestProject;
using CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInClipboard;
using CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile;
using CepepitoCoreAdapters.ServiceProvidersAdapters.OpenTestLocationInEditor;
using CepepitoCoreAdapters.ServiceProvidersAdapters.RetrievePurchasedLicense;
using CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromCodeEditor;
using CepepitoCoreAdapters.ServiceProvidersAdapters.RunTestFromSolutionExplorer;
using CepepitoCoreAdapters.UIAdapters.RunTests;
using Microsoft.VisualStudio.Shell;
using ExtensionRegistrationService.EntryPoints;
using ExtensionRegistrationService.ServiceProviders;
using ExtensionRegistrationServiceAdapters;
using LicenseServiceAdapters;
using Logging;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using VisualStudioAPIConnectors;
using CepepitoLicense = LicenseService.Core.Entities.CepepitoLicense;
using IServiceProvider = System.IServiceProvider;

namespace Cepepito
{
    class ExtensionInitializer
    {
        private const string LicenseServerUrl = "https://gatoos-license-server.bid/api/v1/license";
        private const double CepepitoVersion = 1.0;

        private TestRunnerToolWindow RetrieveTestRunnerToolWindow(Package package)
        {
            // Get the instance number 0 of this tool window. This window is single instance so this instance
            // is actually the only one.
            // The last flag is set to true so that if the tool window does not exists it will be created.
            var window = package.FindToolWindow(typeof(TestRunnerToolWindow), 0, true);
            if (null == window || null == window.Frame)
                throw new NotSupportedException("Cannot create tool window");

            var testRunnerToolWindow = window as TestRunnerToolWindow;
            return testRunnerToolWindow;
        }

        private class AboutDialogDisplayer
        {
            private LicenseService.EntryPoints.LicenseService LicenseService { get; }
            private IServiceProvider ServiceProvider { get; }
            
            public AboutDialogDisplayer(LicenseService.EntryPoints.LicenseService licenseService, IServiceProvider serviceProvider)
            {
                LicenseService = licenseService;
                ServiceProvider = serviceProvider;
            }

            public CommandsRegistrationService.CommandExecutionStatus Execute()
            {
                var aboutDialogViewModel = new AboutControlViewModel(BuildLicenseInformation(LicenseService, LicenseService.GetActiveLicense()), 
                    CepepitoVersion);

                var retrievePurchasedLicenseInformationUseCase = new RetrievePurchasedLicenseInformationUseCase(new PurchaseCodeRetriever(), 
                    new PurchasedLicenseInformationRetriever(LicenseService),
                    aboutDialogViewModel, new DialogBoxErrorMessageNotifier(ServiceProvider));

                aboutDialogViewModel.RetrievePurchasedLicenseInformationUseCase =
                    retrievePurchasedLicenseInformationUseCase;

                var aboutDialog = new AboutDialog(aboutDialogViewModel);
                aboutDialog.ShowDialog();

                return CommandsRegistrationService.CommandExecutionStatus.SuccessfullyExecuted;
            }

            private LicenseInformation BuildLicenseInformation(LicenseService.EntryPoints.LicenseService licenseService, CepepitoLicense activeLicense)
            {
                return new LicenseInformation(
                    licenseService.GetNumberOfDaysLeftForLicense(activeLicense), activeLicense.Trial,
                    activeLicense.UserName, activeLicense.Uid, activeLicense.Type.ToString());
            }
        }

        public void Initialize(Package package, IServiceProvider serviceProvider, PreferencesDialog preferenceDialog)
        {
            using (new ExtensionLoaderStatusBarNotifier(serviceProvider))
            {
                var dteRetriever = new DteRetriever(serviceProvider);
                var licenseService = BuildLicenseService(serviceProvider);
                var licenseUpdateNotifier = new LicenseUpdateNotifier(licenseService);
                var cepepitoUnderlyingCommandsRepository = BuildCepepitoUnderlyingCommandsRepository(serviceProvider);
                var cepepitoTestRunnerUnderlyingCommandsRepository = BuildTestRunnerUnderlyingCommandsRepository(serviceProvider);
                var infrastructure = BuildInfrastructure(serviceProvider, dteRetriever, package, preferenceDialog);
                var aboutDialogDisplayer = new AboutDialogDisplayer(licenseService, serviceProvider);

                var commandsRegistrationService = new CommandsRegistrationService(() => licenseService.IsLicenseValid(),
                    licenseUpdateNotifier, infrastructure, cepepitoUnderlyingCommandsRepository, cepepitoTestRunnerUnderlyingCommandsRepository,
                    () => aboutDialogDisplayer.Execute());

                RegisterContextualMenuCommand(serviceProvider);
                commandsRegistrationService.RegisterCommands();
                RegisterTestRunnerWindowCommands(serviceProvider, dteRetriever, infrastructure.TestRunnerToolWindowControlModel, infrastructure.LoggerFactory);
                preferenceDialog.VisualStudioVersion = new VisualStudioVersionRetriever(dteRetriever).Version;
            }
        }

        private void RegisterContextualMenuCommand(IServiceProvider serviceProvider)
        {
            var contextualMenuCommand = new CommandID(new Guid("277eb236-053b-4f91-9bff-8df20b783172"), 0x1BFF);
            var mcs = serviceProvider.GetService(typeof(IMenuCommandService)) as IMenuCommandService;
            mcs.AddCommand(new MenuCommand(((sender, args) =>
            {
                var txtMgr = (IVsTextManager) serviceProvider.GetService(typeof(SVsTextManager));
                const int mustHaveFocus = 1;
                txtMgr.GetActiveView(mustHaveFocus, null, out var vTextView);
                if (!(vTextView is IVsUserData userData))
                {
                    Console.WriteLine(@"No text view is currently open");
                    return;
                }

                Guid guidViewHost = DefGuidList.guidIWpfTextViewHost;
                userData.GetData(ref guidViewHost, out var holder);
                var viewHost = (IWpfTextViewHost) holder;
                ExecuteTestsContextualMenuCommandHandler.ExecStatic(serviceProvider, viewHost.TextView);
            }), contextualMenuCommand));
        }

        private LicenseService.EntryPoints.LicenseService BuildLicenseService(IServiceProvider serviceProvider)
        {
            return new LicenseService.EntryPoints.LicenseService(
                new LicenseRepository(GetInstallDirectory()),
                new LicenseGenerator(LicenseServerUrl),
                GetPublicKeyCertificate(),
                new ErrorsMessagesNotifier(serviceProvider),
                new ClockManager(GetEventsDateTime()),
                AuthorizationCodeGenerator.GenerateAuthorizationCode());
        }

        private IEnumerable<DateTime> GetEventsDateTime()
        {
            return new EventLog("system").Entries.Cast<EventLogEntry>().Select(e => e.TimeWritten);
        }

        private byte[] GetPublicKeyCertificate()
        {
            byte[] publicKey;

            var assembly = Assembly.GetExecutingAssembly();
            using (var memoryStream = new MemoryStream())
            {
                assembly.GetManifestResourceStream("Cepepito.LicenseVerify.cer")?.CopyTo(memoryStream);
                publicKey = memoryStream.ToArray();
            }

            return publicKey;
        }

        private string GetInstallDirectory()
        {
            var codebase = typeof(CepepitoPackage).Assembly.CodeBase;
            var uri = new Uri(codebase, UriKind.Absolute);
            return Path.GetDirectoryName(uri.LocalPath)?.ToLower();
        }

        private ExtensionRegistrationService.EntryPoints.Infrastructure BuildInfrastructure(
            IServiceProvider serviceProvider, DteRetriever dteRetriever,
            Package package, PreferencesDialog preferenceDialog)
        {
            return new ExtensionRegistrationService.EntryPoints.Infrastructure(
                dialogBoxErrorsMessageNotifier: new DialogBoxErrorMessageNotifier(serviceProvider),
                errorsHandler: new DialogBoxErrorMessageNotifier(serviceProvider),
                testRunnerToolWindowControlModel: new CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerViewerViewModel(() => RetrieveTestRunnerToolWindow(package)),
                currentDocumentInfoRetriever: new CurrentDocumentInfoRetriever(dteRetriever, serviceProvider),
                projectOutputRunner: new ProjectOutputRunner(dteRetriever, new LoggerFactory()), 
                projectOutputDebugger: new ProjectOutputDebugger(dteRetriever, new LoggerFactory()), 
                projectSelector: new CepepitoCoreAdapters.ServiceProvidersAdapters.GenerateCodeInFile.ProjectSelector(dteRetriever), 
                solution: new Solution(dteRetriever, serviceProvider), 
                generateTestSkeletonFromFileWithCursorLocation: Parser.GenerateTestSkeletonFromFileWithCursorLocation, 
                generateMockFromFileWithCursorLocation: Parser.GenerateMockFromFileWithCursorLocation, 
                generateMockSnippetFromFileWithCursorLocation: Parser.GenerateMockSnippetFromFileWithCursorLocation, 
                clipboard: new Clipboard(serviceProvider), 
                selectedProjectRetriever: new SelectedProjectRetriever(dteRetriever),
                testRunnerToolWindowDisplay: () => { RetrieveTestRunnerToolWindow(package).Display();},
                visualStudioVersion: new VisualStudioVersionRetriever(dteRetriever).Version,
                selectedProjectPropertiesRetriever: new SelectedProjectPropertiesRetriever(dteRetriever), 
                solutionProjectsRepository: new SolutionProjectsRepository(dteRetriever), 
                testingLibrariesConfiguration: new TestingLibrariesConfiguration(preferenceDialog),
                fileOverwriteConfirmationQuery: new FileOverwriteConfirmationQuery(serviceProvider),
                mockGenerationProgressDialogBox: new ProgressDialogBox(new ProgressDialogOptions("Mock generation", "Parsing type definition...")),
                testGenerationProgressDialogBox: new ProgressDialogBox(new ProgressDialogOptions("Test skeleton generation", "Parsing type definition...")),
                googleTestIncludeDirectoryPresenceChecker: IncludeDirectoriesChecker.ContainsGoogleTestIncludeDirectory,
                googleTestIncludeDirectoryRetriever: new GoogleTestIncludeDirectoryRetriever(new TestingLibrariesConfiguration(preferenceDialog)), 
                googleMockIncludeDirectoryPresenceChecker: IncludeDirectoriesChecker.ContainsGoogleTestAndGoogleMockIncludeDirectory, 
                googleMockIncludeDirectoryRetriever: new GoogleTestAndGoogleMockIncludeDirectoryRetriever(new TestingLibrariesConfiguration(preferenceDialog)),
                loggerFactory: new LoggerFactory());
        }

        private void RegisterTestRunnerWindowCommands(IServiceProvider serviceProvider,
            DteRetriever dteRetriever, ITestRunnerViewerViewModel testRunnerViewViewModel,
            ILoggerFactory loggerFactory)
        {
            CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerToolWindowControl.TestRunnertoolWindowControlViewModel.InitializeCommandHandlers(
                new OpenTestLocationInEditorUseCaseController(new ProjectsRepository(dteRetriever),
                    new EditorPane(dteRetriever)),
                new RunTestsFromTestRunnerUseCaseController(
                    testRunnerViewViewModel,
                    new DialogBoxErrorMessageNotifier(serviceProvider), new ProjectOutputRunner(dteRetriever, loggerFactory), loggerFactory),
                new RunTestsFromTestRunnerUseCaseController(
                    testRunnerViewViewModel,
                    new DialogBoxErrorMessageNotifier(serviceProvider), new ProjectOutputDebugger(dteRetriever, loggerFactory), loggerFactory));

            CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerToolWindowControl.TestRunnertoolWindowControlViewModel
                    .SubscribeToSolutionCloseEvents =
                () =>
                {
                    dteRetriever.GetDte().Events.SolutionEvents.AfterClosing += () =>
                        CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerToolWindowControl
                            .TestRunnertoolWindowControlViewModel.OnSolutionClosed();

                    // TODO : handle this more gracefully, when project is removed or renamed, update references to it rather than completely clearing the results
                    dteRetriever.GetDte().Events.SolutionEvents.ProjectRemoved += (projectName) =>
                        CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerToolWindowControl
                            .TestRunnertoolWindowControlViewModel.OnSolutionClosed();

                    dteRetriever.GetDte().Events.SolutionEvents.ProjectRenamed += (project, oldName) =>
                        CepepitoCoreAdapters.UIAdapters.RunTests.TestRunnerToolWindowControl
                            .TestRunnertoolWindowControlViewModel.OnSolutionClosed();
                };
        }

        private IUnderlyingCommandRepository BuildTestRunnerUnderlyingCommandsRepository(IServiceProvider serviceProvider)
        {
            var testRunnerWindowCommandSet = new Guid("c3658c0e-818e-4d16-a9d5-b922140efcba");
            return new UnderlyingCommandRepository(testRunnerWindowCommandSet, serviceProvider);
        }

        private IUnderlyingCommandRepository BuildCepepitoUnderlyingCommandsRepository(IServiceProvider serviceProvider)
        {
            var cepepitoCommandsCommandSet = new Guid("277eb236-053b-4f91-9bff-8df20b783172");
            return new UnderlyingCommandRepository(cepepitoCommandsCommandSet, serviceProvider);
        }
    }

    internal class ExtensionLoaderStatusBarNotifier : IDisposable
    {
        private readonly IVsStatusbar _vsStatusbar;

        public ExtensionLoaderStatusBarNotifier(IServiceProvider serviceProvider)
        {
            _vsStatusbar = (IVsStatusbar)serviceProvider.GetService(typeof(SVsStatusbar));

            _vsStatusbar.IsFrozen(out var frozen);

            if (frozen != 0)
            {
                _vsStatusbar.FreezeOutput(0);
            }

            _vsStatusbar.SetText("Loading Cepepito extension...");

            _vsStatusbar.FreezeOutput(1);
        }

        public void Dispose()
        {
            _vsStatusbar.FreezeOutput(0);
            _vsStatusbar.SetText("");
            _vsStatusbar.Clear();
        }
    }

    internal class LicenseUpdateNotifier : ILicenseUpdateNotifier
    {
        private readonly global::LicenseService.EntryPoints.LicenseService _licenseService;

        public LicenseUpdateNotifier(global::LicenseService.EntryPoints.LicenseService licenseService)
        {
            _licenseService = licenseService;
        }

        public event EventHandler ActiveLicenseModified
        {
            add => _licenseService.LicenseUpdated += value;
            remove => _licenseService.LicenseUpdated -= value;
        }
    }

    static class Parser
    {
        public static (string generatedTypeName, string generatedContent, string errorMessage)
            GenerateTestSkeletonFromFileWithCursorLocation(ParserInput parserInput)
        {
            var unsavedDocumentsNames = (from unsavedDocument in parserInput.UnsavedDocuments
                                         select unsavedDocument.name).ToArray();

            var unsavedDocumentsContents = (from unsavedDocument in parserInput.UnsavedDocuments
                                            select unsavedDocument.content).ToArray();

            var sourceProjectIncludeDirectoriesAsArray = parserInput.SourceProjectIncludeDirectories.ToArray();
            var targetProjectIncludeDirectoriesAsArray = parserInput.TargetProjectIncludeDirectories.ToArray();

            var parserResults = CepepitoParserAdapter.generateTestSkeletonFromFileWithCursorLocation(
                parserInput.DocumentName, unsavedDocumentsNames,
                unsavedDocumentsContents, (uint) unsavedDocumentsNames.Length,
                sourceProjectIncludeDirectoriesAsArray.ToArray(), (uint) sourceProjectIncludeDirectoriesAsArray.Length,
                targetProjectIncludeDirectoriesAsArray.ToArray(), (uint) targetProjectIncludeDirectoriesAsArray.Length,
                parserInput.CursorPosition.Line,
                parserInput.CursorPosition.Column);

            var generatedTypeName = parserResults.GetGeneratedTypeName();
            var generatedContent = parserResults.ToString();
            var errorMessage = parserResults.GetErrorMessage();

            parserResults.Release();

            return (generatedTypeName, generatedContent, errorMessage);
        }

        public static (string generatedTypeName, string generatedContent, string errorMessage)
            GenerateMockFromFileWithCursorLocation(ParserInput parserInput)
        {
            var unsavedDocumentsNames = (from unsavedDocument in parserInput.UnsavedDocuments
                                         select unsavedDocument.name).ToArray();

            var unsavedDocumentsContents = (from unsavedDocument in parserInput.UnsavedDocuments
                                            select unsavedDocument.content).ToArray();

            var sourceProjectIncludeDirectoriesAsArray = parserInput.SourceProjectIncludeDirectories.ToArray();
            var targetProjectIncludeDirectoriesAsArray = parserInput.TargetProjectIncludeDirectories.ToArray();

            var parserResults = CepepitoParserAdapter.generateMockFromFileWithCursorLocation(
                parserInput.DocumentName, unsavedDocumentsNames,
                unsavedDocumentsContents, (uint) unsavedDocumentsNames.Length,
                sourceProjectIncludeDirectoriesAsArray.ToArray(), (uint) sourceProjectIncludeDirectoriesAsArray.Length,
                targetProjectIncludeDirectoriesAsArray.ToArray(),
                (uint) targetProjectIncludeDirectoriesAsArray
                    .Length, // correct this to correctly resolve include directories
                parserInput.CursorPosition.Line,
                parserInput.CursorPosition.Column);

            var generatedTypeName = parserResults.GetGeneratedTypeName();
            var generatedContent = parserResults.ToString();
            var errorMessage = parserResults.GetErrorMessage();

            parserResults.Release();

            return (generatedTypeName, generatedContent, errorMessage);
        }

        public static (string generatedTypeName, string generatedContent, string errorMessage)
            GenerateMockSnippetFromFileWithCursorLocation(ParserInput parserInput)
        {
            var unsavedDocumentsNames = (from unsavedDocument in parserInput.UnsavedDocuments
                                         select unsavedDocument.name).ToArray();

            var unsavedDocumentsContents = (from unsavedDocument in parserInput.UnsavedDocuments
                                            select unsavedDocument.content).ToArray();

            var projectIncludeDirectoriesAsArray = parserInput.SourceProjectIncludeDirectories.ToArray();

            var parserResults = CepepitoParserAdapter.generateMockSnippetFromFileWithCursorLocation(
                parserInput.DocumentName, unsavedDocumentsNames,
                unsavedDocumentsContents, (uint)unsavedDocumentsNames.Length,
                projectIncludeDirectoriesAsArray.ToArray(), (uint)projectIncludeDirectoriesAsArray.Length,
                parserInput.CursorPosition.Line,
                parserInput.CursorPosition.Column);

            var generatedTypeName = parserResults.GetGeneratedTypeName();
            var generatedContent = parserResults.ToString();
            var errorMessage = parserResults.GetErrorMessage();

            parserResults.Release();

            return (generatedTypeName, generatedContent, errorMessage);
        }
    }
}