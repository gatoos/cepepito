﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Threading.Tasks;
using ExtensionRegistrationService.Core.Entities;

namespace ExtensionRegistrationService.UnitTests.Helpers
{
    public class FakeDeactivableCommand : IDeactivableCommand
    {
        private Func<Task> ExecutableCommand { get; }
        private Action ExecutableCommandAction { get; }
 
        public FakeDeactivableCommand(Func<Task> executableCommand)
        {
            ExecutableCommand = executableCommand;
            Active = true;
        }

        public FakeDeactivableCommand(Action executableCommand)
        {
            ExecutableCommandAction = executableCommand;
            Active = true;
        }

        public async Task Execute()
        {
            if (ExecutableCommand != null)
            {
                await ExecutableCommand();
            }
            else
            {
                ExecutableCommandAction?.Invoke();
            }
        }

        public bool Active { get; set; }
    }
}