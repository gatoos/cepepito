﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExtensionRegistrationService.Core.Entities;
using ExtensionRegistrationService.ServiceProviders;

namespace ExtensionRegistrationService.UnitTests.Helpers
{
    public class FakeUnderlyingCommandsRepository : IUnderlyingCommandRepository
    {
        public FakeUnderlyingCommandsRepository()
        {
            Commands = new Dictionary<CommandIdentifier, IDeactivableCommand>();
        }

        private Dictionary<CommandIdentifier, IDeactivableCommand> Commands { get; }

        public async Task ExecuteCommand(CommandIdentifier commandIdentifier)
        {
            Commands.TryGetValue(commandIdentifier, out IDeactivableCommand commandToExecute);

            if (commandToExecute == null)
            {
                return;
            }

            if (!commandToExecute.Active)
            {
                return;
            }

            await ((FakeDeactivableCommand) commandToExecute).Execute();
        }

        public bool IsCommandDisabled(CommandIdentifier commandIdentifier)
        {
            Commands.TryGetValue(commandIdentifier, out IDeactivableCommand commandToExecute);
            return commandToExecute != null && !commandToExecute.Active;
        }

        public bool CommandExists(CommandIdentifier commandIdentifier)
        {
            Commands.TryGetValue(commandIdentifier, out IDeactivableCommand commandToExecute);
            return commandToExecute != null;
        }

        public IDeactivableCommand AddCommand(CommandIdentifier commandIdentifier, Action executableCommand)
        {
            var deactivableCommand = new FakeDeactivableCommand(executableCommand);
            Commands[commandIdentifier] = deactivableCommand;
            return deactivableCommand;
        }
    }
}