﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromSolutionExplorer;
using CepepitoCore.UI;
using ExtensionRegistrationService.Core.Entities;
using ExtensionRegistrationService.EntryPoints;
using ExtensionRegistrationService.ServiceProviders;
using ExtensionRegistrationService.UnitTests.Helpers;
using FakeItEasy;
using Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExtensionRegistrationService.UnitTests.Core.UseCases
{
    [TestClass]
    public class RegisterCommandsUseCaseTest
    {
        [TestMethod]
        public void
            When_CallingRegisterCommandsWithAnInvalidLicense_Then_CommandsShouldBeDeactivated()
        {
            bool IsActiveLicenseValid() => false;

            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();
            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(IsActiveLicenseValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();

            Assert.IsTrue(CepepitoCodeGenerationCommandsGUIDs().
                All(id => IsCommandDisabled(cepepitoCommandsRepository, id)));
        }

        [TestMethod]
        public void
            When_CallingRegisterCommandsWithAnInvalidLicenseAndThenUpdatingTheLicenseToAValidOne_Then_CommandsShouldBeActivated()
        {
            var activeLicenseValid = false;
            bool IsActiveLicenseValid() => activeLicenseValid;

            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();
            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(IsActiveLicenseValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();
            activeLicenseValid = true;
            fakeLicenseUpdateNotifier.FireLicenseUpdatedEvent();

            Assert.IsTrue(CepepitoCommandsGUIDs().
                Where(id => id != CommandsRegistrationService.DisplayAboutControlCommandId).
                All(id => !IsCommandDisabled(cepepitoCommandsRepository, id)));
        }

        [TestMethod]
        public void When_CallingEachCepepitoCommandsAndHavinAValidLicense_Then_AllCommandShouldBeExecutedSuccessfullyExecute()
        {
            bool IsActiveLicenseValid() => true;

            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();
            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(IsActiveLicenseValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();
            // TODO : this should be corrected, the registration service is currently inpacted by the underlying commands implementation, we should only
            // TODO : be testing the registration and activation/deactivation of commands based on license status
            /*Assert.IsTrue(CepepitoCommandsGUIDs().
                All(id => HasCommandSuccesfullyExecuted(cepepitoCommandsRepository, id, infrastructure.DialogBoxErrorsMessageNotifier)));*/
        }

        [TestMethod]
        public void When_CallingEachTestRunnerCommandsAndHavingAValidLicense_Then_AllCommandShouldBeExecutedSuccessfullyExecute()
        {
            bool IsActiveLicenseValid() => true;

            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();
            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(IsActiveLicenseValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();
            Assert.IsTrue(TestRunnerCommandsGUIDs().
                All(id => HasCommandSuccesfullyExecuted(testRunnerCommandsRepository, id, infrastructure.DialogBoxErrorsMessageNotifier)));
        }

        [TestMethod]
        public void When_CallingEachCepepitoCodeGenerationCommandsAndHavingAnInvalidLicense_Then_NeitherCommandShouldExecutedAndErrorsShouldBeRaised()
        {
            bool activeLicenseIsValid = true;

            bool IsActiveLicenseValid() => activeLicenseIsValid;
            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();

            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(IsActiveLicenseValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();
            activeLicenseIsValid = false;
            Assert.IsTrue(CepepitoCodeGenerationCommandsGUIDs().
                All(id => HasCommandFailedToExecute(cepepitoCommandsRepository, id, infrastructure.DialogBoxErrorsMessageNotifier)));
        }

        [TestMethod]
        public void When_CallingAboutCommandAndHavingAnInvalidLicense_Then_CommandShouldBeExecutedSuccessfully()
        {
            bool ActiveLicenseIsValid() => false;

            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();
            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(ActiveLicenseIsValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();
            Assert.IsTrue(HasCommandSuccesfullyExecuted(cepepitoCommandsRepository, CommandsRegistrationService.DisplayAboutControlCommandId, infrastructure.DialogBoxErrorsMessageNotifier));
        }

        [TestMethod]
        public void When_CallingEachTestRunnerCommandsAndHavinAnInvalidLicense_Then_AllCommandsShouldBeExecuted()
        {
            bool activeLicenseIsValid = true;
            bool ActiveLicenseIsValid() => activeLicenseIsValid;

            var fakeLicenseUpdateNotifier = new FakeLicenseUpdateNotifier();
            var infrastructure = BuildFakeInfrastructure();
            var cepepitoCommandsRepository = new FakeUnderlyingCommandsRepository();
            var testRunnerCommandsRepository = new FakeUnderlyingCommandsRepository();

            var commandsRegistrationService =
                new CommandsRegistrationService(ActiveLicenseIsValid, fakeLicenseUpdateNotifier, infrastructure, cepepitoCommandsRepository, testRunnerCommandsRepository,
                    () => { });

            commandsRegistrationService.RegisterCommands();
            activeLicenseIsValid = false;
            Assert.IsTrue(TestRunnerCommandsGUIDs().
                All(id => HasCommandSuccesfullyExecuted(testRunnerCommandsRepository, id, infrastructure.DialogBoxErrorsMessageNotifier)));
        }

        private static Infrastructure BuildFakeInfrastructure()
        {
            var infrastructure = new Infrastructure(new FakeErrorsMessagesQueue(),
                A.Fake<IErrorsHandler>(),
            A.Fake<ITestRunnerViewerViewModel>(),
                A.Fake<ICurrentDocumentInfoRetriever>(), A.Fake<IProjectOutputLauncher>(), A.Fake<IProjectOutputLauncher>(),
                A.Fake<IProjectSelector>(), A.Fake<ISolution>(),
                parserInput => ("", "", ""),
                parserInput => ("", "", ""),
                parserInput => ("", "", ""),
                A.Fake<IClipboard>(),
                A.Fake<ISelectedProjectsRetriever>(),
                () => { },
                "",
                A.Fake<ISelectedProjectPropertiesRetriever>(),
                A.Fake<ISolutionProjectsRepository>(),
                A.Fake<ITestingLibrariesConfiguration>(),
                A.Fake<IFileOverwriteConfirmationQuery>(),
                A.Fake<IProgressDialogBox>(),
                A.Fake<IProgressDialogBox>(),
                includeDirectory => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(), includeDirectory => true, A.Fake<IRequiredIncludeDirectoriesRetriever>(),
                A.Fake<ILoggerFactory>());
            return infrastructure;
        }

        private IEnumerable<CommandIdentifier> CepepitoCommandsGUIDs()
        {
            return new[]
            {
                CommandsRegistrationService.RunTestCommandIdentifier,
                CommandsRegistrationService.DebugTestCommandIdentifier,
                CommandsRegistrationService.GenerateTestForClassCommandIdentifier,
                CommandsRegistrationService.GenerateMockFileCommandIdentifier,
                CommandsRegistrationService.GenerateMockInClipboardCommandIdentifier,
                CommandsRegistrationService.RunTestsFromSolutionExplorerCommandIdentifier,
                CommandsRegistrationService.DebugTestsFromSolutionExplorerCommandIdentifier,
                CommandsRegistrationService.CreateTestProjectCommandId,
                CommandsRegistrationService.DisplayAboutControlCommandId
            };
        }

        private IEnumerable<CommandIdentifier> CepepitoCodeGenerationCommandsGUIDs()
        {
            return new[]
            {
                CommandsRegistrationService.GenerateTestForClassCommandIdentifier,
                CommandsRegistrationService.GenerateMockFileCommandIdentifier,
                CommandsRegistrationService.GenerateMockInClipboardCommandIdentifier,
                CommandsRegistrationService.CreateTestProjectCommandId,
            };
        }


        private IEnumerable<CommandIdentifier> TestRunnerCommandsGUIDs()
        {
            return new[]
            {
                CommandsRegistrationService.ShowTestRunnerWindowCommandIdentifier
            };
        }

        private bool IsCommandDisabled(FakeUnderlyingCommandsRepository underlyingCommandRepository, CommandIdentifier commandId)
        {
            return underlyingCommandRepository.IsCommandDisabled(commandId);
        }

        private static bool HasCommandSuccesfullyExecuted(FakeUnderlyingCommandsRepository underlyingCommandRepository,
            CommandIdentifier commandId, IErrorsMessagesQueue errorsMessagesQueue)
        {
            underlyingCommandRepository = underlyingCommandRepository ??
                                          throw new ArgumentNullException(nameof(underlyingCommandRepository));

            if (errorsMessagesQueue is FakeErrorsMessagesQueue fakeErrorsMessageQueue)
            {
                var numberOfErrors = fakeErrorsMessageQueue.Count;
                underlyingCommandRepository.ExecuteCommand(commandId);

                return fakeErrorsMessageQueue.Count == numberOfErrors;
            }

            Assert.Fail();
            return false;
        }

        private static bool HasCommandFailedToExecute(FakeUnderlyingCommandsRepository underlyingCommandRepository,
            CommandIdentifier commandId, IErrorsMessagesQueue errorsMessagesQueue)
        {
            underlyingCommandRepository = underlyingCommandRepository ??
                                          throw new ArgumentNullException(nameof(underlyingCommandRepository));

            if (errorsMessagesQueue is FakeErrorsMessagesQueue fakeErrorsMessageQueue)
            {
                var numberOfErrors = fakeErrorsMessageQueue.Count;

                underlyingCommandRepository.ExecuteCommand(commandId);

                return fakeErrorsMessageQueue.Count == numberOfErrors + 1;
            }

            Assert.Fail();
            return false;
        }
    }

    public class FakeLicenseUpdateNotifier : ILicenseUpdateNotifier
    {
        public event EventHandler ActiveLicenseModified;

        public void FireLicenseUpdatedEvent()
        {
            ActiveLicenseModified?.Invoke(this, EventArgs.Empty);
        }
    }

    public class FakeErrorsMessagesQueue : IErrorsMessagesQueue
    {
        private readonly List<string> _errorMessages;

        public FakeErrorsMessagesQueue()
        {
            _errorMessages = new List<string>();
        }

        public IEnumerable<string> ErrorMessages => _errorMessages;

        public bool IsEmpty => !_errorMessages.Any();
        public int Count => _errorMessages.Count;

        public void AddErrorMessage(string message)
        {
            _errorMessages.Add(message);
        }

        protected bool Equals(FakeErrorsMessagesQueue other)
        {
            return Equals(_errorMessages, other._errorMessages);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FakeErrorsMessagesQueue)obj);
        }

        public override int GetHashCode()
        {
            return (_errorMessages != null ? _errorMessages.GetHashCode() : 0);
        }

        public static bool ErrorMessagesQueueContainOneErrorMessageContaining(FakeErrorsMessagesQueue fakeErrorsMessagesQueue,
            List<string> wordsToMatch)
        {
            if (fakeErrorsMessagesQueue.ErrorMessages.Count() != 1)
                return false;

            foreach (var errorMessage in fakeErrorsMessagesQueue.ErrorMessages)
                foreach (var wordToMatch in wordsToMatch)
                    if (!errorMessage.ToLower().Contains(wordToMatch.ToLower()))
                        return false;


            return true;
        }
    }

}
