﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Threading.Tasks;
using ExtensionRegistrationService.Core.Entities;
using ExtensionRegistrationService.EntryPoints;
using ExtensionRegistrationService.ServiceProviders;
using ExtensionRegistrationService.UnitTests.Helpers;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExtensionRegistrationService.UnitTests
{
    [TestClass]
    public class DeactivableCommandsRepositoryTest
    {
        [TestMethod]
        public void When_RegisteringANewCommandInEmptyCommandsRepository_Then_CommandShouldBeSuccessfullyRegistered()
        {
            async Task CommandExecutorStub(){}
            var commandIdentifier = new CommandIdentifier(0);
            var commandsRepository = new DeactivableCommandsRepository(new FakeUnderlyingCommandsRepository(), A.Fake<IErrorsHandler>());
            commandsRepository.RegisterCommand(commandIdentifier, CommandExecutorStub);

            Assert.IsTrue(commandsRepository.HasCommand(commandIdentifier));
        }

        [TestMethod]
        public void When_RegisteringANewDeactivableAndRetrievingIt_Then_ItShouldBeActive()
        {
            async Task CommandExecutorStub() { }

            var commandIdentifier = new CommandIdentifier(0);
            var commandsRepository = new DeactivableCommandsRepository(new FakeUnderlyingCommandsRepository(), A.Fake<IErrorsHandler>());
            commandsRepository.RegisterCommand(commandIdentifier, CommandExecutorStub, DeactivableStatus.Deactivable);

            Assert.IsTrue(commandsRepository.CommandIsActive(commandIdentifier));
        }

        [TestMethod]
        public void When_RegisteringANewDeactivableAndDeactivatingCommands_Then_ItShoulndtBeActive()
        {
            async Task CommandExecutorStub() { }
            var commandIdentifier = new CommandIdentifier(0);
            var commandsRepository = new DeactivableCommandsRepository(new FakeUnderlyingCommandsRepository(), A.Fake<IErrorsHandler>());
            commandsRepository.RegisterCommand(commandIdentifier, CommandExecutorStub, DeactivableStatus.Deactivable);
            commandsRepository.DeactivateCommands();
            
            Assert.IsFalse(commandsRepository.CommandIsActive(commandIdentifier));
        }

        [TestMethod]
        public void When_RegisteringExplicitelyANewNonDeactivableAndDeactivatingCommands_Then_ItShouldBeActive()
        {
            async Task CommandExecutorStub() { }
            var commandIdentifier = new CommandIdentifier(0);
            var commandsRepository = new DeactivableCommandsRepository(new FakeUnderlyingCommandsRepository(), A.Fake<IErrorsHandler>());
            commandsRepository.RegisterCommand(commandIdentifier, CommandExecutorStub, DeactivableStatus.NotDeactivable);
            commandsRepository.DeactivateCommands();

            Assert.IsTrue(commandsRepository.CommandIsActive(commandIdentifier));
        }

        [TestMethod]
        public void When_RegisteringImplicitelyANewNonDeactivableAndDeactivatingCommands_Then_ItShouldExecuteSuccessfully()
        {
            async Task CommandExecutorStub() { }
            var commandIdentifier = new CommandIdentifier(0);
            var commandsRepository = new DeactivableCommandsRepository(new FakeUnderlyingCommandsRepository(), A.Fake<IErrorsHandler>());
            commandsRepository.RegisterCommand(commandIdentifier, CommandExecutorStub);
            commandsRepository.DeactivateCommands();

            Assert.IsTrue(commandsRepository.CommandIsActive(commandIdentifier));
        }

        [TestMethod]
        public void When_RegisteringANewDeactivableAndDeactivatingAndActivatingCommands_Then_ItShouldExecuteCorrectly()
        {
            async Task CommandExecutorStub() { }
            var commandIdentifier = new CommandIdentifier(0);
            var commandsRepository = new DeactivableCommandsRepository(new FakeUnderlyingCommandsRepository(), A.Fake<IErrorsHandler>());
            commandsRepository.RegisterCommand(commandIdentifier, CommandExecutorStub, DeactivableStatus.Deactivable);
            commandsRepository.DeactivateCommands();
            commandsRepository.ActivateCommands();

            Assert.IsTrue(commandsRepository.CommandIsActive(commandIdentifier));
        }
    }
}