﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LicenseServiceAdapters.UnitTests
{
    [TestClass]
    public class ClockManagerTest
    {
        [TestMethod]
        public void Given_AnEmptyListOfEventDateTimes_WhenCallingClockHasBeenManipulated_Then_ItShouldReturnFalse()
        {
            var clockManager = new ClockManager(new DateTime[] { });
            Assert.IsFalse(clockManager.ClockHasBeenManipulated());
        }

        [TestMethod]
        public void
            Given_AListOfEventDateTimesAllInThePast_WhenCallingClockHasBeenManipulated_Then_ItShouldReturnFalse()
        {
            var clockManager = new ClockManager(new[]
            {
                new DateTime(DateTime.Today.Year - 1, DateTime.Today.Month, DateTime.Today.Day),
                new DateTime(DateTime.Today.Year - 2, DateTime.Today.Month, DateTime.Today.Day)
            });

            Assert.IsFalse(clockManager.ClockHasBeenManipulated());
        }

        [TestMethod]
        public void
            Given_AListOfEventDateTimesWhereSomeAreInTheFuture_When_CallingClockHasBeenManipulated_Then_ItShouldReturnTrue()
        {
            var clockManager = new ClockManager(new[]
            {
                new DateTime(DateTime.Today.Year - 1, DateTime.Today.Month, DateTime.Today.Day),
                DateTime.Today.AddDays(1)
            });

            Assert.IsTrue(clockManager.ClockHasBeenManipulated());
        }
    }
}