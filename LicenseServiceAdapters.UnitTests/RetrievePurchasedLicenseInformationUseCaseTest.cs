﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using FakeItEasy;
using LicenseService.EntryPoints;
using LicenseService.ServiceProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LicenseServiceAdapters.UnitTests
{
    /*[TestClass]
    public class RetrievePurchasedLicenseInformationUseCaseTest
    {
        [TestMethod]
        public void WHEN_purchase_code_is_correctly_retrieved_and_full_license_retriever_works_THEN_it_should_return_the_retrieved_license_information()
        {
            var purchaseCode = "fake purchase code";
            var expectedLicenseInformation = "retrieved license information";

            var fakePurchaseCodeRetriever = A.Fake<IPurchaseCodeRetriever>();
            A.CallTo(() => fakePurchaseCodeRetriever.RetrievePurchaseCode()).
                Returns(purchaseCode);

            var fakePurchasedLicenseInformationRetriever = A.Fake<IPurchasedLicenseInformationRetriever>();
            A.CallTo(() => fakePurchasedLicenseInformationRetriever.RetrievePurchasedLicenseInformation(purchaseCode))
                .Returns((expectedLicenseInformation, null));

            var fakeRetrievePurchasedLicenseInformationUseCaseOutput = A.Fake<IRetrievePurchasedLicenseInformationUseCaseOutput>();

            new RetrievePurchasedLicenseInformationUseCase(fakePurchaseCodeRetriever, fakePurchasedLicenseInformationRetriever,
                fakeRetrievePurchasedLicenseInformationUseCaseOutput, A.Fake<IErrorsMessagesQueue>()).Execute();

            Assert.AreEqual(expectedLicenseInformation, fakeRetrievePurchasedLicenseInformationUseCaseOutput.LicenseInformation);
        }

        [TestMethod]
        public void WHEN_purchase_code_is_correctly_retrieved_and_full_license_retriever_fails_THEN_it_should_not_return_any_license_information_and_enqueue_an_error_message()
        {
            var purchaseCode = "fake purchase code";
            var initialLicenseInformation = "initial license information";
            var errorMessage = "unknown purchase code";

            var fakePurchaseCodeRetriever = A.Fake<IPurchaseCodeRetriever>();
            A.CallTo(() => fakePurchaseCodeRetriever.RetrievePurchaseCode()).
                Returns(purchaseCode);

            var fakeErrorMessageQueue = A.Fake<IErrorsMessagesQueue>();

            var fakePurchasedLicenseInformationRetriever = A.Fake<IPurchasedLicenseInformationRetriever>();
            A.CallTo(() => fakePurchasedLicenseInformationRetriever.RetrievePurchasedLicenseInformation(purchaseCode))
                .Returns((null, errorMessage));
                

            var fakeRetrievePurchasedLicenseInformationUseCaseOutput = A.Fake<IRetrievePurchasedLicenseInformationUseCaseOutput>();
            fakeRetrievePurchasedLicenseInformationUseCaseOutput.LicenseInformation = initialLicenseInformation;


            new RetrievePurchasedLicenseInformationUseCase(fakePurchaseCodeRetriever, fakePurchasedLicenseInformationRetriever,
                fakeRetrievePurchasedLicenseInformationUseCaseOutput, fakeErrorMessageQueue).Execute();

            Assert.AreEqual(initialLicenseInformation, fakeRetrievePurchasedLicenseInformationUseCaseOutput.LicenseInformation);
            A.CallTo(() => fakeErrorMessageQueue.AddErrorMessage(errorMessage)).MustHaveHappened();
        }

        [TestMethod]
        public void WHEN_purchase_code_doesnt_return_nothing_THEN_it_shouldnt_call_full_license_retriever_and_return_no_license_information()
        {
            var initialLicenseInformation = "initial license information";
            
            var fakePurchaseCodeRetriever = A.Fake<IPurchaseCodeRetriever>();
            A.CallTo(() => fakePurchaseCodeRetriever.RetrievePurchaseCode()).
                Returns(null);

            var fakePurchasedLicenseInformationRetriever = A.Fake<IPurchasedLicenseInformationRetriever>();
            
            var fakeRetrievePurchasedLicenseInformationUseCaseOutput = A.Fake<IRetrievePurchasedLicenseInformationUseCaseOutput>();
            fakeRetrievePurchasedLicenseInformationUseCaseOutput.LicenseInformation = initialLicenseInformation;
            
            new RetrievePurchasedLicenseInformationUseCase(fakePurchaseCodeRetriever, fakePurchasedLicenseInformationRetriever,
                fakeRetrievePurchasedLicenseInformationUseCaseOutput, A.Fake<IErrorsMessagesQueue>()).Execute();

            Assert.AreEqual(initialLicenseInformation, fakeRetrievePurchasedLicenseInformationUseCaseOutput.LicenseInformation);
            A.CallTo(() => fakePurchasedLicenseInformationRetriever.RetrievePurchasedLicenseInformation(A<string>._)).MustNotHaveHappened();
        }
    }*/
}
