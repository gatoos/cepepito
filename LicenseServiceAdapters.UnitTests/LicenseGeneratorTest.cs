﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using FakeItEasy;
using LicenseServiceAdapters.ServiceProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LicenseServiceAdapters.UnitTests
{
    [TestClass]
    public class LicenseGeneratorTest
    {
        [TestMethod]
        public void
            When_CallingGenerateTrialLicense_Then_ItShouldCallTheHttpConnectorWithTheGenerateTrialLicenseRestAPIUrl()
        {
            var licenseServerURL = "https://www.validationserver.com";
            var authorizationCode = "fakeAuthorizationCode";
            var licenseServerConnector = A.Fake<ILicenseServerConnector>();
            var expectedRetrievedContentFromUrl = "trialLicenseContent";
            A.CallTo(() =>
                    licenseServerConnector.GetContentFromUrl(
                        licenseServerURL + "/trial?authorizationcode=" + authorizationCode))
                .Returns(expectedRetrievedContentFromUrl);
            var licenseGenerator = new LicenseGenerator(licenseServerURL, licenseServerConnector);

            var retrievedContentFromUrl = licenseGenerator.GenerateTrialLicenseEncryptedString(authorizationCode);

            Assert.AreEqual(expectedRetrievedContentFromUrl, retrievedContentFromUrl);
        }

        [TestMethod]
        public void
            When_CallingGenerateTrialLicenseAndLicenseServerConnectorThrowsAnException_Then_ItShouldForwardTheException()
        {
            var licenseServerURL = "https://www.validationserver.com";
            var authorizationCode = "fakeAuthorizationCode";
            var licenseServerConnectorException = "Unable to connect to server";
            var licenseServerConnector = A.Fake<ILicenseServerConnector>();
            A.CallTo(() =>
                    licenseServerConnector.GetContentFromUrl(licenseServerURL + "/trial?authorizationcode=" +
                                                             authorizationCode))
                .Throws(new Exception(licenseServerConnectorException));

            var licenseGenerator = new LicenseGenerator(licenseServerURL, licenseServerConnector);

            Assert.ThrowsException<Exception>(() =>
                licenseGenerator.GenerateTrialLicenseEncryptedString(authorizationCode));
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicense_Then_ItShouldCallTheHttpConnectorWithTheGenerateTrialLicenseRestAPIUrl()
        {
            var licenseServerURL = "https://www.validationserver.com";
            var authorizationCode = "authorizationCode";
            var purchaseCode = "purchaseCode";
            var licenseServerConnector = A.Fake<ILicenseServerConnector>();
            var expectedRetrievedContentFromUrl = "fullLicenseContent";
            A.CallTo(() =>
                    licenseServerConnector.GetContentFromUrl(
                        licenseServerURL + "/full?purchaseUID=" + purchaseCode + " & authorizationcode=" +
                        authorizationCode))
                .Returns(expectedRetrievedContentFromUrl);

            var licenseGenerator = new LicenseGenerator(licenseServerURL, licenseServerConnector);

            var retrievedContentFromUrl =
                licenseGenerator.GenerateFullLicenseEncryptedString(authorizationCode, purchaseCode);

            Assert.AreEqual(expectedRetrievedContentFromUrl, retrievedContentFromUrl);
        }

        [TestMethod]
        public void
            When_CallingGenerateFullLicenseAndLicenseServerConnectorThrowsAnException_Then_ItShouldForwardTheException()
        {
            var licenseServerURL = "https://www.validationserver.com";
            var authorizationCode = "fakeAuthorizationCode";
            var purchaseCode = "purchaseCode";
            var licenseServerConnectorException = "Unable to connect to server";
            var licenseServerConnector = A.Fake<ILicenseServerConnector>();
            A.CallTo(() =>
                    licenseServerConnector.GetContentFromUrl(licenseServerURL + "/full?purchaseUID=" + purchaseCode +
                                                             " & authorizationcode=" +
                                                             authorizationCode))
                .Throws(new Exception(licenseServerConnectorException));

            var licenseGenerator = new LicenseGenerator(licenseServerURL, licenseServerConnector);

            Assert.ThrowsException<Exception>(() =>
                licenseGenerator.GenerateFullLicenseEncryptedString(authorizationCode, purchaseCode));
        }
    }
}