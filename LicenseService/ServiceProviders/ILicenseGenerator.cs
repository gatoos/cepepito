﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace LicenseService.ServiceProviders
{
    public interface ILicenseGenerator
    {
        string GenerateTrialLicenseEncryptedString(string authorizationCode);
        string GenerateFullLicenseEncryptedString(string authorizationCode, string purchaseCode);
    }
}