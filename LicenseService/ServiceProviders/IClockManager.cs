﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;

namespace LicenseService.ServiceProviders
{
    public interface IClockManager
    {
        DateTime GetCurrentDateTime();
        bool ClockHasBeenManipulated();
    }
}