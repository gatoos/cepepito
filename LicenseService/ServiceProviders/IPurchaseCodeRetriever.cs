﻿namespace LicenseService.ServiceProviders
{
    public interface IPurchaseCodeRetriever
    {
        string RetrievePurchaseCode();
    }
}