﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Globalization;
using System.Xml.Serialization;

namespace LicenseService.Core.Entities
{
    [Serializable, XmlRoot("License")]
    public struct CepepitoLicense
    {
        public CepepitoLicense(string uid, string userName, CepepitoLicenseType type, DateTime createDateTime) : this()
        {
            Uid = uid;
            UserName = userName;
            Type = type;
            CreateDateTime = createDateTime;
        }

        [XmlIgnore] public double Version { get; set; }

        [XmlIgnore] public int DaysLeft { get; set; }

        [XmlIgnore] public DateTime ExpirationDate { get; set; }

        [XmlIgnore]
        public string LicenseInformation
        {
            get
            {
                if (Trial)
                {
                    if (DaysLeft > 0)
                        return UserName + Environment.NewLine +
                               "Version : " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", Version) +
                               Environment.NewLine +
                               "Authorization code : " + Uid + Environment.NewLine +
                               "License : trial version - " + UserName + " day(s) left";
                    return UserName + Environment.NewLine +
                           "Version : " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", Version) +
                           Environment.NewLine +
                           "Authorization code : " + Uid + Environment.NewLine +
                           "License : expired trial version";
                }

                return UserName + Environment.NewLine +
                       "Version : " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", Version) +
                       Environment.NewLine +
                       "Authorization code : " + Uid + Environment.NewLine +
                       "License type : " + Type;
            }
        }

        [XmlIgnore] public bool Trial => Type == CepepitoLicenseType.Trial;

        [XmlIgnore] public string AppName => "Cepepito";

        [XmlElement("UID")] public string Uid { get; set; }

        [XmlElement("UserName")] public string UserName { get; set; }

        [XmlElement("Type")] public CepepitoLicenseType Type { get; set; }

        [XmlElement("CreateDateTime")] public DateTime CreateDateTime { get; set; }
    }
}