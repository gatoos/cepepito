﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace LicenseService.Core.Entities
{
    public enum CepepitoLicenseType
    {
        Student = 0,
        Personal = 1,
        Corporate = 2,
        Trial = 3,
        Invalid = 4
    }
}