﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LicenseService.Core.Entities
{
    internal static class LicenseDecryptor
    {
        private const string SignatureTagLabel = "Signature";

        public static CepepitoLicense DecryptLicense(string encryptedLicense, byte[] publicKeyCertificate)
        {
            if (string.IsNullOrWhiteSpace(encryptedLicense)) throw new Exception("License deencryption error");

            var cleanedEncryptedLicense = RemoveStartingEndingQuotes(encryptedLicense);

            var cert = new X509Certificate2(publicKeyCertificate);
            var rsaKey = (RSACryptoServiceProvider) cert.PublicKey.Key;

            var xmlDoc = new XmlDocument
            {
                PreserveWhitespace = true
            };
            xmlDoc.LoadXml(Encoding.UTF8.GetString(Convert.FromBase64String(cleanedEncryptedLicense)));

            if (!VerifyXml(xmlDoc, rsaKey)) throw new Exception("License deencryption error");

            var nodeList = xmlDoc.GetElementsByTagName(SignatureTagLabel, "*");
            xmlDoc.DocumentElement?.RemoveChild(nodeList[0]);

            var licenseXml = xmlDoc.OuterXml;

            var serializer = new XmlSerializer(typeof(CepepitoLicense));
            using (var reader = new StringReader(licenseXml))
            {
                return (CepepitoLicense) serializer.Deserialize(reader);
            }
        }

        private static string RemoveStartingEndingQuotes(string encryptedLicense)
        {
            return encryptedLicense?.Trim('\"');
        }

        private static bool VerifyXml(XmlDocument xmlDocument, RSACryptoServiceProvider publicKey)
        {
            xmlDocument = xmlDocument ?? throw new ArgumentNullException(nameof(xmlDocument));
            publicKey = publicKey ?? throw new ArgumentNullException(nameof(publicKey));

            var signedXml = new SignedXml(xmlDocument);
            var nodeList = xmlDocument.GetElementsByTagName(SignatureTagLabel, "*");

            if (nodeList.Count <= 0)
                throw new CryptographicException("Verification failed: No Signature was found in the document.");

            if (nodeList.Count >= 2)
                throw new CryptographicException(
                    "Verification failed: More that one signature was found for the document.");

            signedXml.LoadXml((XmlElement) nodeList[0]);

            return signedXml.CheckSignature(publicKey);
        }
    }
}