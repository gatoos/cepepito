﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LicenseService.Core.Entities;
using LicenseService.ServiceProviders;
using LicenseService.UI;

namespace LicenseService.EntryPoints
{
    public class RetrievePurchasedLicenseInformationUseCase
    {
        private class InternalErrorMessageQueue : IErrorsMessagesQueue
        {
            public List<string> ErrorMessages { get; private set; }

            public InternalErrorMessageQueue()
            {
                ErrorMessages = new List<string>();
            }

            public void AddErrorMessage(string errorMessage)
            {
                ErrorMessages.Add(errorMessage);
            }
        }

        private IPurchaseCodeRetriever PurchaseCodeRetriever { get; }
        private LicenseService LicenseService { get; }
        private IRetrievePurchasedLicenseInformationUseCaseOutput RetrievePurchasedLicenseInformationUseCaseOutput { get; }
        private IErrorsMessagesQueue ErrorsMessagesQueue { get; }
        private double ApplicationVersion { get; }
        
        public RetrievePurchasedLicenseInformationUseCase(IPurchaseCodeRetriever purchaseCodeRetriever, 
            LicenseService licenseService,
            IRetrievePurchasedLicenseInformationUseCaseOutput retrievePurchasedLicenseInformationUseCaseOutput, IErrorsMessagesQueue errorsMessagesQueue,
            double applicationVersion)
        {
            PurchaseCodeRetriever = purchaseCodeRetriever;
            LicenseService = licenseService;
            RetrievePurchasedLicenseInformationUseCaseOutput = retrievePurchasedLicenseInformationUseCaseOutput;
            ErrorsMessagesQueue = errorsMessagesQueue;
            ApplicationVersion = applicationVersion;
        }

        public void Execute()
        {
            var purchaseCode = PurchaseCodeRetriever.RetrievePurchaseCode();

            if (purchaseCode == null)
                return;

            var internalErrorMessageQueue = new InternalErrorMessageQueue();
            LicenseService.GenerateFullLicense(purchaseCode);
            var license = LicenseService.GetActiveLicense();

            if (internalErrorMessageQueue.ErrorMessages.Any())
            {
                ErrorsMessagesQueue.AddErrorMessage("An error occured during full license generation");
                return;
            }

            string licenseInformation = BuildLicenseInformation(license);

            RetrievePurchasedLicenseInformationUseCaseOutput.LicenseInformation = licenseInformation;
        }

        private string BuildLicenseInformation(CepepitoLicense license)
        {
            var numberOfDaysLeft = LicenseService.GetNumberOfDaysLeftForLicense(license);
            if (license.Trial)
            {
                if (numberOfDaysLeft > 0)
                    return license.UserName + Environment.NewLine +
                           "Version: " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", ApplicationVersion) +
                           Environment.NewLine +
                           "Authorization code: " + license.Uid + Environment.NewLine +
                           "License: trial version - " + numberOfDaysLeft + " day(s) left";
                return license.UserName + Environment.NewLine +
                       "Version: " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", ApplicationVersion) +
                       Environment.NewLine +
                       "Authorization code: " + license.Uid + Environment.NewLine +
                       "License: expired trial version";
            }
            return license.UserName + Environment.NewLine +
                   "Version: " + string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:0.0}", ApplicationVersion) +
                   Environment.NewLine +
                   "Authorization code: " + license.Uid + Environment.NewLine +
                   "License type: " + license.Type + " - " + numberOfDaysLeft + "day(s) left";
        }
    }
}
