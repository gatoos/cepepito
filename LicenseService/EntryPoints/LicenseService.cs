﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using LicenseService.Core.Entities;
using LicenseService.ServiceProviders;

namespace LicenseService.EntryPoints
{
    public class LicenseService
    {
        private static readonly int TrialLicenseValidityPeriodInDays = 30;
        private static readonly int FullLicenseValidityPeriodInDays = 366;

        public event EventHandler LicenseUpdated;

        public LicenseService(ILicenseRepository licenseRepository, ILicenseGenerator licenseGenerator,
            byte[] publicKeyCertificate, IErrorsMessagesQueue errorsMessagesQueue,
            IClockManager clockManager, string authorizationCode)
        {
            LicenseRepository = licenseRepository ?? throw new ArgumentNullException(nameof(licenseRepository));
            LicenseGenerator = licenseGenerator ?? throw new ArgumentNullException(nameof(licenseGenerator));
            PublicKeyCertificate =
                publicKeyCertificate ?? throw new ArgumentNullException(nameof(publicKeyCertificate));
            ErrorsMessagesQueue = errorsMessagesQueue ?? throw new ArgumentNullException(nameof(errorsMessagesQueue));
            ClockManager = clockManager ?? throw new ArgumentNullException(nameof(clockManager));
            AuthorizationCode = authorizationCode ?? throw new ArgumentNullException(nameof(authorizationCode));
            ActiveLicense = null;
            _clockHasBeenManipulated = new Lazy<bool>(() => ClockManager.ClockHasBeenManipulated());
            _currentDateTimeCache = new Lazy<DateTime>(() => ClockManager.GetCurrentDateTime());
        }

        private ILicenseRepository LicenseRepository { get; }
        private ILicenseGenerator LicenseGenerator { get; }
        private byte[] PublicKeyCertificate { get; }
        private IErrorsMessagesQueue ErrorsMessagesQueue { get; }
        private IClockManager ClockManager { get; }
        private string AuthorizationCode { get; }
        private CepepitoLicense? ActiveLicense { get; set; }
        private readonly Lazy<bool> _clockHasBeenManipulated;
        private readonly Lazy<DateTime> _currentDateTimeCache;

        public CepepitoLicense GetActiveLicense()
        {
            if (ActiveLicense != null) return ActiveLicense.Value;

            var encryptedLicense = LoadExistingEncryptedLicense();

            if (encryptedLicense != null)
                try
                {
                    ActiveLicense = LicenseDecryptor.DecryptLicense(encryptedLicense,
                        PublicKeyCertificate);
                    return ActiveLicense.Value;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

            try
            {
                var generatedEncryptedLicense = LicenseGenerator.GenerateTrialLicenseEncryptedString(AuthorizationCode);
                ActiveLicense = LicenseDecryptor.DecryptLicense(generatedEncryptedLicense,
                    PublicKeyCertificate);
                LicenseRepository.StoreLicenseAsEncryptedString(generatedEncryptedLicense);
                return ActiveLicense.Value;
            }
            catch (Exception e)
            {
                ErrorsMessagesQueue.AddErrorMessage("Error when retrieving trial license." + Environment.NewLine + "Cepepito will be disabled." + Environment.NewLine + "Fix your internet connection and restart Visual studio.");
                ActiveLicense = new CepepitoLicense(AuthorizationCode, "Unregistered user", CepepitoLicenseType.Invalid,
                    ClockManager.GetCurrentDateTime());

                return ActiveLicense.Value;
            }
        }

        private string LoadExistingEncryptedLicense()
        {
            string encryptedLicense = null;

            try
            {
                encryptedLicense = LicenseRepository.LoadLicenseAsEncryptedString();
            }
            catch
            {
            }

            return encryptedLicense;
        }

        public void GenerateFullLicense(string purchaseCode, IErrorsMessagesQueue errorMessageQueue)
        {
            try
            {
                var generatedEncryptedLicense =
                    LicenseGenerator.GenerateFullLicenseEncryptedString(AuthorizationCode, purchaseCode);
                ActiveLicense = LicenseDecryptor.DecryptLicense(generatedEncryptedLicense,
                    PublicKeyCertificate);
                LicenseRepository.StoreLicenseAsEncryptedString(generatedEncryptedLicense);
                LicenseUpdated?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
                errorMessageQueue.AddErrorMessage("Error when retrieving full license. Error details: " + e.Message);
            }
        }

        public void GenerateFullLicense(string purchaseCode)
        {
            GenerateFullLicense(purchaseCode, ErrorsMessagesQueue);
        }

        public bool IsLicenseValid()
        {
            var activeLicense = GetActiveLicense();
            if (activeLicense.Type == CepepitoLicenseType.Invalid) return false;

            if (_clockHasBeenManipulated.Value)
            {
                ErrorsMessagesQueue.AddErrorMessage("System clock has been manipulated. License is invalid");
                return false;
            }

            var expiryLicenseDate = activeLicense.CreateDateTime.AddDays(GetLicenseValidityPeriod(activeLicense.Type));
            if (LicenseDateIsExpired(_currentDateTimeCache.Value, expiryLicenseDate)) return false;

            return true;
        }

        private int GetLicenseValidityPeriod(CepepitoLicenseType licenseType)
        {
            switch (licenseType)
            {
                case CepepitoLicenseType.Invalid:
                    return 0;

                case CepepitoLicenseType.Trial:
                    return TrialLicenseValidityPeriodInDays;

                case CepepitoLicenseType.Corporate:
                case CepepitoLicenseType.Personal:
                case CepepitoLicenseType.Student:
                    return FullLicenseValidityPeriodInDays;

                default:
                    throw new Exception("Invalid license type");
            }
        }

        private bool LicenseDateIsExpired(DateTime currentDateTime, DateTime expiryLicenseDate)
        {
            return currentDateTime > expiryLicenseDate;
        }

        public int GetNumberOfDaysLeftForLicense(CepepitoLicense license)
        {
            var numberOfDaysLeft =
            (license.CreateDateTime.AddDays(GetLicenseValidityPeriod(license.Type)) -
             ClockManager.GetCurrentDateTime()).Days;
            return numberOfDaysLeft > 0 ? numberOfDaysLeft : 0;
        }
    }
}