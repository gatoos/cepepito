﻿namespace LicenseService.UI
{
    public interface IRetrievePurchasedLicenseInformationUseCaseOutput
    {
        string LicenseInformation { get; set; }
    }
}