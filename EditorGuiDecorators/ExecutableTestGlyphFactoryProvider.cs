﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;

namespace Cepepito.EditorGuiDecorators
{
    [Export(typeof(IGlyphFactoryProvider))]
    [Name("ExecutableTestGlyph")]
    [Order(After = "VsTextMarker")]
    [ContentType("C/C++")]
    [TagType(typeof(ExecutableTestTag))]
    internal sealed class ExecutableTestGlyphFactoryProvider : IGlyphFactoryProvider
    {
        public IGlyphFactory GetGlyphFactory(IWpfTextView view, IWpfTextViewMargin margin)
        {
            return new ExecutableTestGlyphFactory();
        }
    }
}
