﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CepepitoCore.Core.Entities;
using CepepitoCore.EntryPoints;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;

namespace Cepepito.EditorGuiDecorators
{
    sealed class ExecutableTestTagger : ITagger<ExecutableTestTag>
    {
        private readonly ITextBuffer _buffer;
        private readonly ITestCaseExecutionStatusQueryService _testCaseExecutionStatusQueryService;

        public ExecutableTestTagger(ITextBuffer buffer,
            ITestCaseExecutionStatusQueryService testStatusService)
        {
            _buffer = buffer;
            _testCaseExecutionStatusQueryService = testStatusService;
            testStatusService.StatusUpdated += TestStatusService_StatusUpdated;
        }

        private void TestStatusService_StatusUpdated(object sender, EventArgs e)
        {
            OnTagsChanged();
        }

        public IEnumerable<ITagSpan<ExecutableTestTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            foreach (var span in spans)
            {
                foreach(var matchingSpan in GetMatchingEntriesSpans(span))
                {
                    var testStatus = _testCaseExecutionStatusQueryService.GetTestStatus(matchingSpan.Item3);
                    yield return new TagSpan<ExecutableTestTag>(new SnapshotSpan(matchingSpan.Item1, matchingSpan.Item2), new ExecutableTestTag(testStatus));
                }
            }
        }

        // TODO : simplify this method (Linq ?)
        private IEnumerable<Tuple<SnapshotPoint, int, TestCaseIdentifier>> GetMatchingEntriesSpans(SnapshotSpan span)
        {
            // TODO : encapsulate this logic in a single class and share it with test runner
            var regexpResult = Regex.Matches(span.GetText(),
                @"^\s*TEST\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)", RegexOptions.Multiline);
            foreach (var matchingSpan in GetMatchingEntriesSpans(regexpResult, span))
                yield return matchingSpan;

            regexpResult = Regex.Matches(span.GetText(),
                @"^\s*TEST_F\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)", RegexOptions.Multiline);
            foreach (var matchingSpan in GetMatchingEntriesSpans(regexpResult, span))
                yield return matchingSpan;

            regexpResult = Regex.Matches(span.GetText(),
                @"^\s*TEST_P\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)", RegexOptions.Multiline);
            foreach (var matchingSpan in GetMatchingEntriesSpans(regexpResult, span))
                yield return matchingSpan;
        }

        private IEnumerable<Tuple<SnapshotPoint, int, TestCaseIdentifier>> GetMatchingEntriesSpans(
            MatchCollection regexpResult, SnapshotSpan span)
        {
            foreach (Match match in regexpResult)
            {
                if (match.Success)
                    yield return new Tuple<SnapshotPoint, int, TestCaseIdentifier>(new SnapshotPoint(span.Snapshot, span.Start + match.Index), match.Length,
                        new TestCaseIdentifier(match.Groups[1].Value + "." + match.Groups[2].Value));
            }
        }

        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        private void OnTagsChanged()
        {
            TagsChanged?.Invoke(this, new SnapshotSpanEventArgs(new SnapshotSpan(_buffer.CurrentSnapshot, 0, _buffer.CurrentSnapshot.Length)));
        }
    }
}
