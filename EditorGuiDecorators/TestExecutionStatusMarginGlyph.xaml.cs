﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cepepito.EditorGuiDecorators
{
    /// <summary>
    /// Interaction logic for TestExecutionStatusMarginGlyph.xaml
    /// </summary>
    public partial class TestExecutionStatusMarginGlyph : UserControl
    {
        public TestExecutionStatusMarginGlyph(string glyphUri)
        {
            GlyphUri = glyphUri;
            InitializeComponent();
            DataContext = this;
        }

        public string GlyphUri { get; }
    }
}
