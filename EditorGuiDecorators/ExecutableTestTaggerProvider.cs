﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.ComponentModel.Composition;
using CepepitoCore.EntryPoints;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;

namespace Cepepito.EditorGuiDecorators
{
    [Export(typeof(ITaggerProvider))]
    [ContentType("C/C++")]
    [TagType(typeof(ExecutableTestTag))]
    class ExecutableTestTaggerProvider : ITaggerProvider
    {
        public ITagger<T> CreateTagger<T>(ITextBuffer buffer) where T : ITag
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            var testStatusService = new TestCaseExecutionStatusQueryServiceProvider().TestCaseExecutionStatusQueryService;
            return new ExecutableTestTagger(buffer, testStatusService) as ITagger<T>;
        }
    }
}
