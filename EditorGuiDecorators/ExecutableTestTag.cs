﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;
using Microsoft.VisualStudio.Text.Editor;

namespace Cepepito.EditorGuiDecorators
{
    class ExecutableTestTag : IGlyphTag
    {
        public TestCaseExecutionStatus TestCaseExecutionStatus { get; }

        public ExecutableTestTag(TestCaseExecutionStatus testCaseExecutionStatus)
        {
            TestCaseExecutionStatus = testCaseExecutionStatus;
        }
    }
}
