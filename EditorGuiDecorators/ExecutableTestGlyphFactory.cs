﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Windows;
using CepepitoCore.Core.Entities;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Formatting;

namespace Cepepito.EditorGuiDecorators
{
    internal class ExecutableTestGlyphFactory : IGlyphFactory
    {
        public UIElement GenerateGlyph(IWpfTextViewLine line, IGlyphTag tag)
        {
            var executableTag = tag as ExecutableTestTag;
            if (executableTag == null)
                return null;

            return new TestExecutionStatusMarginGlyph(GetGlyphImageUri(executableTag.TestCaseExecutionStatus));
        }

        private string GetGlyphImageUri(TestCaseExecutionStatus testCaseExecutionStatus)
        {
            // TODO : move icones resources location in a single class and share it
            switch (testCaseExecutionStatus)
            {
                case TestCaseExecutionStatus.Failed:
                    return "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Critical_16xLG_color.png";

                case TestCaseExecutionStatus.Successfull:
                    return "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Complete_and_ok_16xLG_color.png";

                case TestCaseExecutionStatus.Unknown:
                    return "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Complete_and_ok_16xLG.png";
            }

            return "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Complete_and_ok_16xLG.png";
        }
    }
}
