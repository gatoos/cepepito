﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CepepitoCore.Core.UseCases.RunTests.Common;
using CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode.@internal;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode
{
    internal static class RunTestsFromSourceCodeUseCase
    {
        public static async Task<RunTestsUseCaseOutput> Execute(IProjectOutputLauncher projectOutputLauncher,
            RunTestsFromSourceCodeUseCaseInputData runTestsFromSourceCodeUseCaseInput, ILoggerFactory loggerFactory)
        {
            var testCaseRunnerInputs = TestCaseRunnerInputsBuilder.Build(runTestsFromSourceCodeUseCaseInput, loggerFactory);

            if (testCaseRunnerInputs.TestCasesInformation.Any())
                return await RunTestsFromProjectAndTestCasesIdentifiers.Execute(projectOutputLauncher,
                    new RunTestsFromProjectAndTestCasesIdentifiersInputData(testCaseRunnerInputs), loggerFactory);

            return new RunTestsUseCaseOutput(null, null, new List<string>());
        }
    }
}