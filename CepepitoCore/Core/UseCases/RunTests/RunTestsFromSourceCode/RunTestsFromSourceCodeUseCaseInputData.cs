﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode
{
    internal class RunTestsFromSourceCodeUseCaseInputData
    {
        public RunTestsFromSourceCodeUseCaseInputData(string cursorLocationLineContent, string documentsContent,
            string currentDocumentProjectName)
        {
            DocumentsContent = documentsContent;
            CursorLocationLineContent = cursorLocationLineContent;
            CurrentDocumentProjectName = currentDocumentProjectName;
        }

        public string CursorLocationLineContent { get; }
        public string DocumentsContent { get; }
        public string CurrentDocumentProjectName { get; }
    }
}