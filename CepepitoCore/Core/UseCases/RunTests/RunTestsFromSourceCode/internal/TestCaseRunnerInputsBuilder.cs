﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;
using Logging;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode.@internal
{
    internal static class TestCaseRunnerInputsBuilder
    {
        public static 
            TestCaseRunnerInputs Build(RunTestsFromSourceCodeUseCaseInputData currentProjectCppDocumentInfos, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.BuildPerAssemblyLogger(typeof(TestCaseRunnerInputsBuilder));
            using (new ScopedActionLogger(logger, "Parsing test case informations"))
            {
                TestCaseInformationParser.RetrieveTestCaseInformation(
                    currentProjectCppDocumentInfos.CursorLocationLineContent,
                    out var testSuiteLabel, out var testCaseLabel);

                if (testSuiteLabel.Length == 0 || testCaseLabel.Length == 0)
                {
                    var testCasesInformation =
                        TestCaseInformationParser.RetrieveTestCasesInformation(new List<string>
                        {
                            currentProjectCppDocumentInfos.DocumentsContent
                        });

                    var testCaseRunnerInputs = new TestCaseRunnerInputs(
                        new ProjectIdentifier(currentProjectCppDocumentInfos.CurrentDocumentProjectName),
                        testCasesInformation);

                    logger.Debug(new {TestCaseRunnerInputs = testCaseRunnerInputs});

                    return testCaseRunnerInputs;
                }
                else
                {
                    var testCasesInformation =
                        new List<TestCaseIdentifier> {new TestCaseIdentifier(testSuiteLabel + "." + testCaseLabel)};
                    var testCaseRunnerInputs = new TestCaseRunnerInputs(
                        new ProjectIdentifier(currentProjectCppDocumentInfos.CurrentDocumentProjectName),
                        testCasesInformation);

                    logger.Debug(new {TestCaseRunnerInputs = testCaseRunnerInputs});

                    return testCaseRunnerInputs;
                }
            }
        }
    }
}