﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode.@internal
{
    internal static class TestCaseOutputBuilder
    {
        public static RunTestsUseCaseOutput Build(TestCaseExecutionResults testCaseExecutionResultses,
            TestCaseRunnerInputs testCaseRunnerInputs, IEnumerable<string> errors)
        {
            var testCaseAssociatedProject = BuildTestCasesAssociatedProjects(testCaseRunnerInputs);
            return new RunTestsUseCaseOutput(testCaseExecutionResultses, testCaseAssociatedProject, errors);
        }

        private static TestCaseIdentifierWithAssociatedProjects BuildTestCasesAssociatedProjects(
            TestCaseRunnerInputs testCaseRunnerInputs)
        {
            var testCaseAssociatedProject = new TestCaseIdentifierWithAssociatedProjects();
            foreach (var testCaseDescriptor in testCaseRunnerInputs.TestCasesInformation)
                testCaseAssociatedProject.AddTestCaseAssociatedProject(testCaseDescriptor,
                    testCaseRunnerInputs.TestProjectIdentifier);

            return testCaseAssociatedProject;
        }
    }
}