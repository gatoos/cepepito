﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.Core.UseCases.RunTests
{
    public class RunTestsUseCaseOutput
    {
        public RunTestsUseCaseOutput(TestCaseExecutionResults testCaseExecutionResultses,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects)
        {
            TestCaseExecutionResultses = testCaseExecutionResultses;
            TestCaseIdentifierWithAssociatedProjects = testCaseIdentifierWithAssociatedProjects;
            ErrorMessages = Enumerable.Empty<string>();
        }

        public RunTestsUseCaseOutput(TestCaseExecutionResults testCaseExecutionResultses,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects,
            IEnumerable<string> errorMessages)
        {
            TestCaseExecutionResultses = testCaseExecutionResultses;
            TestCaseIdentifierWithAssociatedProjects = testCaseIdentifierWithAssociatedProjects;
            ErrorMessages = errorMessages;
        }

        public TestCaseExecutionResults TestCaseExecutionResultses { get; set; }
        public TestCaseIdentifierWithAssociatedProjects TestCaseIdentifierWithAssociatedProjects { get; set; }
        public IEnumerable<string> ErrorMessages { get; }

        protected bool Equals(RunTestsUseCaseOutput other)
        {
            return Equals(TestCaseExecutionResultses, other.TestCaseExecutionResultses) &&
                   Equals(TestCaseIdentifierWithAssociatedProjects, other.TestCaseIdentifierWithAssociatedProjects) &&
                   ErrorMessages.SequenceEqual(other.ErrorMessages);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RunTestsUseCaseOutput) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = TestCaseExecutionResultses != null ? TestCaseExecutionResultses.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (TestCaseIdentifierWithAssociatedProjects != null
                               ? TestCaseIdentifierWithAssociatedProjects.GetHashCode()
                               : 0);
                hashCode = (hashCode * 397) ^ (ErrorMessages != null ? ErrorMessages.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}