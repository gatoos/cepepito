﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestFromProject
{
    internal class RunTestFromProjectUseCaseInputData
    {
        public RunTestFromProjectUseCaseInputData(IEnumerable<IEnumerable<string>> cppDocuments,
            ProjectIdentifier projectIdentifier)
        {
            CppDocuments = cppDocuments;
            ProjectIdentifier = projectIdentifier;
        }

        public IEnumerable<IEnumerable<string>> CppDocuments { get; }
        public ProjectIdentifier ProjectIdentifier { get; }
    }
}