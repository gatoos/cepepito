﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.Core.UseCases.RunTests.Common;
using CepepitoCore.Core.UseCases.RunTests.RunTestFromProject.@internal;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestFromProject
{
    public static class RunTestsFromProjectsUseCase
    {
        public static async Task<RunTestsUseCaseOutput> Execute(IProjectOutputLauncher projectOutputLauncher, IEnumerable<ProjectWithContent> selectedProjects, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.BuildPerAssemblyLogger(typeof(RunTestsFromProjectsUseCase));
            var inputsValidityCheck = ValidateInputs(selectedProjects, logger);

            if (inputsValidityCheck.AreValid) return await ExecuteWithValideInputs(projectOutputLauncher, inputsValidityCheck, loggerFactory);

            return new RunTestsUseCaseOutput(null, null, new List<string> {inputsValidityCheck.ErrorMessage});
        }

        private static async Task<RunTestsUseCaseOutput> ExecuteWithValideInputs(IProjectOutputLauncher projectOutputLauncher, InputsValidityCheck inputsValidityCheck, ILoggerFactory loggerFactory)
        {
            var testCaseRunnerInputs = BuildTestCaseRunnerInputsData(inputsValidityCheck);

            if (testCaseRunnerInputs.TestCasesInformation.Any())
                return await RunTestsFromProjectAndTestCasesIdentifiers.Execute(projectOutputLauncher,
                    new RunTestsFromProjectAndTestCasesIdentifiersInputData(testCaseRunnerInputs), loggerFactory);

            return new RunTestsUseCaseOutput(null, null, new List<string>());
        }

        private static TestCaseRunnerInputs BuildTestCaseRunnerInputsData(InputsValidityCheck inputsValidityCheck)
        {
            var testCaseRunnerInputs = TestCaseRunnerInputsBuilder.Build(
                new RunTestFromProjectUseCaseInputData(inputsValidityCheck.UniqueProject.Content,
                    inputsValidityCheck.UniqueProject.Identifier));
            return testCaseRunnerInputs;
        }

        private static InputsValidityCheck ValidateInputs(IEnumerable<ProjectWithContent> selectedProjects, ILogger logger)
        {
            if (!selectedProjects.Any())
            {
                logger.Error("No project selected");
                return new InputsValidityCheck("No project selected");
            }


            if (selectedProjects.Count() > 1)
            {
                logger.Error("Cannot launch tests for different projects");
                return new InputsValidityCheck("Cannot launch tests for different projects");
            }
                
            logger.Debug(new {SelectedProject = selectedProjects.First()});
            return new InputsValidityCheck(selectedProjects.First());
        }
    }
}