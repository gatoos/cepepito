﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestFromProject.@internal
{
    internal static class TestCaseRunnerInputsBuilder
    {
        public static TestCaseRunnerInputs Build(RunTestFromProjectUseCaseInputData runTestsFromProjectUseCaseInputData)
        {
            var testCasesInformation = new List<TestCaseIdentifier>();

            foreach (var documentContent in runTestsFromProjectUseCaseInputData.CppDocuments)
                testCasesInformation.AddRange(TestCaseInformationParser.RetrieveTestCasesInformation(documentContent));

            return new TestCaseRunnerInputs(runTestsFromProjectUseCaseInputData.ProjectIdentifier,
                testCasesInformation);
        }
    }
}