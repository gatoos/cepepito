﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestFromProject.@internal
{
    internal struct InputsValidityCheck
    {
        public bool AreValid { get; }
        public ProjectWithContent UniqueProject { get; }
        public string ErrorMessage { get; }

        public InputsValidityCheck(ProjectWithContent uniqueProject)
        {
            AreValid = true;
            UniqueProject = uniqueProject;
            ErrorMessage = "";
        }

        public InputsValidityCheck(string errorMessage)
        {
            AreValid = false;
            UniqueProject = new ProjectWithContent();
            ErrorMessage = errorMessage;
        }
    }
}