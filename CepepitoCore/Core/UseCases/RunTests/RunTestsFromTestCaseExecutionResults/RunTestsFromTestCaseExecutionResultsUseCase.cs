﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestsFromTestCaseExecutionResults
{
    public static class RunTestsFromTestCaseExecutionResultsUseCase
    {
        public static async Task<RunTestsUseCaseOutput> Execute(IProjectOutputLauncher projectOutputLauncher,
            TestCaseExecutionResults testCaseExecutionResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects,
            ILoggerFactory loggerFactory)
        {
            var selectedTestCases = RetrieveSelectedTestCases(testCaseExecutionResults, loggerFactory);

            return await RunTestsFromTestCasesIdentifiers.Execute(projectOutputLauncher, selectedTestCases,
                testCaseIdentifierWithAssociatedProjects, loggerFactory);
        }

        private static List<TestCaseIdentifier> RetrieveSelectedTestCases(
            TestCaseExecutionResults testCaseExecutionResults, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.BuildPerAssemblyLogger(typeof(RunTestsFromTestCaseExecutionResultsUseCase));
            using (new ScopedActionLogger(logger, "Retrieve selected test cases"))
            {
                var selectedTestCaseExecutionResultNode =
                    RetrieveSelectedTestCaseExecutionResultNode(testCaseExecutionResults);

                if (selectedTestCaseExecutionResultNode == null)
                {
                    logger.Debug("No nodes selected");
                    return new List<TestCaseIdentifier>();
                }
                    

                var selectedTestCases = RetrieveTestCaseFromNode(selectedTestCaseExecutionResultNode);
                logger.Debug(new {SelectedTestCases = selectedTestCases});
                return selectedTestCases;
            }
        }

        private static List<TestCaseIdentifier> RetrieveTestCaseFromNode(
            TestCaseExecutionResults selectedTestCaseExecutionResultsNode)
        {
            var allTestCases = new List<TestCaseIdentifier>();

            var leafNodes = RetrieveLeafNodes(selectedTestCaseExecutionResultsNode);

            foreach (var node in leafNodes)
            {
                var testCaseKey =
                    RetrieveTestCase(node, ".");
                if (testCaseKey != null) allTestCases.Add(testCaseKey);
            }

            return allTestCases;
        }

        private static TestCaseIdentifier RetrieveTestCase(TestCaseExecutionResults node, string pathSeparator)
        {
            string[] stringSeparators = {pathSeparator};
            var nodePathElements = node.FullPath.Split(stringSeparators, StringSplitOptions.None);
            if (nodePathElements.Length == 3)
                return new TestCaseIdentifier(nodePathElements[1] + "." + nodePathElements[2]);
            return null;
        }

        private static IEnumerable<TestCaseExecutionResults> RetrieveLeafNodes(TestCaseExecutionResults testCaseExecutionResults)
        {
            var leafNodes = new List<TestCaseExecutionResults>();

            foreach (var childNode in testCaseExecutionResults.Children)
                leafNodes.AddRange(RetrieveLeafNodes(childNode));

            if (testCaseExecutionResults.Children != null && testCaseExecutionResults.Children.Count == 0)
                leafNodes.Add(testCaseExecutionResults);

            return leafNodes;
        }

        private static TestCaseExecutionResults RetrieveSelectedTestCaseExecutionResultNode(
            TestCaseExecutionResults testCaseExecutionResults)
        {
            if (testCaseExecutionResults.IsSelected) return testCaseExecutionResults;

            TestCaseExecutionResults selectedNode = null;

            foreach (var testCaseExecutionResultNode in testCaseExecutionResults.Children)
            {
                selectedNode = RetrieveSelectedTestCaseExecutionResultNode(testCaseExecutionResultNode);
                if (selectedNode != null) break;
            }

            return selectedNode;
        }
    }
}