﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.Core.UseCases.RunTests.Common;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.UseCases.RunTests.RunTestsFromTestCaseExecutionResults
{
    public static class RunTestsFromTestCasesIdentifiers
    {
        public static async Task<RunTestsUseCaseOutput> Execute(IProjectOutputLauncher projectOutputLauncher, List<TestCaseIdentifier> selectedTestCases,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.BuildPerAssemblyLogger(typeof(RunTestsFromTestCasesIdentifiers));

            if (!selectedTestCases.Any())
            {
                logger.Error("No test cases selected");
                return new RunTestsUseCaseOutput(null, testCaseIdentifierWithAssociatedProjects, new List<string>());
            }
                

            var perProjectTestCases =
                BuildPerProjectTestCases(selectedTestCases, testCaseIdentifierWithAssociatedProjects);

            if (BuildPerProjectTestCasesGeneratedErrorMessage(perProjectTestCases))
                return new RunTestsUseCaseOutput(null, testCaseIdentifierWithAssociatedProjects,
                    new List<string> {perProjectTestCases.Item2});

            if (SelectedTestCasesBelongToDifferentProjects(perProjectTestCases))
            {
                logger.Error("CAnnot launch tests. Tests cases belong to different projects");
                return new RunTestsUseCaseOutput(null, testCaseIdentifierWithAssociatedProjects,
                    new List<string> { "Cannot launch tests. Tests cases belong to different projects" });
            }
                

            return await RunTestsFromProjectAndTestCasesIdentifiers.Execute(projectOutputLauncher,
                new RunTestsFromProjectAndTestCasesIdentifiersInputData(
                    new TestCaseRunnerInputs(perProjectTestCases.Item1.First().Key,
                        perProjectTestCases.Item1.First().Value)
                ), loggerFactory);
        }

        private static bool SelectedTestCasesBelongToDifferentProjects(
            Tuple<Dictionary<ProjectIdentifier, List<TestCaseIdentifier>>, string> perProjectTestCases)
        {
            return perProjectTestCases.Item1 != null && perProjectTestCases.Item1.Keys.Count > 1;
        }

        private static bool BuildPerProjectTestCasesGeneratedErrorMessage(
            Tuple<Dictionary<ProjectIdentifier, List<TestCaseIdentifier>>, string> perProjectTestCases)
        {
            return perProjectTestCases.Item2 != null;
        }

        private static Tuple<Dictionary<ProjectIdentifier, List<TestCaseIdentifier>>, string> BuildPerProjectTestCases(
            IEnumerable<TestCaseIdentifier> selectedTestCases,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects)
        {
            var perProjectTestCases = new Dictionary<ProjectIdentifier, List<TestCaseIdentifier>>();

            foreach (var testCaseIdentifier in selectedTestCases)
            {
                var projectIdentifier =
                    testCaseIdentifierWithAssociatedProjects.GetProjectIdentifier(testCaseIdentifier);

                if (projectIdentifier == null)
                    return new Tuple<Dictionary<ProjectIdentifier, List<TestCaseIdentifier>>, string>(null,
                        "Cannot find project for test case");

                if (!perProjectTestCases.ContainsKey(projectIdentifier))
                    perProjectTestCases.Add(projectIdentifier, new List<TestCaseIdentifier>());

                perProjectTestCases[projectIdentifier].Add(testCaseIdentifier);
            }

            return new Tuple<Dictionary<ProjectIdentifier, List<TestCaseIdentifier>>, string>(perProjectTestCases,
                null);
        }
    }
}