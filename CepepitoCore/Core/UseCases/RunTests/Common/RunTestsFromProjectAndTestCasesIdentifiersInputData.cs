﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;

namespace CepepitoCore.Core.UseCases.RunTests.Common
{
    public class RunTestsFromProjectAndTestCasesIdentifiersInputData
    {
        public RunTestsFromProjectAndTestCasesIdentifiersInputData(TestCaseRunnerInputs testCaseRunnerInputs)
        {
            TestCaseRunnerInputs = testCaseRunnerInputs;
        }

        public TestCaseRunnerInputs TestCaseRunnerInputs { get; }
    }
}