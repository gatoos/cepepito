﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode.@internal;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.UseCases.RunTests.Common
{
    public static class RunTestsFromProjectAndTestCasesIdentifiers
    {
        public static async Task<RunTestsUseCaseOutput> Execute(IProjectOutputLauncher projectOutputLauncher,
            RunTestsFromProjectAndTestCasesIdentifiersInputData runTestsFromProjectAndTestCasesIdentifiersInputData, ILoggerFactory loggerFactory)
        {
            var (testCaseExecutionResults, errors) =
                await TestCasesRunner.Run(projectOutputLauncher,
                    runTestsFromProjectAndTestCasesIdentifiersInputData.TestCaseRunnerInputs, loggerFactory);
            return TestCaseOutputBuilder.Build(testCaseExecutionResults,
                runTestsFromProjectAndTestCasesIdentifiersInputData.TestCaseRunnerInputs, errors);
        }
    }
}