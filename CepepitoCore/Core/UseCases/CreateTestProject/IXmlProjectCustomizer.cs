﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.Core.UseCases.CreateTestProject
{
    public interface IXmlProjectCustomizer
    {
        string BuildCustomizedProjectXmlRepresentation(string projectXmlRepresentation, string sourceProjectFullName,
            string sourceProjectGuid);
    }
}