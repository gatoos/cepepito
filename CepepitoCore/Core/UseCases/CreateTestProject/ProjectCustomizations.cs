﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Collections.Generic;

namespace CepepitoCore.Core.UseCases.CreateTestProject
{
    public struct ProjectCustomizations
    {
        public ProjectFilesCustomizationEnum ProjectFilesCustomization { get; }
        public IEnumerable<string> AdditionalIncludeDirectories { get; }
        public string AdditionalPreprocessorDefinitions { get; }
        public bool AddOriginalProjectStaticLibraryPath { get; }
        public string AdditionalLinkLibrary { get; }
        
        public ProjectCustomizations(ProjectFilesCustomizationEnum projectFilesCustomization,
            IEnumerable<string> additionalIncludeDirectories, string additionalPreprocessorDefinitions, bool addOriginalProjectStaticLibraryPath, string additionalLinkLibrary)
        {
            ProjectFilesCustomization = projectFilesCustomization;
            AdditionalIncludeDirectories = additionalIncludeDirectories;
            AdditionalPreprocessorDefinitions = additionalPreprocessorDefinitions;
            AddOriginalProjectStaticLibraryPath = addOriginalProjectStaticLibraryPath;
            AdditionalLinkLibrary = additionalLinkLibrary;
        }
    }
}