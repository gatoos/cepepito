﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.Core.UseCases.CreateTestProject
{
    public class ProjectCustomizer
    {
        private readonly IXmlProjectCustomizer _xmlProjectCustomizer;
        public ProjectCustomizations ProjectCustomizationsData { get; }

        public ProjectCustomizer(IXmlProjectCustomizer xmlProjectCustomizer,
            ProjectCustomizations projectCustomizations)
        {
            _xmlProjectCustomizer = xmlProjectCustomizer;
            ProjectCustomizationsData = projectCustomizations;
        }

        public string BuildCustomizedProjectXmlRepresentation(string projectXmlRepresentation,
            string sourceProjectFullName, string sourceProjectGuid)
        {
            return _xmlProjectCustomizer.BuildCustomizedProjectXmlRepresentation(projectXmlRepresentation,
                sourceProjectFullName, sourceProjectGuid);
        }
    }
}