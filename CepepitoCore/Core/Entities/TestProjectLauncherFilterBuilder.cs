﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Linq;
using Logging;

namespace CepepitoCore.Core.Entities
{
    internal static class TestProjectLauncherFilterBuilder
    {
        public static string Build(IEnumerable<TestCaseIdentifier> testCasesInformation, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.BuildPerAssemblyLogger(typeof(TestProjectLauncherFilterBuilder));
            logger.Info("Building tests launch filter");
            var testLaunchFilter = "";
            foreach (var testCaseDescription in testCasesInformation)
            {
                logger.Debug(new {CurrentTestCaseDescription=testCaseDescription});

                if (testLaunchFilter.Any())
                    testLaunchFilter += ":";

                testLaunchFilter += "*" + testCaseDescription.Descriptor + "*";
            }
            logger.Debug(new {BuiltTestProjectLaunchFilter=testLaunchFilter});

            return testLaunchFilter;
        }
    }
}