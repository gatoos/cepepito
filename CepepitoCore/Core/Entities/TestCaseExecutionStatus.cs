﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.Core.Entities
{
    public enum TestCaseExecutionStatus
    {
        Unknown,
        Successfull,
        Failed,
        PreviouslySuccessfull,
        PreviouslyFailed
    }
}