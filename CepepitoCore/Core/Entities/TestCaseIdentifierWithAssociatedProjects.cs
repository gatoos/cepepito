﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace CepepitoCore.Core.Entities
{
    public class TestCaseIdentifierWithAssociatedProjects
    {
        private readonly Dictionary<TestCaseIdentifier, ProjectIdentifier> _testCasesAssociatedProjects;

        public TestCaseIdentifierWithAssociatedProjects(
            Dictionary<TestCaseIdentifier, ProjectIdentifier> testCaseAssociatedProjects)
        {
            _testCasesAssociatedProjects = testCaseAssociatedProjects ??
                                           throw new ArgumentNullException(nameof(testCaseAssociatedProjects));
        }

        public TestCaseIdentifierWithAssociatedProjects() : this(
            new Dictionary<TestCaseIdentifier, ProjectIdentifier>())
        {
        }

        protected bool Equals(TestCaseIdentifierWithAssociatedProjects other)
        {
            return _testCasesAssociatedProjects.SequenceEqual(other._testCasesAssociatedProjects);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TestCaseIdentifierWithAssociatedProjects) obj);
        }

        public override int GetHashCode()
        {
            return _testCasesAssociatedProjects != null ? _testCasesAssociatedProjects.GetHashCode() : 0;
        }

        public void AddTestCaseAssociatedProject(TestCaseIdentifier testCaseIdentifier,
            ProjectIdentifier projectIdentifier)
        {
            if(!_testCasesAssociatedProjects.ContainsKey(testCaseIdentifier))
                _testCasesAssociatedProjects.Add(testCaseIdentifier, projectIdentifier);
        }

        public ProjectIdentifier GetProjectIdentifier(TestCaseIdentifier testCaseIdentifier)
        {
            return _testCasesAssociatedProjects.ContainsKey(testCaseIdentifier)
                ? _testCasesAssociatedProjects[testCaseIdentifier]
                : null;
        }

        public void Append(TestCaseIdentifierWithAssociatedProjects source)
        {
            if (source != null)
                foreach (var entry in source._testCasesAssociatedProjects)
                {
                    if (!_testCasesAssociatedProjects.ContainsKey(entry.Key))
                        _testCasesAssociatedProjects.Add(entry.Key, null);
                    _testCasesAssociatedProjects[entry.Key] = entry.Value;
                }
        }
    }
}