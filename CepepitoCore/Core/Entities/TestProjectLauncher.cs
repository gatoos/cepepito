﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.Entities
{
    internal static class TestProjectLauncher
    {
        internal static async Task<XDocument> Launch(IProjectOutputLauncher projectLauncher,ProjectIdentifier testsProjectIdentifier,
            IEnumerable<TestCaseIdentifier> testCasesInformation, ILoggerFactory loggerFactory)
        {
            var testLaunchFilter = TestProjectLauncherFilterBuilder.Build(testCasesInformation, loggerFactory); ;
            return await projectLauncher.LaunchProjectWithArguments(testsProjectIdentifier.Identifier,
                testLaunchFilter);
        }
    }
}