﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Diagnostics;

namespace CepepitoCore.Core.Entities
{
    public class TestCaseIdentifier
    {
        public TestCaseIdentifier(string descriptor)
        {
            Descriptor = descriptor;
            string[] stringSeparators = {"."};
            var nodePathElements = Descriptor.Split(stringSeparators, StringSplitOptions.None);
            Debug.Assert(nodePathElements.Length == 2);
            TestSuite = nodePathElements[0];
            TestCase = nodePathElements[1];
        }

        public TestCaseIdentifier(string testSuite, string testCase)
        {
            TestSuite = testSuite;
            TestCase = testCase;
            Descriptor = TestSuite + "." + TestCase;
        }

        public string Descriptor { get; }

        public string TestSuite { get; }
        public string TestCase { get; }

        public string EmptyTestDeclaration => "TEST(" + TestSuite + ", " + TestCase + ")" + Environment.NewLine + "{}" +
                                              Environment.NewLine;

        protected bool Equals(TestCaseIdentifier other)
        {
            return string.Equals(Descriptor, other.Descriptor);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TestCaseIdentifier) obj);
        }

        public override int GetHashCode()
        {
            return Descriptor != null ? Descriptor.GetHashCode() : 0;
        }
    }
}