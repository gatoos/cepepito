﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using Logging;

namespace CepepitoCore.Core.Entities
{
    public static class TestCaseExecutionResultBuilder
    {
        public static TestCaseExecutionResults BuildTestResultTreeNodeFromXml(XDocument xdocument,
            ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.BuildPerAssemblyLogger(typeof(TestCaseExecutionResultBuilder));
            Debug.Assert(xdocument != null);

            logger.Info("Building test results");
            logger.Debug(new {Description = "Building test results", SerializedResults = xdocument.ToString()});
            
            var testSuitesNode =
                xdocument.Elements("testsuites").SingleOrDefault();
            if (testSuitesNode != null)
                return new TestCaseExecutionResults("Tests Results",
                    from testSuiteNode in testSuitesNode.Descendants("testsuite")
                    select new TestCaseExecutionResults(testSuiteNode.Attribute("name")?.Value,
                        testSuiteNode.Descendants("testcase").Select(
                            testCase => new TestCaseExecutionResults(testCase.Attribute("name")?.Value,
                                testCase.Descendants("failure").Select(failure => failure.Value))
                        )
                    )
                );

            return null;
        }
    }
}