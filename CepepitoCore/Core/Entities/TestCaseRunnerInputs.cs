﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;

namespace CepepitoCore.Core.Entities
{
    public class TestCaseRunnerInputs
    {
        public TestCaseRunnerInputs(ProjectIdentifier testProjectIdentifier,
            List<TestCaseIdentifier> testCasesInformation)
        {
            TestProjectIdentifier = testProjectIdentifier;
            TestCasesInformation = testCasesInformation;
        }

        public ProjectIdentifier TestProjectIdentifier { get; }
        public List<TestCaseIdentifier> TestCasesInformation { get; }
    }
}