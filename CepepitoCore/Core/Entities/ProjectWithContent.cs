﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;

namespace CepepitoCore.Core.Entities
{
    public struct ProjectWithContent
    {
        public ProjectIdentifier Identifier { get; }
        public IEnumerable<IEnumerable<string>> Content { get; }

        public ProjectWithContent(ProjectIdentifier identifier, IEnumerable<IEnumerable<string>> content)
        {
            Identifier = identifier;
            Content = content;
        }
    }
}