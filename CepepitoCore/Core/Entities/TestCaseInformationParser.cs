﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CepepitoCore.Core.Entities
{
    public static class TestCaseInformationParser
    {
        public static void RetrieveTestCaseInformation(string lineContent, out string testSuiteLabel, out string testCaseLabel)
        {
            if (lineContent.Contains("TEST("))
            {
                var regexpResult = Regex.Match(lineContent,
                    @"^\s*TEST\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)");
                testSuiteLabel = regexpResult.Groups[1].Value;
                testCaseLabel = regexpResult.Groups[2].Value;
            }
            else if (lineContent.Contains("TEST_F("))
            {
                var regexpResult = Regex.Match(lineContent,
                    @"^\s*TEST_F\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)");
                testSuiteLabel = regexpResult.Groups[1].Value;
                testCaseLabel = regexpResult.Groups[2].Value;
            }
            else if (lineContent.Contains("TEST_P("))
            {
                var regexpResult = Regex.Match(lineContent,
                    @"^\s*TEST_P\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)");
                testSuiteLabel = regexpResult.Groups[1].Value;
                testCaseLabel = regexpResult.Groups[2].Value;
            }
            else
            {
                testSuiteLabel = "";
                testCaseLabel = "";
            }
        }

        public static List<TestCaseIdentifier> RetrieveTestCasesInformation(IEnumerable<string> documentContent)
        {
            var testCasesInformation = new List<TestCaseIdentifier>();

            foreach (var documentLine in documentContent)
            {
                var regexpResult = Regex.Matches(documentLine,
                    @"^\s*TEST\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)", RegexOptions.Multiline);
                FillTestCasesInformation(regexpResult, testCasesInformation);
                regexpResult = Regex.Matches(documentLine,
                    @"^\s*TEST_F\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)", RegexOptions.Multiline);
                FillTestCasesInformation(regexpResult, testCasesInformation);
                regexpResult = Regex.Matches(documentLine,
                    @"^\s*TEST_P\(([a-zA-Z_].[a-zA-Z_0-9]*)\s*,\s*([a-zA-Z_].[a-zA-Z_0-9]*)\)", RegexOptions.Multiline);
                FillTestCasesInformation(regexpResult, testCasesInformation);
            }

            return testCasesInformation;
        }

        private static void FillTestCasesInformation(MatchCollection regexpResult,
            List<TestCaseIdentifier> testCasesInformation)
        {
            foreach (Match match in regexpResult)
                if (match.Success)
                    testCasesInformation.Add(
                        new TestCaseIdentifier(match.Groups[1].Value + "." + match.Groups[2].Value));
        }
    }
}