﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using CepepitoCore.EntryPoints;

namespace CepepitoCore.Core.Entities
{
    // TODO : make this class internal once the update of the test case statuses are not done in the viewerViewModel anymore
    public class TestCaseExecutionStatusQueryService : ITestCaseExecutionStatusQueryService
    {
        private static readonly Lazy<TestCaseExecutionStatusQueryService> _instance = new Lazy<TestCaseExecutionStatusQueryService>();
        public static TestCaseExecutionStatusQueryService Instance => _instance.Value;
        private readonly Dictionary<TestCaseIdentifier, TestCaseExecutionStatus> _testStatuses = new Dictionary<TestCaseIdentifier, TestCaseExecutionStatus>();

        public void ClearStatuses()
        {
            _testStatuses.Clear();
        }

        public event EventHandler StatusUpdated;

        public TestCaseExecutionStatus GetTestStatus(TestCaseIdentifier testCaseIdentifier)
        {
            return _testStatuses.TryGetValue(testCaseIdentifier, out var status)
                ? status
                : TestCaseExecutionStatus.Unknown;
        }

        public void UpdateStatus(TestCaseIdentifier testCaseIdentifier, TestCaseExecutionStatus testCaseExecutionStatus)
        {
            if (_testStatuses.ContainsKey(testCaseIdentifier))
            {
                _testStatuses[testCaseIdentifier] = testCaseExecutionStatus;
            }
            else
            {
                _testStatuses.Add(testCaseIdentifier, testCaseExecutionStatus);
            }
            
            OnStatusUpdated();
        }

        protected virtual void OnStatusUpdated()
        {
            StatusUpdated?.Invoke(this, EventArgs.Empty);
        }
    }
}
