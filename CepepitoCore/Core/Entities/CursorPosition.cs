﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.Core.Entities
{
    public class CursorPosition
    {
        public uint Line { get; set; }
        public uint Column { get; set; }
    }
}