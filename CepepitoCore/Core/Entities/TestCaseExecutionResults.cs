﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace CepepitoCore.Core.Entities
{
    public class TestCaseExecutionResults : INotifyPropertyChanged
    {
        private ObservableCollection<string> _errorsDescription;

        private bool _isExpanded;

        private bool _isSelected;
        private string _parentPath;
        private TestCaseExecutionStatus _testCaseExecutionStatusMember;

        public TestCaseExecutionResults(string label)
        {
            Label = label;
            _parentPath = "";
            _testCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull;
            _errorsDescription = new ObservableCollection<string>();
            Children = new ObservableCollection<TestCaseExecutionResults>();
            _isSelected = false;
            _isExpanded = true;
        }

        public TestCaseExecutionResults(string label, IEnumerable<TestCaseExecutionResults> children)
        {
            Label = label;
            _parentPath = "";
            _testCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull;
            _errorsDescription = new ObservableCollection<string>();
            Children = new ObservableCollection<TestCaseExecutionResults>();
            foreach (var child in children) AddChildren(child);
            _isSelected = false;
            _isExpanded = true;
        }

        public TestCaseExecutionResults(string label, IEnumerable<string> errorsDescription)
        {
            Label = label;
            _parentPath = "";
            _testCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull;
            _errorsDescription = new ObservableCollection<string>();
            foreach (var errorDescription in errorsDescription)
            {
                ErrorsDescription.Add(errorDescription);
                _testCaseExecutionStatusMember = TestCaseExecutionStatus.Failed;
            }

            Children = new ObservableCollection<TestCaseExecutionResults>();
            _isSelected = false;
            _isExpanded = true;
        }

        public TestCaseExecutionResults(string label, bool selected)
        {
            Label = label;
            _parentPath = "";
            _testCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull;
            _errorsDescription = new ObservableCollection<string>();
            Children = new ObservableCollection<TestCaseExecutionResults>();
            _isSelected = selected;
            _isExpanded = true;
        }

        public TestCaseExecutionResults(string label, bool selected, IEnumerable<TestCaseExecutionResults> children)
        {
            Label = label;
            _parentPath = "";
            _testCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull;
            _errorsDescription = new ObservableCollection<string>();
            Children = new ObservableCollection<TestCaseExecutionResults>();
            foreach (var child in children) AddChildren(child);
            _isSelected = selected;
            _isExpanded = true;
        }

        public string Label { get; }

        public string FullPath
        {
            get
            {
                if (_parentPath != "")
                    return _parentPath + "." + Label;
                return Label;
            }
        }

        public string ParentPath
        {
            set
            {
                _parentPath = value;
                foreach (var node in Children)
                    node.ParentPath = FullPath;
            }
        }

        public TestCaseExecutionStatus TestCaseExecutionStatusMember
        {
            get => _testCaseExecutionStatusMember;
            set
            {
                if (value != _testCaseExecutionStatusMember)
                {
                    _testCaseExecutionStatusMember = value;
                    NotifyPropertyChanged("TestCaseExecutionStatusMember");
                    NotifyPropertyChanged("StatusImagePath");
                }
            }
        }

        public string StatusImagePath
        {
            get
            {
                switch (_testCaseExecutionStatusMember)
                {
                    case TestCaseExecutionStatus.Failed:
                        return
                            "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Critical_16xLG_color.png";
                    case TestCaseExecutionStatus.Successfull:
                        return
                            "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Complete_and_ok_16xLG_color.png";
                    case TestCaseExecutionStatus.PreviouslyFailed:
                        return
                            "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Critical_16xLG.png";
                    case TestCaseExecutionStatus.PreviouslySuccessfull:
                        return
                            "pack://application:,,,/CepepitoCoreAdapters;component/UIAdapters/RunTests/Resources/StatusAnnotations_Complete_and_ok_16xLG.png";
                    default:
                        return "";
                }
            }
        }

        public string FormatedErrorsDescription
        {
            get
            {
                var returnValue = "";
                foreach (var errorDescription in _errorsDescription)
                    returnValue += errorDescription + Environment.NewLine;

                return returnValue;
            }
        }

        public ObservableCollection<string> ErrorsDescription
        {
            get => _errorsDescription;

            set
            {
                if (!value.Equals(_errorsDescription))
                {
                    _errorsDescription = value;
                    NotifyPropertyChanged("ErrorDescription");
                    NotifyPropertyChanged("FormatedErrorsDescription");
                }
            }
        }

        public ObservableCollection<TestCaseExecutionResults> Children { get; }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
        }

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    NotifyPropertyChanged("IsExpanded");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public override bool Equals(object objectOperand)
        {
            if (!(objectOperand is TestCaseExecutionResults operand))
                return false;

            return Equals(operand);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool Equals(TestCaseExecutionResults operand)
        {
            if (Label.Equals(operand.Label) &&
                _parentPath.Equals(operand._parentPath) &&
                _testCaseExecutionStatusMember.Equals(operand.TestCaseExecutionStatusMember) &&
                _errorsDescription.SequenceEqual(operand._errorsDescription) &&
                Children.SequenceEqual(operand.Children))
                return true;
            return false;
        }

        public void AddChildren(TestCaseExecutionResults child)
        {
            child.ParentPath = FullPath;
            Children.Add(child);
        }

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public void Clear()
        {
            Children.Clear();
            ErrorsDescription.Clear();
            TestCaseExecutionStatusMember = TestCaseExecutionStatus.Successfull;
        }
    }
}