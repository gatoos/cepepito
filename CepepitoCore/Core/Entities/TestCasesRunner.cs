﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using Logging;

namespace CepepitoCore.Core.Entities
{
    internal static class TestCasesRunner
    {
        internal static async Task<(TestCaseExecutionResults, IEnumerable<string>)> Run(IProjectOutputLauncher projectLauncher, 
            TestCaseRunnerInputs testCaseRunnerInputs, ILoggerFactory loggerFactory)
        {
            var testExecutionSerializedResults = await ExecuteTests(projectLauncher, testCaseRunnerInputs.TestProjectIdentifier,
                    testCaseRunnerInputs.TestCasesInformation, loggerFactory);
            return (DeserializeTestExecutionResults(testExecutionSerializedResults, loggerFactory), Enumerable.Empty<string>());
        }

        private static async Task<XDocument> ExecuteTests(IProjectOutputLauncher projectLauncher, ProjectIdentifier testProjectIdentifier,
            IEnumerable<TestCaseIdentifier> testCasesInformation, ILoggerFactory loggerFactory)
        {
            return await TestProjectLauncher.Launch(projectLauncher, testProjectIdentifier, testCasesInformation, loggerFactory);
        }

        private static TestCaseExecutionResults DeserializeTestExecutionResults(XDocument testExecutionSerializedResults, ILoggerFactory loggerFactory)
        {
           return TestCaseExecutionResultBuilder.BuildTestResultTreeNodeFromXml(testExecutionSerializedResults, loggerFactory);
        }
    }
}