﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Text.RegularExpressions;

namespace CepepitoCore.Core.Entities
{
    public class ProjectIdentifier
    {
        public ProjectIdentifier(string projectIdentifier)
        {
            Identifier = projectIdentifier;
        }

        public string Identifier { get; }

        public string Name
        {
            get
            {
                var retrieveProjectNameRegexp =
                    new Regex(@"\\([0-9a-zA-Z_\- ]+).vcxproj", RegexOptions.IgnoreCase).Match(
                        Identifier);

                return retrieveProjectNameRegexp.Success ? retrieveProjectNameRegexp.Groups[1].Captures[0].Value : "";
            }

        }



        protected bool Equals(ProjectIdentifier other)
        {
            return string.Equals(Identifier, other.Identifier);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ProjectIdentifier) obj);
        }

        public override int GetHashCode()
        {
            return Identifier != null ? Identifier.GetHashCode() : 0;
        }
    }
}