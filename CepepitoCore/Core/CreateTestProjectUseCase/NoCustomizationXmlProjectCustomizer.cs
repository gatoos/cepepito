﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.UseCases.CreateTestProject;

namespace CepepitoCore.Core.CreateTestProjectUseCase
{
    internal class NoCustomizationXmlProjectCustomizer : IXmlProjectCustomizer
    {
        public string BuildCustomizedProjectXmlRepresentation(string projectXmlRepresentation,
            string sourceProjectFullName,
            string sourceProjectGuid)
        {
            return projectXmlRepresentation;
        }
    }
}