﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Xml;
using CepepitoCore.Core.UseCases.CreateTestProject;

namespace CepepitoCore.Core.CreateTestProjectUseCase
{
    internal class AddProjectReferenceXmlProjectCustomizer : IXmlProjectCustomizer
    {
        public string BuildCustomizedProjectXmlRepresentation(string projectXmlRepresentation,
            string sourceProjectFullName, string sourceProjectGuid)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(projectXmlRepresentation);
            var projectReferencesFatherItemGroup = xmlDocument.SelectNodes("ProjectReference")?.Item(0)?.ParentNode;

            if (projectReferencesFatherItemGroup == null)
            {
                var projectNode = xmlDocument.GetElementsByTagName("Project")?.Item(0);
                if (projectNode != null)
                {
                    projectReferencesFatherItemGroup =
                        xmlDocument.CreateElement("ItemGroup", xmlDocument.DocumentElement?.NamespaceURI);
                    projectNode.AppendChild(projectReferencesFatherItemGroup);
                }
            }

            if (projectReferencesFatherItemGroup != null)
            {
                var newProjectReferenceNode =
                    xmlDocument.CreateElement("ProjectReference", xmlDocument.DocumentElement?.NamespaceURI);
                newProjectReferenceNode.SetAttribute("Include", sourceProjectFullName);
                var newProjectNode = xmlDocument.CreateElement("Project", xmlDocument.DocumentElement?.NamespaceURI);
                newProjectNode.InnerText = sourceProjectGuid;
                newProjectReferenceNode.AppendChild(newProjectNode);
                projectReferencesFatherItemGroup.AppendChild(newProjectReferenceNode);
            }

            return xmlDocument.InnerXml;
        }
    }
}