﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.EntryPoints
{
    public class TestCaseExecutionStatusQueryServiceProvider
    {
        public ITestCaseExecutionStatusQueryService TestCaseExecutionStatusQueryService =>
            CepepitoCore.Core.Entities.TestCaseExecutionStatusQueryService.Instance;
    }
}
