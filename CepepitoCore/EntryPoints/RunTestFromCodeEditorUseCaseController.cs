﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CepepitoCore.Core.UseCases.RunTests;
using CepepitoCore.Core.UseCases.RunTests.RunTestsFromSourceCode;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.UI;
using Logging;

namespace CepepitoCore.EntryPoints
{
    public class RunTestFromCodeEditorUseCaseController
    {
        private readonly ICurrentDocumentInfoRetriever _currentDocumentInfoRetriever;
        private readonly IProjectOutputLauncher _projectOutputLauncher;
        private readonly ITestRunnerViewerViewModel _testRunnerViewerViewModel;
        private readonly IErrorsMessagesQueue _errorsMessageQueue;
        private readonly ILoggerFactory _loggerFactory;

        public RunTestFromCodeEditorUseCaseController(ITestRunnerViewerViewModel testRunnerViewerViewModel,
            ICurrentDocumentInfoRetriever currentDocumentInfoRetriever,
            IProjectOutputLauncher projectOutputLauncher, 
            IErrorsMessagesQueue errorsMessageQueue,
            ILoggerFactory loggerFactory)
        {
            _testRunnerViewerViewModel = testRunnerViewerViewModel;
            _currentDocumentInfoRetriever = currentDocumentInfoRetriever;
            _projectOutputLauncher = projectOutputLauncher;
            _errorsMessageQueue = errorsMessageQueue;
            _loggerFactory = loggerFactory;
        }

        private void SetTestRunningToOn(object sender, EventArgs eventArgs)
        {
            _testRunnerViewerViewModel.TestsAreRunning = true;
        }

        private void SetTestRunningToOff(object sender, EventArgs eventArgs)
        {
            _testRunnerViewerViewModel.TestsAreRunning = false;
        }

        public async Task LaunchTestAtCursorPosition()
        {
            var logger = _loggerFactory.BuildPerAssemblyLogger(typeof(RunTestFromCodeEditorUseCaseController));
            using (new ScopedActionLogger(logger, "Run test from code"))
            {
                _projectOutputLauncher.TestRunningStart += SetTestRunningToOn;
                _projectOutputLauncher.TestRunningEnd += SetTestRunningToOff;

                var testCaseExecutionResult = await GenerateTestsResults();

                if (testCaseExecutionResult.TestCaseExecutionResultses != null &&
                    !testCaseExecutionResult.ErrorMessages.Any())
                {
                    _testRunnerViewerViewModel.UpdateTestCaseExecutionResults(
                        testCaseExecutionResult.TestCaseExecutionResultses,
                        testCaseExecutionResult.TestCaseIdentifierWithAssociatedProjects);
                }

                _projectOutputLauncher.TestRunningStart -= SetTestRunningToOn;
                _projectOutputLauncher.TestRunningEnd -= SetTestRunningToOff;
                UpdateErrorMessages(testCaseExecutionResult.ErrorMessages, logger);
            }
        }

        private async Task<RunTestsUseCaseOutput> GenerateTestsResults()
        {
            var logger = _loggerFactory.BuildPerAssemblyLogger(typeof(RunTestFromCodeEditorUseCaseController));
            logger.Debug("Retrieving current document associated project name");
            var currentDocumentProjectName = _currentDocumentInfoRetriever.RetrieveCurrentDocumentProjectName();
            logger.Debug(new {CurrentDocumentAssociatedProjectName = currentDocumentProjectName});

            RunTestsUseCaseOutput testCaseExecutionResult;

            if (currentDocumentProjectName == null)
            {
                testCaseExecutionResult = new RunTestsUseCaseOutput(null, null,
                    new List<string> {"Current file doesn't belong to any project in the solution. Operation cancelled"});
            }
            else
            {
                testCaseExecutionResult =
                    await RunTestsFromSourceCodeUseCase.Execute(_projectOutputLauncher,
                        BuildUseCaseInputData(currentDocumentProjectName), _loggerFactory);
            }

            return testCaseExecutionResult;
        }

        private void UpdateErrorMessages(IEnumerable<string> errorMessages, ILogger logger)
        {
            logger.Debug(new { Description = "Notifying with error messages", ErrorMessages = errorMessages });
            foreach (var errorMessage in errorMessages) _errorsMessageQueue.AddErrorMessage(errorMessage);
        }

        private RunTestsFromSourceCodeUseCaseInputData BuildUseCaseInputData(string currentDocumentProjectName)
        {
            return new RunTestsFromSourceCodeUseCaseInputData(
                _currentDocumentInfoRetriever.RetrieveCurrentDocumentCursorLocationLineContent(),
                _currentDocumentInfoRetriever.RetrieveCurrentDocumentContent(),
                currentDocumentProjectName);
        }
    }
}