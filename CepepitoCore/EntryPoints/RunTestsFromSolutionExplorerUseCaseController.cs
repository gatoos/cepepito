﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using CepepitoCore.Core.UseCases.RunTests.RunTestFromProject;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromSolutionExplorer;
using CepepitoCore.UI;
using Logging;

namespace CepepitoCore.EntryPoints
{
    public class RunTestsFromSolutionExplorerUseCaseController
    {
        private readonly IErrorsMessagesQueue _errorsMessageQueue;
        private readonly IProjectOutputLauncher _projectOutputLauncher;
        private readonly ISelectedProjectsRetriever _selectedProjectsRetriever;
        private readonly ITestRunnerViewerViewModel _testRunnerViewerViewModel;
        private readonly ILoggerFactory _loggerFactory;

        public RunTestsFromSolutionExplorerUseCaseController(ITestRunnerViewerViewModel testRunnerViewerViewModel,
            IProjectOutputLauncher projectOutputLauncher, ISelectedProjectsRetriever selectedProjectsRetriever,
            IErrorsMessagesQueue errorsMessageQueue,
            ILoggerFactory loggerFactory)
        {
            _testRunnerViewerViewModel = testRunnerViewerViewModel;
            _projectOutputLauncher = projectOutputLauncher;
            _selectedProjectsRetriever = selectedProjectsRetriever;
            _errorsMessageQueue = errorsMessageQueue;
            _loggerFactory = loggerFactory;
        }

        private void SetTestRunningToOn(object sender, EventArgs eventArgs)
        {
            _testRunnerViewerViewModel.TestsAreRunning = true;
        }

        private void SetTestRunningToOff(object sender, EventArgs eventArgs)
        {
            _testRunnerViewerViewModel.TestsAreRunning = false;
        }

        public async void Execute()
        {
            var logger = _loggerFactory.BuildPerAssemblyLogger(typeof(RunTestsFromSolutionExplorerUseCaseController));
            using (new ScopedActionLogger(logger, "Launch tests for solution explorer"))
            {
                _projectOutputLauncher.TestRunningStart += SetTestRunningToOn;
                _projectOutputLauncher.TestRunningEnd += SetTestRunningToOff;

                var testCaseExecutionResult =
                    await RunTestsFromProjectsUseCase.Execute(_projectOutputLauncher, _selectedProjectsRetriever
                        .GetSelectedProjects(), _loggerFactory);

                if (testCaseExecutionResult.TestCaseExecutionResultses != null &&
                    !testCaseExecutionResult.ErrorMessages.Any())
                {
                    _testRunnerViewerViewModel.UpdateTestCaseExecutionResults(
                        testCaseExecutionResult.TestCaseExecutionResultses,
                        testCaseExecutionResult.TestCaseIdentifierWithAssociatedProjects);
                }

                _projectOutputLauncher.TestRunningStart -= SetTestRunningToOn;
                _projectOutputLauncher.TestRunningEnd -= SetTestRunningToOff;

                UpdateErrorMessages(testCaseExecutionResult.ErrorMessages, logger);
            }
        }

        private void UpdateErrorMessages(IEnumerable<string> errorMessages, ILogger logger)
        {
            logger.Debug(new {Description="Notifying with error messages", ErrorMessages=errorMessages});

            foreach (var errorMessage in errorMessages) _errorsMessageQueue.AddErrorMessage(errorMessage);
        }
    }
}