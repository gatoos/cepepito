﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;

namespace CepepitoCore.EntryPoints
{
    public class GenerateCodeInClipboardUseCaseController
    {
        public GenerateCodeInClipboardUseCaseController(
            ICurrentDocumentInfoRetriever currentDocumentInformationRetriever, ISolution solution,
            Func<ParserInput, (string typeName, string content, string errorMessage)> parser,
            IClipboard clipboard,
            IErrorsMessagesQueue errorMessageQueue, IProgressDialogBox progressDialogBox)
        {
            CurrentDocumentInformationRetriever = currentDocumentInformationRetriever;
            Solution = solution;
            Parser = parser;
            Clipboard = clipboard;
            ErrorMessageQueue = errorMessageQueue;
            ProgressDialogBox = progressDialogBox;
        }

        private ICurrentDocumentInfoRetriever CurrentDocumentInformationRetriever { get; }
        private ISolution Solution { get; }
        private Func<ParserInput, (string typeName, string content, string errorMessage)> Parser { get; }
        private IClipboard Clipboard { get; }
        private IErrorsMessagesQueue ErrorMessageQueue { get; }
        private IProgressDialogBox ProgressDialogBox { get; }

        public async Task Execute()
        {
            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                var parsingTask = Task.Factory
                    .StartNew(() => ParseDocumentContent(CurrentDocumentInformationRetriever, Solution, Parser),
                        cancellationTokenSource.Token);

                ProgressDialogBox.Show(parsingTask, cancellationTokenSource);

                var parserResult = await parsingTask;

                if (cancellationTokenSource.Token.IsCancellationRequested)
                    return;

                if (parserResult.generatedContent != null && parserResult.generatedContent.Any())
                    PushGeneratedContentInClipboard(parserResult.generatedContent, ErrorMessageQueue);
                else
                    ErrorMessageQueue.AddErrorMessage(parserResult.errorMessage);
            }
        }

        private static (string generatedTypeName, string generatedContent, string errorMessage) ParseDocumentContent(
            ICurrentDocumentInfoRetriever currentDocumentInformationRetriever,
            ISolution solution,
            Func<ParserInput, (string typeName, string content, string errorMessage)> parser)
        {
            string currentDocumentProjectName =
                currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName();

            if (currentDocumentProjectName == null)
                return ("", "", "Current file doesn't belong to any project in the solution. Operation cancelled");

            return parser(new ParserInput(currentDocumentInformationRetriever.DocumentName,
                solution.UnsavedDocuments,
                solution.GetProjectIncludeDirectories(
                    new ProjectIdentifier(currentDocumentProjectName)),
                Enumerable.Empty<string>(),
                currentDocumentInformationRetriever.CursorPosition));
        }

        private void PushGeneratedContentInClipboard(string generatedContent, IErrorsMessagesQueue errorMessageQueue)
        {
            var newDocumentContent = generatedContent;

            try
            {
                Clipboard.Show("Mock generated and added to clipboard." + Environment.NewLine + "You can paste it anywhere.", newDocumentContent);
            }
            catch (Exception e)
            {
                errorMessageQueue.AddErrorMessage(e.Message);
            }
        }
    }
}