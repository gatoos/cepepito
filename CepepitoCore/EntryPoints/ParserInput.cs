﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.EntryPoints
{
    public struct ParserInput
    {
        public string DocumentName { get; }
        public IEnumerable<(string name, string content)> UnsavedDocuments { get; }
        public IEnumerable<string> SourceProjectIncludeDirectories { get; }
        public IEnumerable<string> TargetProjectIncludeDirectories { get; }
        public CursorPosition CursorPosition { get; }

        public ParserInput(string documentName, IEnumerable<(string name, string content)> unsavedDocuments,
            IEnumerable<string> sourceProjectIncludeDirectories, IEnumerable<string> targetProjectIncludeDirectories, CursorPosition cursorCursorPosition)
        {
            DocumentName = documentName;
            UnsavedDocuments = unsavedDocuments;
            SourceProjectIncludeDirectories = sourceProjectIncludeDirectories;
            TargetProjectIncludeDirectories = targetProjectIncludeDirectories;
            CursorPosition = cursorCursorPosition;
        }
    }
}