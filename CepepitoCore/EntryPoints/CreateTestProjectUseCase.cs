﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System.Collections.Generic;
using System.Threading.Tasks;
using CepepitoCore.Core.CreateTestProjectUseCase;
using CepepitoCore.Core.UseCases.CreateTestProject;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.CreateTestProject;

namespace CepepitoCore.EntryPoints
{
    public class CreateTestProjectUseCase
    {
        private readonly ISelectedProjectPropertiesRetriever _selectedProjectPropertiesRetriever;
        private readonly ISolutionProjectsRepository _solutionProjectsRepository;
        private readonly ITestingLibrariesConfiguration _testingLibrariesConfiguration;
        private readonly string _visualStudioVersion;
        private readonly IErrorsMessagesQueue _errorsMessagesQueue;

        public CreateTestProjectUseCase(ISelectedProjectPropertiesRetriever selectedProjectPropertiesRetriever,
            ISolutionProjectsRepository solutionProjectsRepository,
            ITestingLibrariesConfiguration testingLibrariesConfiguration,
            string visualStudioVersion,
            IErrorsMessagesQueue errorsMessagesQueue)
        {
            _selectedProjectPropertiesRetriever = selectedProjectPropertiesRetriever;
            _solutionProjectsRepository = solutionProjectsRepository;
            _testingLibrariesConfiguration = testingLibrariesConfiguration;
            _visualStudioVersion = visualStudioVersion;
            _errorsMessagesQueue = errorsMessagesQueue;
        }

        public async Task Execute()
        {
            if (!_selectedProjectPropertiesRetriever.ProjectIsCppProject)
            {
                _errorsMessagesQueue.AddErrorMessage("Selected project is not a cpp project");
                return;
            }

            if (_selectedProjectPropertiesRetriever.ProjectContainsTestRunner)
            {
                _errorsMessagesQueue.AddErrorMessage("Selected project already contains a test runner");
                return;
            }
            
            (ProjectCustomizer projectCustomizer, string errorMessage) = BuildProjectCustomizer(_selectedProjectPropertiesRetriever.ProjectType,
                _testingLibrariesConfiguration,
                _visualStudioVersion);

            if (errorMessage != null)
            {
                _errorsMessagesQueue.AddErrorMessage(errorMessage);
                return;
            }

            var customizedProjectXmlDefinition = projectCustomizer.BuildCustomizedProjectXmlRepresentation(
                _selectedProjectPropertiesRetriever.ProjectXmlRepresentation,
                _selectedProjectPropertiesRetriever.ProjectFullName, _selectedProjectPropertiesRetriever.ProjectGuid);

            var toBeBuiltProjectName = buildTestProjectName(_selectedProjectPropertiesRetriever.ProjectName);

            var builtProjectUniqueName = _solutionProjectsRepository.AddProject(toBeBuiltProjectName,
                customizedProjectXmlDefinition, projectCustomizer.ProjectCustomizationsData,
                _selectedProjectPropertiesRetriever.ProjectUniqueName);

            _solutionProjectsRepository.AddFileToProject(builtProjectUniqueName, "TestRunner.cpp", BuildTestRunnerFileContent());
        }

        private string BuildTestRunnerFileContent()
        {
            return @"#include ""gtest/gtest.h""

            int main(int argc, char** argv)
            {
                ::testing::InitGoogleTest(&argc, argv);
                return RUN_ALL_TESTS();
            }
            ";
        }

        private string buildTestProjectName(string projectName)
        {
            return projectName + "Test";
        }

        private (ProjectCustomizer projectCustomizer, string errorMessage) BuildProjectCustomizer(ProjectType projectType,
            ITestingLibrariesConfiguration testingLibrariesConfiguration,
            string visualStudioVersion)
        {
            switch (projectType)
            {
                case ProjectType.StaticLinkedLibrary:
                    return (new ProjectCustomizer(new AddProjectReferenceXmlProjectCustomizer(),
                        new ProjectCustomizations(
                            ProjectFilesCustomizationEnum.RemoveAllFiles,
                            BuildAdditionalIncludeDirectories(testingLibrariesConfiguration),
                            BuildAdditionalPreprocessorDefinitions(visualStudioVersion),
                            true, testingLibrariesConfiguration.GoogleTestLibrary)), null);

                case ProjectType.DynamicLinkedLibrary:
                    return (new ProjectCustomizer(new AddProjectReferenceXmlProjectCustomizer(),
                        new ProjectCustomizations(
                            ProjectFilesCustomizationEnum.RemoveAllFiles,
                            BuildAdditionalIncludeDirectories(testingLibrariesConfiguration),
                            BuildAdditionalPreprocessorDefinitions(visualStudioVersion),
                            false, testingLibrariesConfiguration.GoogleTestLibrary)), null);

                case ProjectType.Executable:
                    return (new ProjectCustomizer(new NoCustomizationXmlProjectCustomizer(),
                        new ProjectCustomizations(ProjectFilesCustomizationEnum.RemoveAllFiles,
                            BuildAdditionalIncludeDirectories(testingLibrariesConfiguration),
                            BuildAdditionalPreprocessorDefinitions(visualStudioVersion),
                            false, testingLibrariesConfiguration.GoogleTestLibrary)), null);
                default:
                    return (null, "Project type not supported");
            }
        }

        private string BuildAdditionalPreprocessorDefinitions(string visualStudioVersion)
        {
            if (visualStudioVersion == "vs2012") return "_VARIADIC_MAX=10";

            return "";
        }

        private IEnumerable<string> BuildAdditionalIncludeDirectories(ITestingLibrariesConfiguration testingLibrariesConfiguration)
        {
            return new List<string>
            {
                testingLibrariesConfiguration.GoogleTestIncludeDirectory,
                testingLibrariesConfiguration.GoogleMockIncludeDirectory
            };
        }
    }
}