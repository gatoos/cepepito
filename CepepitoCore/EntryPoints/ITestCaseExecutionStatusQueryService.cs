﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.EntryPoints
{
    public interface ITestCaseExecutionStatusQueryService
    {
        TestCaseExecutionStatus GetTestStatus(TestCaseIdentifier testCaseIdentifier);
        void ClearStatuses();
        event EventHandler StatusUpdated;
    }
}
