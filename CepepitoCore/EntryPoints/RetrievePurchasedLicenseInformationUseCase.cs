﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.RetrievePurchasedLicense;
using CepepitoCore.UI;

namespace CepepitoCore.EntryPoints
{
    public class RetrievePurchasedLicenseInformationUseCase
    {
        public RetrievePurchasedLicenseInformationUseCase(IPurchaseCodeRetriever purchaseCodeRetriever,
            IPurchasedLicenseInformationRetriever purchasedLicenseInformationRetriever,
            ILicenseInformationViewModel licenseInformationViewModel, IErrorsMessagesQueue errorsMessagesQueue)
        {
            PurchaseCodeRetriever = purchaseCodeRetriever;
            PurchasedLicenseInformationRetriever = purchasedLicenseInformationRetriever;
            LicenseInformationViewModel = licenseInformationViewModel;
            ErrorsMessagesQueue = errorsMessagesQueue;
        }

        private IPurchaseCodeRetriever PurchaseCodeRetriever { get; }
        private IPurchasedLicenseInformationRetriever PurchasedLicenseInformationRetriever { get; }
        private ILicenseInformationViewModel LicenseInformationViewModel { get; }
        private IErrorsMessagesQueue ErrorsMessagesQueue { get; }

        public void Execute()
        {
            var purchaseCode = PurchaseCodeRetriever.RetrievePurchaseCode();

            if (purchaseCode == null)
                return;

            (var licenseInformation, var errorMessage) =
                PurchasedLicenseInformationRetriever.RetrievePurchasedLicenseInformation(purchaseCode);

            if (errorMessage != null)
                ErrorsMessagesQueue.AddErrorMessage(errorMessage);

            if (licenseInformation == null)
                return;

            LicenseInformationViewModel.LicenseInformation = licenseInformation.Value;
        }
    }
}