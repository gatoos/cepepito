﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CepepitoCore.Core.Entities;
using CepepitoCore.ServiceProviders.Interfaces.Common;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard;
using CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile;
using CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor;

namespace CepepitoCore.EntryPoints
{
    public class GenerateCodeInFileUseCaseController
    {
        

        public GenerateCodeInFileUseCaseController(IProjectSelector projectSelector,
            ICurrentDocumentInfoRetriever currentDocumentInformationRetriever, ISolution solution,
            Func<IEnumerable<string>, bool> checkRequiredIncludeDirectoriesPresence, 
            IRequiredIncludeDirectoriesRetriever requiredIncludeDirectoriesRetriever,
            Func<ParserInput, (string typeName, string content, string errorMessage)> parseFunction,
            IErrorsMessagesQueue errorMessageQueue, string generatedFileExtension, IFileOverwriteConfirmationQuery fileOverwriteConfirmationQuery, IProgressDialogBox progressDialogBox)
        {
            ProjectSelector = projectSelector;
            CurrentDocumentInformationRetriever = currentDocumentInformationRetriever;
            Solution = solution;
            RequiredIncludeDirectoriesRetriever = requiredIncludeDirectoriesRetriever;
            CheckRequiredIncludeDirectoriesPresence = checkRequiredIncludeDirectoriesPresence;
            ParseFunction = parseFunction;
            ErrorMessageQueue = errorMessageQueue;
            GeneratedFileExtension = generatedFileExtension;
            FileOverwriteConfirmationQuery = fileOverwriteConfirmationQuery;
            ProgressDialogBox = progressDialogBox;
        }

        private string GeneratedFileExtension { get; }

        private IProjectSelector ProjectSelector { get; }
        private ICurrentDocumentInfoRetriever CurrentDocumentInformationRetriever { get; }
        private ISolution Solution { get; }
        private Func<IEnumerable<string>, bool> CheckRequiredIncludeDirectoriesPresence { get; }
        private IRequiredIncludeDirectoriesRetriever RequiredIncludeDirectoriesRetriever { get; }
        private Func<ParserInput, (string typeName, string content, string errorMessage)> ParseFunction { get; }
        private IErrorsMessagesQueue ErrorMessageQueue { get; }
        private IFileOverwriteConfirmationQuery FileOverwriteConfirmationQuery { get; }
        private IProgressDialogBox ProgressDialogBox { get; }

        public async Task Execute()
        {
            var (targetProjectIdentifier, targetFilenameDirectory) = ProjectSelector.SelectProject();

            if (targetProjectIdentifier != null && targetFilenameDirectory != null)
            {
                if (!TargetProjectContainsRequiredIncludeDirectories(targetProjectIdentifier))
                {
                    var requiredIncludeDirectories = RetrieveRequiredIncludeDirectories(targetProjectIdentifier.Name);
                    if (requiredIncludeDirectories != null && requiredIncludeDirectories.Any())
                        Solution.AddIncludeDirectoriesToProject(targetProjectIdentifier, requiredIncludeDirectories);
                }

                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    var parsingTask = Task.Factory
                        .StartNew(() => ParseDocumentContent(CurrentDocumentInformationRetriever, Solution, ParseFunction, targetProjectIdentifier),
                            cancellationTokenSource.Token);

                    ProgressDialogBox.Show(parsingTask, cancellationTokenSource);

                    var parserResult = await parsingTask;

                    if (cancellationTokenSource.Token.IsCancellationRequested)
                        return;

                    if (parserResult.generatedContent != null && parserResult.generatedContent.Any())
                        GenerateNewFileFromParserResult(BuildTargetFilenameFullPath(targetFilenameDirectory, parserResult.generatedTypeName, GeneratedFileExtension), parserResult.generatedContent, 
                            Solution, targetProjectIdentifier, FileOverwriteConfirmationQuery);
                    else
                        ErrorMessageQueue.AddErrorMessage(parserResult.errorMessage);
                }
            }
        }

        private string BuildTargetFilenameFullPath(string targetFilenameDirectory, string targetFilename, string targetFilenameExtension)
        {
            return Path.Combine(targetFilenameDirectory, targetFilename + "." + targetFilenameExtension);
        }

        private IEnumerable<string> RetrieveRequiredIncludeDirectories(string projectName)
        {
            return RequiredIncludeDirectoriesRetriever.Retrieve(projectName);
        }

        private bool TargetProjectContainsRequiredIncludeDirectories(ProjectIdentifier targetProjectIdentifier)
        {
            var projectIncludeDirectories = Solution.GetProjectIncludeDirectories(targetProjectIdentifier);
            return CheckRequiredIncludeDirectoriesPresence(projectIncludeDirectories);
        }

        private static (string generatedTypeName, string generatedContent, string errorMessage) ParseDocumentContent(
            ICurrentDocumentInfoRetriever currentDocumentInformationRetriever,
            ISolution solution,
            Func<ParserInput, (string typeName, string content, string errorMessage)> parser, ProjectIdentifier targetProjectIdentifier)
        {
            var currentDocumentProjectName =
                currentDocumentInformationRetriever.RetrieveCurrentDocumentProjectName();

            if (currentDocumentProjectName == null)
                return ("", "", "Current file doesn't belong to any project in the solution. Operation cancelled");

            return parser(new ParserInput(currentDocumentInformationRetriever.DocumentName,
                solution.UnsavedDocuments,
                solution.GetProjectIncludeDirectories(
                    new ProjectIdentifier(currentDocumentProjectName)),
                solution.GetProjectIncludeDirectories(targetProjectIdentifier),
                currentDocumentInformationRetriever.CursorPosition));
        }

        private void GenerateNewFileFromParserResult(string generatedFilenameFullPath, string generatedContent,
            ISolution solution, ProjectIdentifier targetProjectIdentifier,
            IFileOverwriteConfirmationQuery fileOverwriteConfirmationQuery)
        {
            if (solution.FileExistsInProject(generatedFilenameFullPath, targetProjectIdentifier))
            {
                bool overwriteConfirmation = fileOverwriteConfirmationQuery.ShouldOverwriteFile(generatedFilenameFullPath);
                if (!overwriteConfirmation)
                    return;
            }
            
            solution.AddAndOpenNewDocumentInProject(targetProjectIdentifier, generatedFilenameFullPath, generatedContent);
        }
    }
}