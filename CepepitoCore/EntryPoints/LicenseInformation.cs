﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.EntryPoints
{
    public struct LicenseInformation
    {
        public LicenseInformation(int numberOfDaysLeft, bool trial, string userName, string uid, string type)
        {
            NumberOfDaysLeft = numberOfDaysLeft;
            Trial = trial;
            UserName = userName;
            Uid = uid;
            Type = type;
        }

        public int NumberOfDaysLeft { get; }
        public bool Trial { get; }
        public string UserName { get; }
        public string Uid { get; }
        public string Type { get; }
    }
}