﻿using System.Collections.Generic;

namespace CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInClipboard
{
    public interface IRequiredIncludeDirectoriesRetriever
    {
        IEnumerable<string> Retrieve(string projectName);
    }
}