﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile
{
    public interface IParser
    {
        (string generatedTypeName, string generatedContent) GenerateTestSkeletonFromFileWithCursorLocation(
            string documentName, IEnumerable<(string name, string content)> solutionUnsavedDocuments,
            IEnumerable<string> projectIncludeDirectories, CursorPosition cursorPosition);
    }
}