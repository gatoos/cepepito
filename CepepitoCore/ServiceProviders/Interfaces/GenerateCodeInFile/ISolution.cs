﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile
{
    public interface ISolution
    {
        IEnumerable<(string name, string content)> UnsavedDocuments { get; }
        IEnumerable<string> GetProjectIncludeDirectories(ProjectIdentifier projectIdentifier);

        void AddAndOpenNewDocumentInProject(ProjectIdentifier targetProjectIdentifier,
            string generatedFilenameFullPath, string documentContent);

        bool FileExistsInProject(string filename, ProjectIdentifier targetProjectIdentifier);
        void AddIncludeDirectoriesToProject(ProjectIdentifier projectIdentifier, IEnumerable<string> includeDirectories);
    }
}