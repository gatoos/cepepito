﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.ServiceProviders.Interfaces.GenerateCodeInFile
{
    public interface IFileOverwriteConfirmationQuery
    {
        bool ShouldOverwriteFile(string generatedFilename);
    }
}