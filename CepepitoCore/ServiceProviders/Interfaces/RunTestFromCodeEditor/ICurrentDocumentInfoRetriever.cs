﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;

namespace CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor
{
    public interface ICurrentDocumentInfoRetriever
    {
        string DocumentName { get; }
        CursorPosition CursorPosition { get; }
        string RetrieveCurrentDocumentCursorLocationLineContent();
        string RetrieveCurrentDocumentContent();
        string RetrieveCurrentDocumentProjectName();
    }
}