﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */

using System;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CepepitoCore.ServiceProviders.Interfaces.RunTestFromCodeEditor
{
    public interface IProjectOutputLauncher
    {
        event EventHandler TestRunningStart;
        event EventHandler TestRunningEnd;
        Task<XDocument> LaunchProjectWithArguments(string currentDocumentProjectName, string testsFilter);
    }
}