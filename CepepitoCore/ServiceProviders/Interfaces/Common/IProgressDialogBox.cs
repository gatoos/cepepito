﻿using System.Threading;
using System.Threading.Tasks;

namespace CepepitoCore.ServiceProviders.Interfaces.Common
{
    public interface IProgressDialogBox
    {
        void Show<T>(Task<T> workerTask, CancellationTokenSource cancellationTokenSource);
    }
}