﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.ServiceProviders.Interfaces.CreateTestProject
{
    public interface ITestingLibrariesConfiguration
    {
        string GoogleTestIncludeDirectory { get; }
        string GoogleMockIncludeDirectory { get; }
        string GoogleTestLibrary { get; }
    }
}
