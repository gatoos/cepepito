﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.UseCases.CreateTestProject;

namespace CepepitoCore.ServiceProviders.Interfaces.CreateTestProject
{
    public interface ISolutionProjectsRepository
    {
        string AddProject(string projectName, string customizedProjectXmlDefinition,
            ProjectCustomizations projectCustomizerProjectCustomizationsData,
            string sourceProjectUniqueName);

        void AddFileToProject(string targetProjectName, string filename, string fileContent);
    }
}