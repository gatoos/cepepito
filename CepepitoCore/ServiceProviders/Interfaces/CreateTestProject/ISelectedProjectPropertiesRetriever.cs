﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.UseCases.CreateTestProject;

namespace CepepitoCore.ServiceProviders.Interfaces.CreateTestProject
{
    public interface ISelectedProjectPropertiesRetriever
    {
        ProjectType ProjectType { get; }
        string ProjectXmlRepresentation { get; }
        string ProjectName { get; }
        string ProjectUniqueName { get; }
        bool ProjectIsCppProject { get; }
        bool ProjectContainsTestRunner { get; }
        string ProjectFullName { get; }
        string ProjectGuid { get; }
    }
}