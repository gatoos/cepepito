﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace CepepitoCore.ServiceProviders.Interfaces.RetrievePurchasedLicense
{
    public interface IPurchaseCodeRetriever
    {
        string RetrievePurchaseCode();
    }
}