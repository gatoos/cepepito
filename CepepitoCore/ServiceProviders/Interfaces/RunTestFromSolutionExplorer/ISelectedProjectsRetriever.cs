﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.Collections.Generic;
using CepepitoCore.Core.Entities;

namespace CepepitoCore.ServiceProviders.Interfaces.RunTestFromSolutionExplorer
{
    public interface ISelectedProjectsRetriever
    {
        IEnumerable<ProjectWithContent> GetSelectedProjects();
    }
}