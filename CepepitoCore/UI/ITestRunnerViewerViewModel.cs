﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.Core.Entities;
using Logging;

namespace CepepitoCore.UI
{
    public interface ITestRunnerViewerViewModel
    {
        bool TestsAreRunning { get; set; }
        TestCaseExecutionResults TestCaseExecutionResults { get; }
        TestCaseIdentifierWithAssociatedProjects TestCaseIdentifierWithAssociatedProjects { get; }

        void UpdateTestCaseExecutionResults(TestCaseExecutionResults testCaseExecutionResults,
            TestCaseIdentifierWithAssociatedProjects testCaseIdentifierWithAssociatedProjects);
    }
}