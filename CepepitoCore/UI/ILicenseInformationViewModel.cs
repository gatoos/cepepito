﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using CepepitoCore.EntryPoints;

namespace CepepitoCore.UI
{
    public interface ILicenseInformationViewModel
    {
        LicenseInformation LicenseInformation { get; set; }
    }
}