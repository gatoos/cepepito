﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace Cepepito.Common.UI
{
    public class TextBoxWpfToWinform : TextBox
    {
        private const uint DlgcWantarrows = 0x0001;
        private const uint DlgcHassetsel = 0x0008;
        private const uint DlgcWantchars = 0x0080;
        private const uint WmGetdlgcode = 0x0087;

        public TextBoxWpfToWinform()
        {
            Loaded += delegate
            {
                var s = PresentationSource.FromVisual(this) as HwndSource;
                s?.AddHook(ChildHwndSourceHook);
            };
        }

        private IntPtr ChildHwndSourceHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WmGetdlgcode)
            {
                handled = true;
                return new IntPtr(DlgcWantchars | DlgcWantarrows | DlgcHassetsel);
            }
            return IntPtr.Zero;
        }
    }
}