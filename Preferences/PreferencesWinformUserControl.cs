﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.Windows.Forms;

namespace Cepepito.Preferences
{
    public partial class PreferencesWinformUserControl : UserControl
    {
        public PreferencesWinformUserControl()
        {
            InitializeComponent();
        }

        public PreferencesUserControl WpfUserControl => elementHost1.Child as PreferencesUserControl;

        private void PreferencesWinformUserControl_Load(object sender, EventArgs e)
        {
        }
    }
}