﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CepepitoCoreAdapters.AdaptersServiceProviders.CreateTestProject;
using Microsoft.Internal.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Shell;
using VisualStudioAPIConnectors;

namespace Cepepito.Preferences
{
    [Guid("438A2DB8-B235-4FCB-B241-34452AFE4FFB")]
    public class PreferencesDialog : DialogPage, INotifyPropertyChanged, ITestingLibrariesConfiguration
    {
        private readonly AssemblyDirectoryRetriever _assemblyDirectoryRetriever =
            new AssemblyDirectoryRetriever();

        public Preferences preferences;

        private PreferencesUserControl preferencesUserControl;

        public PreferencesDialog()
        {
            preferences.UseCepepitoDefault = true;
        }

        public bool UseCepepitoDefault
        {
            get => preferences.UseCepepitoDefault;

            set
            {
                preferences.UseCepepitoDefault = value;
                NotifyPropertyChanged("UseCepepitoDefaultValue");
                NotifyPropertyChanged("EnableCustomGTestConfiguration");
                NotifyPropertyChanged("DisplayableGoogleTestIncludeDirectory");
                NotifyPropertyChanged("DisplayableGoogleMockIncludeDirectory");
                NotifyPropertyChanged("GoogleTestLibrary");
            }
        }

        public bool EnableCustomGTestConfiguration => !preferences.UseCepepitoDefault;

        public string GoogleTestIncludeDirectory
        {
            get
            {
                if (UseCepepitoDefault)
                    return Path.Combine(_assemblyDirectoryRetriever.RetrieveAssemblyDirectory(typeof(PreferencesDialog)),
                        "thirdparties" + Path.DirectorySeparatorChar + "googletest" + Path.DirectorySeparatorChar +
                        "include");
                return preferences.GoogleTestIncludeDirectory;
            }

            set
            {
                if (!UseCepepitoDefault)
                    preferences.GoogleTestIncludeDirectory = value;

                NotifyPropertyChanged("DisplayableGoogleTestIncludeDirectory");
            }
        }

        public string GoogleMockIncludeDirectory
        {
            get
            {
                if (UseCepepitoDefault)
                    return Path.Combine(_assemblyDirectoryRetriever.RetrieveAssemblyDirectory(typeof(PreferencesDialog)),
                        "thirdparties" + Path.DirectorySeparatorChar + "googlemock" + Path.DirectorySeparatorChar +
                        "include");
                return preferences.GoogleMockIncludeDirectory;
            }
            set
            {
                if (!UseCepepitoDefault)
                    preferences.GoogleMockIncludeDirectory = value;
                NotifyPropertyChanged("DisplayableGoogleMockIncludeDirectory");
            }
        }

        public string GoogleTestLibrary
        {
            get
            {
                if (UseCepepitoDefault)
                    return Path.Combine(_assemblyDirectoryRetriever.RetrieveAssemblyDirectory(typeof(PreferencesDialog)),
                        "thirdparties" + Path.DirectorySeparatorChar + "googletest" + Path.DirectorySeparatorChar +
                        "lib" + Path.DirectorySeparatorChar + VisualStudioVersion + Path.DirectorySeparatorChar +
                        "gtest_x86.lib");
                return preferences.GoogleTestLibrary;
            }

            set
            {
                if (!UseCepepitoDefault)
                    preferences.GoogleTestLibrary = value;
                NotifyPropertyChanged("GoogleTestLibrary");
            }
        }

        public string VisualStudioVersion { get; internal set; }

        protected override IWin32Window Window
        {
            get
            {
                var page = new PreferencesWinformUserControl();
                preferencesUserControl = page.WpfUserControl;
                page.WpfUserControl.preferences = this;
                page.WpfUserControl.Initialize();
                return page;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        protected override void OnActivate(CancelEventArgs e)
        {
            base.OnActivate(e);
            RefreshUIWithPreferencesValues();
        }

        private void RefreshUIWithPreferencesValues()
        {
            preferencesUserControl.useCepepitoDefault.IsChecked = UseCepepitoDefault;
            preferencesUserControl.gtestFolderPathTextBox.Text = GoogleTestIncludeDirectory;
            preferencesUserControl.gmockFolderPathTextBox.Text = GoogleMockIncludeDirectory;
            //preferencesUserControl.gtestLibPathTextBox.Text = GoogleTestLibrary;
        }
    }
}