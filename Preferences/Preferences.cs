﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
namespace Cepepito.Preferences
{
    public struct Preferences
    {
        public bool UseCepepitoDefault { get; set; }

        public string GoogleTestIncludeDirectoryDisplay { get; set; }
        public string GoogleMockIncludeDirectoryDisplay { get; set; }
        public string GoogleTestLibraryDisplay { get; set; }

        public string GoogleTestIncludeDirectory { get; set; }
        public string GoogleMockIncludeDirectory { get; set; }
        public string GoogleTestLibrary { get; set; }
    }
}