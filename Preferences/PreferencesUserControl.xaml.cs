﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using UserControl = System.Windows.Controls.UserControl;

namespace Cepepito.Preferences
{
    /// <summary>
    ///     Logique d'interaction pour PreferencesUserControl.xaml
    /// </summary>
    public partial class PreferencesUserControl : UserControl
    {
        internal PreferencesDialog preferences;

        public PreferencesUserControl()
        {
            InitializeComponent();
        }

        public void Initialize()
        {
            DataContext = preferences;
        }

        private void BrowseGtestFolderPathButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK ||
                    result == DialogResult.Yes)
                    preferences.GoogleTestIncludeDirectory = dialog.SelectedPath;
            }
        }

        private void BrowseGMockFolderPathButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK ||
                    result == DialogResult.Yes)
                    preferences.GoogleMockIncludeDirectory = dialog.SelectedPath;
            }
        }

        private void BrowseGtestLibraryPathButton_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                if (Path.GetExtension(openFileDialog.FileName) == ".lib")
                    preferences.GoogleTestLibrary = openFileDialog.FileName;
                else
                    MessageBox.Show("The selected file is not a .lib file", "Incorrect file selected",
                        MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void UserControl_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            preferences.GoogleTestIncludeDirectory = gtestFolderPathTextBox.Text;
            preferences.GoogleMockIncludeDirectory = gmockFolderPathTextBox.Text;
            //preferences.GoogleTestLibrary = gtestLibPathTextBox.Text;
        }
    }
}