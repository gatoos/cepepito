﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.ComponentModel.Design;
using ExtensionRegistrationService.Core.Entities;

namespace ExtensionRegistrationServiceAdapters
{
    public class CommandMenuItemWrapper : IDeactivableCommand
    {
        public MenuCommand MenuCommand { get; }

        public CommandMenuItemWrapper(MenuCommand menuCommand)
        {
            MenuCommand = menuCommand ?? throw new ArgumentNullException(nameof(menuCommand));
        }

        public bool Active
        {
            get => MenuCommand.Enabled;
            set => MenuCommand.Enabled = value;
        }
    }
}