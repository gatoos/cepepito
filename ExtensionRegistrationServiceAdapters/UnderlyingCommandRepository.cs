﻿/* Copyright (C) HEDIA Rached - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential 
 */
using System;
using System.ComponentModel.Design;
using ExtensionRegistrationService.Core.Entities;
using ExtensionRegistrationService.ServiceProviders;

namespace ExtensionRegistrationServiceAdapters
{
    public class UnderlyingCommandRepository : IUnderlyingCommandRepository
    {
        private readonly Guid _commandSet;
        private readonly IServiceProvider _serviceProvider;

        private IMenuCommandService MenuCommandService =>
            _serviceProvider.GetService(typeof(IMenuCommandService)) as IMenuCommandService;

        public UnderlyingCommandRepository(Guid commandSet, IServiceProvider serviceProvider)
        {
            _commandSet = commandSet;
            _serviceProvider = serviceProvider;
        }

        public IDeactivableCommand AddCommand(CommandIdentifier commandIdentifier, Action executableCommand)
        {
            return AddCommandMenuItem(commandIdentifier, executableCommand);
        }

        private IDeactivableCommand AddCommandMenuItem(CommandIdentifier commandIdentifier, Action executableCommand)
        {
            var commandIdObject = new CommandID(_commandSet, commandIdentifier.Identifier);
            var commandMenuItemWrapper =
                new CommandMenuItemWrapper(new MenuCommand((obj, args) => executableCommand(),
                    commandIdObject));

            MenuCommandService.AddCommand(commandMenuItemWrapper.MenuCommand);
            return commandMenuItemWrapper;
        }
    }
}
